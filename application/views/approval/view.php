	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>

<div class="page-head">
  <h2 class="pull-left">Manage Approval</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Approval</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-9">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Approval List</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
                  <th>Approval Name</th>
            			<th>Document Name</th>
                  <th>Default</th>
                  <th>Sequence Priority</th>
                  <th>Creator</th>
                  <th>Sub Creator</th>
            			<th>Opsi</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
            			$no=$this->approval_model->limit+1;
                  $default='[Default]';
            			echo '<tr>';
            			foreach ($approval as $app){
            				echo '<td>'.$no.'</td>';
            				echo '<td>'.$app->approval_name.'</td>';
                    echo '<td>'.$app->doc_name.'</td>';
                    echo '<td>';if($app->default==0){echo 'No';}elseif($app->default==1){echo 'Yes';}else{echo 'Unknown';}'</td>';
                    echo '<td>'.$app->seq_priority_id.'</td>';
                    echo '<td>'.$app->ref_name.'</td>';
                    echo '<td>'.$app->ref_list_name.'</td>';
                    echo '<td><div class="btn-group">
                                  <button class="btn btn-xs btn-success" title="Edit" onclick="loading(\'./approval/edit?id='.$app->id.'\')"><i class="fa fa-pencil"></i></button>
                                  <button id="btndelete" class="btn btn-xs btn-danger" title="Delete" onclick="if(confirm(&quot;anda yakin mau menghapus?&quot;)){delete_app('.$app->id.')}return false;"><i class="fa fa-trash-o"></i></button>
                                </div></td>';
            			echo '</tr>';
            			$no++;
            			}
            		?>
            		</tbody>
            	</table>
            <div class="widget-foot">
              <div class="table-info" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($approval) ?> dari <?= $total_rows ?> data</div>
              <?= $page ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
        function delete_app(id){
          $.ajax({
            type    : 'GET',
            url     : './approval/delete?id='+id,
            //data    : table,
            success : function(data){
              alert("Data Berhasil Dihapus");
              $("#contentAjax").load("./approval");
            }
          });
        }
</script>