   <style>
   		.sort{
			float:left;	
		}
		
		.sort:hover{
			text-decoration:underline;
			cursor:pointer;			
		}
   </style>
<div class="page-head">
  <h2 class="pull-left">Inbox Approval</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="index.html"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Approval</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Inbox Approval</div>
              <div class="widget-icons pull-right">
                <?= '<a href="#searchinbox" data-toggle="modal" onclick="$(\'#contentSearchInbox\').load(\'./approval/search\')" title="Seacrh"><i class="fa fa-search"></i></a>'; ?>
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
                  <th>NO</th>
                  <!-- <th>Approved</th>
                  <th>Sub Approved</th> -->
                  <th><div id="doc_seq_id" class="sort">Doc Category</div>&nbsp;<i class="sorti fadoc_seq_id"></i></th>
                  <th><div id="employee_name" class="sort">Employee</div>&nbsp;<i class="sorti faemployee_name"></i></th>
                  <th><div id="priority_name" class="sort">Priority</div>&nbsp;<i class="sorti fapriority_name"></i></th>
                  <th><div id="seq" class="sort">Seq</div>&nbsp;<i class="sorti faseq"></i></th>
                  <th>File</th>
                  <th><div id="doc_number" class="sort">Doc Number</div>&nbsp;<i class="sorti fadoc_number"></i></th>
                  <th><div id="request_date" class="sort">Request Date</div>&nbsp;<i class="sorti farequest_date"></i></th>
                  <th><div id="due_date" class="sort">Duedate</div>&nbsp;<i class="sorti fadue_date"></i></th>
                  <th>Detail</i></th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $this->inbox_app_model->limit;
                  $no=$this->inbox_app_model->limit+1;
                  echo '<tr>';
                  //echo $_SESSION['username'];
                 // echo $_SESSION['role'];
                  //foreach($users as $usr){
                    //if($usr->username==$_SESSION['username']){
                          foreach ($inbox as $serv){
                             $hover_content=$serv->doc_seq_id.' '.$serv->employee_name;

                             if(!empty($serv->request_date)){
                                $orireq = $serv->request_date;
                                $req_date = date("d-m-Y", strtotime($orireq));
                                
                              }else{
                                $req_date = '';
                              }

                             if(!empty($serv->duedate)){
                                $oridue = $serv->duedate;
                                $duedate = date("d-m-Y", strtotime($oridue));
                                
                              }else{
                                $duedate = '';
                              }
                             
                            //if($serv->name==$serv->ref_list_name){
                              echo '<td>'.$no.'</td>';
                              /*echo '<td>'.$serv->ref_name.'</td>';
                              echo '<td>'.$serv->ref_list_name.'</td>';*/
                              //echo '<td><center><span class="label label-success" title="Desc" data-toggle="popover" data-trigger="hover" data-content="'.$serv->doc_desc.'">'.$serv->doc_name.'</span></td>';
                              echo '<td><center><span data-toggle="popover" data-trigger="hover" data-content="'.$hover_content.'" data-id = '.$serv->doc_seq_id.' class="label label-'.$serv->span.'">'.$serv->doc_name.'</span></td>';
                              echo '<td id="test">'.$serv->employee_name.'</td>';
                              echo '<td>'.$serv->priority_name.'</td>';
                              echo '<td>'.$serv->app_sequence.'</td>';
                              if(!empty($serv->attachment)){
                                echo '<td><center><a href="/eportal2/asset/file/'.$serv->attachment.'" target="_blank"><i class="fa fa-file-text"></i></a></td>';
                              }else{
                                echo '<td style="color:red;"><center><i class="fa fa-file-text"></i></td>';
                              }
                              
                              echo '<td>'.$serv->doc_number.'</td>';
                              echo '<td>'.$req_date.'</td>';
                              echo '<td>'.$duedate.'</td>';
                              //echo '<td><center><a href="#myModal2" data-toggle="modal" onclick="$(\'#contentApproveForm\').load(\'./approval/approve?id='.$serv->id.'\')" class="btn btn-success btn-xs" title="Approve"><i class="fa fa-mail-reply"></i></a> 
							                //     <a href="#myModal3" data-toggle="modal" onclick="$(\'#contentDetailInbox\').load(\'./'.$serv->controller.'/detail2?id='.$serv->doc_seq_id.'\')" class="btn btn-warning btn-xs" title="Detail"><i class="fa fa-eye"></i></a></td>';
                              
                              //if($_SESSION['user_id']==557){
                                if($serv->doc_id==1 OR $serv->doc_id==2){
                                  echo '<td><center><a href="#approvalform" data-toggle="modal" onclick="$(\'#contentApproveForm\').load(\'./approval/approve?id='.$serv->id.'\')" class="btn btn-success btn-xs" title="Approve"><i class="fa fa-mail-reply"></i></a> 
                                   <a href="#detailinbox" data-toggle="modal" onclick="$(\'#contentDetailInbox\').load(\'./'.$serv->controller.'/detail2?id='.$serv->doc_seq_id.'\')" class="btn btn-warning btn-xs" title="Detail"><i class="fa fa-eye"></i></a></td>';
                                 }elseif($serv->doc_id==3){
                                  echo '<td><center><a href="#detailapprove" data-toggle="modal" onclick="$(\'#contentDetailApprove\').load(\'./approval/approve_detail?doc_number='.$serv->doc_number.'\')" class="btn btn-primary btn-xs" title="Approve and Detail"><i class="fa fa-mail-reply"></i></a>
                                  <a href="#detailinbox" data-toggle="modal" onclick="$(\'#contentDetailInbox\').load(\'./'.$serv->controller.'/detail2?id='.$serv->doc_seq_id.'\')" class="btn btn-warning btn-xs" title="Detail"><i class="fa fa-eye"></i></a></td>';
                                 }
                              //}
                              echo '</tr>';
                            $no++;
                          //}


                        }
                    //}
                  //}
            		?>
            		</tbody>
            	</table>
              <div class="widget-foot">
              <div class="table-info" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($inbox) ?> dari <?= $total_rows ?> data</div>
                <?= $page ?>
                <div class="clearfix"></div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
   /* <button class="btn btn-xs btn-success" title="Approve" onclick="$(\'#contentAjax\').load(\'/index.php/approval/approve?id='.$serv->id.'\')"><i class="fa fa-check"></i></button>
                                      <button class="btn btn-xs btn-danger" title="Reject" onclick="$(\'#contentAjax\').load(\'/index.php/approval/reject?id='.$serv->id.'\')"><i class="fa fa-ban"></i></button>
                                      <button class="btn btn-xs btn-warning" title="Revisi" onclick="$(\'#contentAjax\').load(\'/index.php/approval/revisi?id='.$serv->id.'\')"><i class="fa fa-undo"></i></button>*/
  ?>


<div id="approvalform" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Approval Form</h4>
              </div>
              <div class="modal-body">
                <div id="contentApproveForm"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" id="tutupmodal2" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>


<div id="detailinbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Detail Inbox</h4>
              </div>
              <div class="modal-body">
                <div id="contentDetailInbox"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="tutupmodal3" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<div id="searchinbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Search Inbox Approval</h4>
              </div>
              <div class="modal-body">
                <div id="contentSearchInbox"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<div id="detailapprove" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:800px">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Detail and Approve</h4>
              </div>
              <div class="modal-body">
                <div id="contentDetailApprove"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="tutupmodal5" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<input type="hidden" name="sort_by" id="sort_by" value="<?php echo @$_GET["sort"]; ?>" />
<input type="hidden" name="sort_dir" id="sort_dir" value="<?php echo @$_GET["dir"]; ?>" />
<script>

	if($("#sort_by").val()!="")
	{
		var sort_dir = $("#sort_dir").val();

		if(sort_dir=="0")
		{
			$(".fa"+$("#sort_by").val()).removeClass("fa fa-arrow-down");
			$(".fa"+$("#sort_by").val()).addClass("fa fa-arrow-up");
		}
		else
		if(sort_dir=="1")
		{
			$(".fa"+$("#sort_by").val()).removeClass("fa fa-arrow-up");
			$(".fa"+$("#sort_by").val()).addClass("fa fa-arrow-down");
		}

	}


  $(function(){
	$('#approvalform').appendTo("body");
	$('#detailinbox').appendTo("body");
	$('#searchinbox').appendTo("body");
	$('#detailapprove').appendTo("body");

    //$("#datatable").tablesorter({ sortList: [[0,0], [1,0]] });
	$(".sort").click(function(e){
		var sort_dir = $("#sort_dir").val();
		$("#sort_by").val(e.target.id);
		
		$(".sorti").removeClass("fa fa-arrow-down");
		$(".sorti").removeClass("fa fa-arrow-up");
		if(sort_dir=="")
		{
			$(".fa"+e.target.id).removeClass("fa fa-arrow-down");
			$(".fa"+e.target.id).addClass("fa fa-arrow-up");
			$("#sort_dir").val("0");
		}
		else
		if(sort_dir=="0")
		{
			$(".fa"+e.target.id).removeClass("fa fa-arrow-up");
			$(".fa"+e.target.id).addClass("fa fa-arrow-down");
			$("#sort_dir").val("1");
		}
		else
		if(sort_dir=="1")
		{
			$(".fa"+e.target.id).removeClass("fa fa-arrow-down");
			$(".fa"+e.target.id).addClass("fa fa-arrow-up");
			$("#sort_dir").val("0");
		}
		
		if($("#search").val() != "")
		{
			$("#contentAjax").load("./approval/inbox/?_=0"+$("#search").val()+"&sort="+e.target.id+"&dir="+$("#sort_dir").val());
		}
		else
		{
			$("#contentAjax").load("./approval/inbox/?sort="+e.target.id+"&dir="+$("#sort_dir").val());
		}
//		$("#sort_url").val("&sort="+e.target.id+"&dir="+$("#sort_dir").val());
	});
	
	$(".sorti").click(function(e){ e.preventDefault(); });
  });
</script>