	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>

	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit Approval</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                    //print_r($approval);
                    	$attname=array(
                          'name'=>'approval_name',
                          'value'=>$approval->approval_name,
                          'class'=>'form-control'
                        );

                      $attdoc=array(
                  				'name'=>'doc_id',
                  				'value'=>$approval->doc_id,
                  				'class'=>'form-control'
                  			);

                      $attseq=array(
                          'name'=>'seq_priority_id',
                          'value'=>$approval->seq_priority_id,
                          'class'=>'form-control'
                        );

                  		$attsubmit=array(
                  				'name'=>'editapp',
                          'id'=>'editapp',
                  				'content'=>'Edit Approval',
                  				'class'=>'btn btn-primary btn-block'
                  			);
					           ?>

                      <?= form_open('','id=editApp') ?>
                          <fieldset>
                              
                              <div class="form-group">
                                  <?= form_hidden('id',$approval->id) ?>
                                  <?= form_label('Approval Name','approval_name') ?>
                                  <?= form_input($attname) ?>
                              </div>

                              <div class="form-group">
                                    <?= form_label(' Type','doc_id') ?>
                                    <select name="doc_id" class="form-control">
                                    <?php
                                      foreach($docs as $doc){
                                        if($doc->id==$approval->doc_id){
                                          $selected='selected';
                                          echo '<option value='.$doc->id.' selected='.$selected.'>'.$doc->doc_name.'</option>';
                                        }else{
                                          echo '<option value='.$doc->id.'>'.$doc->doc_name.'</option>';
                                        }
                                      }
                                    ?>
                                    </select>
                              </div>
                              
                               <div class="form-group">
                                  <?= form_label('Sequence Priority','seq_priority_id') ?>
                                  <?= form_input($attseq) ?>
                              </div>

                              <div class="form-group">
                                    <?= form_label('Approval By','creator_id') ?>
                                    <select name="creator_id" id="creator_id" class="form-control">
                                    <option value=>== Pilih Creator ==</option>
                                    <?php
                                      foreach($references as $ref){
                                        //echo '<option value='.$ref->id.'>'.$ref->ref_name.'</option>';
                                        //echo "<option value='$ref->id' ".set_select('creator_id', $ref->id).">$ref->ref_name</option>";
                                        if($ref->id==$approval->creator_id){
                                          $selected='selected';
                                          echo '<option value='.$ref->id.' selected='.$selected.'>'.$ref->ref_name.'</option>';
                                        }else{
                                          echo '<option value='.$ref->id.'>'.$ref->ref_name.'</option>';
                                        }
                                      }
                                    ?>
                                    </select>
                              </div>

                              <div class="form-group">
                                <select name="sub_creator_id" id="sub_creator_id" class="form-control">
                                <?php

                                  if(isset($_POST['creator_id'])){
                                    //$data .= "<option value=''>--Pilih UUUU--</option>";
                                    foreach ($references_list as $ref_list) {
                                      /*if($ref_list['ref_id']==$_POST['creator_id']){
                                        $data .= "<option value='$ref_list->id' selected>$ref_list->ref_list_name</option>\n";
                                      }else{*/
                                        $data .= "<option value='$ref_list->id'>$ref_list->ref_list_name</option>\n";
                                      //}
                                    }
                                    echo $data;

                                  }else{
                                    foreach($references_list as $ref_list=>$v){
                                      if($v["id"]==$approval->sub_creator_id)
                                        $selected="selected='selected'";
									  else
									  	$selected='';
                                      echo '<option value='.$v["id"].' '.$selected.'>'.$v["ref_list_name"].'</option>';
                                      
                                      
                                    }
                                  }

                                ?>
                                </select>
                              </div>
                              
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
    $("#editapp").click(function(){
      $.ajax({
        type:'POST',
        url:'./approval/edit_proccess',
        data:$("#editApp").serialize(),
        success: function(data){
          data=jQuery.parseJSON(data);
          if(data.success){
            alert('Data Berhasil diubah');
            $("#contentAjax").load("./approval");
          }
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(function(){
    $("#creator_id").change(function(){
      var creator_id = $("#creator_id").val();
      $.ajax({
        type    : 'GET',
        url     : './approval/get_ref_list/'+creator_id,
        //data    : 'creator_id' + creator_id,
        success : function(data){
          $("#sub_creator_id").html(data);
        }
      });
    });
  });
</script>