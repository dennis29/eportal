<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>ePortalGOS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta http-equiv="Cache-control" content="public">


  <!-- JS -->
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery-ui-1.9.2.custom.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap.js'); ?>"></script>


  <!-- Stylesheets -->
  <link  href="<?php echo base_url('/asset/style/bootstrap.css'); ?>" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="<?php echo base_url('asset/style/font-awesome.css'); ?>"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/jquery-ui-1.9.2.custom.min.css'); ?>"> 
  <!-- Calendar -->
  <!-- Bootstrap toggle -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/bootstrap-switch.css'); ?>">
  <!-- Main stylesheet -->
  <link href="<?php echo base_url('/asset/style/style2.css') ?>" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="<?php echo base_url('/asset/style/widgets.css') ?>" rel="stylesheet">   
    <!-- Gritter Notifications stylesheet -->
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="./asset/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo base_url('/asset/img/favicon/favicon.ico'); ?>">
</head>

<body style="height:100%">
<header>
<div class="navbar navbar-fixed-top bs-docs-nav" role="banner">
  
    <div class="container">
      <a href="./main" class="navbar-brand">ePortal<span class="bold">GOS</span></a>
    </div>

      <!-- Navigation starts -->
</div>
</header>
<div class="content" style="height:100%">
	<div class="sidebar">
    
    </div>
	<div class="mainbar">
          <div class="jumbotron" style="height:100%">

            <div class="alert alert-info" role="alert"><?php echo ($success) ? "Request telah berhasil di-approve" : "Request gagal di-approve<br />".$message; ?></div>
          </div>
    </div>
</div>
<footer>
<div class="navbar navbar-fixed-bottom bs-docs-nav" role="banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <!-- Copyright info -->
            <p class="copy">Copyright &copy; <?= date("Y") ?> | <a href="#">E-portal GOS</a> </p>
      </div>
    </div>
  </div>
</div>
</footer>   
</body>
</html>

<script type="text/javascript">
$(function(){
		$(".sidebar").hide();
});
</script>