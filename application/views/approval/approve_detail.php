  <div class="matter">
    <div class="container">
      <div class="row">
      	<?php if(!empty($detail)){
        ?>
      	<div class="col-md-7">
            <div class="alert alert-success">
            
				    <?php
              if(!empty($detail->request_date)){
                $orireq = $detail->request_date;
                $req_date = date("d-m-Y", strtotime($orireq));
                
              }else{
                $req_date = '';
              }

              if(!empty($detail->start_datefrom)){
                
                $oristart = $detail->start_datefrom;
                $start_date = date("d-m-Y", strtotime($oristart));
              }else{
                
                $start_date = '';
              }

              if(!empty($detail->start_dateto)){
                
                $orito = $detail->start_dateto;
                $to_date = date("d-m-Y", strtotime($orito));
              }else{
                $to_date = '';
              }

              if(!empty($detail->effective_date)){
                
                $orieffe = $detail->effective_date;
                $effe_date = date("d-m-Y", strtotime($orieffe));
              }else{
                
                $effe_date = '';
              }

            ?>
            Request date : <?= $req_date ?><br>
            Doc Sequence : <?= $detail->doc_seq_id ?><br>
            Employee : <?= $detail->employee_name ?><br>
            Client Name : <?= $detail->client_sod_id ?><br>
            NRK : <?= $detail->nrk ?><br>
            Name : <?= $detail->name_sod ?><br>
            Position : <?= $detail->position_sod ?><br>
            Status : <?= $detail->status_sod_name ?><br>
            Start Date From : <?= $start_date ?><br>
            Start Date To : <?= $to_date ?><br>
            Rekomendasi : <?= $detail->rekomendasi_sod_name ?><br>
            Effective date : <?= $effe_date ?><br>
            Reason : <?= $detail->reason ?><br>

            Attachment :
            <?php
              if(!empty($detail->attachment_sod)){
                echo '<a href="/eportal2/asset/file/'.$detail->attachment_sod.'" target="_blank">View</a><br>';
              }else{
                echo '<style="color:red;">Empty<br>';
              }
            ?>
            Department : <?= $detail->department ?><br>
            <!--  PIC PAM : <?= $detail->pic_pam_id ?><br> -->
            SBU Name : <?= $detail->sbu_name ?><br>
            <!--Massal -->
            
            Memo : <?= $detail->app_memo ?><br>
            	
              
            </div>
        </div>
        <?php
            } ?>
         <?php if(!empty($detail)){
         	echo '<div class="col-md-5">';
         }else{
         	echo '<div class="col-md-12">';
         }
         ?>
        
          <div class="widget" border=0>
                <div class="widget-content">
                  <div class="padd">

                  <?php
                    /*foreach($it_services as $it){
                      echo $it->id;
                    }*/
                  ?>

                  	<?php
                  		$attmemo=array(
                  				'name'=>'app_memo',
                  				'class'=>'form-control',
                  				'placeholder'=>'input memo disini'
                  			);
                  		$attapp=array(
                  				'name'=>'approvebtndetail',
                  				'id'=>'approvebtndetail',
                  				//'type'=>'submit',
                  				'content'=>'<i class="fa fa-check"></i> Approve',
                          //'onclick'=>'if(confirm(&quot;anda yakin mau menghapus?&quot;)){approve('.$approve->id.')}return false;',
                  				'class'=>'btn btn-success btn-block'
                          
                  			);
                      $attrej=array(
                          'name'=>'rejectbtndetail',
                          'id'=>'rejectbtndetail',
                          //'type'=>'submit',
                          'content'=>'<i class="fa fa-ban"></i> Reject',
                          'class'=>'btn btn-danger btn-block'
                        );
                      $attrev=array(
                          'name'=>'revisibtndetail',
                          'id'=>'revisibtndetail',
                          //'type'=>'submit',
                          'content'=>'<i class="fa fa-undo"></i> Revisi',
                          'class'=>'btn btn-warning btn-block'
                        );
					          ?>
                      <?= form_open('','id=approveDetailForm') ?>
                          <fieldset>
                          		<div class="form-group">
                                    <?= form_hidden('id',$approve->id) ?>
                                </div>
                                <div class="form-group">

                                    <?= form_label('Memo','app_memo') ?>
                                    <?= form_hidden('oldmemo',$approve->app_memo) ?>
                                    <?= form_textarea($attmemo) ?>

                                    <?= form_hidden('created_by',$approve->created_by) ?>
                                    <?= form_hidden('doc_id',$approve->doc_id) ?>
                                    <?= form_hidden('doc_number',$approve->doc_number) ?>
                                    <?= form_hidden('approval_id',$approve->approval_id) ?>
                                    <?= form_hidden('approved_id',$approve->approved_id) ?>
                                    <?= form_hidden('sub_approved_id',$approve->sub_approved_id) ?>
                                    <?= form_hidden('app_sequence',$approve->app_sequence) ?>
                                    <?= form_hidden('employee_id',$approve->employee_id) ?>

                                    <?php
                                      if($approve->doc_id=='3' AND $approve->app_sequence=='2'){
                                        echo '<input name="doc_print" value="1" type="checkbox"> Check this for SOD Document';
                                      }
                                    ?>

                                </div>
      	                            <?= form_button($attapp) ?>
                                    <?= form_button($attrej) ?>
                                    <?= form_button($attrev) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
      /*function approve(id){
          $.ajax({
            type    : 'GET',
            url     : './approval/app_proccess?id='+id,
            //data    : table,
            success : function(data){
              alert("Okay");
              //$("#contentAjax").load("./approval");
            }
          });
        }*/

	  $(function(){
	    $("#approvebtndetail").click(function(){
	      $.ajax({
	        type:'POST',
	        url:'./approval/app_proccess',
	        data:$("#approveDetailForm").serialize(),
		    beforeSend : function(){
				$("#approvebtndetail").button("loading");
		  	},
	        success:function(data){
	          data=jQuery.parseJSON(data);
	          if(data.success)
            {
	            alert('Data Approved');
				if($("#search").val()!="")
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val()+"&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val());						
					}
				}
				else
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox");						
					}
				}
				      $("#tutupmodal5").trigger("click");
	          }
	        }
	      });
	    });

      $("#rejectbtndetail").click(function(){
        $.ajax({
          type:'POST',
          url:'./approval/reject_proccess',
          data:$("#approveDetailForm").serialize(),
		  beforeSend : function(){
			$("#rejectbtndetail").button("loading");
		  },
          success:function(data){
            data=jQuery.parseJSON(data);
            if(data.success)
            {
              alert('Data Rejected');
				if($("#search").val()!="")
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val()+"&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val());						
					}
				}
				else
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox");						
					}
				}
              $("#tutupmodal5").trigger("click");
            }
          }
        });
      });

      $("#revisibtndetail").click(function(){
        $.ajax({
          type:'POST',
          url:'./approval/revisi_proccess',
          data:$("#approveDetailForm").serialize(),
		  beforeSend : function(){
			$("#revisibtndetail").button("loading");
		  },
          success:function(data){
            data=jQuery.parseJSON(data);
            if(data.success)
            {
              alert('Data Revisi');
				if($("#search").val()!="")
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val()+"&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val());						
					}
				}
				else
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox");						
					}
				}
              $("#tutupmodal5").trigger("click");
            }
          }
        });
      });

	  });
	</script>