  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget" border=0>
                <div class="widget-content">
                  <div class="padd">

                  <?php
                    /*foreach($it_services as $it){
                      echo $it->id;
                    }*/
                  ?>

                  	<?php
                  		$attmemo=array(
                  				'name'=>'app_memo',
                  				'class'=>'form-control',
                  				'placeholder'=>'input memo disini'
                  			);
                  		$attapp=array(
                  				'name'=>'approvebtn',
                  				'id'=>'approvebtn',
                  				//'type'=>'submit',
                  				'content'=>'<i class="fa fa-check"></i> Approve',
                          //'onclick'=>'if(confirm(&quot;anda yakin mau menghapus?&quot;)){approve('.$approve->id.')}return false;',
                  				'class'=>'btn btn-success btn-block'
                          
                  			);
                      $attrej=array(
                          'name'=>'rejectbtn',
                          'id'=>'rejectbtn',
                          //'type'=>'submit',
                          'content'=>'<i class="fa fa-ban"></i> Reject',
                          'class'=>'btn btn-danger btn-block'
                        );
                      $attrev=array(
                          'name'=>'revisibtn',
                          'id'=>'revisibtn',
                          //'type'=>'submit',
                          'content'=>'<i class="fa fa-undo"></i> Revisi',
                          'class'=>'btn btn-warning btn-block'
                        );
					          ?>
                      <?= form_open('','id=formApprove') ?>
                          <fieldset>
                          		<div class="form-group">
                                    <?= form_hidden('id',$approve->id) ?>
                                </div>
                                <div class="form-group">

                                    <?= form_label('Memo','app_memo') ?>
                                    <?= form_hidden('oldmemo',$approve->app_memo) ?>
                                    <?= form_textarea($attmemo) ?>

                                    <?= form_hidden('created_by',$approve->created_by) ?>
                                    <?= form_hidden('doc_id',$approve->doc_id) ?>
                                    <?= form_hidden('doc_number',$approve->doc_number) ?>
                                    <?= form_hidden('approval_id',$approve->approval_id) ?>
                                    <?= form_hidden('approved_id',$approve->approved_id) ?>
                                    <?= form_hidden('sub_approved_id',$approve->sub_approved_id) ?>
                                    <?= form_hidden('app_sequence',$approve->app_sequence) ?>
                                    <?= form_hidden('employee_id',$approve->employee_id) ?>
                                    <?php
                                      if($approve->doc_id=='3'){
                                        echo '<input name="doc_print" value="1" type="checkbox"> Check this for SOD Document';
                                      }
                                    ?>
                                </div>
      	                            <?= form_button($attapp) ?>
                                    <?= form_button($attrej) ?>
                                    <?= form_button($attrev) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
      /*function approve(id){
          $.ajax({
            type    : 'GET',
            url     : './approval/app_proccess?id='+id,
            //data    : table,
            success : function(data){
              alert("Okay");
              //$("#contentAjax").load("./approval");
            }
          });
        }*/


	  $(function(){
	    $("#approvebtn").click(function(){
	      $.ajax({
	        type:'POST',
	        url:'./approval/app_proccess',
	        data:$("#formApprove").serialize(),
			beforeSend : function(){
				$("#approvebtn").button("loading");
			},
	        success:function(data){
	          data=jQuery.parseJSON(data);
				  if(data.success)
				  {
						alert('Data Approved');
				  }
				  else
				  {
						if(data.message!="")
						{
							alert(data.message);								
						}
						else
						{
							alert('Error');															
						}
				  }

				if($("#search").val()!="")
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val()+"&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val());						
					}
				}
				else
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox");						
					}
				}
				
				$("#tutupmodal2").trigger("click");

	        }
	      });
	    });

      $("#rejectbtn").click(function(){
        $.ajax({
          type:'POST',
          url:'./approval/reject_proccess',
          data:$("#formApprove").serialize(),
			beforeSend : function(){
				$("#rejectbtn").button("loading");
			},
          success:function(data){
            data=jQuery.parseJSON(data);
            if(data.success)
            {
              alert('Data Rejected');
				if($("#search").val()!="")
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val()+"&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val());						
					}
				}
				else
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox");						
					}
				}
              $("#tutupmodal2").trigger("click");
            }
          }
        });
      });

      $("#revisibtn").click(function(){
        $.ajax({
          type:'POST',
          url:'./approval/revisi_proccess',
		  beforeSend : function(){
			$("#revisibtn").button("loading");
		  },
          data:$("#formApprove").serialize(),
          success:function(data){
            data=jQuery.parseJSON(data);
            if(data.success)
            {
              alert('Data Revisi');
				if($("#search").val()!="")
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val()+"&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox?_=0"+$("#search").val());						
					}
				}
				else
				{
					if($("#sort_by").val()!="")
					{
						$("#contentAjax").load("./approval/inbox?_=0&sort="+$("#sort_by").val()+"&dir="+$("#sort_dir").val());
					}
					else
					{
						$("#contentAjax").load("./approval/inbox");						
					}
				}
              $("#tutupmodal2").trigger("click");
            }
          }
        });
      });

	  });
	</script>