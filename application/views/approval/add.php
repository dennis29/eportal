	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery-ui-1.9.2.custom.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/bootstrap.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/fullcalendar.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.rateit.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.prettyPhoto.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/raphael-min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/morris.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.flot.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.flot.resize.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.flot.pie.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.flot.stack.js'); ?>' },
		{ url: "<?php echo base_url('/asset/js/jquery.noty.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/themes/default.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/layouts/bottom.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/layouts/topRight.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/layouts/top.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/moment.min.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/daterangepicker.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/sparklines.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/jquery.gritter.min.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/jquery.cleditor.min.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/bootstrap-datetimepicker.min.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/jquery.slimscroll.min.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/bootstrap-switch.min.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/filter.js'); ?>" },
		{ url: "<?php echo base_url('/asset/js/custom2.min.js'); ?>" },
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' }
	);    
    </script>
    <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add Approval</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">
<!-- 
                  <script type="text/javascript">
                  $(document).ready(function(){
                    $("#ref_id").change(function(){
                      var ref_id=$("#ref_id").val();
                      $.ajax({
                        type    : "POST",
                        url     : "<?= base_url();?> index.php/approval/get_list",
                        data    : "ref_id"+ref_id,
                        success : function(data){
                          $("id").html(data);
                        }
                      });
                    })
                  });
                  </script>
                   -->

                  	<?php
                  		$attname=array(
                          'name'=>'approval_name',
                          'autocomplete'=>'off',
                          //'value'=>$user->firstname,
                          'placeholder'=>'Input Approval Name',
                          'id'=>'approval_name',
                          'class'=>'form-control'
                        );
                      $attseq=array(
                          'name'=>'seq_priority_id',
                          //'value'=>$user->firstname,
                          'placeholder'=>'Input Sequence Priority',
                          'class'=>'form-control'
                        );

                      $attdoc=array(
                  				'name'=>'doc_id',
                  				//'value'=>$user->firstname,
                  				'class'=>'form-control'
                  			);

                  		$attsubmit=array(
                  				'name'=>'sendapproval',
                          //'type'=>'submit',
                          'id'=>'sendapproval',
                  				'content'=>'Add Approval',
                  				'class'=>'btn btn-primary btn-block'
                  			);
					           ?>

                      <?= form_open('','id=addApp') ?>
                          <fieldset>
                              
                              <div class="form-group">
                                  <?= form_label('Approval Name','approval_name') ?>
                                  <?= form_input($attname) ?>
                              </div>

                              <div class="form-group">
                                    <?= form_label('Document Type','doc_id') ?>
                                    <select name="doc_id" class="form-control">
                                    <?php
                                      foreach($docs as $doc){
                                        echo '<option value='.$doc->id.'>'.$doc->doc_name.'</option>';
                                      }
                                    ?>
                                    </select>
                              </div>
                              <div class="form-group">
                                    <?= form_label('Default','default') ?>
                                    <select name="default" id="default" class="form-control">
                                      <option value=0>No</option>
                                      <option value=1>Yes</option>
                                    </select>
                                    <p class="text-danger">*Select "Yes" if approval rule is default</p>
                              </div>

                              <div class="form-group">
                                  <?= form_label('Sequence Priority','seq_priority_id') ?>
                                  <?= form_input($attseq) ?>
                              </div>

                              <div class="form-group">
                                    <?= form_label('Approval By','creator_id') ?>
                                    <select name="creator_id" id="creator_id" class="form-control">
                                    <option value=>== Pilih Creator ==</option>
                                    <?php
                                      foreach($references as $ref){
                                        echo '<option value='.$ref->id.'>'.$ref->ref_name.'</option>';
                                      }
                                    ?>
                                    </select>
                              </div>

                              <div class="form-group">
                                    <select name="sub_creator_id" id="sub_creator_id" class="form-control">
                                    <?php
                                      if(isset($_POST['creator_id'])){
                                        foreach($references_list as $ref_list){
                                          $data .= '<option value='.$ref_list->id.'>'.$ref_list->ref_list_name.'</option>';
                                        }
                                        echo $data;
                                      }else{
                                        echo "<option value>== Pilih Sub Creator ==</option>";
                                      }
                                      
                                    ?>
                                    </select>
                              </div>
                              
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                        
                        <div class="errors" style="color:red;"><?= validation_errors() ?></div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
      $("#default").change(function(){
        if($(this).val() == '0'){ 
          $("#creator_id").removeAttr("disabled","disabled");
          $("#sub_creator_id").removeAttr("disabled","disabled");

        }else if($(this).val() == '1'){
          $("#creator_id").attr("disabled","disabled");
          $("#sub_creator_id").attr("disabled","disabled");
        }
      });
    });

    $(function(){
        $("#sendapproval").click(function(){
            $.ajax({
              type: 'POST',
              url: './approval/add_proccess',
              data: $("#addApp").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    $("#contentAjax").load("./approval");
                 }else{
                    alert(data.message);
                 }
              }
            });
        });
    });
</script>

<script type="text/javascript">
  $(function(){
    $("#creator_id").change(function(){
      var creator_id = $("#creator_id").val();
      $.ajax({
        type    : 'GET',
        url     : './approval/get_ref_list/'+creator_id,
        //data    : 'creator_id' + creator_id,
        success : function(data){
          $("#sub_creator_id").html(data);
        }
      });
    });
  });
</script>

<!--<script>
    $.validator.setDefaults({
    });

    $().ready(function() {
      $("#addApp").validate({
        rules: {
          approval_name: {
            required: true,
            minlength: 5
          }
        },
        messages: {
          approval_name: {
            required: "Please enter a approval name",
            minlength: "Your approval name must consist of at least 5 characters"
          }
        },
        submitHandler: function(form) {
          if(form.valid())
            $("#sendapproval").trigger("click");
          //alert("submitted!");
        }

      });
    });
</script>

