<script src="<?php echo base_url('/asset/js/custom2.js'); ?>"></script> <!-- Custom codes -->

<div class="page-head">
  <h2 class="pull-left">Manage IT Services</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">IT Services</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">IT Services List</div>
              <div class="widget-icons pull-right">
                <a href="#"><i class="fa fa-search"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
                  <th>Doc</th>
            			<th>Problem</th>
            			<th>Created For</th>
            			<th>Priority</th>
                  <th>DOC Seq</th>
                  <th>Attachment</th>
                  <th>Status</th>
                  <th>Request Date</th>
                  <th>Duedate</th>
                  <th>Opsi</th>
                  <th>Detail</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
                  $no=$this->it_service_model->limit;
            			$no=$no+1;
            			echo '<tr border color=red>';
                  //echo $_SESSION['username'];

                  //foreach($users as $usr){
                    foreach ($it_services as $serv){
                      // if($usr->employee_name==$serv->employee_name){
                        echo '<td>'.$no.'</td>';
                        echo '<td>'.$serv->doc_name.'</td>';
                        echo '<td>'.$serv->problem_name.'</td>';
                        echo '<td>'.$serv->created_for.'</td>';
                        echo '<td>'.$serv->priority_name.'</td>';
                        echo '<td>'.$serv->doc_seq_id.'</td>';
                        if(!empty($serv->attachment)){
                          echo '<td><a href="/asset/file/service_file/'.$serv->attachment.'" target="_blank">View</a></td>';
                        }else{
                          echo '<td style="color:red;">Empty</td>';
                        }
                        
                        echo '<td><center><span class='.$serv->class_status.'>'.$serv->status_app_name.'</span></td>';
                        echo '<td>'.$serv->request_date.'</td>';

                        echo '<td>'.$serv->duedate.'</td>';
                        if($serv->status_app_id==1 || $serv->status_app_id==7){
                          echo '<td><center><div class="btn-group">
                                  <button class="btn btn-xs btn-success" title="Edit" onclick="$(\'#contentAjax\').load(\'./it_services/edit?id='.$serv->id.'\')"><i class="fa fa-pencil"></i></button>
                                  <button id="btndelete" class="btn btn-xs btn-danger" title="Delete" onclick="if(confirm(&quot;anda yakin mau menghapus?&quot;)){delete_app('.$serv->id.')}return false;"><i class="fa fa-trash-o"></i></button>
                                </div></td>';
                        }else{
                          echo '<td><center><span title="Gak bisa edit :P" class="label label-danger">Can\'t Edit</span></td>';
                        }
                        echo '<td><center><a href="#myModal2" data-toggle="modal" onclick="$(\'#justTest\').load(\'./it_services/detail?id='.$serv->id.'\')" class="btn btn-success btn-xs" title="Detail"><i class="fa fa-eye"></i></a></td>';
                        echo '</tr>';
                        
                        $no++;
                      // }
                    }
                  //}
            		?>
            		</tbody>
            	</table>
            <div class="widget-foot">
              <div class="table-info" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($it_services) ?> dari <?= $total_rows ?> data</div>
              <?= $page ?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Detail IT Service</h4>
              </div>
              <div class="modal-body">
                <div id="justTest"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<script type="text/javascript">
        function delete_app(id){
          $.ajax({
            type    : 'GET',
            url     : './it_services/delete?id='+id,
            //data    : table,
            success : function(data){
              alert("Data Berhasil Dihapus");
              $("#contentAjax").load("./it_services");
            }
          });
        }
</script>