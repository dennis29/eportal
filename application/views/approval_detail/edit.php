	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit Approval Detail</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                      $attseq=array(
                          'name'=>'sequence',
                          'value'=>$detail->sequence,
                          'class'=>'form-control'
                        );

                      $attman=array(
                          'name'=>'mandatory',
                          'value'=>$detail->mandatory,
                          'class'=>'form-control'
                        );

                  		$attsubmit=array(
                  				'name'=>'btnsubmit',
                          'id'=>'btnsubmit',
                  				'content'=>'Edit Approval Detail',
                  				//'type'=>'submit',
                  				'class'=>'btn btn-primary btn-block'
                  			);
					           ?>

                      <?= form_open('','id=editAppDetail') ?>
                          <fieldset>
                              <input type="hidden" name="id" value="<?= $detail->id ?>"></input>                             
                              <div class="form-group">
                                    <?= form_label('Approval','approval_id') ?>
                                    <select name="approval_id" class="form-control">
                                    <?php
                                      foreach($approval as $app){
                                        if($detail->approval_id==$app->id){
                                          $selected='selected';
                                          echo '<option value='.$app->id.' selected='.$selected.'>'.$app->approval_name.'</option>';
                                        }else{
                                          echo '<option value='.$app->id.'>'.$app->approval_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                              </div>
                              <div class="form-group">
                                  <?= form_label('Sequence','sequence') ?>
                                  <?= form_input($attseq) ?>
                              </div>

                              <div class="form-group">
                                    <?= form_label('Mandatory','mandatory') ?>
                                    <select name="mandatory" class="form-control">
                                      <?php
                                        if ($detail->mandatory==1){
                                            echo '<option value=1 selected=selected>Yes</option>';
                                            echo '<option value=0>No</option>';
                                        }else{
                                            echo '<option value=1>Yes</option>';
                                            echo '<option value=0 selected=selected>No</option>';
                                        }
                                      ?>
                                    </select>
                              </div>
                              
                              <div class="form-group">
                                    <?= form_label('Approval By','creator_id') ?>
                                    <select name="creator_id" id="creator_id" class="form-control">
                                    <option value=>== Pilih Creator ==</option>
                                    <?php
                                      foreach($references as $ref){
                                        //echo '<option value='.$ref->id.'>'.$ref->ref_name.'</option>';
                                        //echo "<option value='$ref->id' ".set_select('creator_id', $ref->id).">$ref->ref_name</option>";
                                        if($ref->id==$detail->creator_id){
                                          $selected='selected';
                                          echo '<option value='.$ref->id.' selected='.$selected.'>'.$ref->ref_name.'</option>';
                                        }else{
                                          echo '<option value='.$ref->id.'>'.$ref->ref_name.'</option>';
                                        }
                                      }
                                    ?>
                                    </select>
                              </div>

                              <div class="form-group">
                                <select name="subcreator_id" id="subcreator_id" class="form-control">
                                <?php

                                  if(isset($_POST['creator_id'])){
                                    //$data .= "<option value=''>--Pilih UUUU--</option>";
                                    foreach ($references_list as $ref_list) {
                                      /*if($ref_list['ref_id']==$_POST['creator_id']){
                                        $data .= "<option value='$ref_list->id' selected>$ref_list->ref_list_name</option>\n";
                                      }else{*/
                                        $data .= "<option value='$ref_list->id'>$ref_list->ref_list_name</option>\n";
                                      //}
                                    }
                                    echo $data;

                                  }else{
                                    foreach($references_list as $ref_list=>$v){
                                      if($v["id"]==$detail->subcreator_id)
                                        $selected="selected='selected'";
                  									  else
                  									  	$selected='';
                                      echo '<option value='.$v["id"].' '.$selected.'>'.$v["ref_list_name"].'</option>';
                                      
                                      
                                    }
                                  }

                                ?>
                                </select>
                              </div>
                              
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#btnsubmit").click(function(){
            $.ajax({
              type: 'POST',
              url: './approval_detail/edit_proccess',
              data: $("#editAppDetail").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    $("#contentAjax").load("./approval_detail");
                 }else{
                    alert(data.message);
                 }
              }
            });
        });
    });
</script>

<script type="text/javascript">
  $(function(){
    $("#creator_id").change(function(){
      var creator_id = $("#creator_id").val();
      $.ajax({
        type    : 'GET',
        url     : './approval/get_ref_list/'+creator_id,
        //data    : 'creator_id' + creator_id,
        success : function(data){
          $("#subcreator_id").html(data);
        }
      });
    });
  });
</script>