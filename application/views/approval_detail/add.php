	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery-ui-1.9.2.custom.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom4.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' }
	);    
    </script>
    <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add Approval Detail</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		$attname=array(
                          'name'=>'approval_name',
                          //'value'=>$user->firstname,
                          'class'=>'form-control'
                        );

                      $attdoc=array(
                  				'name'=>'doc_id',
                  				//'value'=>$user->firstname,
                  				'class'=>'form-control'
                  			);

                      $attseq=array(
                          'name'=>'sequence',
                          //'value'=>$user->firstname,
                          'class'=>'form-control'
                        );

                  		$attsubmit=array(
                  				'name'=>'sendapproval',
                          'id'=>'sendapproval',
                  				'content'=>'Add Approval Detail',
                  				//'type'=>'submit',
                  				'class'=>'btn btn-primary btn-block'
                  			);
					           ?>

                      <?= form_open('','id=addAppDetail') ?>
                          <fieldset>
                              <div class="form-group">
                                    <?= form_label('Approval','approval_id') ?>
                                    <select name="approval_id" class="form-control">
                                    <?php
                                      foreach($approval as $app){
                                        echo '<option value='.$app->id.'>'.$app->approval_name.'</option>';
                                      }
                                    ?>
                                    </select>
                              </div>
                              <div class="form-group">
                                  <?= form_label('Sequence','sequence') ?>
                                  <?= form_input($attseq) ?>
                              </div>

                              <div class="form-group">
                                    <?= form_label('Mandatory','mandatory') ?>
                                    <select name="mandatory" class="form-control">
                                        <option value=1>Yes</option>
                                        <option value=0>No</option>
                                    ?>
                                    </select>
                              </div>

                              <div class="form-group">
                                    <?= form_label('Approval By','creator_id') ?>
                                    <select name="creator_id" id="creator_id" class="form-control">
                                    <option value=>== Pilih Creator ==</option>
                                    <?php
                                      foreach($references as $ref){
                                        echo '<option value='.$ref->id.'>'.$ref->ref_name.'</option>';
                                      }
                                    ?>
                                    </select>
                              </div>

                              <div class="form-group">
                                    <select name="subcreator_id" id="subcreator_id" class="form-control">
                                    <?php
                                      if(isset($_POST['creator_id'])){
                                        foreach($references_list as $ref_list){
                                          $data .= '<option value='.$ref_list->id.'>'.$ref_list->ref_list_name.'</option>';
                                        }
                                        echo $data;
                                      }else{
                                        echo "<option value>== Pilih Sub Creator ==</option>";
                                      }
                                      
                                    ?>
                                    </select>
                              </div>
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#sendapproval").click(function(){
            $.ajax({
              type: 'POST',
              url: './approval_detail/add_proccess',
              data: $("#addAppDetail").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    $("#contentAjax").load("./approval_detail");
                 }else{
                    alert(data.message);
                 }
              }
            });
        });
    });
</script>

<script type="text/javascript">
  $(function(){
    $("#creator_id").change(function(){
      var creator_id = $("#creator_id").val();
      $.ajax({
        type    : 'GET',
        url     : './approval/get_ref_list/'+creator_id,
        //data    : 'creator_id' + creator_id,
        success : function(data){
          $("#subcreator_id").html(data);
        }
      });
    });
  });
</script>