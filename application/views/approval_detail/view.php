<div class="page-head">
  <h2 class="pull-left">Manage Approval Detail</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="/"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Approval Detail</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-9">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Approval List Detail</div>
            <div id="datatable_filter" class="dataTables_filter pull-right">
            </div>
<!--            
			  <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
-->
          <div class="clearfix"></div>
          </div>
          <div class="widget-content">
			<div id="datatable_wrapper" class="dataTables_wrapper" role="grid">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
                    <table id="datatable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>NO</th>
                      <th>Approval Name</th>
                      <th>Sequance</th>
                      <th>Mandatory</th>
                      <th>Creator</th>
                      <th>Sub Creator</th>
                      <th>Created</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no=$this->approval_detail_model->limit+1;
                      $default='[Default]';
                            echo '<tr>';
                            foreach ($detail as $det){
                                echo '<td>'.$no.'</td>';
                                echo '<td>'.$det->approval_name.'</td>';
                        echo '<td>'.$det->sequence.'</td>';
                        echo '<td>'.$det->mandatory.'</td>';
                        echo '<td>'.$det->ref_name.'</td>';
                        echo '<td>'.$det->ref_list_name.'</td>';
                        echo '<td>'.$det->created.'</td>';
                        echo '<td><div class="btn-group">
                                      <button class="btn btn-xs btn-success" title="Edit" onclick="loading(\'./approval_detail/edit?id='.$det->id.'\')"><i class="fa fa-pencil"></i></button>
                                      <button id="btndelete" class="btn btn-xs btn-danger" title="Delete" onclick="if(confirm(&quot;anda yakin mau menghapus?&quot;)){delete_app('.$det->id.')}return false;"><i class="fa fa-trash-o"></i></button>
                                    </div></td>';
                            echo '</tr>';
                            $no++;
                            }
                        ?>
                        </tbody>
                    </table>
			</div>
            <div class="widget-foot">
              <div class="table-info" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($detail) ?> dari <?= $total_rows ?> data</div>
              <?= $page ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
        function delete_app(id){
          $.ajax({
            type    : 'GET',
            url     : './approval_detail/delete?id='+id,
            //data    : table,
            success : function(data){
              alert("Data Berhasil Dihapus");
              $("#contentAjax").load("./approval_detail");
            }
          });
        }
</script>