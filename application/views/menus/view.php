<div class="page-head">
	<h2 class="pull-left">Manage Menu</h2>
    <div class="clearfix"></div>
    <!-- Breadcrumb -->
    <div class="bread-crumb">
      <a href="/"><i class="fa fa-home"></i> Home</a> 
      <!-- Divider -->
      <span class="divider">/</span> 
    <a href="#" class="bread-current">Menu</a>
  </div>
  <div class="clearfix"></div>
</div>
  <div class="matter">
    <div class="container">
  		<div class="row">
        <div class="col-md-10">
          <div class="widget">
            <div class="widget-head">
              <div class="pull-left">Daftar Semua Menu</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
              <div class="clearfix"></div>
            </div>
            <div class="widget-content">
            	<!-- <h2>Manage Roles</h2>
            	<a href="roles/add">Add Role</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
      						<th>No</th>
      						<th>Label</th>
      						<th>Link</th>
      						<th>Icon</th>
      						<th>Sort</th>
      						<th>Parent</th>
      						<th>Opsi</th>
    					  </tr>
            		</thead>
            		<tbody>
            		<?php
                $no=1;
    						foreach($menus as $mn){
    							echo '<tr>';
                  echo '<td>'.$no.'</td>';
    							echo '<td>'.$mn->label.'</td>';
    							echo '<td>'.$mn->link.'</td>';
    							echo '<td>'.$mn->i_class.'</td>';
    							echo '<td>'.$mn->sort.'</td>';
    							echo '<td>'.$mn->parent_name.'</td>';
    			                echo '<td><div class="btn-group">
                                              <button class="btn btn-xs btn-success" onclick="loading(\'./menus/edit?id='.$mn->id.'\')" title="edit"><i class="fa fa-pencil"></i></button>
                                              
                                            </div></td>';

    							echo '</tr>';
                  //<button class="btn btn-xs btn-danger" onclick="$(\'#contentAjax\').load(\'/index.php/menus/delete?id='.$mn->id.'\')"><i class="fa fa-trash-o"></i></button>
    						  $no++;
                }
    					?>
            		</tbody>
            	</table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>