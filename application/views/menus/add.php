	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add Menu</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		$attlabel=array(
                  				'name'=>'label',
                  				//'value'=>$user->firstname,
                  				'class'=>'form-control'
                  			);

                  		$attlink=array(
                  				'name'=>'link',
                  				//'value'=>$user->lastname,
                  				'class'=>'form-control'
                  			);

                  		$atti_class=array(
                  				'name'=>'i_class',
                  				//'value'=>$user->email_address,
                          'placeholder'=>'<i class=fa fa-example></i>',
                  				'class'=>'form-control'
                  			);

                  		$attsort=array(
                  				'name'=>'sort',
                  				//'value'=>$user->username,
                  				'class'=>'form-control'
                  			);

                  		$attparent=array(
                  				'name'=>'parent',
                  				'class'=>'form-control'
                  			);

                  		$attsubmit=array(
                  				'name'=>'btnsubmit',
                          'id'=>'btnsubmit',
                  				'content'=>'Add Menu',
                  				//'type'=>'submit',
                  				'class'=>'btn btn-primary btn-block'
                  			);
            					?>

                      <?= form_open('','id=addMenu') ?>
                          <fieldset>
                                <div class="form-group">
                                   <?= form_label('Label : ', 'label') ?>
									                 <?= form_input($attlabel) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Link','link') ?>
                                    <?= form_input($attlink) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Icon','i_class') ?>
                                    <?= form_input($atti_class) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Sort','sort') ?>
                                    <?= form_input($attsort) ?>
                                </div>                              
                                <div class="form-group">
                                    <?= form_label('Parent','parent') ?>
                                    <select name='parent' class="form-control">
                  											<option value=0>==Root==</option>
                  									<?php
                  										foreach($menus as $mn){
                  											echo '<option value='.$mn->id.'>'.$mn->label.'</option>';	
                  										}
                  										
                  									?>
                  									</select>
	                               </div>
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
    $('#btnsubmit').click(function(){
      $.ajax({
        type : 'POST',
        url : './menus/add_proccess',
        data : $('#addMenu').serialize(),
        success : function(data){
          data = jQuery.parseJSON(data);
          if(data.success){
            alert('Data Berhasil disimpan');
            $("#contentAjax").load('./menus');
          }else{
            alert(data.message);
          }
        }
      });
    });
  });
</script>