  <script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' }
	);    
    </script>
	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Request Kendaraan</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		$attcreated=array(
                  				'name'=>'created_for',
                  				//'value'=>$user->firstname,
                  				'class'=>'form-control'
                  			);

                  		$attdesc=array(
                  				'name'=>'description',
                  				//'value'=>$user->lastname,
                  				'class'=>'form-control'
                  			);

                  		$attatta=array(
                  				'name'=>'attachment',
                  				//'value'=>$user->username,
                  				'class'=>'form-control'
                  			);

                      $attdue=array(
                          'name'=>'duedate',
                          'class'=>'form-control'
                        );

                    ?>
                       
                       <?= form_open_multipart('','id=addGA') ?>
                          <fieldset>

                                <input type="hidden" name="param" value="" class="form-control" id="param">
                                    <select name="doc_id" class="form-control"  style="display: none;">
                                    <?php
                                      foreach($docs as $doc){
                                        if($doc->id==4){
                                          echo '<option value='.$doc->id.' selected="selected">'.$doc->doc_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>

                                <div class="form-group">
                                    <label for="destination">Destination</label>
                                    <textarea name="destination" cols="40" rows="3" class="form-control"></textarea>
                                </div>    
                                <div class="form-group">
                                    <label for="city">City</label>                                    
                                    <input name="city" value="" class="form-control" type="text">                                
                                </div>

                                <div class="form-group">
                                    <label for="utility">Utility</label>
                                    <textarea name="utility" cols="40" rows="3" class="form-control"></textarea>
                                </div>
                                <?php
                                  foreach($users as $usr){
                                    if($usr->username==$_SESSION['username']){
                                      echo form_hidden('employee_id',$usr->id);
                                      echo form_hidden('request_by',$usr->employee_id);
                                    }
                                  }
                                ?>
                      
                                <!-- <div class="form-group">
                                    <?= form_label('Priority','priority_id') ?>
                                    <select name="priority_id" class="form-control">
                                    <?php
                                      // foreach($priority as $pri){
                                      //   echo '<option value='.$pri->id.'>'.$pri->priority_name.'</option>';
                                      // }
                                    ?>
                                    </select>
                                </div> -->

                                <div class="form-group">
                                    <?= form_label('Attachment','attachment') ?>
                                    <input type="file" name="attachment" id="attachment">
                                </div>
                                <div class="form-group">
                                    <?= form_label('Date & Time Request','date_req') ?>
                                    <p class="text-danger">* Permintaan maksimum 1 (satu) hari sebelumnya, pk. 16.00 WIB</p>
                                    <div id="datetimepicker1" class="input-append">
                                      <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="date_req">
                                      <span class="add-on">
                                        <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                      </span>
                                    </div>
                                </div>

                                <div id="datetimepicker2" class="input-append">
                                  <input data-format="hh:mm:ss" type="text" class="form-control dtpicker" name="time_req">
                                  <span class="add-on">
                                    <i data-time-icon="fa fa-clock-o" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                  </span>
                                </div>
                                <hr/>

                                <button name="sendapproval" type="button" id="sendapproval" class="btn btn-success btn-block">Send Approval</button>
                                <button name="draftapproval" type="button" id="draftapproval" class="btn btn-danger btn-block">Save Draft</button>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function(){
      var d = new Date();
      var time = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

      $("#addGA").ajaxForm({
              type: 'POST',
              url: './ga_services/add_process',
              data: $("#addGA").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    var $btn = $("button");
                    $btn.button('reset');
                    $("#contentAjax").load("./ga_services");
                 }else{
                  alert(data.message);
                 }
                 var $btn = $("button");
                 $btn.button('reset');
              }
            });
        $("#sendapproval").click(function(){
          if(time>'16:00:00'){
            alert('Permintaan maksimum pukul 16.00 WIB');
          }else{
            var $btn = $("button");
            $btn.button('loading');
            $("#param").val("send");
            $("#addGA").submit();
          }
          
        });
        
        $("#draftapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("draft");
          $("#addGA").submit();
        });

        $("#massal").change(function(){
            $("p").removeAttr("readonly");
        });
    });
</script>