	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>

  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-content">
              <div class="padd invoice">
                <div class="row">
                  <div class="col-md-12">
                        <div class="alert alert-success">
                          Request By : <?= $detail->employee_name ?><br>
                          Divisi : <?= $detail->divisi_name ?><br>
                          Priority : <?= $detail->priority_name ?><br>
                          Destination : <?= $detail->destination ?><br>
                          Utility : <?= $detail->utility ?><br>
                          City : <?= $detail->city ?><br>
                          Attachment : 
                          <?php
                            if(!empty($detail->attachment)){
                              echo '<a href="/eportal2/asset/file/'.$detail->attachment.'" target="_blank">View</a><br>';
                            }else{
                              echo '<style="color:red;">Empty<br>';
                            }
                          ?>
                          Date Request : <?= $detail->date_req ?><br>
                          Time Request : <?= $detail->time_req ?><br>
                        </div>
                        <div class="alert alert-danger">
                          Created Date : <?= $detail->created_date ?><br>
                          Created Time : <?= $detail->created_time ?><br>
                        </div>
                        <?php
                          if($detail->vehicle_id==1){
                        ?>
                        <div class="alert alert-info">
                          Vehiche : <?= $detail->vehicle_name ?><br>
                          No Polisi : <?= $detail->nopol ?><br>
                          Driver Name : <?= $detail->driver_name ?><br>
                        </div>
                        <?php
                          }elseif($detail->vehicle_id==2){
                        ?>
                        <div class="alert alert-info">
                          Vehiche : <?= $detail->vehicle_name ?><br>
                          No Voucher : <?= $detail->no_voucher ?><br>
                        </div>
                        <?php
                          }elseif($detail->vehicle_id==3){
                        ?>
                        <div class="alert alert-info">
                          Vehiche : <?= $detail->vehicle_name ?><br>
                        </div>
                        <?php
                          }else{
                        ?>
                        <div class="alert alert-info">
                          Still Waiting Confirm By Admin GA
                        </div>
                        <?php
                          }
                        ?>
                    </div>
                  </div>
                </div>
              </div>
           </div>
        </div>
      </div>
    </div>
  </div>