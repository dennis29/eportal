	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>


	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit GA Service</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">
                       <?= form_open_multipart('','id=editGA') ?>
                          <fieldset>
                                <input type="hidden" name="param" value="" class="form-control" id="param">
                                <input type="hidden" name="id" value="<?= $ga->id ?>" class="form-control" type="text">
                                    <select name="doc_id" class="form-control"  style="display: none;">
                                    <?php
                                      foreach($docs as $doc){
                                        if($doc->id==4){
                                          echo '<option value='.$doc->id.' selected="selected">'.$doc->doc_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>

                                <div class="form-group">
                                    <label for="city">City</label>                                    
                                    <input name="city" class="form-control" type="text" value="<?= $ga->city ?>">                                
                                </div>

                                <div class="form-group">
                                    <label for="destination">Destination</label>
                                    <textarea name="destination" cols="40" rows="5" class="form-control"><?= $ga->destination ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="utility">Utility</label>
                                    <textarea name="utility" cols="40" rows="5" class="form-control"><?= $ga->utility ?></textarea>
                                </div>
                                <?php
                                  foreach($users as $usr){
                                    if($usr->username==$_SESSION['username']){
                                      echo form_hidden('employee_id',$usr->id);
                                      echo form_hidden('request_by',$usr->employee_id);
                                    }
                                  }
                                ?>
                      
                                <div class="form-group">
                                    <?= form_label('Priority','priority_id') ?>
                                    <select name="priority_id" class="form-control">
                                    <?php
                                      foreach($priority as $pri){
                                        if($ga->priority_id==$pri->id){
                                          echo '<option value='.$pri->id.' selected=selected>'.$pri->priority_name.'</option>';
                                        }else{
                                          echo '<option value='.$pri->id.'>'.$pri->priority_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Attachment','attachment') ?>
                                    <input type="hidden" name="attachment2" value="<?php echo $ga->attachment; ?>" class="form-control">
                                    <input type="file" name="attachment" id="attachment">
                                </div>
                                <div class="form-group">
                                  <label><?php echo $ga->attachment; ?></label>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Date & Time Request','date_req') ?>
                                    <div id="datetimepicker1" class="input-append">
                                      <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="date_req" value="<?= $ga->date_req ?>">
                                      <span class="add-on">
                                        <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                      </span>
                                    </div>
                                </div>

                                <div id="datetimepicker2" class="input-append">
                                  <input data-format="hh:mm:ss" type="text" class="form-control dtpicker" name="time_req" value="<?= $ga->time_req ?>">
                                  <span class="add-on">
                                    <i data-time-icon="fa fa-clock-o" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                  </span>
                                </div>
                                <hr/>

                                <button name="sendapproval" type="button" id="sendapproval" class="btn btn-success btn-block">Send Approval</button>
                                <button name="draftapproval" type="button" id="draftapproval" class="btn btn-danger btn-block">Save Draft</button>

                                <!-- <button name="sendapproval" type="button" id="sendapproval" class="btn noty-success btn-success btn-block">Send Approval</button>
                                <button name="draftapproval" type="button" id="draftapproval" class="btn noty-success btn-danger btn-block">Save Draft</button> -->

                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function(){
       //alert("OKE");
      $("#editGA").ajaxForm({
              type: 'POST',
              url: './ga_services/edit_process',
              data: $("#editGA").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    var $btn = $("button");
                    $btn.button('reset');
                    $("#contentAjax").load("./ga_services");
                 }else{
                  alert(data.message);
                 }
                 var $btn = $("button");
                 $btn.button('reset');
              }
            });
        $("#sendapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("send");
          $("#editGA").submit();
        });
        
        $("#draftapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("draft");
          $("#editGA").submit();
        });

        $("#massal").change(function(){
            $("p").removeAttr("readonly");
        });
    });

$(function(){
      $("#vehicle").change(function(){
        if($(this).val() == '1'){ 

          $("#nopol").attr("type","text");
          $("#nopola").removeAttr("hidden","hidden");

          $("#driver").attr("type","text");
          $("#driverl").removeAttr("hidden","hidden");

          $("#voucher").attr("type","hidden");
          $("#voucherl").attr("hidden","hidden");

          $("#nopol").removeAttr("disabled","disabled");

          $("#driver").removeAttr("disabled","disabled");

          $("#voucher").attr("disabled","disabled");

        }else if($(this).val() == '2'){

          $("#nopol").attr("type","hidden");
          $("#nopola").attr("hidden","hidden");

          $("#driver").attr("type","hidden");
          $("#driverl").attr("hidden","hidden");

          $("#voucher").attr("type","text");
          $("#voucherl").removeAttr("hidden","hidden");

          $("#nopol").attr("disabled","disabled");

          $("#driver").attr("disabled","disabled");

          $("#voucher").removeAttr("disabled","disabled");

        }else if($(this).val() == '3'){

          $("#nopol").attr("type","hidden");
          $("#nopola").attr("hidden","hidden");

          $("#driver").attr("type","hidden");
          $("#driverl").attr("hidden","hidden");

          $("#voucher").attr("type","hidden");
          $("#voucherl").attr("hidden","hidden");

          $("#nopol").attr("disabled","disabled");

          $("#driver").attr("disabled","disabled");

          $("#voucher").attr("disabled","disabled");
        }
      });
    });
</script>