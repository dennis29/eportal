	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>

<div class="page-head">
  <h2 class="pull-left">Manage GA Services</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="/index.php/main"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">GA Services</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">GA Services List</div>
              <div class="widget-icons pull-right">
                <?= '<a href="#search_modalGA" data-toggle="modal" onclick="$(\'#search_contentGA\').load(\'./ga_services/search\')" title="Seacrh"><i class="fa fa-search"></i></a>'; ?>
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a> 
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
                  <th>Doc No.</th>
            			<th class="sembunyi">Date</th>
            			<th class="sembunyi">Time</th>
            			<th class="sembunyi">Priority</th>
                  <th class="sembunyi">Destination</th>
                  <th class="sembunyi">City</th>
                  <th class="sembunyi">Attachment</th>
                  <th>Status</th>
                  <th>Opsi</th>
                  <th>View</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
                  $no=$this->ga_service_model->limit;
            			$no=$no+1;
            			echo '<tr border color=red>';
                  //echo $_SESSION['username'];

                  //foreach($users as $usr){
                    foreach ($ga_services as $ga){
                      // if($usr->employee_name==$serv->employee_name){
                        echo '<td>'.$no.'</td>';
                        echo '<td>'.$ga->doc_seq_id.'</td>';
                        //echo '<td class="sembunyi">'.$ga->employee_name.'</td>';
                        echo '<td class="sembunyi">'.$ga->date_req.'</td>';
                        echo '<td class="sembunyi">'.$ga->time_req.'</td>';
                        echo '<td class="sembunyi">'.$ga->priority_name.'</td>';
                        echo '<td class="sembunyi">'.$ga->destination.'</td>';
                        echo '<td class="sembunyi">'.$ga->city.'</td>';
                        if(!empty($ga->attachment)){
                          echo '<td class="sembunyi"><a href="/eportal2/asset/file/service_file/'.$ga->attachment.'" target="_blank">View</a></td>';
                        }else{
                          echo '<td class="sembunyi" style="color:red;">Empty</td>';
                        }
                        
                        echo '<td><center><span class='.$ga->class_status.'>'.$ga->status_app_name.'</span></td>';
                        if($ga->status_app_id==1 || $ga->status_app_id==7){
                          echo '<td><center><div class="btn-group">
                                  <button class="btn btn-xs btn-success" title="Edit" onclick="$(\'#contentAjax\').load(\'./ga_services/edit?id='.$ga->id.'\')"><i class="fa fa-pencil"></i></button>
                                  <button id="btndelete" class="btn btn-xs btn-danger" title="Delete" onclick="if(confirm(&quot;anda yakin mau menghapus?&quot;)){delete_app('.$ga->id.')}return false;"><i class="fa fa-trash-o"></i></button>
                                </div></td>';
                        }else{
                          echo '<td><center><span title="Gak bisa edit :P" class="label label-danger">Can\'t Edit</span></td>';
                        }

                        echo '<td><center><a href="#detail_modalGA" data-toggle="modal" onclick="$(\'#detail_contentGA\').load(\'./ga_services/detail?id='.$ga->id.'\')" class="btn btn-success btn-xs" title="Detail"><i class="fa fa-eye"></i></a></td>';

                        echo '</tr>';
                        
                        $no++;
                      // }
                    }
                  //}
            		?>
            		</tbody>
            	</table>
            <div class="widget-foot">
              <div class="table-info sembunyi" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($ga_services) ?> dari <?= $total_rows ?> data</div>
              <?= $page ?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="detail_modalGA" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Detail GA Service</h4>
              </div>
              <div class="modal-body">
                <div id="detail_contentGA"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<div id="search_modalGA" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Search GA Service</h4>
              </div>
              <div class="modal-body">
                <div id="search_contentGA"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<script type="text/javascript">
        function delete_app(id){
          $.ajax({
            type    : 'GET',
            url     : './ga_services/delete?id='+id,
            //data    : table,
            success : function(data){
              alert("Data Berhasil Dihapus");
              $("#contentAjax").load("./ga_services");
            }
          });
        }

$(window).resize(function() {
    if (document.documentElement.clientWidth <= 1100) {    
			$(".sembunyi").hide();
    }
	else
	{
			$(".sembunyi").show();		
	}
}).resize();
		
</script>