	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery-ui-1.9.2.custom.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom4.min.js'); ?>' }
	);    
    </script>

  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget" border=0>
                <div class="widget-content">
                  <div class="padd">
                      <?= form_open('','id=searchGA') ?>
                          <fieldset>
                          		<div class="form-group">
                                    <div class="form-group">
                                        <select name="status_app_id" id="status_app_id" class="form-control">
                                          <option value=>== Select Status ==</option>
                                          <?php
                                            foreach($status_app as $sta){
                                              echo '<option value='.$sta->id.'>'.$sta->status_app_name.'</option>';
                                            }
                                          ?>
                                                                              
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="doc_seq_id">Doc Number</label>
                                        <input id="doc_seq_id" name="doc_seq_id" class="form-control" type="text">
                                    </div>
      	                            <button name="btnsearch" type="button" id="btnsearch" onclick="search()" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search</button>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	  function search(){
              $("#contentAjax").load("./ga_services/index?"+$("#searchGA").serialize());
              $("#tutupmodal2").trigger("click");
        }

	</script>