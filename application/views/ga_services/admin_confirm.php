	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' }
	);    
    </script>
	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add GA Service</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">
                       
                       <?= form_open_multipart('','id=confirmGA') ?>
                          <fieldset>
                                <input type="hidden" name="param" value="" class="form-control" id="param">
                                   
                                <div class="form-group">
                                    <?= form_label('Vehicle','vehicle_id') ?>
                                    <select name="vehicle_id" class="form-control" id="vehicle">
                                    <?php
                                      foreach($vehicle as $ve){
                                        echo '<option value='.$ve->id.'>'.$ve->vehicle_name.'</option>';
                                      }
                                    ?>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                     <label for="no_polisi" id="nopola">Nomor Polisi</label>
                                    <select name="nopol_id" class="form-control" id="nopol">
                                    	<option value=></option>
                                    <?php
                                      foreach($nopol as $np){
                                        echo '<option value='.$np->id.'>'.$np->nopol.'</option>';
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="driver_name" id="driverl">Driver Name</label>
                                    <select name="driver_name" class="form-control" id="driver">
                                    	<option value=></option>
                                    <?php
                                      foreach($driver as $dr){
                                        echo '<option value='.$dr->id.'>'.$dr->driver_name.'</option>';
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label hidden="hidden" for="no_voucher" id="voucherl">No Voucher</label>                                    
                                    <input type="hidden" name="no_voucher" id="voucher" value="" class="form-control" type="text" placeholder="Input No Voucher">                                
                                </div>

                                <button name="sendapproval" type="button" id="sendapproval" class="btn btn-success btn-block">Send Approval</button>
                                <button name="draftapproval" type="button" id="draftapproval" class="btn btn-danger btn-block">Save Draft</button>

                                <!-- <button name="sendapproval" type="button" id="sendapproval" class="btn noty-success btn-success btn-block">Send Approval</button>
                                <button name="draftapproval" type="button" id="draftapproval" class="btn noty-success btn-danger btn-block">Save Draft</button> -->

                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function(){
       //alert("OKE");
      $("#confirmGA").ajaxForm({
              type: 'POST',
              url: './ga_services/add_process',
              data: $("#confirmGA").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    var $btn = $("button");
                    $btn.button('reset');
                    $("#contentAjax").load("./ga_services");
                 }else{
                  alert(data.message);
                 }
                 var $btn = $("button");
                 $btn.button('reset');
              }
            });
        $("#sendapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("send");
          $("#confirmGA").submit();
        });
        
        $("#draftapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("draft");
          $("#confirmGA").submit();
        });

        $("#massal").change(function(){
            $("p").removeAttr("readonly");
        });
    });

    $("button").click(function() {
        var $btn = $(this);
        $btn.button('loading');
        // simulating a timeout
        setTimeout(function () {
            $btn.button('reset');
        }, 1000);
    });

    $(function(){
      $("#vehicle").change(function(){
        if($(this).val() == '1'){ 

          $("#nopol").removeAttr("style","display:none;");
          $("#nopola").removeAttr("hidden","hidden");

          $("#driver").removeAttr("style","display:none;");
          $("#driverl").removeAttr("hidden","hidden");

          $("#voucher").attr("type","hidden");
          $("#voucherl").attr("hidden","hidden");

          $("#nopol").removeAttr("disabled","disabled");

          $("#driver").removeAttr("disabled","disabled");

          $("#voucher").attr("disabled","disabled");

        }else if($(this).val() == '2'){

          $("#nopol").attr("style","display:none;");
          $("#nopola").attr("hidden","hidden");

          $("#driver").attr("style","display:none;");
          $("#driverl").attr("hidden","hidden");

          $("#voucher").attr("type","text");
          $("#voucherl").removeAttr("hidden","hidden");

          $("#nopol").attr("disabled","disabled");

          $("#driver").attr("disabled","disabled");

          $("#voucher").removeAttr("disabled","disabled");

        }else if($(this).val() == '3'){

          $("#nopol").attr("style","display:none;");
          $("#nopola").attr("hidden","hidden");

          $("#driver").attr("style","display:none;");
          $("#driverl").attr("hidden","hidden");

          $("#voucher").attr("type","hidden");
          $("#voucherl").attr("hidden","hidden");

          $("#nopol").attr("disabled","disabled");

          $("#driver").attr("disabled","disabled");

          $("#voucher").attr("disabled","disabled");
        }
      });
    });
</script>