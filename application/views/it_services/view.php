	<script type="text/javascript" src="<?php echo base_url('/asset/js/custom2.min.js'); ?>"></script>

<div class="page-head">
  <h2 class="pull-left">Manage IT Services</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">IT Services</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">IT Services List</div>
              <div class="widget-icons pull-right">
                <?= '<a href="#modalSearchIT" data-toggle="modal" onclick="$(\'#contentSearchIT\').load(\'./it_services/search\')" title="Seacrh"><i class="fa fa-search"></i></a>'; ?>
                
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a> 
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table tablesorter table-striped table-bordered table-hover table-responsive">
            		<thead>
            		<tr>
            			<th>NO</th>
                  <th class="sembunyi">Doc</th>
            			<th class="sembunyi">Problem</th>
            			<th class="sembunyi">Created For</th>
            			<th class="sembunyi">Priority</th>
                  <th>DOC Seq</th>
                  <th class="sembunyi">Attachment</th>
                  <th>Status</th>
                  <th class="sembunyi">Request Date</th>
                  <th class="sembunyi">Duedate</th>
                  <th>Opsi</th>
                  <th>Detail</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
                  $no=$this->it_service_model->limit;
            			$no=$no+1;
            			echo '<tr border color=red>';
                  //echo $_SESSION['username'];

                  //foreach($users as $usr){
                    foreach ($it_services as $serv){
                      // if($usr->employee_name==$serv->employee_name){
                        echo '<td>'.$no.'</td>';
                        echo '<td class="sembunyi">'.$serv->doc_name.'</td>';
                        echo '<td class="sembunyi">'.$serv->problem_name.'</td>';
                        echo '<td class="sembunyi">'.$serv->created_for.'</td>';
                        echo '<td class="sembunyi">'.$serv->priority_name.'</td>';
                        echo '<td>'.$serv->doc_seq_id.'</td>';
                        if(!empty($serv->attachment)){
                          echo '<td class="sembunyi"><a href="/asset/file/service_file/'.$serv->attachment.'" target="_blank">View</a></td>';
                        }else{
                          echo '<td class="sembunyi" style="color:red;">Empty</td>';
                        }
                        
                        echo '<td><center><span class='.$serv->class_status.'>'.$serv->status_app_name.'</span></td>';
                        echo '<td class="sembunyi">'.$serv->request_date.'</td>';

                        echo '<td class="sembunyi">'.$serv->duedate.'</td>';
                        if(($serv->status_app_id==1 || $serv->status_app_id==7))
						{
                          echo '<td><center><div class="btn-group">
                                  <button class="btn btn-xs btn-success" title="Edit" onclick="$(\'#contentAjax\').load(\'./it_services/edit?id='.$serv->id.'\')"><i class="fa fa-pencil"></i></button>
                                  <button id="btndelete" class="btn btn-xs btn-danger" title="Delete" onclick="if(confirm(&quot;anda yakin mau menghapus?&quot;)){delete_app('.$serv->id.')}return false;"><i class="fa fa-trash-o"></i></button>
                                </div></td>';
                        }
						else
						{
                          echo '<td><center><span title="Gak bisa edit :P" class="label label-danger">Can\'t Edit</span></td>';
                        }
                        echo '<td><center><a href="#modalDetailIT" data-toggle="modal" onclick="$(\'#contentDetailIT\').load(\'./it_services/detail?id='.$serv->id.'\')" class="btn btn-success btn-xs" title="Detail"><i class="fa fa-eye"></i></a></td>';
                        echo '</tr>';
                        
                        $no++;
                      // }
                    }
                  //}
            		?>
            		</tbody>
            	</table>
            <div class="widget-foot">
              <div class="table-info sembunyi" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($it_services) ?> dari <?= $total_rows ?> data</div>
              <?= $page ?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modalDetailIT" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Detail IT Service</h4>
              </div>
              <div class="modal-body">
                <div id="contentDetailIT"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<div id="modalSearchIT" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Search IT Service</h4>
              </div>
              <div class="modal-body">
                <div id="contentSearchIT"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<script type="text/javascript">
        function delete_app(id){
          $.ajax({
            type    : 'GET',
            url     : './it_services/delete?id='+id,
            //data    : table,
            success : function(data){
              alert("Data Berhasil Dihapus");
              $("#contentAjax").load("./it_services");
            }
          });
        }
		

$(window).resize(function() {
//	console.log(document.documentElement.clientWidth);
    if (document.documentElement.clientWidth <= 1100) {    
			$(".sembunyi").hide();
    }
	else
	{
			$(".sembunyi").show();		
	}
}).resize();
</script>

<script>
  $(function(){
    $("#datatable").tablesorter({ sortList: [[0,0], [1,0]] });
  });
</script>