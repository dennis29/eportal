  <script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap-datetimepicker.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/custom-with-date.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.form.min.js'); ?>"></script>
    
	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add IT Service</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		$attcreated=array(
                  				'name'=>'created_for',
                  				//'value'=>$user->firstname,
                  				'class'=>'form-control'
                  			);

                  		$attdesc=array(
                  				'name'=>'description',
                  				//'value'=>$user->lastname,
                  				'class'=>'form-control'
                  			);

                  		$attatta=array(
                  				'name'=>'attachment',
                  				//'value'=>$user->username,
                  				'class'=>'form-control'
                  			);

                      $attdue=array(
                          'name'=>'duedate',
                          'class'=>'form-control'
                        );

                  		/*$attsubmit=array(
                  				'name'=>'sendapproval',
                          'id' => 'sendapproval',
                  				'content'=>'Send Approval',
                    			//'type'=>'submit',
                          // 'formaction'=>'/index.php/it_services/add_proccess',
                  				'class'=>'btn btn-success btn-block'
                  			);

                      $attdraf=array(
                          'name'=>'draftapproval',
                          'id'=>'draftapproval',
                          'content'=>'Save Draft',
                          //'type'=>'submit',
                          //'formaction'=>'/index.php/it_services/add_draft',
                          'class'=>'btn btn-danger btn-block'
                        );*/

                    ?>
                       
                       <?= form_open_multipart('','id=addIT') ?>
                          <fieldset>
                                <input type="hidden" name="param" value="" class="form-control" type="text" id="param">
                                <div class="form-group">
                                    <?= form_label('Doc Category','doc_id') ?>
                                    <select name="doc_id" id="doc_id" class="form-control">
                                    <?php
                                      foreach($docs as $doc){
                                        echo '<option value='.$doc->id.'>'.$doc->doc_name.'</option>';
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Problem Category','problem_id') ?>
                                    <select name="problem_id" id="problem_id" class="form-control">
										                  <option value="">== Pilih Problem Category ==</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Created for','created_for') ?>
                                    <?= form_input($attcreated) ?>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Description','description') ?>
                                    <?= form_textarea($attdesc) ?>
                                </div>
                                <?php

                                  foreach($users as $usr){
                                    if($usr->username==$_SESSION['username']){
                                      echo form_hidden('employee_id',$usr->id);
                                      echo form_hidden('created_by',$usr->employee_id);
                                    }
                                  }
                                ?>
                      
                                <div class="form-group">
                                    <?= form_label('Priority','priority_id') ?>
                                    <select name="priority_id" class="form-control">
                                    <?php
                                      foreach($priority as $pri){
                                        echo '<option value='.$pri->id.'>'.$pri->priority_name.'</option>';
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Attachment','attachment') ?>
                                    <input type="file" name="attachment" id="attachment">
                                </div>
                                <div class="form-group">
                                    <?= form_label('Duedate','duedate') ?>
                                    <div id="datetimepicker1" class="input-append">
                                      <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="duedate">
                                      <span class="add-on">
                                        <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                      </span>
                                    </div>
                                </div>
                                <button name="sendapproval" type="button" id="sendapproval" class="btn btn-success btn-block" data-loading-text="Loading...">Send Approval</button>
                                <button name="draftapproval" type="button" id="draftapproval" class="btn btn-danger btn-block" data-loading-text="Loading...">Save Draft</button>

                                <!-- <button name="sendapproval" type="button" id="sendapproval" class="btn noty-success btn-success btn-block">Send Approval</button>
                                <button name="draftapproval" type="button" id="draftapproval" class="btn noty-success btn-danger btn-block">Save Draft</button> -->

                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function(){
       //alert("OKE");
      $("#addIT").ajaxForm({
              type: 'POST',
              url: './it_services/add_process',
              data: $("#addIT").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    var $btn = $("button");
                    $btn.button('reset');
                    $("#contentAjax").load("./it_services");
                 }else{
                  alert(data.message);
                 }
                 var $btn = $("button");
                 $btn.button('reset');
              }
            });
        $("#sendapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("send");
          $("#addIT").submit();
        });
        
        $("#draftapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("draft");
          $("#addIT").submit();
        });

        $("#massal").change(function(){
            $("p").removeAttr("readonly");
        });
    });
</script>

<script type="text/javascript">
  $(function(){
    $("#doc_id").change(function(){
      var doc_id = $("#doc_id").val();
      $.ajax({
        type    : 'GET',
        url     : './it_services/get_list/'+doc_id,
        //data    : 'creator_id' + creator_id,
        success : function(data){
          $("#problem_id").html(data);
        }
      });
    });
	
	$("#doc_id").trigger("change");
  });
</script>