  <script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap-datetimepicker.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/custom-with-date.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.form.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/typeahead2.min.js'); ?>"></script>

  <div class="page-head">
  <h2 class="pull-left">Report IT Services</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Report IT Services</a>
      </div>      
  <div class="clearfix"></div>
</div>

  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget" border=0>
            <div class="widget-head">
              <div class="pull-left">Report IT Service</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                      <?= form_open('/it_services/report_download','id=reportIT') ?>
                          <fieldset>
                                  <div class="form-group">
                                        <label for="doc_name">Doc Category</label>
                                        <select name="doc_name" id="doc_name" class="form-control">
                                          <option value=""></option>
                                          <option value="PLT">PLT</option>
                                          <option value="PLK">PLK</option>
                                                                              
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Request Date From','request_date1') ?>
                                        <div id="datetimepicker3" class="input-append">
                                          <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="request_date1">
                                          <span class="add-on">
                                            <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                          </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?= form_label('Request Date To','request_date2') ?>
                                        <div id="datetimepicker4" class="input-append">
                                          <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="request_date2">
                                          <span class="add-on">
                                            <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                          </span>
                                        </div>
                                    </div>
                                    <a href="javascript: submitForm();" class="btn btn-success btn-block"><i class="fa fa-print"></i> Download</a>
                            </fieldset>
                        <?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
function submitForm(){
    $('#reportIT').submit();
}
</script>