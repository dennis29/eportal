  <script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap-datetimepicker.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/custom-with-date.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.form.min.js'); ?>"></script>

	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit IT Service</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		$attcreated=array(
                  				'name'=>'created_for',
                  				'value'=>$it_service->created_for,
                  				'class'=>'form-control'
                  			);

                  		$attdesc=array(
                  				'name'=>'description',
                  				'value'=>$it_service->description,
                  				'class'=>'form-control'
                  			);

                  		$attemployee=array(
                  				'name'=>'employee_id',
                  				'value'=>$it_service->employee_id,
                  				'class'=>'form-control',
                          'readonly'=>'readonly'
                  			);

                  		$attatta=array(
                  				'name'=>'attachment',
                  				'value'=>$it_service->attachment,
                  				'class'=>'form-control'
                  			);

                      $attdue=array(
                          'name'=>'duedate',
                          'value'=>$it_service->duedate,
                          'class'=>'form-control'
                        );

                      $attdraf=array(
                          'name'=>'draftapproval',
                          'id'=>'draftapproval',
                          'content'=>'Save Draft',
                          //'formaction'=>'/index.php/it_services/edit_draft',
                          'class'=>'btn btn-danger btn-block'
                        );

                  		$attsubmit=array(
                  				'name'=>'sendapproval',
                          'id'=>'sendapproval',
                  				'content'=>'Send Approval',
                          //'formaction'=>'/index.php/it_services/edit_send',
                  				'class'=>'btn btn-success btn-block'
                  			);

                      ?>
          					
                        <?= form_open_multipart('','id=editIT') ?>
                          <fieldset>
                                <input type="hidden" name="param" value="" class="form-control" type="text" id="param">
                                <div class="form-group">
                                    <?= form_label('Doc Category','doc_id') ?>
                                    <?= form_hidden('id',$it_service->id); ?>
                                    <select name="doc_id" id="doc_id" class="form-control">
                                    <?php
                                      foreach($docs as $doc){
                                        if($it_service->doc_id==$doc->id AND ($it_service->doc_id!=3 OR $it_service->doc_id!=4)){
                                          $selected='selected';
                                          echo '<option value='.$doc->id.' selected='.$selected.'>'.$doc->doc_name.'</option>';
                                        }else{
                                          echo '<option value='.$doc->id.'>'.$doc->doc_name.'</option>';
                                        }
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Problem Category','problem_id') ?>
                                    <select name="problem_id" id="problem_id" class="form-control">
                                    <?php
/*
                                      foreach($problem as $pro){
                                        if($it_service->problem_id==$pro->id){
                                          $selected='selected';
                                          echo '<option value='.$pro->id.' selected='.$selected.'>'.$pro->problem_name.'</option>';
                                        }else{
                                          echo '<option value='.$pro->id.'>'.$pro->problem_name.'</option>';
                                        }
                                      }
*/
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Created for','created_for') ?>
                                    <?= form_input($attcreated) ?>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Description','description') ?>
                                    <?= form_textarea($attdesc) ?>
                                    <?= form_hidden('created_by',$it_service->created_by) ?>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Employee','employee_id') ?>
                                    <?= form_hidden('employee_id',$it_service->employee_id) ?>
                                </div>
                      
                                <div class="form-group">
                                    <?= form_label('Priority','priority_id') ?>
                                    <select name="priority_id" class="form-control">
                                    <?php
                                      foreach($priority as $pri){
                                        if($it_service->priority_id==$pri->id){
                                          $selected='selected';
                                          echo '<option value='.$pri->id.' selected='.$selected.'>'.$pri->priority_name.'</option>';
                                        }else{
                                          echo '<option value='.$pri->id.'>'.$pri->priority_name.'</option>';
                                        }
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Attachment','attachment') ?>
                                    <input type="file" name="attachment" id="attachment">
                                    <input type="hidden" name="attachment2" value="<?php echo $it_service->attachment; ?>" id="attachment">

                                </div>
                                <div class="form-group">
                                  <label><?php echo $it_service->attachment; ?></label>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Duedate','duedate') ?>
                                    <div id="datetimepicker1" class="input-append">
                                      <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="duedate" value=<?= $it_service->duedate ?>>
                                      <span class="add-on">
                                        <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                      </span>
                                    </div>
                                </div>

	                            <?= form_button($attsubmit) ?>
                              <?= form_button($attdraf) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
      //alert('oke');
      $("#editIT").ajaxForm({
              type: 'POST',
              url: './it_services/edit_process',
              data: $("#editIT").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    var $btn = $("button");
                    $btn.button('reset');
                    $("#contentAjax").load("./it_services");
                 }else{
                  alert(data.message);
                 }
                 var $btn = $("button");
                 $btn.button('reset');
              }
            });
        $("#sendapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("send");
          $("#editIT").submit();
        });
        
        $("#draftapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("draft");
          $("#editIT").submit();
        });
    });
</script>

<script type="text/javascript">
  $(function(){
    $("#doc_id").change(function(){
      var doc_id = $("#doc_id").val();
      var problem_id = $("#problem_id").val();
      $.ajax({
        type    : 'GET',
        url     : './it_services/get_list/'+doc_id+'/'+<?php echo $it_service->problem_id; ?>,
        //data    : 'creator_id' + creator_id,
        success : function(data){
          $("#problem_id").html(data);
        }
      });
    });
	
	$("#doc_id").trigger("change");
  });
</script>