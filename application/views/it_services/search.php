  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget" border=0>
                <div class="widget-content">
                  <div class="padd">
                      <?= form_open('','id=searchIT') ?>
                          <fieldset>
                          		
                      
                                  <div class="form-group">
                                        <label for="doc_seq_id">Doc Number</label>
                                        <input id="doc_seq_id" name="doc_seq_id" class="form-control" type="text">
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Status','status') ?>
                                        <select name="status_app_id" id="status_app_id" class="form-control">
                                          <option value=>== Select Status ==</option>
                                          <?php
                                            foreach($status_app as $sta){
                                              echo '<option value='.$sta->id.'>'.$sta->status_app_name.'</option>';
                                            }
                                          ?>
                                                                              
                                        </select>
                                    </div>
      	                            <button name="btnsearch" type="button" id="btnsearch" onclick="search()" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search</button>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	  function search(){
              $("#contentAjax").load("./it_services/index?"+$("#searchIT").serialize());
              $("#tutupmodal2").trigger("click");
        }

	</script>

  <script type="text/javascript">
  $(function(){
    $("#doc_id").change(function(){
      var doc_id = $("#doc_id").val();
      $.ajax({
        type    : 'GET',
        url     : './it_services/get_list/'+doc_id,
        //data    : 'creator_id' + creator_id,
        success : function(data){
          $("#problem_id").html(data);
        }
      });
    });
  
  $("#doc_id").trigger("change");
  });
</script>