	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/custom4.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' }
	);    
    </script>
  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-content">
              <div class="padd invoice">
                <div class="row">
                  <div class="col-md-12">
                        <div class="alert alert-success">
                          Doc Type : <?= $detail->doc_name ?><br>
                          Problem Category : <?= $detail->problem_name ?><br>
                          Created For : <?= $detail->created_for ?><br>
                          Description : <?= $detail->description ?><br>
                          Employee : <?= $detail->employee_name ?><br>
                          Priority : <?= $detail->priority_name ?><br>
                          Attachment :
                          <?php
                            if(!empty($detail->attachment)){
                              echo '<a href="/eportal2/asset/file/'.$detail->attachment.'" target="_blank">View</a><br>';
                            }else{
                              echo '<style="color:red;">Empty<br>';
                            }
                          ?>
                          Request date : <?= $detail->request_date ?><br>
                          Due date : <?= $detail->duedate ?><br>
                          Status Approval : <?= $detail->status_app_name ?><br>
                          Doc Sequence : <?= $detail->doc_seq_id ?><br>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
           </div>
        </div>
      </div>
    </div>
  </div>