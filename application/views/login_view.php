<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Login - ePortalGOS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">


  <!-- Stylesheets -->
  <link href="<?php echo base_url('/asset/style/bootstrap.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/asset/style/font-awesome.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('/asset/style/style.css'); ?>" rel="stylesheet">

	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap.js'); ?>"></script>
  

  <link rel="shortcut icon" href="img/favicon/favicon.png">
</head>

<body>

<!-- Form area -->
<div class="admin-form">
  <div class="container">

    <div class="row">
      <div class="col-md-offset-2 col-md-8 col-md-offset-2">
        <!-- Widget starts -->
            <div class="widget">
              <!-- Widget head -->
              <div class="widget-head">
                <i class="icon-lock"></i> Login 
              </div>

              <div class="widget-content">
                <div class="padd">
                  <!-- Login form -->

                  	<?php

                  		$attformopen=array(
                  				'class'=>'form-horizontal'
                  			);
                  		$attlabelusername=array(
								'class'=>'control-label col-lg-3'
							);
						$attinputusername=array(
								'class'=>'form-control',
								'autocomplete'=>'off',
								'name'=>'username',
								'id'=>'username'
							);
						$attlabelpassword=array(
								'class'=>'control-label col-lg-3'
							);
						$attinputpassword=array(
								'class'=>'form-control',
								'autocomplete'=>'off',
								'name'=>'password',
								'id'=>'password'
							);
						$attbuttonsubmit=array(
									'class'=>'btn btn-danger btn-block',
									'type'=>'submit',
									'content'=>'Login',
									'name'=>'submit'
								);


						echo form_open('admin', $attformopen); 
					?>

					<div class="form-group">
					<?php
						echo form_label('Username:', 'username',$attlabelusername);
						echo '<div class="col-lg-9">';
						echo form_input($attinputusername);
						echo '</div>';
					?>	
					</div>
					<div class="form-group">
						<?php
							echo form_label('Password:', 'password',$attlabelpassword);
							echo '<div class="col-lg-9">';
							echo form_password($attinputpassword);
							echo '</div>';
						?>
					</div>

					<div class="form-group">
						<div class="col-lg-9 col-lg-offset-3">
							<?php
								echo form_button($attbuttonsubmit);
							?>
						</div>
					</div>
					<?= form_close(); ?>

					<div class="errors" style="color:red;"><?= validation_errors() ?></div>				  
				</div>
                </div>
            </div>  
      </div>
    </div>
  </div> 
</div>
</body>
</html>