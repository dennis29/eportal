<div class="page-head">
	<h2 class="pull-left">Manage Roles</h2>
    <div class="clearfix"></div>
    <!-- Breadcrumb -->
    <div class="bread-crumb">
      <a href="/"><i class="fa fa-home"></i> Home</a> 
      <!-- Divider -->
      <span class="divider">/</span> 
    <a href="#" class="bread-current">Roles</a>
  </div>
  <div class="clearfix"></div>
</div>
  <div class="matter">
    <div class="container">
  		<div class="row">
        <div class="col-md-5">
          <div class="widget">
            <div class="widget-head">
              <div class="pull-left">Daftar Semua Roles</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
              <div class="clearfix"></div>
            </div>
            <div class="widget-content">
            	<!-- <h2>Manage Roles</h2>
            	<a href="roles/add">Add Role</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
            			<th>Role</th>
                  <th>Description</th>
            			<th>Opsi</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
            		$no=$this->role_model->limit+1;
            			foreach($roles as $role){
            				echo '<tr>';
            				echo '<td>'.$no.'</td>';
            				//echo '<td>'.$role->id.'</td>';
            				echo '<td>'.$role->role_name.'</td>';
                    echo '<td>'.$role->role_desc.'</td>';
            				echo '<td><div class="btn-group">
                                          <button class="btn btn-xs btn-success" title="Edit" onclick="loading(\'./roles/edit?id='.$role->id.'\')"><i class="fa fa-pencil"></i></button>
                                          <button class="btn btn-xs btn-warning" title="Edit Access" onclick="$(\'#contentAjax\').load(\'./roles/edit_access?id='.$role->id.'\')"><i class="fa fa-unlock"></i></button>
                                          
                                        </div></td>';
            				echo '</tr>';
            				$no++;
            			}
            		?>
            		</tbody>
            	</table>
          	  <div class="widget-foot">
                <div class="table-info" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($roles) ?> dari <?= $total_rows ?> data</div>
                <?= $page ?>
                <div class="clearfix"></div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>