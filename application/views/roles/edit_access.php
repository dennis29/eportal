	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' }
	);    
    </script>

<div class="page-head">
	<h2 class="pull-left">Manage Menu</h2>
    <div class="clearfix"></div>
    <!-- Breadcrumb -->
    <div class="bread-crumb">
      <a href="/"><i class="fa fa-home"></i> Home</a> 
      <!-- Divider -->
      <span class="divider">/</span> 
    <a href="#" class="bread-current">Menu</a>
  </div>
  <div class="clearfix"></div>
</div>
  <div class="matter">
    <div class="container">
  		<div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-head">
              <div class="pull-left">Daftar Semua Menu</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
              <div class="clearfix"></div>
            </div>
            <div class="widget-content">
            	<!-- <h2>Manage Roles</h2>
            	<a href="roles/add">Add Role</a> -->
            	<?php
            	echo form_open('','id=editAccess');
				echo form_hidden('role_id',$role_only['0']['id']);
				//echo form_label('Name  : ','name');
				//echo form_hidden('name', $role_only['0']['name'], 'readonly=readonly');

				?>

            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
						<th>ID</th>
						<th>Label</th>
						<th>Link</th>
						<th>Parent</th>
						<th>Role Menu Access</th>
					</tr>
            		</thead>
            		<tbody>
            		<?php
						foreach($menus as $menu)
						{

						$create=0;
						$read=0;
						$update=0;
						$delete=0;

						if(isset($arr[$menu->id])){
							$create=$arr[$menu->id]['create'];
							$read=$arr[$menu->id]['read'];
							$update=$arr[$menu->id]['update'];
							$delete=$arr[$menu->id]['delete'];
						}

						$array1 = array('ck', $menu->id, 'create');
						$cName = implode("_", $array1);

						$array2 = array('ck', $menu->id, 'read');
						$rName = implode("_", $array2);

						$array3 = array('ck', $menu->id, 'update');
						$uName = implode("_", $array3);

						$array4 = array('ck', $menu->id, 'delete');
						$dName = implode("_", $array4);

						//echo $cName;

						$data1 = $cName;
						$data2 = $rName;
						$data3 = $uName;
						$data4 = $dName;

						list($cc, $mn_id1, $cb1) = explode("_", $data1);
						list($cc, $mn_id2, $cb2) = explode("_", $data2);
						list($cc, $mn_id3, $cb3) = explode("_", $data3);
						list($cc, $mn_id4, $cb4) = explode("_", $data4);

						//echo $mn_id1.'<br/>';

						/*echo $mn_id1.'<br/>';
						echo $cb1.'<br/><br/>';

						echo $mn_id2.'<br/>';
						echo $cb2.'<br/><br/>';

						echo $mn_id3.'<br/>';
						echo $cb3.'<br/><br/>';

						echo $mn_id4.'<br/>';
						echo $cb4.'<br/><br/>';*/

						/*echo form_input('menu_id',$menu->id);
		            	echo form_checkbox('create','1');
		            	echo form_checkbox('read','1');
		            	echo form_checkbox('update','1');
		            	echo form_checkbox('delete','1');
*/
						echo '<tr>';
						echo '<td>'.$menu->id.'</td>';
						echo '<td>'.$menu->label.'</td>';
						echo '<td>'.$menu->link.'</td>';
						echo '<td>'.$menu->parent_name.'</td>';
						//echo '<td>'.form_input('menu_id').'</td>';
						echo '<td><label class="checkbox-inline">'.form_checkbox($cName,'1', $create).' Create</label>
								  <label class="checkbox-inline">'.form_checkbox($rName,'1', $read).' Read</label>
								  <label class="checkbox-inline">'.form_checkbox($uName,'1', $update).' Update</label>
								  <label class="checkbox-inline">'.form_checkbox($dName,'1', $delete).' Delete</label></td>';
								   /*.form_label('Create ','create').' '
								   .form_checkbox($rName,'1', $read).' '
								   .form_label('Read ','read').' '
								   .form_checkbox($uName,'1', $update).' '
								   .form_label('Update ','update').' '
								   .form_checkbox($dName,'1', $delete).' '
								   .form_label('Delete ','delete').'</td>';*/
						echo '</tr>';
					}
					echo '</table>';

					echo '<br/>';

					$attsubmit=array(
							'name'=>'btnaccess',
							'id'=>'btnaccess',
							'content'=>'Edit Role Access',
							'class'=>'btn btn-success',
						);
					?>
            		</tbody>
            	</table>
          	  <div class="widget-foot">
              	<br><br>
              	<div class="clearfix"></div> 
              </div>
              <div class="padd">
              <?php
              	echo form_button($attsubmit);
				echo form_close();

				/*echo form_open('roles/edit_access_proccess');
				echo form_input('role_id',$role_only['0']['id']);
            	echo form_input('menu_id');
            	echo form_input('create');
            	echo form_input('read');
            	echo form_input('update');
            	echo form_input('delete');
            	echo form_button($attsubmit);
				echo form_close();*/
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

	<script type="text/javascript">
	  $(function(){
	    $("#btnaccess").click(function(){
	      $.ajax({
	        type:'POST',
	        url:'./roles/edit_access_proccess',
	        data:$("#editAccess").serialize(),
	        success:function(data){
	          data=jQuery.parseJSON(data);
	          if(data.success){
	            alert('Data Berhasil Tersimpan');
	            $("#contentAjax").load('./roles');
	          }
	        }
	      });
	    });
	  });
	</script>