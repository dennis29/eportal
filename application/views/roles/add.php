<div class="page-head">
  <h2 class="pull-left">Add Roles</h2>
    <div class="clearfix"></div>
    <!-- Breadcrumb -->
    <div class="bread-crumb">
      <a href="/"><i class="fa fa-home"></i> Home</a> 
      <!-- Divider -->
      <span class="divider">/</span> 
    <a href="#" class="bread-current">Add Roles</a>
  </div>
  <div class="clearfix"></div>
</div>

  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add Role</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		$attname=array(
                  				'name'=>'role_name',
                  				//'value'=>$user->firstname,
                          'placeholder'=>'input role name',
                  				'class'=>'form-control'
                  			);

                      $attdesc=array(
                          'name'=>'role_desc',
                          'placeholder'=>'input role description',
                          'class'=>'form-control'
                        );

                  		$attsubmit=array(
                  				'name'=>'btnsubmit',
                          'id'=>'btnsubmit',
                  				'content'=>'Add Role',
                  				//'type'=>'submit',
                  				'class'=>'btn btn-primary btn-block'
                  			);
            					?>

                      <?= form_open('','id=addROle') ?>
                          <fieldset>
                                <div class="form-group">
                                    <?= form_label('Role','role_name') ?>
                                    <?= form_input($attname) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('description','role_desc') ?>
                                    <?= form_textarea($attdesc) ?>
                                </div>
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
      $("#btnsubmit").click(function(){
        $.ajax({
          type:'POST',
          url:'./roles/add_proccess',
          data:$("#addROle").serialize(),
          success:function(data){
            data=jQuery.parseJSON(data);
            if(data.success){
              alert('Data Berhasil Tersimpan');
              $("#contentAjax").load('./roles');
            }else{
              alert(data.message);
            }
          }
        });
      });
    });
  </script>