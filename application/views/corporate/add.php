	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>


    <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add corporate File</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                       
                       <?= form_open_multipart('','id=addCorporate') ?>
                          <fieldset>
                                <div class="form-group">
                                    <label for="corporate_tab_id">Tab Category</label>
                                    <select name="corporate_tab_id" id="corporate_tab_id" class="form-control">
                                    <?php
                                      foreach($corporate_tab as $hct){
                                        echo '<option value='.$hct->id.'>'.$hct->tab_name.'</option>';
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="no_policy">No Policy</label>                                    
                                    <input name="no_policy" value="" class="form-control" type="text">                               
                                </div>
                                <div class="form-group">
                                    <label for="title">title</label>                                    
                                    <input name="title" value="" class="form-control" type="text">                               
                                </div>
                                <div class="form-group">
                                    <label for="desc_corporate">Description</label>                             
                                    <textarea name="desc_corporate" cols="40" rows="5" class="form-control"></textarea>                      
                                </div>
                                <div class="form-group">
                                    <label hidden="hidden" for="status_revisi" id="status_revisi1">Status Revisi</label>                                    
                                    <input name="status_revisi" id="status_revisi" value="" class="form-control" type="hidden">                               
                                </div>
                                <div class="form-group">
                                    <label for="sop_ref" id="sop_ref1" hidden="hidden">SOP Ref</label>
                                    <select name="sop_ref" id="sop_ref" style="display:none;" class="form-control">
                                    <?php
                                      echo '<option value=0>== Select Document No ==</option>';
                                      foreach($corporate as $cor){
                                          echo '<option value='.$cor->no_policy.'>'.$cor->no_policy.'</option>';  
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="attachment">Attachment</label>
                                    <input type="file" name="attachment" id="attachment">
                                </div>
                              <button name="send" type="button" id="send" class="btn btn-primary btn-block">Send</button>
                            </fieldset>

                        <?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
  	//alert('oke');
      $("#addCorporate").ajaxForm({
              type: 'POST',
              url: './corporate/add_process',
              data: $("#addCorporate").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    $("#contentAjax").load("./corporate/corporate/policy");
                 }else{
                  alert(data.message);
                 }
              }
            });
        $("#send").click(function(){
          $("#addCorporate").submit();
        });
    });

  $(function(){
      $("#corporate_tab_id").change(function(){
        if($(this).val() == '2'){
        	$("#status_revisi").attr("type","text");
          	$("#status_revisi1").removeAttr("hidden","hidden");
          	$("#status_revisi").removeAttr("disabled","disabled");

          	$("#sop_ref").removeAttr("style","display:none");
          	$("#sop_ref1").removeAttr("hidden","hidden");
          	$("#sop_ref").removeAttr("disabled","disabled");
        }else{
        	$("#status_revisi").attr("type","hidden");
          	$("#status_revisi1").attr("hidden","hidden");
          	$("#status_revisi").attr("disabled","disabled");

          	$("#sop_ref").attr("style","display:none");
          	$("#sop_ref1").attr("hidden","hidden");
          	$("#sop_ref").attr("disabled","disabled");
        }
      });
    });

</script>