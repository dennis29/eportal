	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>


    <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit corporate File</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                       
                       <?= form_open_multipart('','id=editCorporate') ?>
                          <fieldset>
                                <div class="form-group">
                                    <input name="id" value="<?= $corporate->id ?>" class="form-control" type="hidden">
                                    <label for="corporate_tab_id">Tab Category</label>
                                    <select name="corporate_tab_id" class="form-control">
                                    <?php
                                      foreach($corporate_tab as $hct){
                                        if($hct->id==$corporate->corporate_tab_id){
                                          echo '<option value='.$hct->id.' selected=selected>'.$hct->tab_name.'</option>';  
                                        }else{
                                          echo '<option value='.$hct->id.'>'.$hct->tab_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="no_policy">No Policy</label>                                    
                                    <input name="no_policy" value="<?= $corporate->no_policy ?>" class="form-control" type="text">                               
                                </div>
                                <div class="form-group">
                                    <label for="title">title</label>                                    
                                    <input name="title" value="<?= $corporate->title ?>" class="form-control" type="text">                               
                                </div>
                                <div class="form-group">
                                    <label for="desc_corporate">Description</label>                             
                                    <textarea name="desc_corporate" cols="40" rows="5" class="form-control"><?= $corporate->desc_corporate ?></textarea>                      
                                </div>
                                <div class="form-group">
                                    <label <?php if($corporate->corporate_tab_id==2){echo ' ';}else{echo 'hidden="hidden"';} ?> for="status_revisi" id="status_revisi1">Status Revisi</label>                                    
                                    <input name="status_revisi" id="status_revisi" value="<?php echo $corporate->status_revisi ?>" class="form-control" <?php if($corporate->corporate_tab_id==2){echo 'type="input"';}else{echo 'type="hidden"';} ?>>                               
                                </div>
                                <div class="form-group">
                                    <label <?php if($corporate->corporate_tab_id==2){echo ' ';}else{echo 'hidden="hidden"';} ?>for="sop_ref">SOP Ref</label>
                                    <select name="sop_ref" class="form-control" <?php if($corporate->corporate_tab_id==2){echo ' ';}else{echo 'style="display:none;"';} ?>>
                                    <?php
                                      echo '<option value=0>== Select Document No ==</option>';
                                      foreach($corporate_all as $cor){
                                        if($corporate->sop_ref==$cor->no_policy){
                                          echo '<option value='.$cor->no_policy.' selected=selected>'.$cor->no_policy.'</option>'; 
                                          //echo 'ada'; 
                                        }else{
                                          echo '<option value='.$cor->no_policy.'>'.$cor->no_policy.'</option>';
                                          //echo 'gada';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="attachment">Attachment</label>
                                    <input type="file" name="attachment" id="attachment">
                                    <input type="hidden" name="attachment2" id="attachment2" value="<?php echo $corporate->attachment; ?>">
                                </div>
                                <div class="form-group">
                                  <label><?php echo $corporate->attachment; ?></label>
                                </div>
                              <button name="send" type="button" id="send" class="btn btn-primary btn-block">Send</button>
                            </fieldset>

                        <?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
      $("#editCorporate").ajaxForm({
              type: 'POST',
              url: './corporate/edit_process',
              data: $("#editCorporate").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    $("#contentAjax").load("./corporate/policy");
                 }else{
                  alert(data.message);
                 }
              }
            });
        $("#send").click(function(){
          $("#editCorporate").submit();
        });
    });

</script>