	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>


    <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit HCM File</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                       
                       <?= form_open_multipart('','id=editHCM') ?>
                          <fieldset>
                                <input id="diselect" type="hidden" value="<?= $hcm->for_direct ?>">
                                <div class="form-group">
                                    <input name="id" value="<?= $hcm->id ?>" class="form-control" type="hidden">
                                    <label for="hcm_tab_id">Tab Category</label>
                                    <select name="hcm_tab_id" class="form-control">
                                    <?php
                                      foreach($hcm_tab as $hct){
                                        if($hct->id==$hcm->hcm_tab_id){
                                          echo '<option value='.$hct->id.' selected=selected>'.$hct->tab_name.'</option>';  
                                        }else{
                                          echo '<option value='.$hct->id.'>'.$hct->tab_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="no_policy">No Policy</label>                                    
                                    <input name="no_policy" value="<?= $hcm->no_policy ?>" class="form-control" type="text">                               
                                </div>
                                <div class="form-group">
                                    <label for="title">title</label>                                    
                                    <input name="title" value="<?= $hcm->title ?>" class="form-control" type="text">                               
                                </div>
                                <div class="form-group">
                                    <label for="desc_hcm">Description</label>                             
                                    <textarea name="desc_hcm" cols="40" rows="5" class="form-control"><?= $hcm->desc_hcm ?></textarea>                      
                                </div>
                                <div class="form-group">
                                    <label for="attachment">Attachment</label>
                                    <input type="file" name="attachment" id="attachment">
                                    <input type="hidden" name="attachment2" id="attachment2" value="<?php echo $hcm->attachment; ?>">
                                </div>
                                <div class="form-group">
                                  <label><?php echo $hcm->attachment; ?></label>
                                </div>
                              <button name="send" type="button" id="send" class="btn btn-primary btn-block">Send</button>
                            </fieldset>

                        <?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
      var funcnya = $("#diselect").val();
      //alert(funcnya);
      $("#editHCM").ajaxForm({
              type: 'POST',
              url: './hcm/edit_process',
              data: $("#editHCM").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    $("#contentAjax").load("./hcm/"+funcnya);
                 }else{
                  alert(data.message);
                 }
              }
            });
        $("#send").click(function(){
          $("#editHCM").submit();
        });
    });

</script>