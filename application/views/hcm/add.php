	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>


    <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add HCM File</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                       
                       <?= form_open_multipart('','id=addHCM') ?>
                          <fieldset>
                                <div class="form-group">
                                    <label for="hcm_tab_id">Tab Category</label>
                                    <select name="hcm_tab_id" class="form-control">
                                    <?php
                                      foreach($hcm_tab as $hct){
                                        echo '<option value='.$hct->id.'>'.$hct->tab_name.'</option>';
                                      }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="no_policy">No Policy</label>                                    
                                    <input name="no_policy" value="" class="form-control" type="text">                               
                                </div>
                                <div class="form-group">
                                    <label for="title">title</label>                                    
                                    <input name="title" value="" class="form-control" type="text">                               
                                </div>
                                <div class="form-group">
                                    <label for="desc_hcm">Description</label>                             
                                    <textarea name="desc_hcm" cols="40" rows="5" class="form-control"></textarea>                      
                                </div>
                                <div class="form-group">
                                    <label for="attachment">Attachment</label>
                                    <input type="file" name="attachment" id="attachment">
                                </div>
                              <button name="send" type="button" id="send" class="btn btn-primary btn-block">Send</button>
                            </fieldset>

                        <?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
      $("#addHCM").ajaxForm({
              type: 'POST',
              url: './hcm/add_process',
              data: $("#addHCM").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    $("#contentAjax").load("./hcm/struktur_organisasi");
                 }else{
                  alert(data.message);
                 }
              }
            });
        $("#send").click(function(){
          $("#addHCM").submit();
        });
    });

</script>