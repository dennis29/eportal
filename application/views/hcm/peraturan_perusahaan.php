	<script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
	basket.require(
		{ url: '<?php echo base_url('/asset/js/jquery.form.min.js'); ?>' },
		{ url: '<?php echo base_url('/asset/js/custom2.min.js'); ?>' }
	);    
    </script>

<div class="page-head">
  <h2 class="pull-left">Peraturan Perusahaan</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="/index.php/main"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Peraturan Perusahaan</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Peraturan Perusahaan List</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
                  		<th>No Policy</th>
            			<th>Title</th>
            			<th>desc_hcm</th>
            			<th>File</th>
            			<th>created</th>
		                <th>Opsi</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
                  		//$no=$this->sod_model->limit;
            			$no=0;
            			$no=$no+1;
            			echo '<tr border color=red>';
                  //echo $_SESSION['username'];

                  //foreach($users as $usr){
                    foreach ($hcm as $hc){
                    	if($hc->hcm_tab_id==2){
                    		echo '<td>'.$no.'</td>';
	                        //echo '<td>'.$sod2->doc_id.'</td>';
	                        //echo '<td>'.$hc->hc_tab_name.'</td>';
	                        echo '<td>'.$hc->no_policy.'</td>';
	                        echo '<td>'.$hc->title.'</td>';
	                        echo '<td>'.$hc->desc_hcm.'</td>';
	                        if(!empty($hc->attachment)){
                                echo '<td><center><a href="/eportal2/asset/file/'.$hc->attachment.'" target="_blank"><i class="fa fa-file-text"></i></a></td>';
                              }else{
                                echo '<td style="color:red;"><center><i class="fa fa-file-text"></i></td>';
                              }
	                        echo '<td>'.$hc->created.'</td>';
							echo '<td><center><div class="btn-group">';
	                        if($edit==1)
							{
	                         	echo '<button class="btn btn-xs btn-success" title="Edit" onclick="loading(\'./hcm/edit?id='.$hc->id.'\')"><i class="fa fa-pencil"></i></button>';
							}
							
							if($delete==1)
							{
                            	echo '<button id="btndelete" class="btn btn-xs btn-danger" title="Delete" onclick="if(confirm(&quot;anda yakin mau menghapus?&quot;)){delete_app('.$hc->id.')}return false;"><i class="fa fa-trash-o"></i></button>
									  </div></center>';
							}
						    echo '</td></tr>';
	                        
	                        $no++;
                    	}
                    }
                  //}
            		?>
            		</tbody>
            	</table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
        function delete_app(id){
          $.ajax({
            type    : 'GET',
            url     : './hcm/delete?id='+id,
            //data    : table,
            success : function(data){
              alert("Data Berhasil Dihapus");
              $("#contentAjax").load("./hcm/peraturan_perusahaan");
            }
          });
        }
</script>