<div class="page-head">
  <h2 class="pull-left">Add User Role</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Add User Role</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
    <div class="container">
      	<div class="row">
        	<div class="col-md-5">
          		<div class="widget">   
            		<div class="widget-head">
              			<div class="pull-left">Add User Role</div>
	                  	<div class="widget-icons pull-right">
		                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
		                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
		                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
	                  	</div>
            			<div class="clearfix"></div>
            		</div>
					<div class="widget-content">
			        	<div class="padd">
						<form onsubmit="return false;" id="addUserRole">	        					
							<fieldset>
								<div class="form-group">
									<select name="user_id" class="form-control">
										<?php
										print_r($users);
											foreach($users as $user){
												echo '<option value='.$user->id.'>'.$user->username.'</option>';
											}
										?>
									</select>
								</div>
								<div class="form-group">
									<select name="role_id" class="form-control">
										<?php
											foreach ($roles as $role) {
												echo '<option value='.$role->id.'>'.$role->role_name.'</option>';
											}
										?>
									</select>
								</div>
								<button name="btnadd" id="btnadd" type="button" class="btn btn-primary">Add User Role</button>							
							</fieldset>
						</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  $(function(){
    $('#btnadd').click(function(){
      $.ajax({
        type : 'POST',
        url : './user_role/add_proccess',
        data : $('#addUserRole').serialize(),
        success : function(data){
          data = jQuery.parseJSON(data);
          if(data.success){
            alert('Data Berhasil disimpan');
            $("#contentAjax").load('./user_role');
          }
        }
      });
    });
  });
</script>