<div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget" border=0>
                <div class="widget-content">
                  <div class="padd">
                      <?= form_open('','id=searchUserRole') ?>
                          <fieldset>
                                <div class="form-group">
                                  <label for="username">Username</label>
                                  <input type="text" name="username" class="form-control" list="username"> 
                                  <datalist id="username">
                                    <option value=></option>
                                    <?php
                                      foreach($users as $user){
                                        echo '<option value='.$user->username.'>'.$user->username.'</option>';
                                      }
                                    ?>
                                  </datalist>
                                </div>
                                <div class="form-group">
                                  <label for="employee_id">Full Name</label>
                                  <select name="employee_id" class="form-control">
                                    <option value=></option>
                                    <?php
                                      foreach ($employee as $emp) {
                                        echo '<option value='.$emp->id.'>'.$emp->employee_name.'</option>';
                                      }
                                    ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="role_id">Role</label>
                                  <select name="role_id" class="form-control">
                                    <option value=></option>
                                    <?php
                                      foreach ($roles as $role) {
                                        echo '<option value='.$role->id.'>'.$role->role_name.'</option>';
                                      }
                                    ?>
                                  </select>
                                </div>
                                <button name="btnsearch" type="button" id="btnsearch" onclick="search()" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search</button>
                            </fieldset>
                        <?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function search(){
              $("#contentAjax").load("./user_role/index?"+$("#searchUserRole").serialize());
              $("#tutupmodal2").trigger("click");
        }

  </script>