<script type="text/javascript" src="<?php echo base_url('/asset/js/custom2.min.js'); ?>"></script>
<div class="page-head">
  <h2 class="pull-left">Manage User Role</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Manage User Role</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-7">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Daftar Semua User</div>
              <div class="widget-icons pull-right">
                <?= '<a href="#modalUserRole" data-toggle="modal" onclick="$(\'#contentSearchUserRole\').load(\'./user_role/search\')" title="Seacrh"><i class="fa fa-search"></i></a>'; ?>
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
      						<th>User</th>
                  <th>Full Name</th>
      						<th>Role</th>
      						<th>Opsi</th>
      					</tr>
            		</thead>
            		<tbody>
            		<?php

            			$no=$this->user_role_model->limit+1;
            			echo '<tr>';
            			foreach($user_role as $ur){
      							echo '<tr>';
      							echo '<td>'.$no.'</td>';
      							echo '<td>'.$ur->username.'</td>';
                    echo '<td>'.$ur->employee_name.'</td>';
      							echo '<td>'.$ur->role_name.'</td>';
      							echo '<td><div class="btn-group">
                              <button class="btn btn-xs btn-success" title="Edit" onclick="loading(\'./user_role/edit?id='.$ur->id.'\')"><i class="fa fa-pencil"></i></button>
                              <button class="btn btn-xs btn-danger" title="Delete" onclick="if(confirm(&quot;anda yakin mau menghapus?&quot;)){delete_app('.$ur->id.')}return false;"><i class="fa fa-trash-o"></i></button>
                            </div>
                          </td>';
      							echo '</tr>';
	            			$no++;
            			}
            		?>
            		</tbody>
            	</table>
            <div class="widget-foot">
              <div class="table-info" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($user_role) ?> dari <?= $total_rows ?> data</div>
                <?= $page ?>
                <div class="clearfix"></div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
        function delete_app(id){
          $.ajax({
            type    : 'GET',
            url     : './user_role/delete?id='+id,
            //data    : table,
            success : function(data){
              alert("Data Berhasil Dihapus");
              $("#contentAjax").load("./user_role");
            }
          });
        }
</script>

<div id="modalUserRole" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalUserRoleLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Search User Role</h4>
              </div>
              <div class="modal-body">
                <div id="contentSearchUserRole"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>