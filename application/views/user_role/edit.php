<div class="page-head">
  <h2 class="pull-left">Edit User Role</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Edit User Role</a>
      </div>      
  <div class="clearfix"></div>
</div>

    <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit User</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php

                  		$attsubmit=array(
                  				'name'=>'btnedit',
                  				'id'=>'btnedit',
                  				'content'=>'Update User Role',
                  				//'type'=>'submit',
                  				'class'=>'btn btn-primary btn-block'
                  			);
					           ?>

                      <?= form_open('','id=editUserRole') ?>
                        <fieldset>
                               <?= form_hidden('id',$user->id) ?>
                          <div class="form-group">
                           	<?php echo form_label('User','user_id'); ?>
                            <select name="user_id" class="form-control">
            									<?php
            										foreach($users as $usr){
            											if($user->user_id==$usr->id){
            												$selected='selected';
            												echo '<option value='.$usr->id.' selected='.$selected.'>'.$usr->username.'</option>';
            											}else{
            												echo '<option value='.$usr->id.'>'.$usr->username.'</option>';
            											}
            										}
            									?>
        									  </select>
      								    </div>
      									<?php
      										echo form_label('Role','role_id');
      									?>
                      <div class="form-group">
      									<select name="role_id" class="form-control">
      									<?php
      										foreach($roles as $rol){
      											if($user->role_id==$rol->id){
      												$selected='selected';
      												echo '<option value='.$rol->id.' selected='.$selected.'>'.$rol->role_name.'</option>';
      											}else{
      												echo '<option value='.$rol->id.'>'.$rol->role_name.'</option>';
      											}
      										}
      									?>
      									</select>
                      </div>
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
    $('#btnedit').click(function(){
      $.ajax({
        type : 'POST',
        url : './user_role/edit_proccess',
        data : $('#editUserRole').serialize(),
        success : function(data){
          data = jQuery.parseJSON(data);
          if(data.success){
            alert('Data Berhasil disimpan');
            $("#contentAjax").load('./user_role');
          }
        }
      });
    });
  });
</script>