<!DOCTYPE html>
<html>
<head>
	<title>Manage Role Menu</title>
</head>
<body>
	<h2>Manage Role Menu</h2>
	<a href="role_menu/add">Add Menu</a>
	<table border="1">
		<tr>
			<th>ID</th>
			<th>Role</th>
			<th>Menu</th>
			<th>Opsi</th>
		</tr>

		<?php
			foreach($role_menu as $rm){
				echo '<tr>';
				echo '<td>'.$rm->id.'</td>';
				echo '<td>'.$rm->name.'</td>';
				echo '<td>'.$rm->label.'</td>';
				echo '<td><a href=./role_menu/edit?id='.$rm->id.'>Edit</a> || 
						  <a href=./role_menu/delete?id='.$rm->id.' onclick="if (confirm(&quot;anda yakin mau menghapus?&quot;)){ 
                                      return true; 
                                    } return false;" title=delete>Delete</a></td>';
				echo '</tr>';
			}
		?>
	</table>
</body>
</html>