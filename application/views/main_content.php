<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>ePortal-GOS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">


<!--
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />      <!--<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
-->
  <!-- Stylesheets -->
  <link href="/asset/style/bootstrap.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="/asset/style/font-awesome.css"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="/asset/style/jquery-ui-1.9.2.custom.min.css"> 
  <!-- Calendar -->
  <link rel="stylesheet" href="/asset/style/fullcalendar.css">
  <!-- prettyPhoto -->
  <link rel="stylesheet" href="/asset/style/prettyPhoto.css">  
  <!-- Star rating -->
  <link rel="stylesheet" href="/asset/style/rateit.css">
  <!-- Date picker -->
  <link rel="stylesheet" href="/asset/style/bootstrap-datetimepicker.min.css">
  <!-- CLEditor -->
  <link rel="stylesheet" href="/asset/style/jquery.cleditor.css"> 
  <!-- Uniform -->
  <link rel="stylesheet" href="/asset/style/uniform.default.html"> 
  <!-- Uniform -->
  <link rel="stylesheet" href="/asset/style/daterangepicker-bs3.css" />
  <!-- Bootstrap toggle -->
  <link rel="stylesheet" href="/asset/style/bootstrap-switch.css">
  <!-- Main stylesheet -->
  <link href="/asset/style/style.css" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="/asset/style/widgets.css" rel="stylesheet">   
    <!-- Gritter Notifications stylesheet -->
  <link href="/asset/style/jquery.gritter.css" rel="stylesheet">
  
  <!-- HTML5 Support for IE -->
    <!--[if lt IE 9]>
  <script src="/asset/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="/asset/img/favicon/favicon.ico">
</head>

<body>
<header>
<div class="navbar navbar-fixed-top bs-docs-nav" role="banner">
  
    <div class="container">
      <!-- Menu button for smallar screens -->
      <div class="navbar-header">
		  <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse"><span>Menu</span></button>
      	  <a href="#" class="pull-left menubutton hidden-xs"><i class="fa fa-bars"></i></a>
		  <!-- Site name for smallar screens -->
		  <a href="#" onclick="$('#contentAjax').load('/index.php/dashboard')" class="navbar-brand">e-portal<span class="bold">GOS</span></a>
		</div>

      <!-- Navigation starts -->
      <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">         
        
        <!-- Links -->
        <ul class="nav navbar-nav pull-right">
          <li class="dropdown pull-right user-data">            
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <img src="/asset/img/user1.png"> <?= $_SESSION['username'] ?> <b class="caret"></b>              
            </a>
            
            <!-- Dropdown menu -->
            <ul class="dropdown-menu">
              <li><a href="#" onclick="$('#contentAjax').load('/index.php/employee/profile')"><i class="fa fa-user"></i> Profile</a></li>
              <li><a href="#" onclick="$('#contentAjax').load('/index.php/users/change_password')"><i class="fa fa-cogs"></i> Change Password</a></li>
              <li><a href="/index.php/admin/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
          </li>
          <!-- Upload to server link. Class "dropdown-big" creates big dropdown -->
          <li class="dropdown dropdown-big leftonmobile">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cloud-upload"></i></a>
            <!-- Dropdown -->
            <ul class="dropdown-menu">
              <li>
                <!-- Progress bar -->
                <p>Photo Upload in Progress</p>
                <!-- Bootstrap progress bar -->
                <div class="progress progress-striped active">
					<div class="progress-bar progress-bar-info"  role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
						<span class="sr-only">40% Complete</span>
					</div>
			    </div>

                <hr />

                <!-- Progress bar -->
                <p>Video Upload in Progress</p>
                <!-- Bootstrap progress bar -->
                <div class="progress progress-striped active">
					<div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
						<span class="sr-only">80% Complete</span>
					</div>
			    </div> 

                <hr />             

                <!-- Dropdown menu footer -->
                <div class="drop-foot">
                  <a href="#">View All</a>
                </div>

              </li>
            </ul>
          </li>

          <!-- Sync to server link -->
          <li class="dropdown dropdown-big leftonmobile">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-refresh"></i></a>
            <!-- Dropdown -->
            <ul class="dropdown-menu">
              <li>
                <!-- Using "fa fa-spin" class to rotate icon. -->
                <p><span class="label label-info"><i class="fa fa-cloud"></i></span> Syncing Photos to Server</p>
                <hr />
                <p><span class="label label-warning"><i class="fa fa-cloud"></i></span> Syncing Bookmarks Lists to Cloud</p>

                <hr />

                <!-- Dropdown menu footer -->
                <div class="drop-foot">
                  <a href="#">View All</a>
                </div>

              </li>
            </ul>
          </li>
          <li class="dropdown dropdown-big leftonmobile">
              <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                <i class="fa fa-comments"></i><span class="label label-info">6</span> 
              </a>

                <ul class="dropdown-menu">
                  <li class="dropdown-header padless">
                    <!-- Heading - h5 -->
                    <h5><i class="fa fa-comments"></i> Chats</h5>
                    <!-- Use hr tag to add border -->                   
                  </li>
                  <li>
                    <hr />
                    <!-- List item heading h6 -->
                    <h6><a href="#">Hi :)</a> <span class="label label-warning pull-right">10:42</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">How are you?</a> <span class="label label-warning pull-right">20:42</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">What are you doing?</a> <span class="label label-warning pull-right">14:42</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>                  
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li>
            
            <!-- Message button with number of latest messages count-->
            <li class="dropdown dropdown-big messages-dd leftonmobile">
              <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i> <span class="label label-primary">3</span> 
              </a>

                <ul class="dropdown-menu">
                  <li class="dropdown-header padless">
                    <!-- Heading - h5 -->
                    <h5><i class="fa fa-envelope-alt"></i> Messages</h5>
                    <!-- Use hr tag to add border -->
                    
                  </li>
                  <li>
                    <hr /><!-- List item heading h6 -->
                    <h6><a href="#">Hello how are you?</a></h6>
                    <!-- List item para -->
                    <p>Quisque eu consectetur erat eget  semper...</p>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">Today is wonderful?</a></h6>
                    <p>Quisque eu consectetur erat eget  semper...</p>
                    <hr />
                  </li>
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li>

            <!-- Members button with number of latest members count -->
            <li class="dropdown dropdown-big">
              <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                <i class="fa fa-user"></i> <span class="label label-success">7</span> 
              </a>

                <ul class="dropdown-menu">
                  <li class="dropdown-header padless">
                    <!-- Heading - h5 -->
                    <h5><i class="fa fa-user"></i> Users</h5>
                    <!-- Use hr tag to add border -->                    
                  </li>
                  <li>
                    <hr />
                    <!-- List item heading h6-->
                    <h6><a href="#">John Doe</a> <span class="label label-warning pull-right">Free</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">Iron Man</a> <span class="label label-important pull-right">Premium</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>
                  <li>
                    <h6><a href="#">Salamander</a> <span class="label label-warning pull-right">Free</span></h6>
                    <div class="clearfix"></div>
                    <hr />
                  </li>                  
                  <li>
                    <div class="drop-foot">
                      <a href="#">View All</a>
                    </div>
                  </li>                                    
                </ul>
            </li> 
        </ul>
      </nav>

    </div>
  </div>
</header>
<!-- Main content starts -->

<div class="content">

  	<!-- Sidebar -->
    <div class="sidebar">
        <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
        <!-- Search form -->
        <form class="navbar-form" role="search">
    			<div class="form-group">
    				<input type="text" class="form-control" placeholder="Search">
            <button class="btn search-button" type="submit"><i class="fa fa-search"></i></button>
    			</div>
    		</form>
        <!--- Sidebar navigation -->
        <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->

        <ul id="nav">
        	<?php
        		echo $menus_hierarki;
        	?>
        </ul>


    </div>
    <!-- Sidebar ends -->

  	<!-- Main bar -->
  	<div class="mainbar">
        <div id="contentAjax">
        </div>
    </div>

   <!-- Mainbar ends -->
   <div class="clearfix"></div>

</div>
<!-- Content ends -->

<!-- Footer starts -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <!-- Copyright info -->
            <p class="copy">Copyright &copy; 2013 | <a href="#">Your Site</a> </p>
      </div>
    </div>
  </div>
</footer> 	

<!-- Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 

</body>
</html>

<!-- JS -->
<!--
  <script src="/asset/js/jquery.js"></script>
-->
  <script src="/asset/js/jquery.min.js"></script>
  <script src="/asset/js/jquery.form.min.js"></script>
  <script src="/asset/js/bootstrap.js"></script> <!-- Bootstrap -->
  <script src="/asset/js/jquery-ui-1.9.2.custom.min.js"></script> <!-- jQuery UI -->
  <script src="/asset/js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
  <script src="/asset/js/jquery.rateit.min.js"></script> <!-- RateIt - Star rating -->
  <script src="/asset/js/jquery.prettyPhoto.js"></script> <!-- prettyPhoto -->

  <script src="/asset/js/raphael-min.js"></script>
  <script src="/asset/js/morris.min.js"></script>

  <!-- jQuery Flot -->
  <!--
  <script src="/asset/js/excanvas.min.js"></script>
  <script src="/asset/js/jquery.flot.js"></script>
  <script src="/asset/js/jquery.flot.resize.js"></script>
  <script src="/asset/js/jquery.flot.pie.js"></script>
  <script src="/asset/js/jquery.flot.stack.js"></script>
-->
  <!-- jQuery Notification - Noty -->
  <script src="/asset/js/jquery.noty.js"></script> <!-- jQuery Notify -->
  <script src="/asset/js/themes/default.js"></script> <!-- jQuery Notify -->
  <script src="/asset/js/layouts/bottom.js"></script> <!-- jQuery Notify -->
  <script src="/asset/js/layouts/topRight.js"></script> <!-- jQuery Notify -->
  <script src="/asset/js/layouts/top.js"></script> <!-- jQuery Notify -->
  <!-- jQuery Notification ends -->

  <!-- Daterangepicker -->
  <script src="/asset/js/moment.min.js"></script>
  <script src="/asset/js/daterangepicker.js"></script>

  <script src="/asset/js/sparklines.js"></script> <!-- Sparklines -->
  <script src="/asset/js/jquery.gritter.min.js"></script>
  <script src="/asset/js/jquery.cleditor.min.js"></script> <!-- CLEditor -->
  <script src="/asset/js/bootstrap-datetimepicker.min.js"></script> <!-- Date picker -->
  <script src="/asset/js/jquery.slimscroll.min.js"></script> <!-- jQuery SlimScroll -->
  <script src="/asset/js/bootstrap-switch.min.js"></script> <!-- Bootstrap Toggle -->
  <script src="/asset/js/filter.js"></script> <!-- Filter for support page -->
  <script src="/asset/js/custom.js"></script> <!-- Custom codes -->
  <script src="/asset/js/charts.js"></script> <!-- Charts & Graphs -->

  <script src="/asset/js/index.js"></script> <!-- Index Javascripts -->
  