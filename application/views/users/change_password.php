  <div class="page-head">
  <h2 class="pull-left">Change Password</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Change Password</a>
      </div>      
  <div class="clearfix"></div>
</div>

  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Change Password</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		foreach($user as $usr){
          							if($_SESSION['username']===$usr->username){
          								//echo $usr->username.'<br/>';
          								//echo $usr->password.'<br/>';

          								$attid=array(
          									'type'=>'hidden',
          	                  				'name'=>'id',
          	                  				'value'=>$usr->id,
          	                  				'class'=>'form-control'
          	                  			);

		                  		$attoldpass=array(
		                  				'name'=>'oldpass',
		                  				//'value'=>$usr->password,
                              'placeholder'=>'input old password',
		                  				'class'=>'form-control',
		                  				'required'=>'required'
		                  			);

		                  		$attpassdb=array(
		                  				'type'=>'hidden',
		                  				'name'=>'passdb',
		                  				'value'=>$usr->password,
		                  				'class'=>'form-control'
		                  			);

		                  		$attpass1=array(
		                  				'name'=>'password',
		                  				//'value'=>$usr->password,
		                  				'class'=>'form-control',
		                  				'placeholder'=>'input new password',
		                  				'required'=>'required'
		                  			);

		                  		$attpass2=array(
		                  				'name'=>'pass2',
		                  				//'value'=>$usr->password,
		                  				'class'=>'form-control',
                              'placeholder'=>'confirm new password',
		                  				'required'=>'required'
		                  			);

		                  		$attsubmit=array(
		                  				'name'=>'changepass',
                              'id'=>'changepass',
		                  				'content'=>'Change Password',
		                  				//'type'=>'submit',
		                  				'class'=>'btn btn-primary btn-block'
		                  			);
		                  		}
		                  }
					           ?>

                      <?= form_open('','id=changePassword') ?>
                          <fieldset>
                          		<div class="form-group">
                                    <?= form_input($attid) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Password Lama','oldpass') ?>
                                    <?= form_password($attoldpass) ?>
                                </div>
                                <div class="form-group">
                                    <? //form_label('Password DB','passdb') ?>
                                    <?= form_input($attpassdb) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Password Baru','password') ?>
                                    <?= form_password($attpass1) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Ulangi Password Baru','pass2') ?>
                                    <?= form_password($attpass2) ?>
                                </div>
                             
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>

                      	<div class="errors" style="color:red;"><?= validation_errors() ?></div>	
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
    $('#changepass').click(function(){
      $.ajax({
        type : 'POST',
        url : './users/change_pass_proccess',
        data : $('#changePassword').serialize(),
        success : function(data){
          data = jQuery.parseJSON(data);
          if(data.success){
            alert('Data Berhasil disimpan');
            $("#contentAjax").load('./users');
          }else{
            alert(data.message);
          }
        }
      });
    });
  });
</script>