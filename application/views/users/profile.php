<div class="page-head">
  <h2 class="pull-left">Manage Profile</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Profile</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Profile</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
                  <th>Username</th>
            			<th>Name</th>
            			<th>TTL</th>
            			<th>Email</th>
            			<th>Lokasi</th>
                  <th>Divisi</th>
                  <th>Level</th>
                  <th>Group</th>
            			<th>Opsi</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
                $no=1;
                foreach ($profile as $pro) {
                  if($_SESSION['username']==$pro->username and $pro->employee_id!=0){
                    echo '<td>'.$pro->username.'</td>';
                    echo '<td>'.$pro->employee_name.'</td>';
                    echo '<td>'.$pro->t_lahir.', '.$pro->tl_lahir.'</td>';
                    echo '<td>'.$pro->email.'</td>';
                    echo '<td>'.$pro->lokasi.'</td>';
                    echo '<td>'.$pro->divisi_name.'</td>';
                    echo '<td>'.$pro->level_id.'</td>';
                    echo '<td>'.$pro->group_id.'</td>';
                    echo '<td><div class="btn-group">
                            <button class="btn btn-xs btn-success" title="Edit" onclick="$(\'#contentAjax\').load(\'./employee/edit?id='.$pro->employee_id.'\')"><i class="fa fa-pencil"></i></button>
                          </div></td>';
                    echo '</tr>';
                    $no++;
                    }
                  }
            		?>
            		</tbody>
            	</table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>