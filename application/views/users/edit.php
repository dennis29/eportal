<div class="page-head">
  <h2 class="pull-left">Edit User</h2>
    <div class="clearfix"></div>
    <!-- Breadcrumb -->
    <div class="bread-crumb">
      <a href="#"><i class="fa fa-home"></i> Home</a> 
      <!-- Divider -->
      <span class="divider">/</span> 
    <a href="#" class="bread-current">Edit User</a>
  </div>
  <div class="clearfix"></div>
</div>

  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit User</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		$attusr=array(
                  				'name'=>'username',
                  				'value'=>$user->username,
                  				'class'=>'form-control'
                  			);

                  		$attsubmit=array(
                  				'name'=>'edituser',
                          'id'=>'edituser',
                  				'content'=>'Update',
                  				//'type'=>'submit',
                  				'class'=>'btn btn-primary btn-block'
                  			);
            					?>

                      <?= form_open('','id=myForm') ?>
                          <fieldset>
                          		<div class="form-group">
                                    <?= form_hidden('id',$user->id) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Username','username') ?>
                                    <?= form_input($attusr) ?>
                                </div>

                                <div class="form-group">
                                  <label for="is_activated">Activated</label>
                                  <select name="is_activated" class="form-control">
                                    <?php
                                      //foreach($users as $usr){
                                        if($user->is_activated==1){
                                          $selected='selected';
                                          echo '<option value=1 selected='.$selected.'>Active</option>';
                                          echo '<option value=0>Non-active</option>';
                                        }else{
                                          $selected='selected';
                                          echo '<option value=1>Active</option>';
                                          echo '<option value=0 selected='.$selected.'>Non-active</option>';
                                        }
                                      //}
                                    ?>
                                  </select>
                                </div> 
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
    $("#edituser").click(function(){
      $.ajax({
        type:'POST',
        url:'./users/edit_proccess',
        data:$("#myForm").serialize(),
        success: function(data){
          data=jQuery.parseJSON(data);
          if(data.success){
            alert('Data Berhasil diubah');
            $("#contentAjax").load("./users");
          }else{
            alert(data.message);
          }
        }
      });
    });
  });
</script>