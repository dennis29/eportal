<script src="<?php echo base_url('/asset/js/custom2.js'); ?>"></script> <!-- Custom codes -->
<script src="<?php echo base_url('/asset/js/jquery.form.min.js'); ?>"></script>

<div class="page-head">
  <h2 class="pull-left">Manage Users</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Manage Users</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-7">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Daftar Semua User</div>
              <div class="widget-icons pull-right">
                <?= '<a href="#userModal" data-toggle="modal" onclick="$(\'#contentDetailUser\').load(\'./users/search\')" title="Seacrh"><i class="fa fa-search"></i></a>'; ?>
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
            			<th>Username</th>
                  <th>Status Aktif</th>
                  <th>Employee</th>
            			<th>Opsi</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
            			$no=$this->user_model->limit+1;
            			echo '<tr>';
            			foreach ($users as $usr){
            				echo '<td>'.$no.'</td>';
            				//echo '<td>'.$usr->firstname.'</td>';
            				//echo '<td>'.$usr->lastname.'</td>';
            				//echo '<td>'.$usr->email_address.'</td>';
            				echo '<td>'.$usr->username.'</td>';
                    echo '<td>';if($usr->is_activated==1){echo 'Active';}else{echo 'Non-Active'; }'</td>';
                    echo '<td>'.$usr->employee_name.'</td>';
            				//echo '<td>'.$usr->name.'</td>';
                            echo '<td><div class="btn-group">
                                          <button class="btn btn-xs btn-success" title="Edit" onclick="loading(\'./users/edit?id='.$usr->id.'\')"><i class="fa fa-pencil"></i></button>
                                          <button class="btn btn-xs btn-danger" title="Reset Password" onclick="if(confirm(&quot;Reset Password '.$usr->employee_name.'?&quot;)){reset_pass('.$usr->id.')}return false;"><i class="fa fa-undo"></i></button>
                                      </div></td>';
            			echo '</tr>';
            			$no++;
                  //<button class="btn btn-xs btn-danger" title="Delete" onclick="$(\'#contentAjax\').load(\'/index.php/users/delete?id='.$usr->id.'\')"><i class="fa fa-trash-o"></i></button>
                                          
            			}
            		?>
            		</tbody>
            	</table>
              <div class="widget-foot">
                <div class="table-info" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($users) ?> dari <?= $total_rows ?> data</div>
                <?= $page ?>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="userModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Search User</h4>
              </div>
              <div class="modal-body">
                <div id="contentDetailUser"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<script type="text/javascript">
        function reset_pass(id){
          $.ajax({
            type    : 'GET',
            url     : './users/reset_pass?id='+id,
            //data    : table,
            success : function(data){
              alert('Kembali ke password default "itgos2012"');
              $("#contentAjax").load("./users");
            }
          });
        }
</script>