  <script type="text/javascript" src="<?php echo base_url('/asset/js/basket.min.js'); ?>"></script>
    <script type="text/javascript">
  basket.require(
    { url: '<?php echo base_url('/asset/js/jquery.js'); ?>' },
    { url: '<?php echo base_url('/asset/js/jquery-ui-1.9.2.custom.min.js'); ?>' },
    { url: '<?php echo base_url('/asset/js/custom4.min.js'); ?>' }
  );    
    </script>

  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget" border=0>
                <div class="widget-content">
                  <div class="padd">
                      <?= form_open('','id=searchUser') ?>
                          <fieldset>
                                <div class="form-group">
                                  <input type="text" name="username" class="form-control" list="username"> 
                                  <datalist id="username">
                                    <option value=></option>
                                    <?php
                                      foreach($users as $user){
                                        echo '<option value='.$user->username.'>'.$user->username.'</option>';
                                      }
                                    ?>
                                  </datalist>
                                </div>
                                <!-- <div class="form-group">
                                    <select name="username" id="username" class="form-control">
                                      <option value=></option>
                                      <?php
                                        foreach($users as $user){
                                          echo '<option value='.$user->username.'>'.$user->username.'</option>';
                                        }
                                      ?>
                                                                          
                                    </select>
                                </div>
     -->                            <div class="form-group">
                                    <select name="is_activated" id="is_activated" class="form-control">
                                      <option value=></option>
                                      <option value=1>Active</option>
                                      <option value=0>Non-Active</option>                                                                          
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="employee_id" id="employee_id" class="form-control">
                                      <option value=></option>
                                      <?php
                                        foreach($employee as $emp){
                                          echo '<option value='.$emp->id.'>'.$emp->employee_name.'</option>';
                                        }
                                      ?>
                                                                          
                                    </select>
                                </div>
                                <button name="btnsearch" type="button" id="btnsearch" onclick="search()" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search</button>
                            </fieldset>
                        <?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function search(){
              $("#contentAjax").load("./users/index?"+$("#searchUser").serialize());
              $("#tutupmodal2").trigger("click");
        }

  </script>