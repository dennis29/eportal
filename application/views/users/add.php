<div class="page-head">
  <h2 class="pull-left">Add User</h2>
    <div class="clearfix"></div>
    <!-- Breadcrumb -->
    <div class="bread-crumb">
      <a href="#"><i class="fa fa-home"></i> Home</a> 
      <!-- Divider -->
      <span class="divider">/</span> 
    <a href="#" class="bread-current">Add User</a>
  </div>
  <div class="clearfix"></div>
</div>

<div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add User</div>
                <div class="widget-icons pull-right">
                  <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                  <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                  <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>
              <div class="clearfix"></div>
            </div>

                <div class="widget-content">
                  <div class="padd">
                    <form onsubmit="return false;" id="addUser" method="post" accept-charset="utf-8">                          
                      <fieldset>
                        <div class="form-group">
                          <label for="username">Username</label>                                    
                          <input type="text" name="username" value="" class="form-control" placeholder="input username">                                
                        </div>
                        <div class="form-group">
                          <label for="employee_id">Employee</label>
                          <select name="employee_id" class="form-control">
                            <option value=>== Select Employee ==</option>
                            <?php
                              foreach ($employee as $emp) {
                                echo '<option value='.$emp->id.'>'.$emp->employee_name.'</option>';
                              }
                            ?>
                          </select>
                          <p class="text-danger">*Buat Employee terlebih dahulu</p>
                        </div>                            
                        <button name="btnadd" type="button" id="btnadd" class="btn btn-primary btn-block">Add User</button>                           
                      </fieldset>
                    </form>
                  </div>
                </div>
            </div>
            </div>
        </div>
  </div>
</div>

<script type="text/javascript">
  $(function(){
    $('#btnadd').click(function(){
      $.ajax({
        type : 'POST',
        url : './users/add_proccess',
        data : $('#addUser').serialize(),
        success : function(data){
          data = jQuery.parseJSON(data);
          if(data.success){
            alert('Data Berhasil disimpan');
            $("#contentAjax").load('./users');
          }else{
            alert(data.message);
          }
        }
      });
    });
  });
</script>