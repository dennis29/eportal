  <script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap-datetimepicker.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/custom-with-date.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.form.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/typeahead2.min.js'); ?>"></script>
	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Edit SOD</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                      $attclient=array(
                          'name'=>'client_sod_id',
                          'value'=>$sod->client_sod_id,
                          'class'=>'form-control'
                        );

                  		$attcreated=array(
                  				'name'=>'created_for',
                  				//'value'=>$user->firstname,
                  				'class'=>'form-control'
                  			);

                  		$attdesc=array(
                  				'name'=>'reason',
                  				'value'=>$sod->reason,
                  				'class'=>'form-control'
                  			);

                      $attdep=array(
                          'name'=>'department',
                          'value'=>$sod->department,
                          'class'=>'form-control'
                        );

                  		$attatta=array(
                  				'name'=>'attachment',
                  				//'value'=>$user->username,
                  				'class'=>'form-control'
                  			);

                      $attdue=array(
                          'name'=>'duedate',
                          'class'=>'form-control'
                        );

                  		$attsubmit=array(
                  				'name'=>'sendapproval',
                          'id' => 'sendapproval',
                  				'content'=>'Send Approval',
                    			//'type'=>'submit',
                          // 'formaction'=>'/index.php/it_services/add_proccess',
                  				'class'=>'btn btn-success btn-block'
                  			);

                      $attdraf=array(
                          'name'=>'draftapproval',
                          'id'=>'draftapproval',
                          'content'=>'Save Draft',
                          //'type'=>'submit',
                          //'formaction'=>'/index.php/it_services/add_draft',
                          'class'=>'btn btn-danger btn-block'
                        );

                    ?>
                       
                       <?= form_open_multipart('','id=editSOD') ?>
                          <fieldset>
                                    <input type="hidden" name="param" value="" class="form-control" type="text" id="param">
                                    <input type="hidden" name="id" value="<?= $sod->id ?>" class="form-control" type="text">
                                <div class="form-group">
                                    <select name="doc_id" class="form-control" style="display: none;">
                                    <?php
                                      foreach($docs as $doc){
                                        if($doc->id==3){
                                          echo '<option value='.$sod->doc_id.' selected="selected">'.$sod->doc_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Client','client_sod_id') ?>
<!--                                    <select name="client_sod_id" class="form-control">
                                    <?php
//                                     foreach($client as $cl){
//                                        if($cl->id==$sod->client_sod_id){
//                                         echo '<option value='.$cl->id.' selected="selected">'.$cl->client_name.'</option>';
//                                        }else{
//                                          echo '<option value='.$cl->id.'>'.$cl->client_name.'</option>';
//                                        }
 //                                         
//                                      }
                                    ?>
                                    </select>
-->                                    <input type="text" id="client_sod_id" autocomplete="off" name="client_sod_id" class="form-control" value="<?php echo $sod->client_sod_id; ?>" data-provide="typeahead">
                                </div>

                                <div class="form-group">
                                    <?= form_label('Effective Date','effective_date') ?>
                                    <div id="datetimepicker1" class="input-append">
                                      <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="effective_date" value="<?= $sod->effective_date ?>">
                                      <span class="add-on">
                                        <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                      </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('SBU','sbu_id') ?>
                                    <select name="sbu_id" class="form-control">
                                    <?php
                                      foreach($sbu as $sbu){
                                        if($sbu->id==$sod->sbu_id){
                                          echo '<option value='.$sbu->id.' selected="selected">'.$sbu->sbu_name.'</option>';
                                        }else{
                                          echo '<option value='.$sbu->id.'>'.$sbu->sbu_name.'</option>';
                                        }
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="massal">Massal</label>
                                    <select name="massal" class="form-control" id="massal" onchange="load_massal()">
                                      <option value="1">YES</option>
                                      <option value="2">NO</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="nrk">NRK</label>
                                    <input name="nrk" class="form-control" type="text" id="nrkid" onchange="load_xml(this.value)" value="<?= $sod->nrk ?>">
                                </div>
                                <div class="form-group">
                                    <label for="name_sod">Name</label>
                                    <input name="name_sod" class="form-control" type="text" id="nameid" placeholder="autocomplete by NRK" value="<?= $sod->name_sod ?>">
                                </div>
                                <div class="form-group">
                                    <label for="position_sod">Position</label>
                                    <input name="position_sod" class="form-control" type="text" id="posiId" placeholder="autocomplete by NRK" value="<?= $sod->position_sod ?>">
                                </div>

                                <div class="form-group">
                                    <?= form_label('Status','status_sod_id') ?>
                                    <select name="status_sod_id" class="form-control">
                                    <?php
                                      foreach($status_sod as $stat){
                                        if($stat->id==$sod->status_sod_id){
                                          echo '<option value='.$stat->id.' selected="selected">'.$stat->status_sod_name.'</option>';
                                        }else{
                                          echo '<option value='.$stat->id.'>'.$stat->status_sod_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Rekomendasi','rekomendasi_sod_id') ?>
                                    <select name="rekomendasi_sod_id" class="form-control">
                                    <?php
                                      foreach($rekomendasi as $rek){
                                        if($rek->id==$sod->rekomendasi_sod_id){
                                          echo '<option value='.$rek->id.' selected="selected">'.$rek->rekomendasi_sod_name.'</option>';
                                        }else{
                                          echo '<option value='.$rek->id.'>'.$rek->rekomendasi_sod_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Start Date From','start_datefrom') ?>
                                    <div id="datetimepicker3" class="input-append">
                                      <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="start_datefrom" value="<?= $sod->start_datefrom ?>">
                                      <span class="add-on">
                                        <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                      </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Start Date To','start_dateto') ?>
                                    <div id="datetimepicker4" class="input-append">
                                      <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="start_dateto" value="<?= $sod->start_dateto ?>">
                                      <span class="add-on">
                                        <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                      </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Reason','reason') ?>
                                    <?= form_textarea($attdesc) ?>
                                </div>
                                <?php
                                  foreach($users as $usr){
                                    if($usr->username==$_SESSION['username']){
                                      echo form_hidden('employee_id',$usr->id);
                                      echo form_hidden('created_by',$usr->employee_id);
                                    }
                                  }
                                ?>

                                <div class="form-group">
                                    <label for="department">department</label>
                                    <input name="department" value="<?= $sod->department ?>" class="form-control" type="text">
                                </div>
                      
                                <div class="form-group">
                                    <?= form_label('Priority','priority_id') ?>
                                    <select name="priority_id" class="form-control">
                                    <?php
                                      foreach($priority as $pri){
                                        if($pri->id==$sod->priority_id){
                                          echo '<option value='.$pri->id.' selected="selected">'.$pri->priority_name.'</option>';
                                        }else{
                                          echo '<option value='.$pri->id.'>'.$pri->priority_name.'</option>';
                                        }
                                        
                                      }
                                    ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="attachment2" value="<?php echo $sod->attachment_sod; ?>">
                                    <?= form_label('Attachment','attachment_sod') ?>
                                    <input type="file" name="attachment_sod" id="attachment_sod">
                                </div>
                                <div class="form-group">
                                  <label><?php echo $sod->attachment_sod; ?></label>
                                </div>
                              <?= form_button($attsubmit) ?>
                              <?= form_button($attdraf) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
       //alert("OKE");
      $("#editSOD").ajaxForm({
              type: 'POST',
              url: './sod/edit_process',
              data: $("#editSOD").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    alert("Data Berhasil Disimpan");
                    var $btn = $("button");
                    $btn.button('reset');
                    $("#contentAjax").load("./sod");
                 }else{
                    alert(data.message);
                 }
                 var $btn = $("button");
                 $btn.button('reset');
              }
            });
        $("#sendapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("send");
          $("#editSOD").submit();
        });
        
        $("#draftapproval").click(function(){
          var $btn = $("button");
          $btn.button('loading');
          $("#param").val("draft");
          $("#editSOD").submit();
        });

        $("#massal").change(function(){
            $("p").removeAttr("readonly");
        });
		var timeout = 0;
		$('#client_sod_id').typeahead({
			source: function (query, process) {
				if (timeout) {
					clearTimeout(timeout);
				}
	
				timeout = setTimeout(function() {
					return $.get('./sod/load_client/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   //console.log(data);
					   return process(data.value);
					});
				}, 500);
			},
			autoSelect : true,
			minLength : 3,
			delay : 3,
			updater : function(items){
				var current = items;
//				console.log(current);
				$('#client_sod_id').val(current);
				return items;
			}
		});

		$('#client_sod_id').change(function() {
			var current = $('#client_sod_id').typeahead("getActive");
//			console.log(current);
			try{
//				$('#client_sod_id').val(current);
			}catch(e){
				alert("Pilih Client dari List yang Ada");
			}
		});

		$('#client_sod_id').focusout(function() {
			if($('#client_sod_id').val()==""){
				$('#client_sod_id').val("");
			}
		});	
		
		
		
    });

  function load_xml(nrk){
          $.ajax({
            type    : 'GET',
            url     : './sod/load_xml/'+nrk,
            //data    : table,
            success : function(data){
              var res = data.split("|");
              //alert(res);
              //document.getElementById("demo").innerHTML = res;
              //alert(res[1]);
              /*if(res!=null)){
                alert('gak kosong');
              }else{
                alert('kosong');
              }*/
              var name = document.getElementById("nameid");
              name.value = res[1];

              var posi = document.getElementById("posiId");
              posi.value = res[2];
            }
          });
        }

  function load_massal() {
    var x = document.getElementById("massal").value;
    if(x==1){
      $("#nrkid").attr("disabled","disabled");
      $("#nameid").attr("disabled","disabled");
      $("#posiId").attr("disabled","disabled");
    }else if(x==2){
      $("#nrkid").removeAttr("disabled");
      $("#nameid").removeAttr("disabled");
      $("#posiId").removeAttr("disabled");
    }
     
  }
</script>