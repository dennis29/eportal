  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-content">
              <div class="padd invoice">
                <div class="row">
                  <div class="col-md-12">
                        <div class="alert alert-success">
                          <?php
                            if(!empty($detail->request_date)){
                              $orireq = $detail->request_date;
                              $req_date = date("d-m-Y", strtotime($orireq));
                              
                            }else{
                              $req_date = '';
                            }

                            if(!empty($detail->start_datefrom)){
                              
                              $oristart = $detail->start_datefrom;
                              $start_date = date("d-m-Y", strtotime($oristart));
                            }else{
                              
                              $start_date = '';
                            }

                            if(!empty($detail->start_dateto)){
                              
                              $orito = $detail->start_dateto;
                              $to_date = date("d-m-Y", strtotime($orito));
                            }else{
                              $to_date = '';
                            }

                            if(!empty($detail->effective_date)){
                              
                              $orieffe = $detail->effective_date;
                              $effe_date = date("d-m-Y", strtotime($orieffe));
                            }else{
                              
                              $effe_date = '';
                            }

                          ?>
                          Request date : <?= $req_date ?><br>
                          Doc Sequence : <?= $detail->doc_seq_id ?><br>
                          Employee : <?= $detail->employee_name ?><br>
                          Client Name : <?= $detail->client_sod_id ?><br>
                          NRK : <?= $detail->nrk ?><br>
                          Name : <?= $detail->name_sod ?><br>
                          Position : <?= $detail->position_sod ?><br>
                          Status : <?= $detail->status_sod_name ?><br>
                          Start Date From : <?= $start_date ?><br>
                          Start Date To : <?= $to_date ?><br>
                          Rekomendasi : <?= $detail->rekomendasi_sod_name ?><br>
                          Effective date : <?= $effe_date ?><br>
                          Reason : <?= $detail->reason ?><br>

                          Attachment :
                          <?php
                            if(!empty($detail->attachment_sod)){
                              echo '<a href="/eportal2/asset/file/'.$detail->attachment_sod.'" target="_blank">View</a><br>';
                            }else{
                              echo '<style="color:red;">Empty<br>';
                            }
                          ?>
                          Department : <?= $detail->department ?><br>
                          <!--  PIC PAM : <?= $detail->pic_pam_id ?><br> -->
                          SBU Name : <?= $detail->sbu_name ?><br>
                          <!--Massal -->
                          
                          Memo : <?= $detail->app_memo ?><br>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
           </div>
        </div>
      </div>
    </div>
  </div>