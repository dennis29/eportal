<script src="<?php echo base_url('/asset/js/custom2.js'); ?>"></script> <!-- Custom codes -->
<script src="<?php echo base_url('/asset/js/jquery.form.min.js'); ?>"></script>

<div class="page-head">
  <h2 class="pull-left">Manage SOD Document</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="/index.php/main"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a onclick="loading('/index.php/sod')" href="#" class="bread-current">SOD Document</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">SOD Document List</div>
              <div class="widget-icons pull-right">
                <?= '<a href="#modalSODDocSearch" data-toggle="modal" onclick="$(\'#contentSODDocSearch\').load(\'./sod/search_print\')" title="Seacrh"><i class="fa fa-search"></i></a>'; ?>
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table tablesorter table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
                  <th>Doc Number</th>
                  <th class="sembunyi1">Enployee</th>
                  <th class="sembunyi1">Client</th>
                <th class="sembunyi1">Effective date</th>
                <th class="sembunyi1">SBU</th>
                <th class="sembunyi2">NRK</th>
                <th class="sembunyi2">Name</th>
                  <th class="sembunyi2">Position</th>
                  <th>Status</th>
                  <th class="sembunyi3">Rekomendasi</th>
                  <th class="sembunyi3">File</th>
                  <th class="sembunyi3">Request date</th>
                  <th>Opsi</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
                  $no=$this->sod_model->limit;
            			$no=$no+1;
            			echo '<tr border color=red>';
                  //echo $_SESSION['username'];

                  //foreach($users as $usr){
                    foreach ($sod as $sod2){
                      // if($usr->employee_name==$serv->employee_name){
                        echo '<td>'.$no.'</td>';
                        //echo '<td>'.$sod2->status_app_id.'</td>';
                        echo '<td>'.$sod2->doc_seq_id.'</td>';
                        echo '<td class="sembunyi1">'.$sod2->employee_name.'</td>';
                        echo '<td class="sembunyi1">'.$sod2->client_sod_id.'</td>';
                        echo '<td class="sembunyi1">'.$sod2->effective_date.'</td>';
                        echo '<td class="sembunyi1">'.$sod2->sbu_name.'</td>';
                        echo '<td class="sembunyi2">'.$sod2->nrk.'</td>';
                        echo '<td class="sembunyi2">'.$sod2->name_sod.'</td>';
                        echo '<td class="sembunyi2">'.$sod2->position_sod.'</td>';
                        //echo '<td>'.$sod2->status_sod_id.'</td>';
                        echo '<td><center><span class='.$sod2->class_status.'>'.$sod2->status_app_name.'</span></td>';
                        echo '<td class="sembunyi3">'.$sod2->rekomendasi_sod_name.'</td>';
                        //echo '<td>'.$sod2->start_datefrom.'</td>';
                        //echo '<td>'.$sod2->start_dateto.'</td>';
                        //echo '<td>'.$sod2->reason.'</td>';
                        //echo '<td>'.$sod2->pic_pam_id.'</td>';
                        //echo '<td>'.$sod2->department.'</td>';
                        //echo '<td>'.$sod2->attachment_sod.'</td>';
                        if(!empty($sod2->attachment_sod)){
                                echo '<td><center><a href="/eportal2/asset/file/'.$sod2->attachment_sod.'" target="_blank"><i class="fa fa-file-text"></i></a></td>';
                              }else{
                                echo '<td style="color:red;"><center><i class="fa fa-file-text"></i></td>';
                              }
                        if(!empty($sod2->request_date)){
                          $orireq = $sod2->request_date;
                          $req_date = date("d-m-Y", strtotime($orireq));
                          
                        }else{
                          $req_date = '';
                        }
                        echo '<td class="sembunyi3">'.$req_date.'</td>';
                        //echo '<td><center><span class='.$sod2->class_status.'>'.$sod2->status_app_name.'</span></td>';
                        //echo '<td>'.$sod2->request_date.'</td>';
                        echo '<td><center><a href="#modalSODDocDetail" data-toggle="modal" onclick="$(\'#contentSODDocDetail\').load(\'./sod/detail?id='.$sod2->id.'\')" class="btn btn-warning btn-xs" title="Detail"><i class="fa fa-eye"></i></a> ';
                        if($sod2->print_status==1){
                          echo '<button id="btndelete" class="btn btn-xs btn-success" title="Printed" onclick="alert(&quot;File ini sudah di Print&quot;)"><i class="fa fa-check"></i></button></td>';
                        }else{
                          echo '<button id="btndelete" class="btn btn-xs btn-primary" title="Print" onclick="if(confirm(&quot;Print this document?&quot;)){print_doc('.$sod2->id.')}return false;"><i class="fa fa-print"></i></button></td>';
                        }
                            
                        echo '</tr>';
                        
                        $no++;
                      // }
                    }
                  //}
            		?>
            		</tbody>
            	</table>
            <div class="widget-foot">
              <div class="table-info sembunyi" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($sod) ?> dari <?= $total_rows ?> data</div>
              <?= $page ?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modalSODDocDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Detail SOD</h4>
              </div>
              <div class="modal-body">
                <div id="contentSODDocDetail"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<div id="modalSODDocSearch" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Search SOD Service</h4>
              </div>
              <div class="modal-body">
                <div id="contentSODDocSearch"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<script type="text/javascript">

        function print_doc(id){
          $.ajax({
            type    : 'GET',
            url     : './sod/update_print_status?id='+id,
            //data    : table,
            success : function(data){
              alert("Printed");
              $("#contentAjax").load("./sod/sod_document");
            }
          });
        }

$(window).resize(function() {
	console.log(document.documentElement.clientWidth);
    if (document.documentElement.clientWidth > 1300 && document.documentElement.clientWidth <= 1400) {    
			$(".sembunyi1").hide();
			$(".sembunyi2").show();
			$(".sembunyi3").show();
    }
	else
    if (document.documentElement.clientWidth > 1200 && document.documentElement.clientWidth <= 1300) {    
			$(".sembunyi1").hide();
			$(".sembunyi2").hide();
			$(".sembunyi3").show();
    }
	else
    if (document.documentElement.clientWidth <= 1200) {    
			$(".sembunyi1").hide();
			$(".sembunyi2").hide();
			$(".sembunyi3").hide();
    }
	else
	{
			$(".sembunyi1").show();		
			$(".sembunyi2").show();		
			$(".sembunyi3").show();		
	}
}).resize();
</script>

<script>
  $(function(){
    $("#datatable").tablesorter({ sortList: [[0,0], [1,0]] });
  });
</script>