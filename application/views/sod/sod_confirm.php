   <style>
   		.sort{
			float:left;	
		}
		
		.sort:hover{
			text-decoration:underline;
			cursor:pointer;			
		}
   </style>

<div class="page-head">
  <h2 class="pull-left">SOD Confirm</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="./main"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a onclick="loading('./sod/sod_confirm')" href="#" class="bread-current"><span class="label label-warning">SOD Confirm</span></a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">SOD Confirm List</div>
              <div class="widget-icons pull-right">
                <?= '<a href="#searchSODConfirm" data-toggle="modal" onclick="$(\'#contentsodconfirmsearch\').load(\'./sod/search_confirm\')" title="Seacrh"><i class="fa fa-search"></i></a>'; ?>
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table tablesorter table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
                  <th><div id="doc_seq_id" class="sort">Doc Number</div>&nbsp;<i class="sorti fadoc_seq_id"></i></th>
                  <th class="sembunyi1"><div id="employee_name" class="sort">Employee</div>&nbsp;<i class="sorti faemployee_name"></i></th>
                  <th class="sembunyi1"><div id="client_sod_id" class="sort">Client</div>&nbsp;<i class="sorti faclient_sod_id"></i></th>
                <th class="sembunyi1"><div id="effective_date" class="sort">Effective date</div>&nbsp;<i class="sorti faeffective_date"></i></th>
                <th class="sembunyi1"><div id="sbu_name" class="sort">SBU</div>&nbsp;<i class="sorti fasbu_name"></i></th>
                <th class="sembunyi2"><div id="nrk" class="sort">NRK</div>&nbsp;<i class="sorti fanrk"></i></th>
                <th class="sembunyi2"><div id="name_sod" class="sort">Name</div>&nbsp;<i class="sorti faname_sod"></i></th>
                  <th class="sembunyi2"><div id="position_sod" class="sort">Position</div>&nbsp;<i class="sorti faposition_sod"></i></th>
                  <th><div id="status_app_name" class="sort">Status</div>&nbsp;<i class="sorti fastatus_app_name"></i></th>
                  <th class="sembunyi3"><div id="rekomendasi_sod_name" class="sort">Rekomendasi</div>&nbsp;<i class="sorti farekomendasi_sod_name"></i></th>
                  <th class="sembunyi3">file</th>
                  <th class="sembunyi3"><div id="request_date" class="sort">Req date&nbsp;<i class="sorti farequest_date"></i></th>
                  <th>Approve</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
                  $no=$this->sod_model->limit;
            			$no=$no+1;
            			echo '<tr border color=red>';
                  //echo $_SESSION['username'];

                  //print_r($sod);

                  //foreach($users as $usr){
                    foreach ($sod as $sod2){
                      $hover_content='Employee : '.$sod2->employee_name.' || Client : '.$sod2->client_sod_id;
                      // if($usr->employee_name==$serv->employee_name){
                        echo '<td>'.$no.'</td>';
                        //echo '<td>'.$sod2->status_app_id.'</td>';
                        echo '<td>'.$sod2->doc_seq_id.'</td>';
                        echo '<td class="sembunyi1">'.$sod2->employee_name.'</td>';
                        echo '<td class="sembunyi1">'.$sod2->client_sod_id.'</td>';
                        echo '<td class="sembunyi1">'.$sod2->effective_date.'</td>';
                        echo '<td class="sembunyi1">'.$sod2->sbu_name.'</td>';
                        echo '<td class="sembunyi2">'.$sod2->nrk.'</td>';
                        echo '<td class="sembunyi2">'.$sod2->name_sod.'</td>';
                        echo '<td class="sembunyi2">'.$sod2->position_sod.'</td>';
                        //echo '<td>'.$sod2->status_sod_id.'</td>';
                        echo '<td><center><span class='.$sod2->class_status.'>'.$sod2->status_app_name.'</span></td>';
                        echo '<td class="sembunyi3">'.$sod2->rekomendasi_sod_name.'</td>';
                        //echo '<td>'.$sod2->start_datefrom.'</td>';
                        //echo '<td>'.$sod2->start_dateto.'</td>';
                        //echo '<td>'.$sod2->reason.'</td>';
                        //echo '<td>'.$sod2->pic_pam_id.'</td>';
                        //echo '<td>'.$sod2->department.'</td>';
                        if(!empty($sod2->attachment_sod)){
                          echo '<td><center><a href="/eportal2/asset/file/'.$sod2->attachment_sod.'" target="_blank"><i class="fa fa-file-text"></i></a></td>';
                        }else{
                          echo '<td style="color:red;"><center><i class="fa fa-file-text"></i></td>';
                        }
                        if(!empty($sod2->request_date)){
                          $orireq = $sod2->request_date;
                          $req_date = date("d-m-Y", strtotime($orireq));
                          
                        }else{
                          $req_date = '';
                        }
                        echo '<td class="sembunyi3">'.$req_date.'</td>';

                        //echo '<td><center><span class='.$sod2->class_status.'>'.$sod2->status_app_name.'</span></td>';
                        //echo '<td>'.$sod2->request_date.'</td>';
                        
                        echo '<td><center><a href="#approveSODConfirm" data-toggle="modal" onclick="$(\'#justTest4\').load(\'./approval/approve_sod?doc_number='.$sod2->doc_seq_id.'\')" class="btn btn-success btn-xs" title="Approve"><i class="fa fa-mail-reply"></i></a>';
                        echo ' <a href="#modalsoddetail" data-toggle="modal" onclick="$(\'#justTest\').load(\'./sod/detail?id='.$sod2->id.'\')" class="btn btn-warning btn-xs" title="Detail"><i class="fa fa-eye"></i></a></td>';
                        
                        echo '</tr>';
                        
                        $no++;
                      // }
                    }
                  //}
            		?>
            		</tbody>
            	</table>
            <div class="widget-foot">
              <div class="table-info sembunyi" style="position: absolute;padding-top: 15px;">Menampilkan <?= count($sod) ?> dari <?= $total_rows ?> data</div>
              <?= $page ?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modalsoddetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Detail SOD</h4>
              </div>
              <div class="modal-body">
                <div id="justTest"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<div id="searchSODConfirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Search SOD Service</h4>
              </div>
              <div class="modal-body">
                <div id="contentsodconfirmsearch"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<div id="approveSODConfirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Approval Form</h4>
              </div>
              <div class="modal-body">
                <div id="justTest4"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" id="tutupmodal4" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<input type="hidden" name="sort_by" id="sort_by" value="<?php echo @$_GET["sort"]; ?>" />
<input type="hidden" name="sort_dir" id="sort_dir" value="<?php echo @$_GET["dir"]; ?>" />

<script type="text/javascript">

$(window).resize(function() {
	console.log(document.documentElement.clientWidth);
    if (document.documentElement.clientWidth > 1300 && document.documentElement.clientWidth <= 1400) {    
			$(".sembunyi1").hide();
			$(".sembunyi2").show();
			$(".sembunyi3").show();
    }
	else
    if (document.documentElement.clientWidth > 1200 && document.documentElement.clientWidth <= 1300) {    
			$(".sembunyi1").hide();
			$(".sembunyi2").hide();
			$(".sembunyi3").show();
    }
	else
    if (document.documentElement.clientWidth <= 1200) {    
			$(".sembunyi1").hide();
			$(".sembunyi2").hide();
			$(".sembunyi3").hide();
    }
	else
	{
			$(".sembunyi1").show();		
			$(".sembunyi2").show();		
			$(".sembunyi3").show();		
	}
}).resize();


	if($("#sort_by").val()!="")
	{
		var sort_dir = $("#sort_dir").val();

		if(sort_dir=="0")
		{
			$(".fa"+$("#sort_by").val()).removeClass("fa fa-arrow-down");
			$(".fa"+$("#sort_by").val()).addClass("fa fa-arrow-up");
		}
		else
		if(sort_dir=="1")
		{
			$(".fa"+$("#sort_by").val()).removeClass("fa fa-arrow-up");
			$(".fa"+$("#sort_by").val()).addClass("fa fa-arrow-down");
		}

	}
</script>

<script type="text/javascript">
	if($("#sort_by").val()!="")
	{
		var sort_dir = $("#sort_dir").val();

		if(sort_dir=="0")
		{
			$(".fa"+$("#sort_by").val()).removeClass("fa fa-arrow-down");
			$(".fa"+$("#sort_by").val()).addClass("fa fa-arrow-up");
		}
		else
		if(sort_dir=="1")
		{
			$(".fa"+$("#sort_by").val()).removeClass("fa fa-arrow-up");
			$(".fa"+$("#sort_by").val()).addClass("fa fa-arrow-down");
		}

	}
  $(function(){
	$('#modalsoddetail').appendTo("body");
	$('#searchSODConfirm').appendTo("body");
	$('#approveSODConfirm').appendTo("body");

	$(".sort").click(function(e){
		var sort_dir = $("#sort_dir").val();
		$("#sort_by").val(e.target.id);
		
		$(".sorti").removeClass("fa fa-arrow-down");
		$(".sorti").removeClass("fa fa-arrow-up");
		if(sort_dir=="")
		{
			$(".fa"+e.target.id).removeClass("fa fa-arrow-down");
			$(".fa"+e.target.id).addClass("fa fa-arrow-up");
			$("#sort_dir").val("0");
		}
		else
		if(sort_dir=="0")
		{
			$(".fa"+e.target.id).removeClass("fa fa-arrow-up");
			$(".fa"+e.target.id).addClass("fa fa-arrow-down");
			$("#sort_dir").val("1");
		}
		else
		if(sort_dir=="1")
		{
			$(".fa"+e.target.id).removeClass("fa fa-arrow-down");
			$(".fa"+e.target.id).addClass("fa fa-arrow-up");
			$("#sort_dir").val("0");
		}
		
		if($("#search").val() != "")
		{
			$("#contentAjax").load("./sod/sod_confirm/?_=0"+$("#search").val()+"&sort="+e.target.id+"&dir="+$("#sort_dir").val());
		}
		else
		{
			$("#contentAjax").load("./sod/sod_confirm/?sort="+e.target.id+"&dir="+$("#sort_dir").val());
		}
	});
	
	$(".sorti").click(function(e){ e.preventDefault(); });


        $("#btnsearch").click(function(){
            $.ajax({
              type: 'POST',
              url: './sod/search_confirm',
              data: $("#formSearch").serialize(),
              success: function(data)
              {
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                    $("#contentAjax").load("./sod/sod_confirm");
                    $("#tutupmodal4").trigger("click");
                 }else{
                    alert(data.message);
                 }
              }
            });
        });
    });
</script>