  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget" border=0>
                <div class="widget-content">
                  <div class="padd">
                      <?= form_open('','id=formsearchsod') ?>
                          <fieldset>
                              <div class="form-group">
                                   <!--  <div class="form-group">
                                        <input id="client_sod_id" name="client_sod_id" class="form-control" type="text">
                                    </div> -->
                                    <div class="form-group">
                                        <label for="status_app_id">Status</label>
                                        <select name="status_app_id" id="status_app_id" class="form-control">
                                          <option value=></option>
                                          <?php
                                            foreach($status_app as $sta){
                                              echo '<option value='.$sta->id.'>'.$sta->status_app_name.'</option>';
                                            }
                                          ?>
                                                                              
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="doc_seq_id">Doc Number</label>
                                        <input id="doc_seq_id" name="doc_seq_id" class="form-control" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nrk">NRK</label>
                                        <input id="nrk" name="nrk" class="form-control" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="name_sod">Name</label>
                                        <input id="name_sod" name="name_sod" class="form-control" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="rekomendasi_sod_id">Rekomendasi</label>
                                        <select name="rekomendasi_sod_id" id="rekomendasi_sod_id" class="form-control">
                                          <option value=></option>
                                          <?php
                                            foreach($rekomendasi as $rek){
                                              echo '<option value='.$rek->id.'>'.$rek->rekomendasi_sod_name.'</option>';
                                            }
                                          ?>
                                                                              
                                        </select>
                                    </div>
                                   <div class="form-group">
                                        <label for="id">Employee</label>
                                        <select name="id" id="id" class="form-control">
                                          <option value=></option>
                                          <?php
                                            foreach($employee as $emp){
                                              echo '<option value='.$emp->id.'>'.$emp->employee_name.'</option>';
                                            }
                                          ?>
                                                                              
                                        </select>
                                    </div>
                                    <button name="btnsearch" type="button" id="btnsearch" onclick="search()" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search</button>
                            </fieldset>
                        <?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function search(){
              $("#contentAjax").load("./sod/index?"+$("#formsearchsod").serialize());
              $("#search").val("&"+$("#formsearchsod").serialize());
              $("#tutupmodal2").trigger("click");
        }

  </script>