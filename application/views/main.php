<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>ePortalGOS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta http-equiv="Cache-control" content="public">


  <!-- JS -->
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery-ui-1.9.2.custom.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/fullcalendar.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.rateit.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.prettyPhoto.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/raphael-min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/morris.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.flot.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.flot.resize.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.flot.pie.js'); ?>"></script>

	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.flot.stack.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.noty.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/themes/default.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/layouts/bottom.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/layouts/topRight.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/layouts/top.js'); ?>"></script>

	<script type="text/javascript" src="<?php echo base_url('/asset/js/moment.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/daterangepicker.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/sparklines.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.gritter.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.cleditor.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap-datetimepicker.min.js'); ?>"></script>

	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.slimscroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/bootstrap-switch.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/filter.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/typeahead2.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/custom.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.form.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.tablesorter.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('/asset/js/jquery.tablesorter.widgets.js'); ?>"></script>


  <!-- Stylesheets -->
  <link  href="<?php echo base_url('/asset/style/bootstrap.css'); ?>" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="<?php echo base_url('asset/style/font-awesome.css'); ?>"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/jquery-ui-1.9.2.custom.min.css'); ?>"> 
  <!-- Calendar -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/fullcalendar.css'); ?>">
  <!-- prettyPhoto -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/prettyPhoto.css'); ?>">  
  <!-- Star rating -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/rateit.css'); ?>">
  <!-- Date picker -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/bootstrap-datetimepicker.min.css'); ?>">
  <!-- CLEditor -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/jquery.cleditor.css'); ?>"> 
  <!-- Uniform -->
  <!-- <link rel="stylesheet" href="./asset/style/uniform.default.html">  -->
  <!-- Uniform -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/daterangepicker-bs3.css'); ?>" />
  <!-- Bootstrap toggle -->
  <link rel="stylesheet" href="<?php echo base_url('/asset/style/bootstrap-switch.css'); ?>">
  <!-- Main stylesheet -->
  <link href="<?php echo base_url('/asset/style/style.css') ?>" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="<?php echo base_url('/asset/style/widgets.css') ?>" rel="stylesheet">   
    <!-- Gritter Notifications stylesheet -->
  <link href="<?php echo base_url('/asset/style/jquery.gritter.css') ?>" rel="stylesheet">   
  
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="./asset/js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo base_url('/asset/img/favicon/favicon.ico'); ?>">
</head>

<body>
<header>
<div class="navbar navbar-fixed-top bs-docs-nav" role="banner">
  
    <div class="container">
      <!-- Menu button for smallar screens -->
      <div class="navbar-header">
      <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse"><span>Menu</span></button>
      <a href="#" class="pull-left menubutton hidden-xs"><i class="fa fa-bars"></i></a>
      <!-- Site name for smallar screens -->
      <a href="./main" class="navbar-brand">ePortal<span class="bold">GOS</span></a>
    </div>

      <!-- Navigation starts -->
      <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">         
        
        <!-- Links -->
        <ul class="nav navbar-nav pull-right">
          <li class="dropdown pull-right user-data">            
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <i class="fa fa-user"></i><?= $_SESSION['employee'] ?><b class="caret"></b>              
            </a>
            
            <!-- Dropdown menu -->
            <ul class="dropdown-menu">
              <li><a href="#" onclick="loading('./users/profile')"><i class="fa fa-user"></i> Profile</a></li>
              <li><a href="#" onclick="loading('./users/change_password')"><i class="fa fa-cogs"></i> Change Password</a></li>
              <li><a href="<?php echo './admin/logout' ?>"><i class="fa fa-key"></i> Logout</a></li>
            </ul>
          </li>
            <!-- Message button with number of latest messages count-->
            <li class="dropdown dropdown-big messages-dd leftonmobile">
              <a class="dropdown-toggle" href="#" onclick="loading('./approval/inbox')" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i> 
				          <span class="label label-success" id="plung"></span>               
              </a>
                
            </li>
        </ul>
      </nav>

    </div>
  </div>
</header>
<!-- Main content starts -->

<div class="content">

    <!-- Sidebar -->
    <div class="sidebar">
        <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
        <!-- Search form -->
        <form class="navbar-form" role="search">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
            <button class="btn search-button" type="submit"><i class="fa fa-search"></i></button>
          </div>
        </form>
        <!--- Sidebar navigation -->
        <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->
        <ul id="nav">
          <?php
            echo $menus_hierarki;
          ?>
        </ul>

    </div>
    <!-- Sidebar ends -->
    <script type="text/javascript">
        function loading(url) {
          $('#contentAjax').html('<center><img style="margin-top:250px;" src="http://eportal.gos.co.id/eportal2/asset/img/131.GIF"></img>');
          $('#contentAjax').load(url);
		  $('#search').val('');
		  $('.modal').remove();		  
        }

        function loading2(url) {
          $('#contentAjax').html('<center><img style="margin-top:250px;" src="http://eportal.gos.co.id/eportal2/asset/img/131.GIF"></img>');
          $('#contentAjax').load(url);
		  $('.modal').remove();		  
        }
		
		$(function(){
			$.ajaxPrefilter('script', function(options) {
				options.cache = true;
			});			
			var task = function(){
					clearTimeout(timeOutId, 2000);						
					
					$.ajax({
								url : "./main/getInbox/",				 
								method : "GET",
								success : function(response){
									var d = jQuery.parseJSON(response);
									if(d.total==0)
									{
										$("#plung").hide();
									}
									else
									{
										$("#plung").show();
										$("#plung").html(d.total);
									}
									timeOutId = setTimeout(task, 2000);						
								},
								error : function(response){
									console.log(response);
									timeOutId = setTimeout(task, 2000);						
								}
				
					});
			 };
			 // Wait 500ms before calling our function. If the user presses another key 
			 // during that 500ms, it will be cancelled and we'll wait another 500ms.
		
			var timeOutId = 0;
		//	task();
			//OR use BELOW line to wait 10 secs before first call
			timeOutId = setTimeout(task, 2000); 				
		});
    </script>
    <!-- Main bar -->    
    <div class="mainbar">
      <div id="contentAjax">
        <!-- <div class="alert alert-danger" role="alert">
          Update ePortal<br />
          - Sort and Search for SOD Confirm and Inbox Approval<br/>
          - Bug Fixed
        </div> -->
          <div class="jumbotron">

            <div class="alert alert-info" role="alert">Welcome, <?= $_SESSION['employee'] ?></div>
            <div class="alert alert-info" role="alert">
            Dear Pengguna ePortal, <br />
            Silahkan sampaikan feedback, kritik atau saran terkait ePortal baru ke email aries.it@gos.co.id atau iksan.it@gos.co.id atau bisa juga telp langsung ke ext 6005
            </div>
            <!-- <div class="alert alert-danger" role="alert">Your Role is <?= $_SESSION['role'][0] ?></div> -->
          </div>
      </div>
    </div>

    <!-- Matter ends -->

    </div>

   <!-- Mainbar ends -->
   <div class="clearfix"></div>

</div>
<!-- Content ends -->
<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
            <div class="modal-content" style="z-index:9999999;">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Approval Form</h4>
              </div>
              <div class="modal-body">
                <div id="justTest"></div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
              </div>
            </div>
  </div>
</div>

<!-- Footer starts -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <!-- Copyright info -->
            <p class="copy">Copyright &copy; <?= date("Y") ?> | <a href="#">E-portal GOS</a> </p>
      </div>
    </div>
  </div>
</footer>   

<!-- Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 
<input type="hidden" id="search" name="search" value="" />
</body>
</html>