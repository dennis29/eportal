<div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="widget" border=0>
                <div class="widget-content">
                  <div class="padd">
                      <?= form_open('','id=searchEmp') ?>
                          <fieldset>
                          		<div class="form-group">
                                    <div class="form-group">
                                        <select name="id" id="id" class="form-control">
                                          <option value=></option>
                                          <?php
                                            foreach($employee as $emp){
                                              echo '<option value='.$emp->id.'>'.$emp->employee_name.'</option>';
                                            }
                                          ?>
                                                                              
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nik">NIK</label>
                                        <input id="nik" name="nik" class="form-control" type="text">
                                    </div>
      	                            <button name="btnsearch" type="button" id="btnsearch" onclick="search()" class="btn btn-success btn-block"><i class="fa fa-search"></i> Search</button>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

	  function search(){
              $("#contentAjax").load("./employee/index?"+$("#searchEmp").serialize());
              $("#tutupmodal2").trigger("click");
        }

	</script>