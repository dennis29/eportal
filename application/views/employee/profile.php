<div class="page-head">
  <h2 class="pull-left">Manage Profile</h2>
    <div class="clearfix"></div>
      <!-- Breadcrumb -->
      <div class="bread-crumb">
        <a href="#"><i class="fa fa-home"></i> Home</a> 
        <!-- Divider -->
        <span class="divider">/</span> 
        <a href="#" class="bread-current">Profile</a>
      </div>      
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
		<div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Profile</div>
              <div class="widget-icons pull-right">
                <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                <a href="#" class="wclose"><i class="fa fa-times"></i></a>
              </div>  
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
          	<!-- <h2>Manage Users</h2>
          	<a href="users/add">Add Users</a> -->
            	<table id="datatable" class="table table-striped table-bordered table-hover">
            		<thead>
            		<tr>
            			<th>NO</th>
            			<th>Name</th>
            			<th>TTL</th>
            			<th>Email</th>
            			<th>Lokasi</th>
                  <th>Divisi</th>
                  <th>Level</th>
            			<th>Opsi</th>
            		</tr>
            		</thead>
            		<tbody>
            		<?php
            			$no=1;
            			echo '<tr>';
            			foreach ($profile as $em){
                    foreach ($users as $usr){
                      if($_SESSION['username']==$usr->username){
                        if($em->user_id==$usr->id){
                          echo '<td>'.$no.'</td>';
                          echo '<td>'.$em->employee_name.'</td>';
                          echo '<td>'.$em->t_lahir.', '.$em->tl_lahir.'</td>';
                          echo '<td>'.$em->email.'</td>';
                          echo '<td>'.$em->lokasi.'</td>';
                          echo '<td>'.$em->divisi_name.'</td>';
                          echo '<td>'.$em->level_name.'</td>';
                                  echo '<td><div class="btn-group">
                                                <button class="btn btn-xs btn-success" title="Edit" onclick="$(\'#contentAjax\').load(\'/index.php/employee/edit?id='.$em->id.'\')"><i class="fa fa-pencil"></i></button>
                                                <button class="btn btn-xs btn-danger" title="Delete" onclick="$(\'#contentAjax\').load(\'/index.php/employee/delete?id='.$em->id.'\')"><i class="fa fa-trash-o"></i></button>
                                              </div></td>';
                          echo '</tr>';
                          $no++;
                        }
                      }
                    }
            			}
            		?>
            		</tbody>
            	</table>
            <div class="widget-foot">
            <br><br>
              <div class="clearfix"></div> 
            </div>
            <div class="padd">
              <a onclick="$('#contentAjax').load('/index.php/employee/add_profile')"><button class="btn btn-warning">Tambah Profile</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>