  <script src="<?php echo base_url('/asset/js/custom-with-date.js'); ?>"></script> <!-- Custom codes -->
  <script src="<?php echo base_url('/asset/js/jquery.form.min.js'); ?>"></script>
  <!-- jQuery Notification ends -->

	  <div class="matter">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="widget">   
            <div class="widget-head">
              <div class="pull-left">Add Employee</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="#" class="wsettings"><i class="fa fa-wrench"></i></a>  
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                  	<?php
                  		$attname=array(
                  				'name'=>'employee_name',
                  				//'value'=>$user->firstname,
                  				'class'=>'form-control'
                  			);

                  		$attt=array(
                  				'name'=>'t_lahir',
                  				//'value'=>$user->lastname,
                  				'class'=>'form-control'
                  			);

                  		$atttl=array(
                  				'name'=>'tl_lahir',
                  				//'value'=>$user->email_address,
                  				'class'=>'form-control'
                  			);

                  		$attemail=array(
                  				'name'=>'email',
                  				//'value'=>$user->username,
                  				'class'=>'form-control'
                  			);

                  		$attlokasi=array(
                  				'name'=>'lokasi',
                  				'class'=>'form-control'
                  			);

                      $attdivi=array(
                          'name'=>'divisi',
                          'class'=>'form-control'
                        );

                  		$attsubmit=array(
                  				'name'=>'empadd',
                          'id'=>'empadd',
                  				'content'=>'Add Employee',
                  				'class'=>'btn btn-primary btn-block'
                  			);
					           ?>

                      <?= form_open('','id=addEmployee') ?>
                          <fieldset>
                                <div class="form-group">
                                    <?= form_label('Full Name','employee_name') ?>
                                    <?= form_input($attname) ?>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Tempat Lahir','t_lahir') ?>
                                    <?= form_input($attt) ?>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Tanggal Lahir','tl_lahir') ?>
                                    <div id="datetimepicker1" class="input-append">
                                      <input data-format="yyyy-MM-dd" type="text" class="form-control dtpicker" name="tl_lahir">
                                      <span class="add-on">
                                        <i data-time-icon="fa fa-time" data-date-icon="fa fa-calendar" class="btn btn-info"></i>
                                      </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Email','email') ?>
                                    <?= form_input($attemail) ?>
                                </div>
                                 <div class="form-group">
                                    <?= form_label('Lokasi','lokasi') ?>
                                    <?= form_textarea($attlokasi) ?>
                                </div>
                                <div class="form-group">
                                      <?= form_label('Department Head','dephead_id') ?>
                                      <select name="dephead_id" class="form-control">
                                        <option value=0>== Pilih Dephead ==</option>
                                      <?php
                                        foreach($employee as $emp){
                                          echo '<option value='.$emp->id.'>'.$emp->employee_name.'</option>';
                                        }
                                      ?>
                                      </select>
                                </div>
                                <div class="form-group">
                                      <?= form_label('Division Head','divhead_id') ?>
                                      <select name="divhead_id" class="form-control">
                                        <option value=0>== Pilih Divhead ==</option>
                                      <?php
                                        foreach($employee as $emp){
                                          echo '<option value='.$emp->id.'>'.$emp->employee_name.'</option>';
                                        }
                                      ?>
                                      </select>
                                </div>
                                <!-- <div class="form-group">
                                      <?= form_label('Divisi','divisi') ?>
                                      <select name="divisi" class="form-control">
                                        <option value=0>== Pilih Divisi ==</option>
                                      <?php
                                        foreach($divisi as $divi){
                                          echo '<option value='.$divi->id.'>'.$divi->divisi_name.'</option>';
                                        }
                                      ?>
                                      </select>
                                </div>
                                <div class="form-group">
                                      <?= form_label('Level','level_id') ?>
                                      <select name="level_id" class="form-control">
                                        <option value=0>== Pilih Level ==</option>
                                      <?php
                                        foreach($ref_lv as $rf){
                                          echo '<option value='.$rf->id.'>'.$rf->ref_list_name.'</option>';
                                        }
                                      ?>
                                      </select>
                                </div>
                                <div class="form-group">
                                      <?= form_label('Group','group_id') ?>
                                      <select name="group_id" class="form-control">
                                        <option value=0>== Pilih Group ==</option>
                                      <?php
                                        foreach($ref_gr as $rf){
                                          echo '<option value='.$rf->id.'>'.$rf->ref_list_name.'</option>';
                                        }
                                      ?>
                                      </select>
                                </div> -->
	                            <?= form_button($attsubmit) ?>
                          	</fieldset>
                      	<?= form_close(); ?>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(function(){
    $("#empadd").click(function(){
      $.ajax({
        type:'POST',
        url:'./employee/add_proccess',
        data:$("#addEmployee").serialize(),
        success:function(data){
          data=jQuery.parseJSON(data);
          if(data.success){
            alert('Data Berhasil Tersimpan');
            $("#contentAjax").load('./employee');
          }else{
            alert(data.message);
          }
        }
      });
    });
  });
</script>