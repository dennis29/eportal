<?php
	Class reference_list_model extends CI_Model{
		public function get_references_list(){
			if(!empty($this->where)) $this->db->where($this->where);
			
			$q=$this->db->get('reference_list');
			return $q->result();
		}

		public function get_ref_list($creator_id){
			$this->db->where('ref_id',$creator_id);
			$q=$this->db->get('reference_list');
			if($q->num_rows()>0){
				return $q->result_array();
			}else{
				return array();
			}
		}
	}
?>