<?php
	class hcm_model extends CI_Model{
		public function get_hcm(){
			//$this->db->order_by('menus.label');
			$q=$this->db->select('hcm.id,
								  hcm.hcm_tab_id,
								  hcm.no_policy,
								  hcm.title,
								  hcm.desc_hcm,
								  hcm.attachment,
								  hcm.created,

								  hcm_tab.id as hcm_tab_id,
								  hcm_tab.tab_id,
								  hcm_tab.for_direct')
						->from('hcm')
						->join('hcm_tab','hcm_tab.id=hcm_tab_id','left')
						->get();
			return $q->result();
		}

		public function get_hcm_tab(){
			$q=$this->db->get('hcm_tab');
			return $q->result();
		}

		public function get_hcm_byid($data){
			$this->db->where($data);
			$q=$this->db->select('hcm.id,
								  hcm.hcm_tab_id,
								  hcm.no_policy,
								  hcm.title,
								  hcm.desc_hcm,
								  hcm.attachment,
								  hcm.created,

								  hcm_tab.id as hcm_tab_id,
								  hcm_tab.tab_id,
								  hcm_tab.for_direct')
						->from('hcm')
						->join('hcm_tab','hcm_tab.id=hcm_tab_id','left')
						->get();
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('hcm',$data);
			//echo $this->db->last_query();
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('hcm',$data);
			return $q;
		}

		public function delete($data){
			$this->db->delete('hcm',$data);
			return TRUE;
		}
	}
?>