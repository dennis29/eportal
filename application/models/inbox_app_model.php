<?php
	class inbox_app_model extends CI_Model{
		var $where =  array();
		var $where1 =  array();
		var $order_by =  array();
		var $or_where = array();
		var $limit = 0;
		var $offset = 0;
		var $doc_num = "";
		var $distinct = "";
		var $select = "";
		public function add($data3){
			$this->db->insert('inbox_app',$data3);
			return TRUE;
		}
		
		function update($data)
		{
			$this->db->update('inbox_app',$data, $this->where);
		}

		public function inbox(){

				//$this->limit; //start
				//$this->offset; //per page
				if(!empty($this->where)) $this->db->where($this->where);

				$var_emp=(isset($_GET["employee_id"])) ? $_GET["employee_id"] : "";
				$var_doc_num=(isset($_GET["doc_number"])) ? $_GET["doc_number"] : "";
				$var_search = "";
				
				if($var_emp!="") $var_search .= "AND `inbox_app`.`employee_id` = '".$var_emp."'";
				if($var_doc_num!="") $var_search .= "AND `inbox_app`.`doc_number` like '%".$var_doc_num."%'";
				
				if($this->doc_num!="") $var_search .= "AND `inbox_app`.`doc_number` like '%".$this->doc_num."%'";
				
				$limit = "";
				if($this->limit!=0 || $this->offset!=0)
				{
					$limit = "LIMIT ".$this->limit.", ".$this->offset;	
				}
				/*if($_SESSION['user_id']==557){
					echo $this->db->last_query();
					$where1=$this->inbox_app_model->where;
					if(!empty($where1)){
						foreach ($where1 as $where2) {
							//print_r($key);
							print_r($where2);
							echo 
							//echo $value->employee_id;
						}
					}
					
				}*/

				$user_id = $_SESSION['user_id'];
				$sesrole=$_SESSION['role'];
				$sesroleid=$_SESSION['role_id'];
				$employee_id=$_SESSION['employee_id'];
				$role="'".implode("','",$sesrole)."'";
				$role_id="'".implode("','",$sesroleid)."'";

				/*$this->it_service_model->offset=0;
				$data['it_services']=$this->it_service_model->get_it_services();
				//print_r($data['it_services']);
				foreach($data['it_services'] as $is){
					echo $is->doc_seq_id;
					echo 'status'.$is->status_app_id;
				}*/
				if(!empty($this->order_by))
				{
					$sql_order = $this->order_by[0]." ".$this->order_by[1];	
				}
				else
				{
					$sql_order = "`inbox_app`.`request_date` DESC, `inbox_app`.`doc_number` DESC";
				}
				$q=$this->db->query("SELECT `inbox_app`.`id`, 
											`inbox_app`.`approved_id`, 
											`inbox_app`.`sub_approved_id`, 
											`inbox_app`.`doc_id`, 
											`inbox_app`.`description`, 
											`inbox_app`.`employee_id`, 
											`inbox_app`.`priority_id`, 
											`inbox_app`.`app_sequence`, 
											`inbox_app`.`attachment`, 
											`inbox_app`.`app_memo`, 
											`inbox_app`.`request_date`, 
											`inbox_app`.`duedate`, 
											`inbox_app`.`doc_number`, 
											`inbox_app`.`created_time`, 
											`inbox_app`.`updated_time`, 

											`doc`.`id` as doc_id, 
											`doc`.`doc_name`, 
											`doc`.`doc_desc`,
											`doc`.`controller`,
											`doc`.`span`,

											`reference`.`id` as approved_id, 
											`reference`.`ref_name`, 
											`reference_list`.`id` as sub_approved_id, 
											`reference_list`.`ref_list_name`, 

											`employee`.`id` as employee_id, 
											`employee`.`employee_name`, 

											`priority`.`id` as priority_id, 
											`priority`.`priority_name`,

											case 
											when `doc`.`doc_name` = 'SOD'
											then `sod`.`doc_seq_id`
											when `doc`.`doc_name` = 'PLT'
											then `it_services`.`doc_seq_id`
											when `doc`.`doc_name` = 'PLK'
											then `it_services`.`doc_seq_id`
											when `doc`.`doc_name` = 'GA'
											then `ga_services`.`doc_seq_id`
											end as doc_seq_id,
											`it_services`.`status_app_id`

									FROM (`inbox_app`)
											
											JOIN `doc` ON `inbox_app`.`doc_id`=`doc`.`id`
											LEFT JOIN `reference` ON `inbox_app`.`approved_id`=`reference`.`id`
											LEFT JOIN `reference_list` ON `inbox_app`.`sub_approved_id`=`reference_list`.`id`
											JOIN `employee` ON `inbox_app`.`employee_id`=`employee`.`id`
											JOIN `priority` ON `inbox_app`.`priority_id`=`priority`.`id`
											LEFT JOIN `it_services` ON `inbox_app`.`doc_number`=`it_services`.`doc_seq_id`
											LEFT JOIN `sod` ON `inbox_app`.`doc_number`=`sod`.`doc_seq_id` 
											LEFT JOIN `ga_services` ON `inbox_app`.`doc_number`=`ga_services`.`doc_seq_id` 

									WHERE (`reference_list`.`ref_list_name` in ($role))
									OR
									((`inbox_app`.approved_id IN (5,6) AND `inbox_app`.sub_approved_id=".$employee_id.")
									OR (`inbox_app`.approved_id = 2 AND `inbox_app`.sub_approved_id IN (".$role_id."))
									OR (`inbox_app`.approved_id = 7 AND `inbox_app`.sub_approved_id=".$user_id."))
									AND
									(`it_services`.`status_app_id` in (2,3,4)
									OR `sod`.`status_app_id` in (2,3,4)
									OR `ga_services`.`status_app_id` in (2,3,4))
									".$var_search." 
									ORDER BY ".$sql_order."
									".$limit."
									");
				return $q->result();
		}


		public function get()
		{
				//$this->limit; //start
				//$this->offset; //per page
				if(!empty($this->where)) $this->db->where($this->where);

				
				if($this->limit!=0 || $this->offset!=0)
				{
					$this->db->limit($this->limit,$this->offset);	
				}
				
				if($this->distinct!="")
				{
					$this->db->distinct($this->distinct);	
				}

				if($this->select!="")
				{
					$this->db->select($this->select);	
				}
				
				if(!empty($this->order_by))
				{
					foreach($this->order_by as $k=>$v)
					{
						$this->db->order_by($v["sort"]." ".$v["dir"]);
					}
				}
				
				$this->db->from('inbox_app');
				$q=$this->db->get();
				$this->db->flush_cache();
				return $q->result();
		}

		public function inbox_nolimit(){

				$this->limit; //start
				$this->offset; //per page
				if(!empty($this->where)) $this->db->where($this->where);

				$user_id = $_SESSION['user_id'];
				$sesrole=$_SESSION['role'];
				$sesroleid=$_SESSION['role_id'];
				$employee_id=$_SESSION['employee_id'];
				$role="'".implode("','",$sesrole)."'";
				$role_id="'".implode("','",$sesroleid)."'";

				/*$this->it_service_model->offset=0;
				$data['it_services']=$this->it_service_model->get_it_services();
				//print_r($data['it_services']);
				foreach($data['it_services'] as $is){
					echo $is->doc_seq_id;
					echo 'status'.$is->status_app_id;
				}*/

				if(!empty($this->order_by))
				{
					$sql_order = $this->order_by[0]." ".$this->order_by[1];	
				}
				else
				{
					$sql_order = "`inbox_app`.`request_date` DESC, `inbox_app`.`doc_number` DESC";
				}

				$var_emp=(isset($_GET["employee_id"])) ? $_GET["employee_id"] : "";
				$var_doc_num=(isset($_GET["doc_number"])) ? $_GET["doc_number"] : "";
				$var_search = "";
				if($var_emp!="") $var_search .= "AND `inbox_app`.`employee_id` = '".$var_emp."'";
				if($var_doc_num!="") $var_search .= "AND `inbox_app`.`doc_number` like '%".$var_doc_num."%'";

				$q=$this->db->query("SELECT `inbox_app`.`id`, 
											`inbox_app`.`approved_id`, 
											`inbox_app`.`sub_approved_id`, 
											`inbox_app`.`doc_id`, 
											`inbox_app`.`description`, 
											`inbox_app`.`employee_id`, 
											`inbox_app`.`priority_id`, 
											`inbox_app`.`app_sequence`, 
											`inbox_app`.`attachment`, 
											`inbox_app`.`app_memo`, 
											`inbox_app`.`request_date`, 
											`inbox_app`.`duedate`, 
											`inbox_app`.`doc_number`, 
											`inbox_app`.`created_time`, 
											`inbox_app`.`updated_time`, 

											`doc`.`id` as doc_id, 
											`doc`.`doc_name`, 
											`doc`.`doc_desc`,
											`doc`.`controller`,

											`reference`.`id` as approved_id, 
											`reference`.`ref_name`, 
											`reference_list`.`id` as sub_approved_id, 
											`reference_list`.`ref_list_name`, 

											`employee`.`id` as employee_id, 
											`employee`.`employee_name`, 

											`priority`.`id` as priority_id, 
											`priority`.`priority_name`,

											case 
											when `doc`.`doc_name` = 'SOD'
											then `sod`.`doc_seq_id`
											when `doc`.`doc_name` = 'PLT'
											then `it_services`.`doc_seq_id`
											when `doc`.`doc_name` = 'PLK'
											then `it_services`.`doc_seq_id`
											when `doc`.`doc_name` = 'GA'
											then `ga_services`.`doc_seq_id`
											end as doc_seq_id,
											`it_services`.`status_app_id`

									FROM (`inbox_app`)
											
											JOIN `doc` ON `inbox_app`.`doc_id`=`doc`.`id`
											LEFT JOIN `reference` ON `inbox_app`.`approved_id`=`reference`.`id`
											LEFT JOIN `reference_list` ON `inbox_app`.`sub_approved_id`=`reference_list`.`id`
											JOIN `employee` ON `inbox_app`.`employee_id`=`employee`.`id`
											JOIN `priority` ON `inbox_app`.`priority_id`=`priority`.`id`
											LEFT JOIN `it_services` ON `inbox_app`.`doc_number`=`it_services`.`doc_seq_id`
											LEFT JOIN `sod` ON `inbox_app`.`doc_number`=`sod`.`doc_seq_id`
											LEFT JOIN `ga_services` ON `inbox_app`.`doc_number`=`ga_services`.`doc_seq_id`

									WHERE (`reference_list`.`ref_list_name` in ($role))
									OR
									((`inbox_app`.approved_id IN (5,6) AND `inbox_app`.sub_approved_id=".$employee_id.")
									OR (`inbox_app`.approved_id = 2 AND `inbox_app`.sub_approved_id IN (".$role_id."))
									OR (`inbox_app`.approved_id = 7 AND `inbox_app`.sub_approved_id=".$user_id."))
									AND
									(`it_services`.`status_app_id` in (2,3,4)
									OR `sod`.`status_app_id` in (2,3,4)
									OR `ga_services`.`status_app_id` in (2,3,4))
									".$var_search."
									ORDER BY ".$sql_order."
									");
				//echo $this->db->last_query();
				return $q->result();
		}

		public function inbox_up(){

				$this->limit; //start
				$this->offset; //per page

				$user_id = $_SESSION['user_id'];
				$sesrole=$_SESSION['role'];
				$sesroleid=$_SESSION['role_id'];
				$employee_id=$_SESSION['employee_id'];
				$role="'".implode("','",$sesrole)."'";
				$role_id="'".implode("','",$sesroleid)."'";

				/*$this->it_service_model->offset=0;
				$data['it_services']=$this->it_service_model->get_it_services();
				//print_r($data['it_services']);
				foreach($data['it_services'] as $is){
					echo $is->doc_seq_id;
					echo 'status'.$is->status_app_id;
				}*/

				$q=$this->db->query("SELECT `inbox_app`.`id`, 
											`inbox_app`.`approved_id`, 
											`inbox_app`.`sub_approved_id`, 
											`inbox_app`.`doc_id`, 
											`inbox_app`.`description`, 
											`inbox_app`.`employee_id`, 
											`inbox_app`.`priority_id`, 
											`inbox_app`.`app_sequence`, 
											`inbox_app`.`attachment`, 
											`inbox_app`.`app_memo`, 
											`inbox_app`.`request_date`, 
											`inbox_app`.`duedate`, 
											`inbox_app`.`doc_number`, 
											`inbox_app`.`created_time`, 
											`inbox_app`.`updated_time`, 

											`doc`.`id` as doc_id, 
											`doc`.`doc_name`, 
											`doc`.`doc_desc`,

											`reference`.`id` as approved_id, 
											`reference`.`ref_name`, 
											`reference_list`.`id` as sub_approved_id, 
											`reference_list`.`ref_list_name`, 

											`employee`.`id` as employee_id, 
											`employee`.`employee_name`, 

											`priority`.`id` as priority_id, 
											`priority`.`priority_name`,

											`it_services`.`doc_seq_id`,
											`it_services`.`status_app_id`

									FROM (`inbox_app`)
											
											JOIN `doc` ON `inbox_app`.`doc_id`=`doc`.`id`
											LEFT JOIN `reference` ON `inbox_app`.`approved_id`=`reference`.`id`
											LEFT JOIN `reference_list` ON `inbox_app`.`sub_approved_id`=`reference_list`.`id`
											JOIN `employee` ON `inbox_app`.`employee_id`=`employee`.`id`
											JOIN `priority` ON `inbox_app`.`priority_id`=`priority`.`id`
											JOIN `it_services` ON `inbox_app`.`doc_number`=`it_services`.`doc_seq_id`

									WHERE (`reference_list`.`ref_list_name` in ($role))
									OR (`inbox_app`.approved_id IN (5,6) AND `inbox_app`.sub_approved_id=".$employee_id.")
									OR (`inbox_app`.approved_id = 2 AND `inbox_app`.sub_approved_id IN (".$role_id."))
									OR (`inbox_app`.approved_id = 7 AND `inbox_app`.sub_approved_id=".$user_id.")
									
									ORDER BY `inbox_app`.`request_date`
									LIMIT $this->limit,$this->offset
									");
				//echo $this->db->last_query();
				return $q->result();
			
		}

		public function get_approve_byid($data){
			$this->db->where($data);
			$q=$this->db->get('inbox_app');
			$data=$q->first_row();
			return $data;
		}

		public function approve($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('inbox_app',$data);
			return $q;
		}

		public function max(){
			if(!empty($this->where)) $this->db->where($this->where);
			$this->db->select_max('sequence');
			$query = $this->db->get('approval_detail');
			return $query->result();
		}
	}
	
?>