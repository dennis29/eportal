<?php
	Class doc_sequence_model extends CI_Model{
		var $where = array();
		public function get_doc_seq(){
			if(!empty($this->where)) $this->db->where($this->where);
			$q=$this->db->get('doc_sequence');
			return $q->result();
		}

		public function edit($data2){
			$this->db->where('id',$data2['id']);
			$q=$this->db->update('doc_sequence',$data2);
			return $q;
		}
	}
?>