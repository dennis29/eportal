<?php
	Class approval_model extends CI_Model{

		var $where = array();
		var $offset = 0;
		var $employee_id = 0;

		public function get_approval(){

			if(!empty($this->where)) $this->db->where($this->where);
			if($this->offset!=0)($this->db->limit($this->offset,$this->limit));
			
			$q=$this->db->select('approval.id,
								  approval.approval_name,
								  approval.doc_id,
								  approval.default,
								  approval.seq_priority_id,
								  approval.created,
								  approval.creator_id,
								  approval.sub_creator_id,

								  doc.id as doc_id,
								  doc.doc_name,

								  reference.id as creator_id,
								  reference.ref_name,

								  case reference.id
								  when 2 
								  	   then (select id from roles where id = approval.sub_creator_id)
								  when 5 
								  	   then (select dephead_id from employee where id = '.$this->employee_id.')
								  when 6 
								  	   then (select divhead_id from employee where id = '.$this->employee_id.')
								  when 7 
								  	   then (select username from users where id = approval.sub_creator_id)
								  else reference_list.id
								  end as sub_creator_id,
								  case reference.id
								  when 2 
								  	   then (select role_name from roles where id = approval.sub_creator_id)
								  when 7 
								  	   then (select username from users where id = approval.sub_creator_id)
								  end as ref_list_name', FALSE)
						->from('approval')
						->join('doc','approval.doc_id=doc.id','left')
						->join('reference','approval.creator_id=reference.id','left')
						->join('reference_list','approval.sub_creator_id=reference_list.id','left')
						->get();
			return $q->result();
		}


		public function get_detail(){
			if(!empty($this->where)) $this->db->where($this->where);
			//if($this->offset!=0)($this->db->limit($this->offset,$this->limit));
			
			$q=$this->db->select('approval.id,
								  approval.approval_name,
								  approval.doc_id,
								  approval.default,
								  approval.seq_priority_id,
								  approval.created,
								  approval.creator_id,
								  approval.sub_creator_id')
						->from('approval')
						->get();
			return $q->result_array();
		}
		
		public function get_approval_byid($data){
			$this->db->where($data);
			$q=$this->db->get('approval');
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('approval',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('approval',$data);
			return $q;
		}

		public function delete($data){
			$this->db->delete('approval',$data);
			return TRUE;
		}

		public function get_ref(){
			$q=$this->db->get('reference');
			if($q->num_rows()>0){
				return $q->result_array();
			}else{
				return array();
			}
		}

		public function get_ref_list($creator_id){
			$this->db->where('id',$creator_id);
			$q=$this->db->get('reference_list');
			if($q->num_rows()>0){
				return $q->result_array();
			}else{
				return array();
			}
		}
	}
?>