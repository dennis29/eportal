<?php
	Class role_model extends CI_Model{
		var $offset=0;
		var $select = "";
		public function get_roles(){
			if($this->offset!=0)$this->db->limit($this->offset, $this->limit);

			$q=$this->db->get('roles');
			return $q->result();
		}

		public function get_roles_array(){
			if($this->offset!=0)$this->db->limit($this->offset, $this->limit);
			
			$this->db->select($this->select);
			$q=$this->db->order_by('role_name')->get('roles');
			return $q->result_array();
		}

		public function get_role_byid($data){
			$this->db->where($data);
			$q=$this->db->get('roles');
			$data=$q->first_row();
			return $data;
		}

		public function get_role_only($data){
			$this->db->where($data);
			$q=$this->db->get('roles');
			$data=$q->result_array();
			return $data;
		}

		public function add($data){
			$this->db->insert('roles',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('roles',$data);
			return $q;
		}

		public function get_role_menu_byid($data){
			$this->db->where($data);
			$this->db->select('role_menu.id,
							   role_menu.role_id,
							   role_menu.menu_id,
							   role_menu.create,
							   role_menu.read,
							   role_menu.update,
							   role_menu.delete,
							   roles.id as role_id,
							   roles.role_name');
			$this->db->from('role_menu');
			$this->db->join('roles','role_menu.role_id=roles.id','left');
			//$this->db->limit('offset');
			$q=$this->db->get();
			//$q=$this->db->get('roles');
			$data=$q->result_array();
			return $data;
		}

		public function edit_access_post($data){
			if(!empty($data['id'])){
				$this->db->where('id',$data['id']);
				$q=$this->db->update('role_menu',$data);
				return $q;
			}else{
				$this->db->insert('role_menu',$data);
				return TRUE;
			}
		}

	}
?>