<?php
	Class user_model extends CI_Model{
		var $where = array();
		var $offset=0;
		var $limit=0;
		public function get_users(){
			if(!empty($this->where)) $this->db->where($this->where);
			$this->db->select('users.id,
							   users.username,
							   users.password,
							   users.employee_id,	
							   users.is_activated,				   
							   
							   roles.role_name,

							   employee.id,
							   employee.employee_name')
					  ->from('users')
					  ->order_by('users.username')
					  ->join('employee','employee.id=users.employee_id','left')
					  ->join('user_role','users.id=user_role.user_id')
					  ->join('roles','roles.id=user_role.role_id','left');
			$q=$this->db->get();
			return $q->result();
		}

		public function get()
		{
				//$this->limit; //start
				//$this->offset; //per page
				if(!empty($this->where)) 
					$this->db->where($this->where);

				
				if($this->limit!=0 || $this->offset!=0)
				{
					$this->db->limit($this->limit,$this->offset);	
				}
				
				if(!empty($this->order_by))
				{
					foreach($this->order_by as $k=>$v)
					{
						$this->db->order_by($v["sort"].",".$v["dir"]);
					}
				}
				
				$this->db->from('users');
				$q=$this->db->get();
				//echo $this->db->last_query();
				$this->db->flush_cache();
				$this->where = array();
				$this->offset=0;
				$this->limit=0;
				
				
				return $q->result();
		}

		public function send_mail(){
			if($this->limit>0) $this->db->limit($this->limit);
			if(!empty($this->where)) $this->db->where($this->where);
			$this->db->select('users.id,
							   users.username,
							   users.password,
							   users.employee_id,	
							   users.is_activated,				   
							   
							   roles.id as role_id,
							   roles.role_name,

							   employee.id,
							   employee.employee_name,
							   employee.email,
							   employee.dephead_id,
							   employee.divhead_id,
							   employee.level_id,
							   employee.group_id')
					  ->from('users')
					  //->order_by('users.id')
					  //->group_by('users.id')
					  //->having('COUNT(*) > 1')
					  ->join('employee','employee.id=users.employee_id','left')
					  ->join('user_role','users.id=user_role.user_id')
					  ->join('roles','roles.id=user_role.role_id','left');
			$q=$this->db->get();
//			echo $this->db->last_query();
			return $q->result();
		}

		public function get_usr(){
			
			if($this->offset!=0) $this->db->limit($this->offset , $this->limit);
			if(!empty($this->where)) $this->db->where($this->where);
			$this->db->select('users.id,
							   users.username,
							   users.password,
							   users.employee_id,
							   users.is_activated,				   

							   employee.id as employee_id,
							   employee.employee_name')
					  ->from('users')
					  ->order_by('users.username')
					  ->join('employee','employee.id=users.employee_id','left');
			$q=$this->db->get();
			return $q->result();
		}

		public function get_users_array(){
			if($this->offset!=0)$this->db->limit($this->offset, $this->limit);
			if(!empty($this->where)) $this->db->where($this->where);
			$this->db->select($this->select);
			$q=$this->db->order_by('username')->get('users');
			return $q->result_array();

			echo $this->db->last_query();
		}

		public function get_user_byid($data){
			$this->db->where($data);
			$q=$this->db->get('users');
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('users',$data);
			return TRUE;
		}

		public function get_roles(){
			$q=$this->db->get('roles');
			return $q->result();
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('users',$data);
			return $q;
		}

		public function delete($data){
			$this->db->delete('users',$data);
			return TRUE;
		}

		public function change_password($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('users',$data);
			return $q;
		}

		public function reset_pass($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->query('UPDATE users SET password="'.md5(itgos2012).'" WHERE id='.$data['id'].'');
			//echo $this->db->last_query();
			return $q;
		}

		public function activated(){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('users',$data);
			return $q;
		}

		public function get_profile(){
			$q=$this->db->select('users.id,
								  users.username,
								  users.employee_id,

								  employee.id,
								  employee.employee_name,
								  employee.t_lahir,
								  employee.tl_lahir,
								  employee.email,
								  employee.lokasi,
								  employee.level_id,
								  employee.group_id,
								  employee.divisi,

								  lv.ref_list_name as level_id,
								  gr.ref_list_name as group_id,

								  divisi.id as divisi,
								  divisi.divisi_name')

						->from('users')
						->join('employee','users.employee_id=employee.id','left')
						->join('reference_list as lv','lv.id=employee.level_id','left')
						->join('reference_list as gr','gr.id=employee.group_id','left')
						->join('divisi','employee.divisi=divisi.id','left')
						->get();
			return $q->result();
		}
	}

?>