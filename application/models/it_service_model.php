<?php
	Class it_service_model extends CI_Model{

		/*public function all(){
			$q=$this->db->get('it_services');
			return $q->result();
		}*/
		
		var $where = array();
		var $offset=0;

		public function count_all_it_services(){
		
			if(!empty($this->where)) $this->db->where($this->where);
//			if($this->offset!=0) $this->db->limit($this->offset , $this->limit);

			$username=$_SESSION['username'];

			$this->db->select('count(*) as total');

			$this->db->from('it_services');
			$this->db->join('employee','it_services.employee_id=employee.id','left');
			$this->db->join('doc','it_services.doc_id=doc.id','left');
			$this->db->join('problem','it_services.problem_id=problem.id','left');
			$this->db->join('priority','it_services.priority_id=priority.id','left');
			$this->db->join('status_app','it_services.status_app_id=status_app.id','left');
			$this->db->join('users','users.employee_id=employee.id','left');
			//$this->db->limit('10','0');
			$q=$this->db->get();
			return $q->result();
			//$q=$this->db->get('it_services');
			//return $q->result();
		}


		public function get_it_services(){
		
			if(!empty($this->where)) $this->db->where($this->where);
			if($this->offset!=0) $this->db->limit($this->offset , $this->limit);

			$username=$_SESSION['username'];

			$this->db->select('it_services.id,
							   it_services.doc_id,
							   it_services.doc_seq_id,
							   it_services.problem_id,
							   it_services.created_for,
							   it_services.created_by,
							   it_services.description,
							   it_services.employee_id,
							   it_services.priority_id,
							   it_services.attachment,
							   it_services.request_date,
							   it_services.duedate,
							   it_services.complete,

							   employee.id as employee_id,
							   employee.employee_name,

							   doc.id as doc_id,
							   doc.doc_name,
							   
							   problem.id as problem_id,
							   problem.problem_name,

							   priority.id as priority_id,
							   priority.priority_name,

							   status_app.id as status_app_id,
							   status_app.status_app_name,
							   status_app.class_status,

							   users.username,
							   users.employee_id
							   ');

			$this->db->from('it_services');
			$this->db->join('employee','it_services.employee_id=employee.id','left');
			$this->db->join('doc','it_services.doc_id=doc.id','left');
			$this->db->join('problem','it_services.problem_id=problem.id','left');
			$this->db->join('priority','it_services.priority_id=priority.id','left');
			$this->db->join('status_app','it_services.status_app_id=status_app.id','left');
			$this->db->join('users','users.employee_id=employee.id','left');
			$this->db->order_by('it_services.id DESC');
			//$this->db->limit('10','0');
			$q=$this->db->get();
			return $q->result();
			//$q=$this->db->get('it_services');
			//return $q->result();
		}

		public function add_itservice_byrole()
		{
			$sesrole=$_SESSION['role'];
			$seslevel= (!empty($_SESSION['level'])) ? $_SESSION['level'] : 0;
			$sesgroup= (!empty($_SESSION['group'])) ? $_SESSION['group'] : 0;
			$sesuser = $_SESSION['username'];
			$user_id = $_SESSION['user_id'];
			$role_id = $_SESSION['role_id'];
			$employee_id = $_SESSION['employee_id'];
			$role_id=implode(",",$role_id);
			
			$role="'".implode("','",$sesrole)."'";

			$docnya=$this->input->post('doc_id');

			//approval ada
			$q=$this->db->query("SELECT `approval`.`id`, 
										`approval`.`approval_name`, 
										`approval`.`doc_id`, 
										`approval`.`default`,
										`approval`.`seq_priority_id`, 
										`approval`.`created`,
										
										 case
										 when `approval`.`creator_id` = 2
										 then 
										 	(select id from roles where id = `approval_detail`.`subcreator_id`)
										 when `approval`.`creator_id` = 5 
								  	   	 then (select dephead_id from employee where id = ".$employee_id.")
										 when `approval`.`creator_id` = 6 
										  	   then (select divhead_id from employee where id = ".$employee_id.")
										 when `approval`.`creator_id` = 7
										 then 
										 	(select det.id from users det where det.id = ".$user_id.")
										 else
										 	0
										 end as `sub_creator_id`,
										`approval`.`creator_id` as tipe,
										`approval`.`sub_creator_id` as id_creator,
										`approval_detail`.`approval_id`,
										`approval_detail`.`creator_id`, 
										 case
										 when `approval_detail`.`creator_id` = 2
										 then 
										 	(select id from roles where id = `approval_detail`.`subcreator_id`)
										 when `approval_detail`.`creator_id` = 5
										 then 
											(select dephead_id from employee where id = ".$employee_id.")
										 when `approval_detail`.`creator_id` = 6
										 then 
										 	(select divhead_id from employee where id = ".$employee_id.")
										 else 
										 	`approval_detail`.`subcreator_id`
										 end as `subcreator_id`,
										`approval_detail`.`sequence`,
										`doc`.`id` as doc_id, 
										`doc`.`doc_name`
								  FROM (`approval`) 
								  INNER JOIN `approval_detail` ON `approval_detail`.`approval_id`=`approval`.`id`
								  INNER JOIN `doc` ON `approval`.`doc_id`=`doc`.`id` 
								  WHERE `approval`.`doc_id` = '$docnya'
								  AND `approval`.`default` = 0
								  AND `approval`.`creator_id` <> 0
								  AND `approval`.`sub_creator_id` <> 0
								  AND `approval`.`sub_creator_id` = 
								  case
										 when `approval`.`creator_id` = 2
										 then 
										 	(select id from roles where id = `approval`.`sub_creator_id`)
										 when `approval`.`creator_id` = 5 
								  	   	 	then (select dephead_id from employee where id = ".$employee_id.")
										 when `approval`.`creator_id` = 6 
									     	then (select divhead_id from employee where id = ".$employee_id.")
										 when `approval`.`creator_id` = 7
										 	then 
										 	(select det.id from users det where det.id = ".$user_id.")
								  end
								  ORDER BY `approval`.`seq_priority_id`, `approval_detail`.`sequence` LIMIT 1
								  ");
			//echo 'ada gak?' .$q->num_rows();
			//echo $q->num_rows();
			if($q->num_rows()!=0 ){
				/*echo 'satu';
				echo '<pre>';
				echo $this->db->last_query();
				print_r($q->result());
				echo '</pre>';*/				
				return $q->result();
			}else{
				//echo 'dua';

				$p=$this->db->query("SELECT `approval`.`id`, 
										`approval`.`approval_name`, 
										`approval`.`doc_id`,
										`approval`.`default`,
										`approval`.`seq_priority_id`, 
										`approval`.`created`,

										`approval_detail`.`sequence`,

										`doc`.`id` as doc_id, 
										`doc`.`doc_name`,

										`approval_detail`.`id` as `approval_id`,
										`approval_detail`.`creator_id`, 

										case
										 when `approval_detail`.`creator_id` = 2
										 then 
										 	(select id from roles where id = `approval_detail`.`subcreator_id`)
										 when `approval_detail`.`creator_id` = 5
										 then 
											(select dephead_id from employee where id = ".$employee_id.")
										 when `approval_detail`.`creator_id` = 6
										 then 
										 	(select divhead_id from employee where id = ".$employee_id.")
										 else 
										 	`approval_detail`.`subcreator_id`
										 end as `subcreator_id`

								  FROM (`approval`) 

								  LEFT JOIN `doc` ON `approval`.`doc_id`=`doc`.`id` 	
								  LEFT JOIN `approval_detail` ON `approval_id`=`approval`.`id`
								  
								  WHERE `approval`.`doc_id` = '$docnya'
								  AND `approval`.`default`=1

								  ORDER BY `approval`.`seq_priority_id`, `approval_detail`.`sequence` LIMIT 1");

				//echo $this->db->last_query().$p->num_rows();
				//return $p->result();
				
				if($p->num_rows()==0)
				{
					//echo 'tiga';
					$r = $this->db->query("SELECT `approval`.`id`, 
											`approval`.`approval_name`, 
											`approval`.`doc_id`,
											`approval`.`default`,
											`approval`.`seq_priority_id`, 
											`approval`.`created`,
											 case
										 when `approval`.`creator_id` = 2
										 then 
										 	(select det.id from roles det where det.id in (".$role_id."))
										 
										 when `approval`.`creator_id` = 7
										 then 
										 	(select det.id from users det where det.id = ".$user_id.")
											 else
												0
											 end as `sub_creator_id`,
	
											`approval_detail`.`approval_id`,
											`approval_detail`.`creator_id`, 
											`approval_detail`.`subcreator_id`,
											`approval_detail`.`sequence`,
	
											`doc`.`id` as doc_id, 
											`doc`.`doc_name`,
	
											`approval_detail`.`id` as `approval_id`,
											`approval_detail`.`creator_id`, 
											(select dephead_id from employee where id = (select employee_id from users where id = ".$user_id.")) as `subcreator_id`
	
									  FROM (`approval`) 
	
									  LEFT JOIN `doc` ON `approval`.`doc_id`=`doc`.`id` 	
									  LEFT JOIN `approval_detail` ON `approval_id`=`approval`.`id`
									  LEFT JOIN `reference` ON `approval_detail`.`creator_id`=`reference`.`id` 
									  LEFT JOIN `reference_list` ON `approval_detail`.`subcreator_id`=`reference_list`.`id` 
									  WHERE `approval`.`doc_id` = '$docnya'
									  AND `approval`.`default`=0
									  AND `reference`.`id` = 5
									  AND `approval`.`creator_id` <> 0
									  AND `approval`.`sub_creator_id` <> 0
									  AND `approval`.`sub_creator_id` = 
									  case
										 when `approval`.`creator_id` = 2
										 then 
										 	(select id from roles where id = `approval_detail`.`subcreator_id`)
										 when `approval`.`creator_id` = 5 
								  	   	 then (select dephead_id from employee where id = ".$employee_id.")
										 when `approval`.`creator_id` = 6 
										  	   then (select divhead_id from employee where id = ".$employee_id.")
										 when `approval`.`creator_id` = 7
										 then 
										 	(select det.id from users det where det.id = ".$user_id.")
									  end
									  ORDER BY `approval`.`seq_priority_id`,`approval_detail`.`sequence` LIMIT 1");
					
					if($r->num_rows()== 0)
					{
						//echo 'empat';
						$s = $this->db->query("SELECT `approval`.`id`, 
												`approval`.`approval_name`, 
												`approval`.`doc_id`,
												`approval`.`default`,
												`approval`.`seq_priority_id`, 
												`approval`.`created`,
		
												case
												 when `approval`.`creator_id` = 2
												 then 
												 	(select id from roles where id = `approval_detail`.`subcreator_id`)
												 when `approval`.`creator_id` = 5 
										  	   	 then (select dephead_id from employee where id = ".$employee_id.")
												 when `approval`.`creator_id` = 6 
												  	   then (select divhead_id from employee where id = ".$employee_id.")
												 when `approval`.`creator_id` = 7
												 then 
												 	(select det.id from users det where det.id = ".$user_id.")
												 else
													0
												 end as `sub_creator_id`,
												`approval_detail`.`approval_id`,
												`approval_detail`.`creator_id`, 
												`approval_detail`.`subcreator_id`,
												`approval_detail`.`sequence`,
		
												`doc`.`id` as doc_id, 
												`doc`.`doc_name`,
		
												`approval_detail`.`id` as `approval_id`,
												`approval_detail`.`creator_id`, 
												(select divhead_id from employee where id = (select employee_id from users where id = ".$user_id.")) as `subcreator_id`
		
										  FROM (`approval`) 
		
										  LEFT JOIN `doc` ON `approval`.`doc_id`=`doc`.`id` 	
										  LEFT JOIN `approval_detail` ON `approval_id`=`approval`.`id`
										  LEFT JOIN `reference` ON `approval_detail`.`creator_id`=`reference`.`id` 
										  LEFT JOIN `reference_list` ON `approval_detail`.`subcreator_id`=`reference_list`.`id` 
										  WHERE `approval`.`doc_id` = '$docnya'
										  AND `approval`.`default`=0
										  AND `reference`.`id` = 6
										  AND `approval`.`creator_id` <> 0
										  AND `approval`.`sub_creator_id` <> 0
										  AND `approval`.`sub_creator_id` = 
										  case
										 when `approval`.`creator_id` = 2
										 then 
										 	(select id from roles where id = `approval_detail`.`subcreator_id`)
										 when `approval`.`creator_id` = 5 
								  	   	 then (select dephead_id from employee where id = ".$employee_id.")
										 when `approval`.`creator_id` = 6 
										  	   then (select divhead_id from employee where id = ".$employee_id.")
										 when `approval`.`creator_id` = 7
										 then 
										 	(select det.id from users det where det.id = ".$user_id.")
										  end
										  ORDER BY `approval`.`seq_priority_id`,`approval_detail`.`sequence` LIMIT 1");
						return $s->result();
					}
					return $r->result();
					//echo 'ada';
				}
				else
				{
					return $p->result();	
					//echo 'ada2';			
				}
			}
			//echo 'ada';
			/*die();
			$this->db->last_query();
			return $q->result();*/
		}

		public function get_it_service_byid($data){
			$this->db->where($data);
			$this->db->select('it_services.id,
							   it_services.doc_id,
							   it_services.doc_seq_id,
							   it_services.problem_id,
							   it_services.created_for,
							   it_services.created_by,
							   it_services.description,
							   it_services.employee_id,
							   it_services.priority_id,
							   it_services.attachment,
							   it_services.request_date,
							   it_services.duedate,
							   it_services.complete,

							   employee.id as employee_id,
							   employee.employee_name,

							   doc.id as doc_id,
							   doc.doc_name,
							   
							   problem.id as problem_id,
							   problem.problem_name,

							   priority.id as priority_id,
							   priority.priority_name,

							   status_app.id as status_app_id,
							   status_app.status_app_name,
							   status_app.class_status,

							   users.username,
							   users.employee_id
							   ');

			$this->db->from('it_services');
			$this->db->join('employee','it_services.employee_id=employee.id','left');
			$this->db->join('doc','it_services.doc_id=doc.id','left');
			$this->db->join('problem','it_services.problem_id=problem.id','left');
			$this->db->join('priority','it_services.priority_id=priority.id','left');
			$this->db->join('status_app','it_services.status_app_id=status_app.id','left');
			$this->db->join('users','users.employee_id=employee.id','left');
			$this->db->order_by('it_services.id');
			//$this->db->limit('10','0');
			$q=$this->db->get();
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('it_services',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('it_services',$data);
			return $q;
		}

		public function edit_status($data_stat){
			$this->db->where('doc_seq_id',$data_stat['doc_seq_id']);
			
			$q=$this->db->update('it_services',$data_stat);
			return $q;
		}

		public function delete($data){
			$this->db->delete('it_services',$data);
			return TRUE;
		}
	}
?>