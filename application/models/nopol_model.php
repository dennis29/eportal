<?php
	Class nopol_model extends CI_Model{
		var $where = array();
		var $offset=0;
		var $limit=0;

		public function get_nopol(){
			if(!empty($this->limit)) $this->db->limit($this->limit);
			if(!empty($this->where)) $this->db->where($this->where);
			$q=$this->db->get('police_number');
			return $q->result();
		}
	}
?>