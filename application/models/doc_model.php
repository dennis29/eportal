<?php
	Class doc_model extends CI_Model{
		var $where = array();
		var $where_in = array();
		public function get_docs(){
			if(!empty($this->where))
				$this->db->where($this->where);
			if(!empty($this->where_in))
				$this->db->where_in($this->where_in[0],$this->where_in[1]);
			$q=$this->db->get('doc');
			return $q->result();
		}
	}
?>