<?php
	Class role_menu_model extends CI_Model
	{
		var $where_in = array();
		public function get_role_menu(){
			$this->db->select('role_menu.id,
							   role_menu.role_id,
							   role_menu.menu_id,
							   role_menu.create,
							   role_menu.read,
							   role_menu.update,
							   role_menu.delete,
							   roles.id as role_id,
							   roles.role_name,
							   menus.id as menu_id,
							   menus.label'

				);
			$this->db->from('role_menu');
			$this->db->order_by('role_menu.id');
			$this->db->join('roles','role_menu.role_id=roles.id','left');
			$this->db->join('menus','role_menu.menu_id=menus.id','left');
			$q=$this->db->get();
			return $q->result();
		}

		public function get_role_menu_by($where){
			$this->db->where($where);
			if(!empty($this->where_in))
			{
				$this->db->where_in($this->where_in[0],$this->where_in[1]);	
			}
			$this->db->from('role_menu');
			$q=$this->db->get();
			//$q=$this->db->get('roles');
			$data=$q->result_array();
			return $data;
		}

		public function edit_access_post($where,$data){
			$this->db->where($where);
			$q=$this->db->update('role_menu',$data);
			return $q;
		}

		public function create_access_post($where,$data){
			$this->db->insert('role_menu',$data);
			return TRUE;
		}
	}
?>