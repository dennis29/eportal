<?php
	Class sod_model extends CI_Model{
		var $where = array();
		var $offset = 0;
		var $limit = 0;
		var $order_by = "";
		public function get_sod(){
			//if(!empty($this->where)) $this->db->where($this->where);
			$where2=array();

			if($this->offset!=0)($this->db->limit($this->offset,$this->limit));
			if(!empty($this->where)){
				foreach($this->where as $k=>$v){
					if($k=='id'){
						$where2['employee.id']=$v;
					}else{
						$where2[$k]=$v;
					}
				}
				$this->db->where($where2);
			}

			if(!empty($this->like)) $this->db->like($this->like);

			if($this->order_by=="") $this->order_by = 'request_date DESC,doc_seq_id DESC';

			$q=$this->db->select('sod.id,
							      sod.doc_id,
							      sod.employee_id,
							      sod.client_sod_id,
							      sod.effective_date,
							      sod.sbu_id,
							      sod.nrk,
							      sod.name_sod,
							      sod.position_sod,
							      sod.status_sod_id,
							      sod.status_app_id,
							      sod.rekomendasi_sod_id,
							      sod.start_datefrom,
							      sod.start_dateto,
							      sod.reason,
							      sod.pic_pam_id,
							      sod.department,
							      sod.priority_id,
							      sod.doc_seq_id,
							      sod.attachment_sod,
							      sod.request_date,
							      sod.created_by,
							      sod.doc_print,
							      sod.print_status,

							      employee.employee_name,

							      doc.id as doc_id,
							 	  doc.doc_name,


							 	  sbu.id as sbu_id,
							 	  sbu.sbu_name,

							 	  status_sod.id as status_sod_id,
							 	  status_sod.status_sod_name,

							 	  status_app.id as status_app_id,
							 	  status_app.status_app_name,
							 	  status_app.class_status,

							 	  rekomendasi_sod.id as rekomendasi_sod_id,
							 	  rekomendasi_sod.rekomendasi_sod_name')
						->from('sod')
						->join('doc','sod.doc_id=doc.id','left')
						->join('employee','sod.employee_id=employee.id','left')
						->join('sbu','sod.sbu_id=sbu.id','left')
						->join('status_sod','sod.status_sod_id=status_sod.id','left')
						->join('status_app','sod.status_app_id=status_app.id','left')
						->join('rekomendasi_sod','sod.rekomendasi_sod_id=rekomendasi_sod.id','left')
						->order_by($this->order_by)
						->get();
			return $q->result();
			//echo $this->db->last_query();
		}

		public function add($data){
			$this->db->insert('sod',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('sod',$data);
			return $q;
		}

		public function get_sod_byid($data){
			$this->db->where($data);
			$q=$this->db->select('sod.id,
							      sod.doc_id,
							      sod.employee_id,
							      sod.client_sod_id,
							      sod.effective_date,
							      sod.sbu_id,
							      sod.nrk,
							      sod.name_sod,
							      sod.position_sod,
							      sod.status_sod_id,
							      sod.status_app_id,
							      sod.rekomendasi_sod_id,
							      sod.start_datefrom,
							      sod.start_dateto,
							      sod.reason,
							      sod.pic_pam_id,
							      sod.department,
							      sod.priority_id,
							      sod.doc_seq_id,
							      sod.attachment_sod,
							      sod.request_date,
							      sod.created_by,
							      sod.doc_print,
							      sod.print_status,

							      employee.id as employee_id,
							      employee.employee_name,

							      doc.id as doc_id,
							 	  doc.doc_name,

							 	  sbu.id as sbu_id,
							 	  sbu.sbu_name,

							 	  status_sod.id as status_sod_id,
							 	  status_sod.status_sod_name,

							 	  status_app.id as status_app_id,
							 	  status_app.status_app_name,
							 	  status_app.class_status,

							 	  rekomendasi_sod.id as rekomendasi_sod_id,
							 	  rekomendasi_sod.rekomendasi_sod_name,
								  
								  inbox_app.app_memo')
						->from('sod')
						->join('doc','sod.doc_id=doc.id','left')
						->join('employee','sod.employee_id=employee.id','left')
						->join('sbu','sod.sbu_id=sbu.id','left')
						->join('status_sod','sod.status_sod_id=status_sod.id','left')
						->join('status_app','sod.status_app_id=status_app.id','left')
						->join('rekomendasi_sod','sod.rekomendasi_sod_id=rekomendasi_sod.id','left')
						->join('inbox_app','sod.doc_seq_id=inbox_app.doc_number','left')
						->order_by('request_date DESC')
						->get();
			$data=$q->first_row();
			return $data;
		}

		public function edit_status($data_stat){
			$this->db->where('doc_seq_id',$data_stat['doc_seq_id']);
			
			$q=$this->db->update('sod',$data_stat);
			return $q;
		}

		public function delete($data){
			$this->db->delete('sod',$data);
			return TRUE;
		}

		public function update_print_status($data){
			$this->db->where('id',$_GET['id']);
			$q=$this->db->update('sod',$data);
			echo $this->db->last_query();
			return $q;
		}
		
	}
?>