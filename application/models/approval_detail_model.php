<?php
	Class approval_detail_model extends CI_Model{

		var $where = array();
		var $offset = 0;
		var $employee_id = 0;

		public function get_detail(){
			if(!empty($this->where)) $this->db->where($this->where);
			if($this->offset!=0)($this->db->limit($this->offset,$this->limit));

			$q=$this->db->select('approval_detail.id,
								  approval_detail.sequence,
								  approval_detail.mandatory,
								  approval_detail.creator_id,
								  approval_detail.subcreator_id,
								  approval_detail.created,

								  approval.id as approval_id,
								  approval.approval_name,

								  reference.id as creator_id,
								  reference.ref_name,
								  
								  case reference.id
								  when 2 
								  	   then (select id from roles where id = approval_detail.subcreator_id)
								  when 5 
								  	   then (select dephead_id from employee where id = '.$this->employee_id.')
								  when 6 
								  	   then (select divhead_id from employee where id = '.$this->employee_id.')
								  when 7 
								  	   then (select id from users where id = approval_detail.subcreator_id)
								  else reference_list.id
								  end as subcreator_id,
								  case reference.id
								  when 2 
								  	   then (select role_name from roles where id = approval_detail.subcreator_id)
								  when 7 
								  	   then (select username from users where id = approval_detail.subcreator_id)
								  else reference_list.ref_list_name
								  end as ref_list_name', FALSE)
						->from('approval_detail')
						->join('approval','approval_detail.approval_id=approval.id','left')
						->join('reference','approval_detail.creator_id=reference.id','left')
						->join('reference_list','approval_detail.subcreator_id=reference_list.id','left')
						->get();
			return $q->result();
		}

		public function just_seq(){
			$q=$this->db->get('approval_detail');
			return $q->result();
		}

		public function get_approval_detail_byid($data){
			$this->db->where($data);
			$q=$this->db->get('approval_detail');
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('approval_detail',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('approval_detail',$data);
			return $q;
		}

		public function delete($data){
			$this->db->delete('approval_detail',$data);
			return TRUE;
		}
	}
?>