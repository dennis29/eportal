<?php
	class employee_model extends CI_Model{

		var $offset = 0;
		var $where = array();
		var $limit = 0;

		public function get_employee(){
			$where2=array();

			if($this->offset!=0)($this->db->limit($this->offset,$this->limit));
			if(!empty($this->where)){
				foreach($this->where as $k=>$v){
					if($k=='id'){
						$where2['emp.id']=$v;
					}else{
						$where2[$k]=$v;
					}
				}
				$this->db->where($where2);
			}

			$this->db->select('emp.id,
							   emp.employee_name,
							   emp.nik,
							   emp.t_lahir,
							   emp.tl_lahir,
							   emp.email,
							   emp.lokasi,
							   emp.divisi,
							   emp.level_id,
							   emp.group_id,
							   (select employee_name from employee where id=emp.dephead_id) as dep_name,
							   (select employee_name from employee where id=emp.divhead_id) as div_name,

							   reference_list.ref_list_name as level_id,
							   gr.ref_list_name as group_id,

							   divisi.id as divisi,
							   divisi.divisi_name');
			$this->db->from('employee as emp');
			$this->db->order_by('employee_name');
			$this->db->join('divisi','emp.divisi=divisi.id','left');
			$this->db->join('reference_list','reference_list.id=emp.level_id','left');
			$this->db->join('reference_list as gr','gr.id=emp.group_id','left');
			$q=$this->db->get();
			return $q->result();
		}

		public function get()
		{
				//$this->limit; //start
				//$this->offset; //per page
				if(!empty($this->where)) $this->db->where($this->where);

				
				if($this->limit!=0 || $this->offset!=0)
				{
					$this->db->limit($this->limit,$this->offset);	
				}
				
				if(!empty($this->order_by))
				{
					foreach($this->order_by as $k=>$v)
					{
						$this->db->order_by($v["sort"].",".$v["dir"]);
					}
				}
				
				$this->db->from('employee');
				$q=$this->db->get();
				return $q->result();
		}

		public function get_level(){
			$this->db->select('employee.id,
							   employee.level_id,

							   reference_list.id as level_id,
							   reference_list.ref_list_name');
			$this->db->from('employee');
			$this->db->order_by('id');
			$this->db->join('reference_list','reference_list.id=employee.level_id','left');
			$q=$this->db->get();
			return $q->result();
		}

		public function get_group(){
			$this->db->select('employee.id,
							   employee.group_id,

							   reference_list.id as group_id,
							   reference_list.ref_list_name');
			$this->db->from('employee');
			$this->db->order_by('id');
			$this->db->join('reference_list','reference_list.id=employee.group_id','left');
			$q=$this->db->get();
			return $q->result();
		}

		public function get_employee_byid($data){
			$this->db->where($data);
			$q=$this->db->get('employee');
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('employee',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('employee',$data);
			return $q;
		}

		public function delete($data){
			$this->db->delete('employee',$data);
			return TRUE;
		}
	}
?>