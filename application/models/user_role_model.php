<?php
	Class user_role_model extends CI_Model{

		var $offset=0;
		var $limit = 0;
		var $where = array();

		public function get_user_role(){
			if($this->offset!=0)$this->db->limit($this->offset,$this->limit);
			if(!empty($this->where)) $this->db->where($this->where);

			$this->db->select('user_role.id,
							   user_role.user_id,
							   user_role.role_id,
							   users.id as user_id,
							   users.username,
							   roles.id as role_id,
							   roles.role_name,

							   employee.employee_name');
			$this->db->from('user_role');
			$this->db->order_by('users.username');
			$this->db->join('users','user_role.user_id=users.id','left');
			$this->db->join('roles','user_role.role_id=roles.id','left');
			$this->db->join('employee','employee.id=users.employee_id','left');
			$q=$this->db->get();
			return $q->result();
		}

		public function get()
		{
				//$this->limit; //start
				//$this->offset; //per page
				if(!empty($this->where)) $this->db->where($this->where);

				
				if($this->limit!=0 || $this->offset!=0)
				{
					$this->db->limit($this->limit,$this->offset);	
				}
				
				if(!empty($this->order_by))
				{
					foreach($this->order_by as $k=>$v)
					{
						$this->db->order_by($v["sort"].",".$v["dir"]);
					}
				}
				
				$this->db->from('user_role');
				$q=$this->db->get();
				return $q->result();
		}

		public function get_user_role_byid($data){
			$this->db->where($data);
			$q=$this->db->get('user_role');
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$q=$this->db->insert('user_role',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('user_role',$data);
			return $q;
		}

		public function delete($data){
			$this->db->delete('user_role',$data);
			return TRUE;
		}
	}
?>