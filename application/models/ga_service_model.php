<?php
	Class ga_service_model extends CI_Model{

		var $offset = 0;
		var $where = array();
		public function get_ga_services(){
			if($this->offset!=0) $this->db->limit($this->offset , $this->limit);
			if(!empty($this->where)) $this->db->where($this->where);
			if(!empty($this->order_by)) $this->db->order_by($this->order_by);

			$q=$this->db->select('ga_services.id,
								  ga_services.request_by,
								  ga_services.employee_id,
								  ga_services.division_id,
								  ga_services.vehicle_id,
								  ga_services.no_polisi,
								  ga_services.driver_name,
								  ga_services.no_voucher,
								  ga_services.date_req,
								  ga_services.time_req,
								  ga_services.priority_id,
								  ga_services.status_app_id,
								  ga_services.attachment,
								  ga_services.destination,
								  ga_services.city,
								  ga_services.utility,
								  ga_services.doc_seq_id,
								  ga_services.created_date,
								  ga_services.created_time,

								  doc.id as doc_id,
								  doc.doc_name,

								  employee.id as employee_id,
								  employee.employee_name,

								  priority.id as priority_id,
								  priority.priority_name,

								  status_app.id as status_app_id,
								  status_app.status_app_name,
								  status_app.class_status,

								  divisi.id as division_id,
								  divisi.divisi_name,

								  driver.driver_name,
								  police_number.nopol')
						->from('ga_services')
						->join('doc','doc.id=doc_id','left')
						->join('employee','employee.id=employee_id','left')
						->join('priority','priority.id=priority_id','left')
						->join('status_app','status_app.id=status_app_id','left')
						->join('divisi','divisi.id=division_id','left')
						->join('driver','driver.id=ga_services.driver_name','left')
						->join('police_number','police_number.id=ga_services.no_polisi','left')
						->order_by('doc_seq_id DESC,created_date DESC, created_time DESC')
						->get();
			
			//$q->this->db->get('ga_services');

			//echo $this->db->last_query();
			return $q->result();
		}

		public function get_ga_service_byid($data){
			$this->db->where($data);
			$q=$this->db->get('ga_services');
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('ga_services',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('ga_services',$data);
			return $q;
		}
		public function delete($data){
			$this->db->delete('ga_services',$data);
			return TRUE;
		}

		public function get_ga_byid($data){
			$this->db->where($data);
			$q=$this->db->select('ga_services.id,
								  ga_services.request_by,
								  ga_services.employee_id,
								  ga_services.division_id,
								  ga_services.vehicle_id,
								  ga_services.no_polisi,
								  ga_services.driver_name,
								  ga_services.no_voucher,
								  ga_services.date_req,
								  ga_services.time_req,
								  ga_services.priority_id,
								  ga_services.status_app_id,
								  ga_services.attachment,
								  ga_services.destination,
								  ga_services.city,
								  ga_services.utility,
								  ga_services.doc_seq_id,
								  ga_services.created_date,
								  ga_services.created_time,

								  doc.id as doc_id,
								  doc.doc_name,

								  employee.id as employee_id,
								  employee.employee_name,

								  priority.id as priority_id,
								  priority.priority_name,

								  status_app.id as status_app_id,
								  status_app.status_app_name,
								  status_app.class_status,

								  divisi.id as division_id,
								  divisi.divisi_name,

								  vehicle.id as vehicle_id,
								  vehicle.vehicle_name,

								  inbox_app.doc_number,
								  driver.driver_name,
								  police_number.nopol')
						->from('ga_services')
						->join('doc','doc.id=doc_id','left')
						->join('employee','employee.id=employee_id','left')
						->join('priority','priority.id=priority_id','left')
						->join('status_app','status_app.id=status_app_id','left')
						->join('divisi','divisi.id=division_id','left')
						->join('vehicle','vehicle.id=vehicle_id','left')
						->join('driver','driver.id=ga_services.driver_name','left')
						->join('police_number','police_number.id=ga_services.no_polisi','left')
						->join('inbox_app','ga_services.doc_seq_id=inbox_app.doc_number','left')
						->get();
			$data=$q->first_row();
			return $data;
		}
		
		public function edit_status($data_stat){
			$this->db->where('doc_seq_id',$data_stat['doc_seq_id']);
			
			$q=$this->db->update('ga_services',$data_stat);
			return $q;
		}
		
	}

?>