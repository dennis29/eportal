<?php
	class corporate_model extends CI_Model{
		public function get_corporate(){
			//$this->db->order_by('menus.label');
			$q=$this->db->select('corporate.id,
								  corporate.corporate_tab_id,
								  corporate.no_policy,
								  corporate.title,
								  corporate.desc_corporate,
								  corporate.attachment,
								  corporate.created,
								  corporate.status_revisi,
								  corporate.sop_ref,

								  corporate_tab.id as corporate_tab_id,
								  corporate_tab.tab_id')
						->from('corporate')
						->join('corporate_tab','corporate_tab.id=corporate_tab_id','left')
						->get();
			return $q->result();
		}

		public function get_corporate_tab(){
			$q=$this->db->get('corporate_tab');
			return $q->result();
		}

		public function get_corporate_byid($data){
			$this->db->where($data);
			$q=$this->db->select('corporate.id,
								  corporate.corporate_tab_id,
								  corporate.no_policy,
								  corporate.title,
								  corporate.desc_corporate,
								  corporate.attachment,
								  corporate.created,
								  corporate.status_revisi,
								  corporate.sop_ref,

								  corporate_tab.id as corporate_tab_id,
								  corporate_tab.tab_id')
						->from('corporate')
						->join('corporate_tab','corporate_tab.id=corporate_tab_id','left')
						->get();
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('corporate',$data);
			//echo $this->db->last_query();
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('corporate',$data);
			return $q;
		}

		public function delete($data){
			$this->db->delete('corporate',$data);
			return TRUE;
		}
	}
?>