<?php
	Class menu_model extends CI_Model{
		public function get_menus(){
			$q=$this->db->select('mn.id,
								  mn.label,
								  mn.link,
								  mn.i_class,
								  (select label from menus where id=mn.parent) as parent_name,
								  mn.sort')
						->from('menus as mn')
						->order_by('mn.label')
						->get();
			return $q->result();
		}

		public function get_menu_byid($data){
			$this->db->where($data);
			$q=$this->db->get('menus');
			$data=$q->first_row();
			return $data;
		}

		public function add($data){
			$this->db->insert('menus',$data);
			return TRUE;
		}

		public function edit($data){
			$this->db->where('id',$data['id']);
			$q=$this->db->update('menus',$data);
			return $q;
		}

		public function delete($data){
			$this->db->delete('menus',$data);
			return TRUE;
		}

		public function get_menu_byrole($data){
			$q=$this->db->where($data)
						->where("(`role_menu`.`create`=1 or `role_menu`.`read`=1 or `role_menu`.`update`=1 or `role_menu`.`delete`=1)", NULL, FALSE)
						->select('menus.label,menus.link,menus.i_class,menus.parent,menus.id')
						->distinct()
						->from('users')
						->order_by('menus.sort')
						->join('user_role','user_role.user_id=users.id')
						->join('role_menu','role_menu.role_id=user_role.role_id')
						->join('menus','role_menu.menu_id=menus.id')
						->get();
			return $q->result();
		}
	}
?>