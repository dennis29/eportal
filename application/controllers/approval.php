<?php
	class Approval extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model(array(
					'approval_model',
					'approval_detail_model',
					'doc_model',
					'reference_model',
					'reference_list_model',
					'inbox_app_model',
					'doc_history_model',
					'it_service_model',
					'sod_model',
					'ga_service_model',
					'user_model',
					'employee_model'
				)
			);
		}

		public function index($id=0){
			$config['base_url']='./approval/index';

			$data['total']=$this->db->get('approval');
			$config['total_rows']=$data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];

			$config['per_page']=10;

			$this->pagination->initialize($config);
			$data['page']=$this->pagination->create_links();

			$this->approval_model->limit=$id;
			$this->approval_model->offset=$config['per_page'];


			$data['approval']=$this->approval_model->get_approval();

			/*echo $this->db->last_query();

			echo '<pre>';
			print_r($data['approval']);
			echo '</pre>';*/

			$this->load->view('approval/view',$data);
		}

		

		public function add(){
			$data['docs']=$this->doc_model->get_docs();

			/*echo '<pre>';
			print_r($data['references']);
			echo '</pre>';*/
			$data['references']=$this->reference_model->get_references();
			$data['references_list']=$this->reference_list_model->get_references_list();

			$this->load->view('approval/add',$data);
		}

		public function get_ref_list($creator_id){
			//$creator_id = $this->input->post('creator_id');
			
			if($creator_id==7)
				{
				$this->load->model("user_model");
				$role_list = $this->user_model->get_usr();
				$data = "<option value=''>== Select Sub Creator ==</option>";
				//echo $this->db->last_query();
				foreach ($role_list as $ref_list=>$value) {
					$data .= "<option value='".$value->id."'>".$value->username."</option>\n";
				
				}
			}
			elseif($creator_id==2)
			{
				$this->load->model("role_model");
				$role_list = $this->role_model->get_roles();
				$data = "<option value=''>== Select Sub Creator ==</option>";
				//echo $this->db->last_query();
				foreach ($role_list as $ref_list=>$value) {
					$data .= "<option value='".$value->id."'>".$value->role_name."</option>\n";
				
				}
			}else{
				$references_list = $this->reference_list_model->get_ref_list($creator_id);
				$data = "<option value=''>== Select Sub Creator ==</option>";
				//echo $this->db->last_query();
				foreach ($references_list as $ref_list=>$value) {
					$data .= "<option value='$value[id]'>$value[ref_list_name]</option>\n";
				}
			}
			echo $data;
		}

		/*public function get_list(){
			$ref_id=$this->input->post('ref_id');
			$ref_list=$this->approval_model->get_ref_list($ref_id);
			$data.="<option value=''>--Pilih--</option>";
			echo $this->db->last_query();
			foreach ($ref_list as $rf) {
				$data.="<option value=".$rf['id'].">".$rf['ref_name2']."</option>";
			}
			echo $data;
		}*/

		public function add_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('approval_name','Approval Name','required');
			$this->form_validation->set_rules('seq_priority_id','Sequence','required|integer');

			if($this->form_validation->run() != false){
				$data=array(
					'approval_name'=>$this->input->post('approval_name'),
					'doc_id'=>$this->input->post('doc_id'),
					'default'=>$this->input->post('default'),
					'creator_id'=>$this->input->post('creator_id'),
					'sub_creator_id'=>$this->input->post('sub_creator_id'),
					'default'=>$this->input->post('default'),
					'seq_priority_id'=>$this->input->post('seq_priority_id')
				);
				$this->approval_model->add($data);
				//print_r($data);
				echo json_encode(array("success" => true));
			}else{

				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('approval_name');
				$this->form_validation->set_message('seq_priority_id');

			}
			
		}

		public function edit(){
			$param=array(
					'id'=>$_GET['id']
				);
			$data['approval']=$this->approval_model->get_approval_byid($param);
			$data['docs']=$this->doc_model->get_docs();

			$data['references']=$this->reference_model->get_references();
			
			if($data["approval"]->creator_id==2)
			{
				$this->load->model("role_model");
				$this->role_model->select = "id, role_name as ref_list_name";
				$data['references_list']=$this->role_model->get_roles_array();
			}
			elseif($data["approval"]->creator_id==7)
			{
				$this->load->model("user_model");
				$this->user_model->select = "id, username as ref_list_name";
				$data['references_list']=$this->user_model->get_users_array();
			}
			else
			{
				$data['references_list']=$this->reference_list_model->get_ref_list($data["approval"]->creator_id);
			}
			
			$this->load->view('approval/edit',$data);
		} 

		public function edit_proccess(){
			$data=array(
					'id'=>$this->input->post('id'),
					'approval_name'=>$this->input->post('approval_name'),
					'doc_id'=>$this->input->post('doc_id'),
					'creator_id'=>$this->input->post('creator_id'),
					'sub_creator_id'=>$this->input->post('sub_creator_id')
				);
			$this->approval_model->edit($data);
			//redirect('main');
			echo json_encode(array('success'=>true));
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->approval_model->delete($data);
			//redirect('main');
			echo json_encode(array('success'=>TRUE));
		}

		public function inbox($id=0)
		{
			$where = "";
			$q = "";
			foreach($_GET as $k=>$v)
			{
				if(!in_array($k, array("per_page","sort","dir")))
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->inbox_app_model->where[$k] = $v;
						$q .= "&".$k."=".$v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}

			if(isset($_GET["per_page"]))
			{
				if($_GET["per_page"]=="")
				{
					$id = 0;
				}
				else
				{
					$id = $_GET["per_page"];
				}
			}
			else
			{
					$id = 0;				
			}
			if(isset($_GET["sort"]))
			{
				$this->inbox_app_model->order_by = array($_GET["sort"], ($_GET["dir"]==1) ? "ASC" : "DESC");
				$q .= "&sort=".$_GET["sort"]."&dir=".$_GET["dir"];
			}
			$config['base_url']='./approval/inbox/?_=0'.$q;

			$data['total']=$this->inbox_app_model->inbox_nolimit();
			$config['total_rows'] = count($data['total']);
			$data['total_rows']=$config['total_rows'];
			$config['page_query_string'] = TRUE;

			$config['per_page'] = 10;
			$config['num_links'] = 10;

			$this->pagination->initialize($config);
			$data['page']=$this->pagination->create_links();

			$this->inbox_app_model->limit = $id; //start
			$this->inbox_app_model->offset = $config['per_page']; //per page

			/*$this->it_service_model->offset=0;
			$data['it_services']=$this->it_service_model->get_it_services();*/
			//print_r($data['it_services']);

			$data['inbox']=$this->inbox_app_model->inbox();

			//echo count($data['inbox']);
			//print_r($data);
			//print_r($data);
			//echo $this->db->last_query();
			$this->load->view('approval/inbox',$data);
			
		}

		public function approve(){
			$param=array('id'=>$_GET['id']);
			$data['approve']=$this->inbox_app_model->get_approve_byid($param);

			$this->it_service_model->where = array('it_services.doc_id'=>$data['approve']->doc_id,
												   'doc_seq_id'=>$data['approve']->doc_number);

			$this->it_service_model->offset=0;
			$data['it_services']=$this->it_service_model->get_it_services();

			//echo $data['approve']->doc_id;
			//echo $data['approve']->doc_number;
			/*echo '<pre>';
			print_r($data['approve']);
			echo '</pre>';*/

			//print_r($data['approve']);

			$this->load->view('approval/approve',$data);
		}

		public function approve_sod(){
			$param=array('doc_number'=>$_GET['doc_number']);
			//print_r($_GET);
			$data['approve']=$this->inbox_app_model->get_approve_byid($param);
			//echo $this->input->post('doc_print');
			//$this->it_service_model->where = array('it_services.doc_id'=>$data['approve']->doc_id,
			//									   'doc_seq_id'=>$data['approve']->doc_number);

			$this->it_service_model->offset=0;
			//$data['it_services']=$this->it_service_model->get_it_services();

			//echo $data['approve']->doc_id;
			//echo $data['approve']->doc_number;
			//echo $this->db->last_query();
			/*echo '<pre>';
			print_r($data['approve']);
			echo '</pre>';*/

			//print_r($data['approve']);

			$this->load->view('approval/approve_sod',$data);
		}

		public function approve_detail(){
			$param=array('doc_number'=>$_GET['doc_number']);
			//print_r($_GET);
			$data['approve']=$this->inbox_app_model->get_approve_byid($param);
			$data['detail']=$this->sod_model->get_sod_byid($param);

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/

			$this->load->view('approval/approve_detail',$data);
		}

		public function app_proccess()
		{
			$this->inbox_app_model->where = array("approval_id" => $this->input->post("approval_id"));
			$data['max']=$this->inbox_app_model->max();
			$max=$data['max'][0]->sequence;
			//echo 'max: '.$max.'<br/>';
			$this->approval_detail_model->where = array("approval_id" => $this->input->post("approval_id"),
														"sequence" => $this->input->post("app_sequence")+1);
			$this->approval_detail_model->employee_id = $this->input->post("employee_id");
			$data['detail']=$this->approval_detail_model->get_detail();
			/*echo '<pre>';
			print_r($data['detail']);
			echo '</pre>';*/
			
			if(!empty($data['detail']))
			{
				foreach($data['detail'] as $det){
					//echo 'Seq: '.$det->sequence;
					if($det->sequence<=$max){
						if($det->sequence < $max)
							$st = 2;
						else
							$st = 3;
						if($this->input->post('approval_id')==$det->approval_id)
						{
							$this->inbox_app_model->doc_num = $this->input->post('doc_number');
							$rsinbox = $this->inbox_app_model->inbox();
							$this->inbox_app_model->doc_num = "";
							//echo $this->db->last_query();
							if(count($rsinbox)==0)
							{
								echo json_encode(array("success" => false, "message" => "Anda Tidak Memiliki Privilege Untuk Approval Ini atau Approval Sudah Di-approve"));
								return false;
							}
							//print_r($det);
							$idnya=$det->id;
							$seqnya=$det->sequence;
							$appnya=$det->approval_id;
							$creatornya=$det->creator_id;
							$subnya=$det->subcreator_id;

							//inbox app
							$data = array(
									'id'=>$this->input->post('id'),
									'app_memo'=>$this->input->post('oldmemo').$this->input->post('app_memo').'<br/><br/>memo by: '.$_SESSION['employee'].'<br/><br/>',
									'approval_id'=>$appnya,
									'approved_id'=>$creatornya,
									'sub_approved_id'=>$subnya,
									'email' => '0',
									'app_sequence'=>$seqnya,
								);

							//doc history
							$data4 = array(
									'approval_id'=>$appnya,
									'approved_id'=>$subnya,
									'doc_id'=>$this->input->post('doc_id'),
									'doc_status'=>$st,
									'doc_number'=>$this->input->post('doc_number'),
									'approv_date'=>date("Y-m-d H:i:s"),
									'status'=>$st,
									'created'=>date("Y-m-d H:i:s"),
									'updated'=>date("Y-m-d H:i:s")
								);

						//	print_r($data4);
						//	print_r($data);
							/*echo '<pre>';

							print_r($data);
							print_r($data4);
							print_r($det);
							echo '</pre>';
							echo $this->db->last_query();*/

							
							$this->inbox_app_model->approve($data);

							$this->doc_history_model->add($data4);

							//it service
							$data_stat=array(
									'doc_seq_id'=>$this->input->post('doc_number'),
									'status_app_id'=>$st
								);

							//echo 'blom max';
							/*echo '<pre>';
							print_r($data_stat);
							echo '</pre>';*/
							//echo $this->db->last_query();
							
							$this->it_service_model->edit_status($data_stat);
							$this->sod_model->edit_status($data_stat);
							$this->ga_service_model->edit_status($data_stat);

							//send email function
							//send email function
							$acc=array();
							$arr=array();

							//$this->user_model->limit = 1;
							//$this->user_model->where=array('employee_id'=>$this->input->post('employee_id'));
							//$data['buat_cc']=$this->user_model->send_mail();

							//echo 'buat cc';
							//print_r($data['buat_cc']);

							//cc mail
							//foreach ($data['buat_cc'] as $cc) {
							//	$acc[]=$cc->email;
							//}

							//print_r($data['buat_cc']);
							//print_r($app2);

//							if($app2[0]->creator_id==2){
							if($det->creator_id==2){
								$this->user_model->limit = 0;
								$this->user_model->where=array('role_id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();
								//echo $app2[0]->subcreator_id;
								//print_r($data['send_mail']);

							}elseif($det->creator_id==3){
								$this->user_model->limit = 0;
								$this->user_model->where=array('level_id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}elseif($det->creator_id==4){
								$this->user_model->limit = 0;
								$this->user_model->where=array('group_id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}elseif($det->creator_id==5){
								$this->user_model->limit = 1;
								$this->user_model->where=array('employee.id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}elseif($det->creator_id==6){
								$this->user_model->limit = 1;
								$this->user_model->where=array('employee.id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}elseif($det->creator_id==7){
								$this->user_model->limit = 1;
								$this->user_model->where=array('users.id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}

							//print_r($data['send_mail']);
							foreach ($data['send_mail'] as $key => $value) {
								//echo $key;

								//print_r($value);
								$arr[0]='helpdesk.it@gos.co.id';
								$arr[]=$value->email;
								
								//echo $ada2=implode(',', $ada);
							}
							//echo $arr;
							//$tonya=implode(",",$arr);

							//$his->

							//echo $tonya;
							//echo 'acc: '.$acc[0];
							//echo $list = array($tonya);
							//$role="'".implode("','",$sesrole)."'";

							//email
							$config=$this->config->item('email');

							$this->load->library('email',$config);
							$this->email->initialize($config);

							$this->email->from('eportal@gos.co.id', 'ePortal GOS');
							
							$this->email->to("iksan.it@gos.co.id");
							
							//$this->email->to($tonya);
							
							//$this->email->cc($acc[0]);
							//$this->email->cc('another@another-example.com'); 
							//$this->email->bcc('them@their-example.com'); 

							$sub_email="Approval Request";

							if($this->input->post('priority_id')==1){
								$pri_name="High";
							}elseif($this->input->post('priority_id')==2){
								$pri_name="Medium";
							}elseif($this->input->post('priority_id')==3){
								$pri_name="Low";
							}else{
								$pri_name="Unknown";
							}

							$request_by=$_SESSION['employee'];
							$created_for=$this->input->post('created_for');
							$request_date=date("Y-m-d");
							$duedate=$this->input->post('duedate');
							$priority=$pri_name;
							$desc_mail=$this->input->post('description');

							$this->email->subject($sub_email);
							//$this->email->message();	
							
							//print_r($this->email);
							//$this->email->send();
							
							echo json_encode(array("success" => true));

						}
					}
				}
			}else{
				/*foreach($data['detail'] as $det){
					print_r($data['detail']);
					$idnya=$det->id;
					$seqnya=$det->sequence;
					$appnya=$det->approval_id;
					$creatornya=$det->creator_id;
					$subnya=$det->subcreator_id;*/

					//inbox app
					$data = array(
							'id'=>$this->input->post('id'),
							'app_memo'=>$this->input->post('oldmemo').$this->input->post('app_memo').'<br/><br/>memo by: '.$_SESSION['employee'].'<br/><br/>',
							'approved_id'=>7,
							'sub_approved_id'=>$this->input->post('created_by')
						);
					
					if($this->input->post('created_by')==$this->input->post('sub_approved_id')){
						$data_stat=array(
							'doc_seq_id'=>$this->input->post('doc_number'),
							'status_app_id'=>5
						);

						$data = array(
								'id'=>$this->input->post('id'),
								'app_memo'=>$this->input->post('oldmemo').$this->input->post('app_memo').'<br/><br/>memo by: '.$_SESSION['employee'].'<br/><br/>',
								'email ' => '1',
								'approved_id'=>7,
								'sub_approved_id'=>$this->input->post('created_by')
							);

						//doc history
						$data4 = array(
							'approval_id'=>0,
							'approved_id'=>7,
							'doc_id'=>$this->input->post('doc_id'),
							'doc_status'=>5,
							'doc_number'=>$this->input->post('doc_number'),
							'approv_date'=>date("Y-m-d H:i:s"),
							'status'=>5,
							'created'=>date("Y-m-d H:i:s")
						);
						
					
					}else{
						$data_stat=array(
							'doc_seq_id'=>$this->input->post('doc_number'),
							'status_app_id'=>4,
							'doc_print'=>$this->input->post('doc_print')
						);

						$data = array(
								'id'=>$this->input->post('id'),
								'app_memo'=>$this->input->post('oldmemo').$this->input->post('app_memo').'<br/><br/>memo by: '.$_SESSION['employee'].'<br/><br/>',
								'approved_id'=>7,
								'email' => '0',
								'sub_approved_id'=>$this->input->post('created_by')
							);

						//doc history
						$data4 = array(
							'approval_id'=>0,
							'approved_id'=>7,
							'doc_id'=>$this->input->post('doc_id'),
							'doc_status'=>4,
							'doc_number'=>$this->input->post('doc_number'),
							'approv_date'=>date("Y-m-d H:i:s"),
							'status'=>4,
							'created'=>date("Y-m-d H:i:s")
						);						
					}
					
					//print_r($data);
					/*echo 'udah max';
					echo '<pre>';
					print_r($data_stat);aa
					echo '</pre>';
					echo $this->db->last_query();*/
					$this->inbox_app_model->doc_num = $this->input->post('doc_number');
					$rsinbox = $this->inbox_app_model->inbox();
					$this->inbox_app_model->doc_num = "";
					//echo $this->db->last_query();
					if(count($rsinbox)==0)
					{
						echo json_encode(array("success" => false, "message" => "Anda Tidak Memiliki Privilege Untuk Approval Ini atau Approval Sudah Di-approve"));
						return false;
					}
					$this->doc_history_model->add($data4);
					$this->inbox_app_model->approve($data);
					$this->it_service_model->edit_status($data_stat);
					$this->ga_service_model->edit_status($data_stat);
					$this->sod_model->edit_status($data_stat);

					echo json_encode(array("success" => true));
				//}
			}
		}




		public function reject_proccess(){
					$data = array(
							'id'=>$this->input->post('id'),
							'app_memo'=>$this->input->post('oldmemo').$this->input->post('app_memo').'<br/><br/>memo by: '.$_SESSION['employee'].'<br/><br/>'
						);
					
					
					$data_stat=array(
						'doc_seq_id'=>$this->input->post('doc_number'),
						'status_app_id'=>6
					);

					//doc history
					$data4 = array(
						'approval_id'=>0,
						'approved_id'=>0,
						'doc_id'=>$this->input->post('doc_id'),
						'doc_status'=>6,
						'doc_number'=>$this->input->post('doc_number'),
						'approv_date'=>date("Y-m-d H:i:s"),
						'status'=>6,
						'created'=>date("Y-m-d H:i:s")
					);
					/*echo 'udah max';
					echo '<pre>';
					print_r($data_stat);
					echo '</pre>';
					echo $this->db->last_query();*/
					$this->doc_history_model->add($data4);
					$this->inbox_app_model->approve($data);
					$this->it_service_model->edit_status($data_stat);
					$this->sod_model->edit_status($data_stat);

					echo json_encode(array("success" => true));
		}

		public function revisi_proccess(){
			$data = array(
							'id'=>$this->input->post('id'),
							'app_memo'=>$this->input->post('oldmemo').$this->input->post('app_memo').'<br/><br/>memo by: '.$_SESSION['employee'].'<br/><br/>'
						);
					
					
					$data_stat=array(
						'doc_seq_id'=>$this->input->post('doc_number'),
						'status_app_id'=>7
					);

					//input doc_history
					$data4 = array(
						'approval_id'=>0,
						'approved_id'=>0,
						'doc_id'=>$this->input->post('doc_id'),
						'doc_status'=>7,
						'doc_number'=>$this->input->post('doc_number'),
						'approv_date'=>date("Y-m-d H:i:s"),
						'status'=>7,
						'created'=>date("Y-m-d H:i:s")
					);
					/*echo 'udah max';
					echo '<pre>';
					print_r($data_stat);
					echo '</pre>';
					echo $this->db->last_query();*/
					$this->doc_history_model->add($data4);
					$this->inbox_app_model->approve($data);
					$this->it_service_model->edit_status($data_stat);
					$this->sod_model->edit_status($data_stat);

					echo json_encode(array("success" => true));
		}

		public function search(){
			//$param = array('ga_services.doc_seq_id'=>$_GET['id']);
			//$data['detail']=$this->ga_service_model->get_ga_byid($param);
			//$this->ga_service_model->where=array('request_by'=>523);
			$this->employee_model->offset=0;
			$data['employee']=$this->employee_model->get_employee();
			$data['inbox']=$this->inbox_app_model->inbox();
			$this->load->view('approval/search',$data);


			//echo $this->db->last_query();

			/*echo '<pre>';
			print_r($data['employee']);
			echo '</pre>';*/
		}
		
		
		function app_email()
		{
			error_reporting(E_ALL);
			ini_set('display_errors', 1);
			
			$this->inbox_app_model->where["email"] = '0';
//			$this->inbox_app_model->limit = 5;
			
			$this->inbox_app_model->select = "sub_approved_id, approved_id";
			$this->inbox_app_model->distinct = "sub_approved_id, approved_id";
			$this->inbox_app_model->order_by[] = array("sort" => "sub_approved_id", "dir" => "asc");
			$this->inbox_app_model->order_by[] = array("sort" => "approved_id", "dir" => "asc");
			$dt["data"] = $this->inbox_app_model->get();
			
			$rs = "";
			foreach($dt["data"] as $k=>$v)
			{
				$email=array();
				$empl_name = array();
				$this->inbox_app_model->select = "";
				$this->inbox_app_model->distinct = "";
				$this->inbox_app_model->order_by = array();

				$this->inbox_app_model->where["sub_approved_id"] = $v->sub_approved_id;
				$this->inbox_app_model->where["approved_id"] = $v->approved_id;
				$this->inbox_app_model->where["email"] = 0;
				$dte["data"] = $this->inbox_app_model->get();
				
				echo $this->db->last_query();
				exit();

				//echo $v->sub_approved_id." ".$v->approved_id."<br />";
				if(count($dte["data"])>0)
				{
					
					$this->inbox_app_model->update(array("email"=>1));
					//$this->employee_model->where = array("id" => $v->employee_id);
					//$data['employee']=$this->employee_model->get_employee();
					if($v->approved_id==2) // role
					{
						$this->load->model("user_role_model", "ur");
						$this->ur->where["role_id"] = $v->sub_approved_id;
						$usr = $this->ur->get();
	
						
						foreach($usr as $empl)
						{
							$this->load->model("user_model", "u");
							$this->u->where["id"] = $empl->user_id;
							$usr2 = $this->u->get();
	
							$this->employee_model->where["id"] = $usr2[0]->employee_id;
							$empls = $this->employee_model->get();
							$email[] = $empls[0]->email;
							$empl_name[] = $empls[0]->employee_name;
						}
					}
					else
					if($v->approved_id==3) // level
					{
						$this->load->model("user_model", "u");
						$this->u->where["level_id"] = $v->sub_approved_id;
						$usr = $this->u->get();
						
						foreach($usr as $empl)
						{
							$this->employee_model->where["id"] = $empl->employee_id;
							$empls = $this->employee_model->get();
							$empl_name[] = $empls[0]->employee_name;
							$email[] = $empls[0]->email;
						}
					}
					else
					if($v->approved_id==4) // group
					{
						$this->load->model("user_model", "u");
						$this->u->where["group_id"] = $v->sub_approved_id;
						$usr = $this->u->get();
						
						foreach($usr as $empl)
						{
							$this->employee_model->where["id"] = $empl->employee_id;
							$empls = $this->employee_model->get();
							$empl_name[] = $empls[0]->employee_name;
							$email[] = $empls[0]->email;
						}
					}
					else
					if($v->approved_id==5) // atasan 1
					{
						$this->employee_model->where["id"] = $v->sub_approved_id;
						$empls = $this->employee_model->get();
						$empl_name[] = @$empls[0]->employee_name;
						$email[] = @$empls[0]->email;
					}
					else
					if($v->approved_id==6) // atasan 2
					{
						$this->employee_model->where["id"] = $v->sub_approved_id;
						$empls = $this->employee_model->get();
						$empl_name[] = @$empls[0]->employee_name;
						$email[] = @$empls[0]->email;
					}
					else
					if($v->approved_id==7) // user
					{
						$this->load->model("user_model", "u");
						$this->u->where["id"] = $v->sub_approved_id;
						$usr = $this->u->get();
	
						$this->load->model("employee_model", "e");
						$this->e->where["id"] = $usr[0]->employee_id;
						$usr = $this->e->get();
	
						$empl_name[] = $usr[0]->employee_name;
						$email[] = $usr[0]->email;
					}
					
					//echo "<pre>";
					//print_r($email);
					//echo "</pre>";
	
	
					foreach($dte["data"] as $k2=>$v2)
					{
						$this->doc_model->where["id"] = $v2->doc_id;
						$rd = $this->doc_model->get_docs();
						$dte["data"][$k2]->doc_name = $rd[0]->doc_name;
		
						$this->employee_model->where["id"] = $v2->employee_id;
						$rd = $this->employee_model->get_employee();
						$dte["data"][$k2]->request_name = $rd[0]->employee_name;					
					}
					
					if(!empty($dte["data"]))
					{
						if(!empty($email))
						{
							$config = $this->config->item('email');

							$this->load->library('email',$config);
							$this->email->initialize($config);

							$this->email->from('eportal@gos.co.id', 'ePortal GOS');

							foreach($email as $e=>$es)
							{
								$dte["greeting"] = "Hai, ".@$empl_name[$e];
								$rs = $this->load->view('approval/email',$dte,true);			
								
								$this->email->to("iksan.it@gos.co.id");
								
								$sub_email="Approval Request for ".@$empl_name[$e];
	
	
								$this->email->subject($sub_email);
								$this->email->message($rs);	
								
								//print_r($this->email);
								$this->email->send();
							}
						}
					}
				}
			}
			//$this->inbox_app_model->where = array("email" => 1);
			//$this->inbox_app_model->update(array("email"=>0));
			
			//echo $rs;
			//return $rs;
		}
	}
?>