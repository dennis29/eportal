<?php
	Class User_role extends CI_Controller{
		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model(array(
					'user_model',
					'role_model',
					'user_role_model',
					'employee_model'
				)
			);
		}

		public function index($id=0){
			$where = "";
			foreach($_GET as $k=>$v)
			{
				if($k!="per_page")
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->user_role_model->where[$k] = $v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}
			$config['page_query_string'] = TRUE;
			$config['base_url']='./user_role/index?'.$where;

			$data['total']=$this->user_role_model->get_user_role();
			$config['total_rows']=count($data['total']);
			$data['total_rows']=$config['total_rows'];

			$config['per_page']=20;

			$this->pagination->initialize($config);
			$data['page']=$this->pagination->create_links();

			$this->user_role_model->limit=$id;
			$this->user_role_model->offset=$config['per_page'];

			$data['user_role']=$this->user_role_model->get_user_role();
			//print_r($data);
			$this->load->view('user_role/view',$data);

			/*if($_SESSION['user_id']==557){
				echo $this->db->last_query();
			}*/
			
		}

		public function add(){

			$this->role_model->offset=0;

			$data['users']=$this->user_model->get_usr();
			$data['roles']=$this->role_model->get_roles();

			/*echo '<pre>';
			print_r($data['users']);
			echo '</pre>';*/

			$this->load->view('user_role/add',$data);
		}

		public function add_proccess(){
			$data=array(
					'user_id'=>$this->input->post('user_id'),
					'role_id'=>$this->input->post('role_id')
				);
			$this->user_role_model->add($data);
			//print_r($data);
			//redirect('user_role');
			echo json_encode(array('success'=>TRUE));
		}

		public function edit(){

			$param=array(
					'id'=>$_GET['id']
				);
			$this->user_role_model->offset=0;
			$data['user']=$this->user_role_model->get_user_role_byid($param);

			$data['users']=$this->user_model->get_usr();
			$data['roles']=$this->role_model->get_roles();

			//print_r($data['roles']);

			$this->load->view('user_role/edit',$data);
		}

		public function edit_proccess(){
			$data=array(
					'id'=>$this->input->post('id'),
					'user_id'=>$this->input->post('user_id'),
					'role_id'=>$this->input->post('role_id')
				);
			$this->user_role_model->edit($data);
			//print_r($data);
			//redirect('user_role');
			echo json_encode(array('success'=>TRUE));
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->user_role_model->delete($data);
			//redirect('user_role');
		}

		public function search(){

			$this->role_model->offset=0;

			$data['users']=$this->user_model->get_usr();
			$data['roles']=$this->role_model->get_roles();
			$data['employee']=$this->employee_model->get_employee();

			/*echo '<pre>';
			print_r($data['users']);
			echo '</pre>';*/

			$this->load->view('user_role/search',$data);
		}
	}
?>