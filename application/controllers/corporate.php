<?php
	Class corporate extends CI_Controller{
		function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model(array(
				'corporate_model'
				)
			);
		}

		public function index(){

			$data['corporate']=$this->corporate_model->get_corporate();
			$data['corporate_tab']=$this->corporate_model->get_corporate_tab();
			/*echo '<pre>';
			print_r($data['corporate']);
			echo '</pre>';*/
			$this->load->view('corporate/view',$data);
		}

		public function policy($id=0){
			$config['base_url'] = './corporate/policy';

			$data['total'] = $this->db->get('corporate');
			$config['total_rows'] = $data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->corporate_model->limit = $id; //start
			$this->corporate_model->offset = $config['per_page']; //per page

			$data['corporate']=$this->corporate_model->get_corporate();
			$data['corporate_tab']=$this->corporate_model->get_corporate_tab();
			/*echo '<pre>';
			print_r($data['corporate']);
			echo '</pre>';*/
			$this->load->view('corporate/policy',$data);
		}

		public function sop($id=0){
			$config['base_url'] = './corporate/sop';

			$data['total'] = $this->db->get('corporate');
			$config['total_rows'] = $data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->corporate_model->limit = $id; //start
			$this->corporate_model->offset = $config['per_page']; //per page

			$data['corporate']=$this->corporate_model->get_corporate();
			$data['corporate_tab']=$this->corporate_model->get_corporate_tab();
			/*echo '<pre>';
			print_r($data['corporate']);
			echo '</pre>';*/
			$this->load->view('corporate/sop',$data);
		}

		public function form($id=0){
			$config['base_url'] = './corporate/form';

			$data['total'] = $this->db->get('corporate');
			$config['total_rows'] = $data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->corporate_model->limit = $id; //start
			$this->corporate_model->offset = $config['per_page']; //per page

			$data['corporate']=$this->corporate_model->get_corporate();
			$data['corporate_tab']=$this->corporate_model->get_corporate_tab();
			/*echo '<pre>';
			print_r($data['corporate']);
			echo '</pre>';*/
			$this->load->view('corporate/form',$data);
		}

		public function iom($id=0){
			$config['base_url'] = './corporate/iom';

			$data['total'] = $this->db->get('corporate');
			$config['total_rows'] = $data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->corporate_model->limit = $id; //start
			$this->corporate_model->offset = $config['per_page']; //per page

			$data['corporate']=$this->corporate_model->get_corporate();
			$data['corporate_tab']=$this->corporate_model->get_corporate_tab();
			/*echo '<pre>';
			print_r($data['corporate']);
			echo '</pre>';*/
			$this->load->view('corporate/iom',$data);
		}

		public function add(){
			$data['corporate']=$this->corporate_model->get_corporate();
			$data['corporate_tab']=$this->corporate_model->get_corporate_tab();
			//print_r($data);
			$this->load->view('corporate/add',$data);

			//print_r($data['corporate']);
		}

		public function add_process(){
			//upload file
			/*echo '<pre>';
			print_r($_FILES['attachment']);
			echo '</pre>';*/

			$config['upload_path'] = './asset/file';
			$config['allowed_types'] = '*';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
		    
			$this->upload->set_allowed_types('*');

			//$data['upload_data'] = '';

			if(!$this->upload->do_upload('attachment')){
				$error = array('error' => $this->upload->display_errors());
				$upload_data = 0;
				//$this->load->view('upload_form', $error);
				//print_r($error);
				//print_r($config);
				//$filenya=$this->input->post('attachment2');
			}
			else{
				$upload_data = $this->upload->data();
				//$file_name = $upload_data['file_name'];
				//$filenya=$upload_data['file_name'];
			}
			
			//inbox app
			$data=array(
					'corporate_tab_id'=>$this->input->post('corporate_tab_id'),
					'no_policy'=>$this->input->post('no_policy'),
					'title'=>$this->input->post('title'),
					'desc_corporate'=>$this->input->post('desc_corporate'),
					'status_revisi'=>$this->input->post('status_revisi'),
					'sop_ref'=>$this->input->post('sop_ref'),
					'created'=>date("Y-m-d"),
					'attachment'=>$upload_data['file_name']
				);
			/**/
			//print_r($data);
			$this->corporate_model->add($data);
			echo json_encode(array("success" => true));
		}

		public function edit(){
			$param=array(
					'corporate.id'=>$_GET['id']
				);
			$data['corporate_all']=$this->corporate_model->get_corporate();
			$data['corporate']=$this->corporate_model->get_corporate_byid($param);
			$data['corporate_tab']=$this->corporate_model->get_corporate_tab();
			$this->load->view('corporate/edit',$data);

			/*echo '<pre>';
			print_r($data);
			echo '<pre>';*/

			//print_r($data['corporate']);
		}

		public function edit_process(){
			$config['upload_path'] = './asset/file';
			$config['allowed_types'] = '*';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
		    
			$this->upload->set_allowed_types('*');

			//$data['upload_data'] = '';

			if(!$this->upload->do_upload('attachment')){
				//$error = array('error' => $this->upload->display_errors());
				//$upload_data = 0;
				//$this->load->view('upload_form', $error);
				//print_r($error);
				//print_r($config);
				echo 'gada';
				$filenya=$this->input->post('attachment2');
			}
			else{
				$upload_data = $this->upload->data();
				//$file_name = $upload_data['file_name'];
				echo 'ada';
				$filenya=$upload_data['file_name'];
			}
			
			//inbox app
			$data=array(
					'id'=>$this->input->post('id'),
					'corporate_tab_id'=>$this->input->post('corporate_tab_id'),
					'no_policy'=>$this->input->post('no_policy'),
					'title'=>$this->input->post('title'),
					'desc_corporate'=>$this->input->post('desc_corporate'),
					'created'=>date("Y-m-d"),
					'attachment'=>$filenya
				);

			//print_r($data);

			$this->corporate_model->edit($data);
			echo json_encode(array("success" => true));
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->corporate_model->delete($data);
			echo json_encode(array('success'=>true));
		}
	}
?>