<?php
	class Main extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}
		}

		public function index()
		{
			//header("location:http://eportal.gos.co.id/eportal2/index.php/");
			$this->load->model('menu_model');
			
			if($_SERVER['REMOTE_ADDR']==='203.174.13.58')
			{
				//print_r($_SESSION);
			}
			$param=array(
					'username'=>$_SESSION['username']
				);

			//print_r($_SESSION);
		
			$data['menus']=$this->menu_model->get_menu_byrole($param);
			$data['menus_hierarki'] = $this->hierarki(0,$data['menus']);

			//echo $this->db->last_query();
			

			$this->load->model('inbox_app_model');
			$data['inbox_count']=$this->inbox_app_model->inbox_nolimit();
			$data['inbox']=$this->inbox_app_model->inbox();

			$this->load->view('main',$data);
		}

		public function hierarki($parent,$menu)
		{
			$str="";
			foreach($menu as $k=>$v)
			{
				if($v->parent==$parent)
				{
					//$str .= '<ul>'.'<a href=#>'.$v->label."</a></ul><br/>";
					$child = $this->hierarki($v->id,$menu);
					if($child!=""){
						$str .= '<li class="has_sub">'.'<a href=#>'.$v->i_class.' <span>'.$v->label."</span> <span class=pull-right><i class=fa fa-chevron-left></i></span></a>";
						$str .= '<ul>'.$child.'</ul></li>';
					}else{
						//$str .= '<li>'.'<a href=# onclick="$(\'#contentAjax\').load(\''.$v->link.'\')" class="open">'.$v->i_class.'<span>'.$v->label."</span></a></li>";
						$str .= '<li>'.'<a href=# onclick="loading(\''.base_url($v->link).'\')" class="open">'.$v->i_class.'<span>'.$v->label."</span></a></li>";
					}
				}
			}
			return $str;
		}
		
		public function getInbox()
		{
			$this->load->model('inbox_app_model');
			$inbox=$this->inbox_app_model->inbox_nolimit();
			
			echo json_encode(array("total" => count($inbox)));
		}
	}
?>