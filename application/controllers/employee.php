<?php
	Class Employee extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model('employee_model');
			$this->load->model('divisi_model');
			$this->load->model('reference_list_model');
		}

		public function index($id=0){
			$where = "";
			foreach($_GET as $k=>$v)
			{
				if($k!="per_page")
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->employee_model->where[$k] = $v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}

			$config['page_query_string'] = TRUE;
			$config['base_url']='./employee/index?'.$where;

			$data['total']=$this->employee_model->get_employee();
			$config['total_rows']=count($data['total']);
			$data['total_rows']=$config['total_rows'];

			$config['per_page']=20;

			$this->pagination->initialize($config);
			$data['page']=$this->pagination->create_links();

			$this->employee_model->limit = $id; //start
			$this->employee_model->offset = $config['per_page']; //per page
			$data['employee']=$this->employee_model->get_employee();
			//echo $this->db->last_query();

			$data['divisi']=$this->divisi_model->get_divisi();
			$data['level']=$this->employee_model->get_level();
			$data['group']=$this->employee_model->get_group();
			
			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
			//echo $this->db->last_query();

			$this->load->view('employee/view',$data);
		}

		public function add($id=0){
			$this->employee_model->offset=$id;

			$data['employee']=$this->employee_model->get_employee();
			$data['divisi']=$this->divisi_model->get_divisi();

			//level
			$this->reference_list_model->where=array('ref_id'=>3);
			$data['ref_lv']=$this->reference_list_model->get_references_list();

			//group
			$this->reference_list_model->where=array('ref_id'=>4);
			$data['ref_gr']=$this->reference_list_model->get_references_list();

			$this->load->view('employee/add',$data);
		}

		public function add_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('employee_name','Full Name','required');

			if($this->form_validation->run() != false){
				$data=array(
						'employee_name'=>$this->input->post('employee_name'),
						't_lahir'=>$this->input->post('t_lahir'),
						'tl_lahir'=>$this->input->post('tl_lahir'),
						'email'=>$this->input->post('email'),
						'lokasi'=>$this->input->post('lokasi'),
						'divisi'=>$this->input->post('divisi'),
						'level_id'=>$this->input->post('level_id'),
						'group_id'=>$this->input->post('group_id'),
						'dephead_id'=>$this->input->post('dephead_id'),
						'divhead_id'=>$this->input->post('divhead_id')
					);
				$this->employee_model->add($data);
				//redirect('main');
				echo json_encode(array('success'=>true));
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('employee_name');

			}
		}

		public function edit(){
			$param=array(
					'id'=>$_GET['id']
				);
			$this->employee_model->offset=0;
			$data['employee_all']=$this->employee_model->get_employee();
			$data['employee']=$this->employee_model->get_employee_byid($param);
			$data['divisi']=$this->divisi_model->get_divisi();
			
			//level
			$this->reference_list_model->where=array('ref_id'=>3);
			$data['ref_lv']=$this->reference_list_model->get_references_list();

			//group
			$this->reference_list_model->where=array('ref_id'=>4);
			$data['ref_gr']=$this->reference_list_model->get_references_list();

			//print_r($data);

			$this->load->view('employee/edit',$data);
		}

		public function edit_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('employee_name','Full Name','required');

			if($this->form_validation->run() != false){
				$data=array(
						'id'=>$this->input->post('id'),
						'employee_name'=>$this->input->post('employee_name'),
						't_lahir'=>$this->input->post('t_lahir'),
						'tl_lahir'=>$this->input->post('tl_lahir'),
						'email'=>$this->input->post('email'),
						'lokasi'=>$this->input->post('lokasi'),
						'divisi'=>$this->input->post('divisi'),
						'level_id'=>$this->input->post('level_id'),
						'group_id'=>$this->input->post('group_id'),
						'dephead_id'=>$this->input->post('dephead_id'),
						'divhead_id'=>$this->input->post('divhead_id')
					);
				$this->employee_model->edit($data);
				//redirect('main');
				echo json_encode(array('success'=>true));
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('employee_name');

			}
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->employee_model->delete($data);
			echo json_encode(array('success'=>true));
		}

		public function profile($id=0){
			$this->employee_model->offset=$id;

			$this->load->model('user_model');
			$data['users']=$this->user_model->get_usr();
			$data['profile']=$this->employee_model->get_employee();
			$this->load->view('employee/profile',$data);

			//print_r($data['profile']);
		}

		public function search($id=0){
			$this->employee_model->offset=$id;

			$data['employee']=$this->employee_model->get_employee();

			$this->load->view('employee/search',$data);
		}
	}
?>