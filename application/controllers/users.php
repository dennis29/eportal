<?php
	Class Users extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model(array(
				'user_model',
				'employee_model'
				)
			);
		}

		public function index($id=0){
			$where = "";
			foreach($_GET as $k=>$v)
			{
				if($k!="per_page")
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->user_model->where[$k] = $v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}
			$config['page_query_string'] = TRUE;
			$config['base_url']='./users/index?'.$where;

			$data['total']=$this->user_model->get_usr();
			$config['total_rows']=count($data['total']);
			$data['total_rows']=$config['total_rows'];

			$config['num_link']=5;
			$config['per_page']=20;

			$this->pagination->initialize($config);
			$data['page']=$this->pagination->create_links();

			$this->user_model->limit=$id;
			$this->user_model->offset=$config['per_page'];
			//$this->load->model('user_model');
			
			$data['users']=$this->user_model->get_usr();
			//print_r($data['employee']);

			//echo $this->db->last_query();

			$this->load->view('users/view',$data);
		}

		public function add($id=0){
			//$this->load->model('user_model');
			//$data['roles']=$this->user_model->get_roles();
			$this->employee_model->offset=$id;
			$data['employee']=$this->employee_model->get_employee();
			//print_r($data['employee']);
			//echo $this->db->last_query();

			$this->load->view('users/add',$data);
		}

		public function add_proccess(){
			//$this->load->model('user_model');
			$this->load->library('form_validation');
			$this->form_validation->set_message('is_unique', '%s sudah ada');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('username','Username','required|is_unique[users.username]');
			$this->form_validation->set_rules('employee_id','Employee','required');

			if($this->form_validation->run() != false){
				$data=array(
					'username'=>$this->input->post('username'),
					'password'=>md5('itgos2012'),
					'employee_id'=>$this->input->post('employee_id'),
					'is_activated'=>'1'
				);

				$this->user_model->add($data);
				//redirect('users');
				echo json_encode(array('success'=>true));
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('employee_id');
				$this->form_validation->set_message('username');
			}
			
		}

		public function edit(){
			//$this->load->model('user_model');
			$param=array(
					'id'=>$_GET['id']
				);
			$data['users']=$this->user_model->get_usr();
			$data['user']=$this->user_model->get_user_byid($param);
			$data['roles']=$this->user_model->get_roles();
			
			

			$this->load->view('users/edit',$data);
		}

		public function edit_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('username','Username','required');

			if($this->form_validation->run() != false){
				$data=array(
					'id'=>$this->input->post('id'),
					'username'=>$this->input->post('username'),
					'is_activated'=>$this->input->post('is_activated')
					//'password'=>md5($this->input->post('password'))		
				);
				$this->user_model->edit($data);
				//redirect('users');
				echo json_encode(array('success'=>true));
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
			}
			
		}

		public function delete(){
			//$this->load->model('user_model');
			$data=array(
					'id'=>$_GET['id']
				);
			$this->user_model->delete($data);
			//redirect('users');
			echo json_encode(array('success'=>true));			
		}

		public function change_password(){

			$data['user']=$this->user_model->get_usr();

			$this->load->view('users/change_password',$data);
		}

		public function change_pass_proccess(){
			$this->load->library('form_validation');

			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('oldpass','Old Password','required');
			$this->form_validation->set_rules('password','New Password', 'required');
			$this->form_validation->set_rules('pass2','New Password 2', 'required');
			
			$passold=md5($this->input->post('oldpass'));
			$passdb=$this->input->post('passdb');
			$pass1=md5($this->input->post('password'));
			$pass2=md5($this->input->post('pass2'));

			/*if($this->form_validation->run() != false){
				echo 'valid';
				if($passold==$passdb){
					echo json_encode(array('ada'=>true,"message"=>"match"));
				}

			}else{
				echo json_encode(array('gada'=>true,"message"=>"not match"));
			}*/
			if($passold==$passdb){
				//echo 'passold sama pass db sama<br/>';
				if($pass1==$pass2){
					$data=array(
							'id'=>$this->input->post('id'),
							//'oldpass'=>md5($this->input->post('oldpass')),
							'password'=>md5($this->input->post('password'))
							//'pass2'=>md5($this->input->post('pass2'))
						);
					$this->user_model->change_password($data);
					//echo 'Change Password Success<br/>';
					echo json_encode(array('success'=>true));
					$this->form_validation->set_message('password');
					//redirect('main');
				}else{
					//echo 'Password Tidak sama<br/>';
					//redirect('/users/change_password');
					echo json_encode(array('passbeda'=>true,"message"=>"Password baru dan confirm password tidak sama"));
				}
			}else{
				//echo 'passold sama pass db beda<br/>';
				echo json_encode(array('passold'=>true,"message"=>"Password lama tidak sesuai"));
			}
		}

		public function activated(){
			echo $this->db->last_query();
			$data=array(
					'id'=>$_GET['id'],
					'is_activated'=>'1'
				);
			$this->user_model->activated($data);
			echo json_encode(array('success'=>true));
		}

		public function profile(){
			$data['profile']=$this->user_model->get_profile();
			$this->load->view('users/profile',$data);

			/*echo '<pre>';
			print_r($data['profile']);
			echo '</pre>';*/
		}

		public function reset_pass(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->user_model->reset_pass($data);
			//redirect('users');
			echo json_encode(array('success'=>true));
		}

		public function search(){

			$this->role_model->offset=0;

			$data['users']=$this->user_model->get_usr();
			$data['employee']=$this->employee_model->get_employee();

			$this->load->view('users/search',$data);
		}
	}
?>