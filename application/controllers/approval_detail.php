<?php
	Class Approval_detail extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model(array(
					'approval_detail_model',
					'approval_model',
					'doc_model',
					'reference_model',
					'reference_list_model'
				)
			);
		}

		public function index($id=0){
			$config['base_url']='./approval_detail/index';

			$data['total']=$this->db->get('approval_detail');
			$config['total_rows']=$data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];

			$config['per_page']=10;

			$this->pagination->initialize($config);
			$data['page']=$this->pagination->create_links();

			$this->approval_detail_model->limit=$id;
			$this->approval_detail_model->offset=$config['per_page'];
			
			$data["approval"] = $this->approval_model->get_detail();
			
			$data['detail']=$this->approval_detail_model->get_detail();

			$this->load->view('approval_detail/view',$data);
		}

		public function add(){
			$this->approval_model->offset=0;

			$data['approval']=$this->approval_model->get_approval();
			$data['docs']=$this->doc_model->get_docs();
			$data['references']=$this->reference_model->get_references();
			$data['references_list']=$this->reference_list_model->get_references_list();

			$this->load->view('/approval_detail/add',$data);
		}

		public function add_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('sequence','Sequence','required|integer');

			if($this->form_validation->run() != false){
				$data=array(
						'approval_id'=>$this->input->post('approval_id'),
						'sequence'=>$this->input->post('sequence'),
						'mandatory'=>$this->input->post('mandatory'),
						'creator_id'=>$this->input->post('creator_id'),
						'subcreator_id'=>$this->input->post('subcreator_id'),
						'created'=>date("Y-m-d")
					);
				$this->approval_detail_model->add($data);
				//redirect('main');
				echo json_encode(array('success'=>true));
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('sequence');
			}
		}

		public function edit(){
			$param=array(
					'id'=>$_GET['id']
				);

			$this->approval_model->offset=0;

			$data['detail']=$this->approval_detail_model->get_approval_detail_byid($param);
			$data['approval']=$this->approval_model->get_approval();
			$data['docs']=$this->doc_model->get_docs();
			$data['references']=$this->reference_model->get_references();
			
			if($data["detail"]->creator_id==2)
			{
				$this->load->model("role_model");
				$this->role_model->select = "id, role_name as ref_list_name";
				$data['references_list']=$this->role_model->get_roles_array();
			}
			elseif($data["detail"]->creator_id==7)
			{
				$this->load->model("user_model");
				$this->user_model->select = "id, username as ref_list_name";
				$data['references_list']=$this->user_model->get_users_array();
			}
			else
			{
				$data['references_list']=$this->reference_list_model->get_ref_list($data["detail"]->creator_id);
			}
//			print_r($data['detail']);

			$this->load->view('/approval_detail/edit',$data);
		}

		public function edit_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('sequence','Sequence','required|integer');

			if($this->form_validation->run() != false){
				$data=array(
						'id'=>$this->input->post('id'),
						'approval_id'=>$this->input->post('approval_id'),
						'sequence'=>$this->input->post('sequence'),
						'mandatory'=>$this->input->post('mandatory'),
						'creator_id'=>$this->input->post('creator_id'),
						'subcreator_id'=>$this->input->post('subcreator_id')
					);
				$this->approval_detail_model->edit($data);

				//print_r($data);
				//redirect('main');
				echo json_encode(array('success'=>true));
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('sequence');
			}
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);

			$this->approval_detail_model->delete($data);
			//redirect('main');
			echo json_encode(array('success'=>true));
		}
	}
?>