<?php
	class Email extends CI_Controller{

		public function __construct(){
			parent::__construct();

			$this->load->model(array(
					'approval_model',
					'approval_detail_model',
					'doc_model',
					'reference_model',
					'reference_list_model',
					'inbox_app_model',
					'doc_history_model',
					'it_service_model',
					'sod_model',
					'ga_service_model',
					'user_model',
					'employee_model'
				)
			);
		}

		function app_email()
		{			
			error_reporting(E_ALL);
			ini_set('display_errors', 1);
			$this->inbox_app_model->where["email"] = '0';
//			$this->inbox_app_model->limit = 5;
			
			$this->inbox_app_model->select = "sub_approved_id, approved_id";
			$this->inbox_app_model->distinct = "sub_approved_id, approved_id";
			$this->inbox_app_model->order_by[] = array("sort" => "sub_approved_id", "dir" => "asc");
			$this->inbox_app_model->order_by[] = array("sort" => "approved_id", "dir" => "asc");
			$dt["data"] = $this->inbox_app_model->get();

			//$data['get2']=$this->inbox_app_model->get2();
			//print_r($data['get2']);

			print_r($dt["data"]);
			
			$rs = "";
			foreach($dt["data"] as $k=>$v)
			{
				$email=array();
				$empl_name = array();
				$uname = array();
				$this->inbox_app_model->select = "";
				$this->inbox_app_model->distinct = "";
				$this->inbox_app_model->order_by = array();

				$this->inbox_app_model->where["sub_approved_id"] = $v->sub_approved_id;
				$this->inbox_app_model->where["approved_id"] = $v->approved_id;
				$this->inbox_app_model->where["email"] = 0;
				$dte["data"] = $this->inbox_app_model->get();
				
				//echo $this->db->last_query();
				//exit();

				//echo $v->sub_approved_id." ".$v->approved_id."<br />";
				if(count($dte["data"])>0)
				{
					
					$this->load->model("user_model", "u");
					$this->inbox_app_model->update(array("email"=>1));
					//$this->employee_model->where = array("id" => $v->employee_id);
					//$data['employee']=$this->employee_model->get_employee();
					if($v->approved_id==2) // role
					{
						$this->load->model("user_role_model", "ur");
						$this->ur->where["role_id"] = $v->sub_approved_id;
						$usr = $this->ur->get();
	
						
						foreach($usr as $empl)
						{
							$this->load->model("user_model", "u");
							$this->u->where["id"] = $empl->user_id;
							$usr2 = $this->u->get();
							$uname[] = @$usr2[0]->username;
	
							$this->employee_model->where["id"] = @$usr2[0]->employee_id;
							$empls = $this->employee_model->get();
							$email[] = @$empls[0]->email;
							$empl_name[] = @$empls[0]->employee_name;
						}
					}
					else
					if($v->approved_id==3) // level
					{
						$this->load->model("user_model", "u");
						$this->u->where["level_id"] = $v->sub_approved_id;
						$usr = $this->u->get();
						
						foreach($usr as $empl)
						{
							$uname[] = @$empl->username;
							$this->employee_model->where["id"] = $empl->employee_id;
							$empls = $this->employee_model->get();
							$empl_name[] = @$empls[0]->employee_name;
							$email[] = @$empls[0]->email;
						}
					}
					else
					if($v->approved_id==4) // group
					{
						$this->load->model("user_model", "u");
						$this->u->where["group_id"] = $v->sub_approved_id;
						$usr = $this->u->get();
						
						foreach($usr as $empl)
						{
							$uname[] = @$empl->username;
							$this->employee_model->where["id"] = $empl->employee_id;
							$empls = $this->employee_model->get();
							$empl_name[] = $empls[0]->employee_name;
							$email[] = $empls[0]->email;
						}
					}
					else
					if($v->approved_id==5) // atasan 1
					{
						$this->load->model("user_model", "u");
						$this->u->where["employee_id"] = $v->sub_approved_id;
						$usr = $this->u->get();
						$uname[] = @$usr[0]->username;

						$this->employee_model->where["id"] = $v->sub_approved_id;
						$empls = $this->employee_model->get();
						$empl_name[] = @$empls[0]->employee_name;
						$email[] = @$empls[0]->email;
					}
					else
					if($v->approved_id==6) // atasan 2
					{
						$this->u->where["employee_id"] = $v->sub_approved_id;
						$usr = $this->u->get();
						$uname[] = @$usr[0]->username;

						$this->employee_model->where["id"] = $v->sub_approved_id;
						$empls = $this->employee_model->get();
						$empl_name[] = @$empls[0]->employee_name;
						$email[] = @$empls[0]->email;
					}
					else
					if($v->approved_id==7) // user
					{
						$this->load->model("user_model", "u");
						$this->u->where["id"] = $v->sub_approved_id;
						$usr = $this->u->get();
						$uname[] = @$usr[0]->username;
	
						$this->load->model("employee_model", "e");
						$this->e->where["id"] = @$usr[0]->employee_id;
						$usr = $this->e->get();
	
						$empl_name[] = @$usr[0]->employee_name;
						$email[] = @$usr[0]->email;
					}
					
					/*echo "<pre>";
					print_r($dte["data"]);
					echo "</pre>";*/
	
	
					foreach($dte["data"] as $k2=>$v2)
					{
						$this->doc_model->where["id"] = $v2->doc_id;
						$rd = $this->doc_model->get_docs();
						$dte["data"][$k2]->doc_name = $rd[0]->doc_name;
		
						$this->employee_model->where["id"] = $v2->employee_id;
						$rd = $this->employee_model->get_employee();
						$dte["data"][$k2]->request_name = $rd[0]->employee_name;				
						//$dte["data"][$k2]->request_name = $rd[0]->employee_name;

						$this->it_service_model->where["doc_seq_id"] = $v2->doc_number;
						$this->sod_model->where["doc_seq_id"] = $v2->doc_number;
						$this->ga_service_model->where["doc_seq_id"] = $v2->doc_number;

						//it
						$it = $this->it_service_model->get_it_services();
						$dte["data"][$k2]->doc_num = $it[0]->doc_seq_id;
						$dte["data"][$k2]->problem_name = $it[0]->problem_name;
						$dte["data"][$k2]->created_for = $it[0]->created_for;
						$dte["data"][$k2]->description = $it[0]->description;

						//sod
						$sod = $this->sod_model->get_sod();
						$dte["data"][$k2]->doc_num = $sod[0]->doc_seq_id;
						$dte["data"][$k2]->client = $sod[0]->client_sod_id;
						$dte["data"][$k2]->nrk = $sod[0]->nrk;	
						$dte["data"][$k2]->name = $sod[0]->name_sod;
						$dte["data"][$k2]->position = $sod[0]->position_sod;
						$dte["data"][$k2]->status = $sod[0]->status_sod_name;
						$dte["data"][$k2]->start_datefrom = $sod[0]->start_datefrom;
						$dte["data"][$k2]->start_dateto = $sod[0]->start_dateto;
						$dte["data"][$k2]->rekomendasi = $sod[0]->rekomendasi_sod_name;
						$dte["data"][$k2]->effective_date = $sod[0]->effective_date;
						$dte["data"][$k2]->reason = $sod[0]->reason;
						$dte["data"][$k2]->department = $sod[0]->department;
						$dte["data"][$k2]->sbu_name = $sod[0]->sbu_name;


						//ga
						$ga = $this->ga_service_model->get_ga_services();
						
						
					}
					
					if(!empty($dte["data"]))
					{
						if(!empty($email))
						{
							$config = $this->config->item('email');

							$this->load->library('email',$config);
							$this->email->initialize($config);

							$this->email->from('eportal@gos.co.id', 'ePortal GOS');

							foreach($email as $e=>$es)
							{
								$dte["uname"] = @$uname[$e];
								$dte["greeting"] = "Hai, ".@$empl_name[$e];
								$rs = $this->load->view('approval/email',$dte,true);			
								
								$this->email->to("aries.it@gos.co.id");
								
								$sub_email="Approval Request for ".@$empl_name[$e];
	
	
								$this->email->subject($sub_email);
								$this->email->message($rs);	
								
								if($es!="") 
								{
									echo "<pre>";
									print_r($dte);
									echo "<pre>";
									$this->email->send();
									$this->db->reconnect();
								}
							}
						}
					}
				}
			}
			//$this->inbox_app_model->where = array("email" => 1);
			//$this->inbox_app_model->update(array("email"=>0));
			
			//echo $rs;
			//return $rs;
		}

		public function app_process_email($pr)
		{
			error_reporting(E_ALL);
			ini_set('display_errors', 1);

			$p = explode("/", base64_decode(str_pad(strtr($pr, '-_', '+/'), strlen($pr) % 4, '=', STR_PAD_RIGHT)));
			$inbox_id = $p[0];
			print_r($p); exit();
			$this->login($p[1]);
			$this->inbox_app_model->where = array("id" => $inbox_id);
			$rs = $this->inbox_app_model->get();
			$this->db->flush_cache();

			$this->inbox_app_model->where = array("approval_id" => $rs[0]->approval_id);
			$data['max']=$this->inbox_app_model->max();
			$max=$data['max'][0]->sequence;
			//echo 'max: '.$max.'<br/>';
			$this->approval_detail_model->where = array("approval_id" => $rs[0]->approval_id,
														"sequence" => $rs[0]->app_sequence+1);
			$this->approval_detail_model->employee_id = $rs[0]->employee_id;
			$data['detail']=$this->approval_detail_model->get_detail();
			/*echo '<pre>';
			print_r($data['detail']);
			echo '</pre>';*/
			
			if(!empty($data['detail']))
			{
				foreach($data['detail'] as $det){
					//echo 'Seq: '.$det->sequence;
					if($det->sequence<=$max){
						if($det->sequence < $max)
							$st = 2;
						else
							$st = 3;
						if($rs[0]->approval_id==$det->approval_id)
						{
							$this->inbox_app_model->doc_num = $rs[0]->doc_number;
							$rsinbox = $this->inbox_app_model->inbox();
							$this->inbox_app_model->doc_num = "";
							//echo $this->db->last_query();
							if(count($rsinbox)==0)
							{
								$this->load->view("approval/email_landing",array("success"=>false,"message" => "Anda Tidak Memiliki Akses untuk meng-approve Approval ini atau Approval telah di-approve"));
								return false;
							}
							//print_r($det);
							$idnya=$det->id;
							$seqnya=$det->sequence;
							$appnya=$det->approval_id;
							$creatornya=$det->creator_id;
							$subnya=$det->subcreator_id;

							//inbox app
							$data = array(
									'id'=>$this->input->post('id'),
									'app_memo'=>$this->input->post('oldmemo').$rs[0]->app_memo.'<br/><br/>memo by: '.$_SESSION['employee'].'<br/><br/>',
									'approval_id'=>$appnya,
									'approved_id'=>$creatornya,
									'sub_approved_id'=>$subnya,
									'email' => '0',
									'app_sequence'=>$seqnya,
								);

							//doc history
							$data4 = array(
									'approval_id'=>$appnya,
									'approved_id'=>$subnya,
									'doc_id'=>$rs[0]->doc_id,
									'doc_status'=>$st,
									'doc_number'=>$rs[0]->doc_number,
									'approv_date'=>date("Y-m-d H:i:s"),
									'status'=>$st,
									'created'=>date("Y-m-d H:i:s"),
									'updated'=>date("Y-m-d H:i:s")
								);

						//	print_r($data4);
						//	print_r($data);
							/*echo '<pre>';

							print_r($data);
							print_r($data4);
							print_r($det);
							echo '</pre>';
							echo $this->db->last_query();*/

							
							$this->inbox_app_model->approve($data);

							$this->doc_history_model->add($data4);

							//it service
							$data_stat=array(
									'doc_seq_id'=>$rs[0]->doc_number,
									'status_app_id'=>$st
								);

							//echo 'blom max';
							/*echo '<pre>';
							print_r($data_stat);
							echo '</pre>';*/
							//echo $this->db->last_query();
							
							$this->it_service_model->edit_status($data_stat);
							$this->sod_model->edit_status($data_stat);
							$this->ga_service_model->edit_status($data_stat);

							//send email function
							//send email function
							$acc=array();
							$arr=array();

							//$this->user_model->limit = 1;
							//$this->user_model->where=array('employee_id'=>$this->input->post('employee_id'));
							//$data['buat_cc']=$this->user_model->send_mail();

							//echo 'buat cc';
							//print_r($data['buat_cc']);

							//cc mail
							//foreach ($data['buat_cc'] as $cc) {
							//	$acc[]=$cc->email;
							//}

							//print_r($data['buat_cc']);
							//print_r($app2);

//							if($app2[0]->creator_id==2){
							if($det->creator_id==2){
								$this->user_model->limit = 0;
								$this->user_model->where=array('role_id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();
								//echo $app2[0]->subcreator_id;
								//print_r($data['send_mail']);

							}elseif($det->creator_id==3){
								$this->user_model->limit = 0;
								$this->user_model->where=array('level_id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}elseif($det->creator_id==4){
								$this->user_model->limit = 0;
								$this->user_model->where=array('group_id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}elseif($det->creator_id==5){
								$this->user_model->limit = 1;
								$this->user_model->where=array('employee.id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}elseif($det->creator_id==6){
								$this->user_model->limit = 1;
								$this->user_model->where=array('employee.id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}elseif($det->creator_id==7){
								$this->user_model->limit = 1;
								$this->user_model->where=array('users.id'=>$det->subcreator_id);
								$data['send_mail']=$this->user_model->send_mail();

							}

							//print_r($data['send_mail']);
							foreach ($data['send_mail'] as $key => $value) {
								//echo $key;

								//print_r($value);
								$arr[0]='helpdesk.it@gos.co.id';
								$arr[]=$value->email;
								
								//echo $ada2=implode(',', $ada);
							}
							//echo $arr;
							//$tonya=implode(",",$arr);

							//$his->

							//echo $tonya;
							//echo 'acc: '.$acc[0];
							//echo $list = array($tonya);
							//$role="'".implode("','",$sesrole)."'";

							//email
							$config=$this->config->item('email');

							$this->load->library('email',$config);
							$this->email->initialize($config);

							$this->email->from('eportal@gos.co.id', 'ePortal GOS');
							
							$this->email->to("aries.it@gos.co.id");
							
							//$this->email->to($tonya);
							
							//$this->email->cc($acc[0]);
							//$this->email->cc('another@another-example.com'); 
							//$this->email->bcc('them@their-example.com'); 

							$sub_email="Approval Request";

							if($this->input->post('priority_id')==1){
								$pri_name="High";
							}elseif($this->input->post('priority_id')==2){
								$pri_name="Medium";
							}elseif($this->input->post('priority_id')==3){
								$pri_name="Low";
							}else{
								$pri_name="Unknown";
							}

/*
							$request_by=$_SESSION['employee'];
							$created_for=$this->input->post('created_for');
							$request_date=date("Y-m-d");
							$duedate=$this->input->post('duedate');
							$priority=$pri_name;
							$desc_mail=$this->input->post('description');

*/							$this->email->subject($sub_email);
							//$this->email->message();	
							
							//print_r($this->email);
							//$this->email->send();
							
							$this->load->view("approval/email_landing",array("success"=>true));

						}
					}
				}
			}else{
				/*foreach($data['detail'] as $det){
					print_r($data['detail']);
					$idnya=$det->id;
					$seqnya=$det->sequence;
					$appnya=$det->approval_id;
					$creatornya=$det->creator_id;
					$subnya=$det->subcreator_id;*/

					//inbox app
					$data = array(
							'id'=>$rs[0]->id,
							'approved_id'=>7,
							'sub_approved_id'=>$rs[0]->created_by
						);
					
					if($rs[0]->created_by==$rs[0]->sub_approved_id){
						$data_stat=array(
							'doc_seq_id'=>$rs[0]->doc_number,
							'status_app_id'=>5
						);

						$data = array(
								'id'=>$rs[0]->id,
								'email ' => '1',
								'approved_id'=>7,
								'sub_approved_id'=>$rs[0]->created_by
							);

						//doc history
						$data4 = array(
							'approval_id'=>0,
							'approved_id'=>7,
							'doc_id'=>$rs[0]->doc_id,
							'doc_status'=>5,
							'doc_number'=>$rs[0]->doc_number,
							'approv_date'=>date("Y-m-d H:i:s"),
							'status'=>5,
							'created'=>date("Y-m-d H:i:s")
						);
						
					
					}else{
						$data_stat=array(
							'doc_seq_id'=>$rs[0]->doc_number,
							'status_app_id'=>4
						);

						$data = array(

								'id'=>$rs[0]->id,
								'approved_id'=>7,
								'email' => '0',
								'sub_approved_id'=>$rs[0]->created_by
							);

						//doc history
						$data4 = array(
							'approval_id'=>0,
							'approved_id'=>7,
							'doc_id'=>$rs[0]->doc_id,
							'doc_status'=>4,
							'doc_number'=>$rs[0]->doc_number,
							'approv_date'=>date("Y-m-d H:i:s"),
							'status'=>4,
							'created'=>date("Y-m-d H:i:s")
						);						
					}
					
					//print_r($data);
					/*echo 'udah max';
					echo '<pre>';
					print_r($data_stat);aa
					echo '</pre>';
					echo $this->db->last_query();*/
					$this->inbox_app_model->doc_num = $rs[0]->doc_number;
					$rsinbox = $this->inbox_app_model->inbox();
					$this->inbox_app_model->doc_num = "";
					//echo $this->db->last_query();
					if(count($rsinbox)==0)
					{
						$this->load->view("approval/email_landing",array("success"=>false,"message" => "Anda Tidak Memiliki Akses untuk meng-approve Approval ini atau Approval telah di-approve"));
						return false;
					}
					$this->doc_history_model->add($data4);
					$this->inbox_app_model->approve($data);
					$this->it_service_model->edit_status($data_stat);
					$this->ga_service_model->edit_status($data_stat);
					$this->sod_model->edit_status($data_stat);

					$this->load->view("approval/email_landing",array("success"=>true));
				//}
			}
		}
		
		public function login($username=""){

			if($username != ""){
				//bila validasi benar maka akses database
				$this->load->model('admin_model');
				//$this->admin_model->verify_user('admin@admin.com','admin');
				$param=$this->admin_model->verify_user(
												$username, 
												'itgos2016'
											);
				/*echo '<pre>';
				print_r($param);
				echo '</pre>';*/

				if($param!==false){
					$_SESSION['username']=$param[0]->username;
					$_SESSION['user_id']=$param[0]->user_id;
					$_SESSION['employee_id']=$param[0]->employee_id;
					foreach ($param as $key => $value) 
					{
						$_SESSION['role'][]=$value->role_name;
						$_SESSION['role_id'][]=$value->role_id;
						$_SESSION['level']=$value->level_id;
						$_SESSION['group']=$value->group_id;
						$_SESSION['employee']=$value->employee_name;
					}
				}
			}

			//echo $this->db->last_query();

		}

	}
?>