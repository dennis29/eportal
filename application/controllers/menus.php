<?php
	Class Menus extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}
			
			$this->load->model('menu_model');
		}

		public function index(){
			$data['menus']=$this->menu_model->get_menus();
			//echo $this->db->last_query();
			$this->load->view('menus/view',$data);
		}

		public function add(){
			$data['menus']=$this->menu_model->get_menus();
			$this->load->view('menus/add',$data);
		}

		public function add_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('label','Label','required');
			$this->form_validation->set_rules('link','Link','required');
			$this->form_validation->set_rules('i_class','Icon','required');
			$this->form_validation->set_rules('sort','Sort','integer');
			$this->form_validation->set_rules('parent','Parent','required');

			if($this->form_validation->run() != false){
				$data=array(
					'label'=>$this->input->post('label'),
					'link'=>$this->input->post('link'),
					'i_class'=>$this->input->post('i_class'),
					'sort'=>$this->input->post('sort'),
					'parent'=>$this->input->post('parent')				
				);
				$this->menu_model->add($data);
				//redirect('main');
				echo json_encode(array('success'=>true));
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('label');
				$this->form_validation->set_message('link');
				$this->form_validation->set_message('i_class');
				$this->form_validation->set_message('sort');
				$this->form_validation->set_message('parent');
			}
		}

		public function edit(){
			$param=array(
					'id'=>$_GET['id']
				);
			$data['menus']=$this->menu_model->get_menus();
			$data['menu']=$this->menu_model->get_menu_byid($param);

			//print_r($data);

			$this->load->view('menus/edit',$data);
		}

		public function edit_proccess(){
			$data=array(
					'id'=>$this->input->post('id'),
					'label'=>$this->input->post('label'),
					'link'=>$this->input->post('link'),
					'i_class'=>$this->input->post('i_class'),
					'sort'=>$this->input->post('sort'),
					'parent'=>$this->input->post('parent')
				);
			$this->menu_model->edit($data);
			//redirect('main');
			echo json_encode(array('success'=>true));
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->menu_model->delete($data);
			redirect('menus');
		}

		public function cekmenu()
		{
			$this->load->model('menu_model');
			$data['menus']=$this->menu_model->get_menus();
			$data['menus_hierarki'] = $this->hierarki(0,$data['menus']);


			//echo '<ul id=nav>';
			//echo $data['menus_hierarki'];
			//echo '</ul>';

			$this->load->view('main',$data);

			//$this->load->view('main',$data);
		}

		public function hierarki($parent,$menu)
		{
			$str="";
			foreach($menu as $k=>$v)
			{
				if($v->parent==$parent)
				{
					//$str .= '<ul>'.'<a href=#>'.$v->label."</a></ul><br/>";
					$child = $this->hierarki($v->id,$menu);
					if($child!=""){
						$str .= '<li class="has_sub">'.'<a href=#>'.$v->i_class.' <span>'.$v->label."</span> <span class=pull-right><i class=fa fa-chevron-left></i></span></a>";
						$str .= '<ul>'.$child.'</ul></li>';
					}else{
						$str .= '<li>'.'<a href=# onclick="$(\'#contentAjax\').load(\''.$v->link.'\')" class="open">'.$v->i_class.' '.$v->label."</a></li>";
					}
				}
			}
			return $str;
		}
	}
?>