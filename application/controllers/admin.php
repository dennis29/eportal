<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Admin extends CI_Controller{

		function __construct(){
			parent::__construct();
			@session_start();
		}

		public function index(){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password', 'required |min_length[4]');

			if($this->form_validation->run() != false){
				//bila validasi benar maka akses database
				$this->load->model('admin_model');
				//$this->admin_model->verify_user('admin@admin.com','admin');
				$param=$this->admin_model->verify_user(
												$this->input->post('username'), 
												$this->input->post('password')
											);
				/*echo '<pre>';
				print_r($param);
				echo '</pre>';*/

				if($param!==false){
					$_SESSION['username']=$param[0]->username;
					$_SESSION['user_id']=$param[0]->user_id;
					$_SESSION['employee_id']=$param[0]->employee_id;
					foreach ($param as $key => $value) 
					{
						$_SESSION['role'][]=$value->role_name;
						$_SESSION['role_id'][]=$value->role_id;
						$_SESSION['level']=$value->level_id;
						$_SESSION['group']=$value->group_id;
						$_SESSION['employee']=$value->employee_name;
						/*echo '<pre>';
						print_r($_SESSION);
						echo '</pre>';*/
					}

					redirect('main');
				}
			}

			//echo $this->db->last_query();

			$this->load->view('login_view');
		}

		public function logout(){
			session_destroy();
			$this->load->view('login_view');
		}
	}

?>