<?php
	Class hcm extends CI_Controller{
		function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model(array(
				'hcm_model'
				)
			);
		}

		public function index(){

			$data['hcm']=$this->hcm_model->get_hcm();
			$data['hcm_tab']=$this->hcm_model->get_hcm_tab();
			/*echo '<pre>';
			print_r($data['hcm']);
			echo '</pre>';*/
			$this->load->view('hcm/view',$data);
		}

		public function struktur_organisasi($id=0){
			$config['base_url'] = './hcm/struktur_organisasi';

			$data['total'] = $this->db->get('hcm');
			$config['total_rows'] = $data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->load->model("role_menu_model","rm");
//			$this->rm->select = "edit");
			@session_start();
			$this->rm->where_in[0]="role_id";
			$this->rm->where_in[1]=$_SESSION["role_id"];			
			$param2 = array("menu_id" => 47,"update" => 1);
			$rsrm = $this->rm->get_role_menu_by($param2);
			$data["edit"] = ((count($rsrm)>0) ? 1 : 0);

			$this->rm->where_in[0]="role_id";
			$this->rm->where_in[1]=$_SESSION["role_id"];			
			$param2 = array("menu_id" => 47,"delete" => 1);
			$rsrm = $this->rm->get_role_menu_by($param2);
			$data["delete"] = ((count($rsrm)>0) ? 1 : 0);

			$this->hcm_model->limit = $id; //start
			$this->hcm_model->offset = $config['per_page']; //per page

			$data['hcm']=$this->hcm_model->get_hcm();
			$data['hcm_tab']=$this->hcm_model->get_hcm_tab();
			/*echo '<pre>';
			print_r($data['hcm']);
			echo '</pre>';*/
			$this->load->view('hcm/struktur_organisasi',$data);
		}

		public function peraturan_perusahaan($id=0){
			$config['base_url'] = './hcm/peraturan_perusahaan';

			$data['total'] = $this->db->get('hcm');
			$config['total_rows'] = $data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->load->model("role_menu_model","rm");
//			$this->rm->select = "edit");
			@session_start();
			$this->rm->where_in[0]="role_id";
			$this->rm->where_in[1]=$_SESSION["role_id"];			
			$param2 = array("menu_id" => 48,"update" => 1);
			$rsrm = $this->rm->get_role_menu_by($param2);
			$data["edit"] = ((count($rsrm)>0) ? 1 : 0);

			$this->rm->where_in[0]="role_id";
			$this->rm->where_in[1]=$_SESSION["role_id"];			
			$param2 = array("menu_id" => 48,"delete" => 1);
			$rsrm = $this->rm->get_role_menu_by($param2);
			$data["delete"] = ((count($rsrm)>0) ? 1 : 0);

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->hcm_model->limit = $id; //start
			$this->hcm_model->offset = $config['per_page']; //per page

			$data['hcm']=$this->hcm_model->get_hcm();
			$data['hcm_tab']=$this->hcm_model->get_hcm_tab();
			/*echo '<pre>';
			print_r($data['hcm']);
			echo '</pre>';*/
			$this->load->view('hcm/peraturan_perusahaan',$data);
		}

		public function kebijakan_hr($id=0){
			$config['base_url'] = './hcm/kebijakan_hr';

			$data['total'] = $this->db->get('hcm');
			$config['total_rows'] = $data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->hcm_model->limit = $id; //start
			$this->hcm_model->offset = $config['per_page']; //per page


			$this->load->model("role_menu_model","rm");
//			$this->rm->select = "edit");
			@session_start();
			$this->rm->where_in[0]="role_id";
			$this->rm->where_in[1]=$_SESSION["role_id"];			
			$param2 = array("menu_id" => 49,"update" => 1);
			$rsrm = $this->rm->get_role_menu_by($param2);
			$data["edit"] = ((count($rsrm)>0) ? 1 : 0);

			$this->rm->where_in[0]="role_id";
			$this->rm->where_in[1]=$_SESSION["role_id"];			
			$param2 = array("menu_id" => 49,"delete" => 1);
			$rsrm = $this->rm->get_role_menu_by($param2);
			$data["delete"] = ((count($rsrm)>0) ? 1 : 0);
			
			$data['hcm']=$this->hcm_model->get_hcm();
			$data['hcm_tab']=$this->hcm_model->get_hcm_tab();
			/*echo '<pre>';
			print_r($data['hcm']);
			echo '</pre>';*/
			$this->load->view('hcm/kebijakan_hr',$data);
		}

		public function form_hr($id=0){
			$config['base_url'] = './hcm/form_hr';

			$data['total'] = $this->db->get('hcm');
			$config['total_rows'] = $data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->load->model("role_menu_model","rm");
//			$this->rm->select = "edit");
			@session_start();
			$this->rm->where_in[0]="role_id";
			$this->rm->where_in[1]=$_SESSION["role_id"];			
			$param2 = array("menu_id" => 50,"update" => 1);
			$rsrm = $this->rm->get_role_menu_by($param2);
			$data["edit"] = ((count($rsrm)>0) ? 1 : 0);

			$this->rm->where_in[0]="role_id";
			$this->rm->where_in[1]=$_SESSION["role_id"];			
			$param2 = array("menu_id" => 50,"delete" => 1);
			$rsrm = $this->rm->get_role_menu_by($param2);
			$data["delete"] = ((count($rsrm)>0) ? 1 : 0);

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->hcm_model->limit = $id; //start
			$this->hcm_model->offset = $config['per_page']; //per page

			$data['hcm']=$this->hcm_model->get_hcm();
			$data['hcm_tab']=$this->hcm_model->get_hcm_tab();
			
			/*
			echo '<pre>';
			print_r($data['hcm']);
			echo '</pre>';
			*/
						
			$this->load->view('hcm/form_hr',$data);
		}

		public function add(){
			$data['hcm_tab']=$this->hcm_model->get_hcm_tab();

			$this->load->view('hcm/add',$data);
		}

		public function add_process(){
			//upload file
			/*echo '<pre>';
			print_r($_FILES['attachment']);
			echo '</pre>';*/

			$config['upload_path'] = './asset/file';
			$config['allowed_types'] = '*';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
		    
			$this->upload->set_allowed_types('*');

			//$data['upload_data'] = '';

			if(!$this->upload->do_upload('attachment')){
				$error = array('error' => $this->upload->display_errors());
				$upload_data = 0;
				//$this->load->view('upload_form', $error);
				//print_r($error);
				//print_r($config);
				//$filenya=$this->input->post('attachment2');
			}
			else{
				$upload_data = $this->upload->data();
				//$file_name = $upload_data['file_name'];
				//$filenya=$upload_data['file_name'];
			}
			
			//inbox app
			$data=array(
					'hcm_tab_id'=>$this->input->post('hcm_tab_id'),
					'no_policy'=>$this->input->post('no_policy'),
					'title'=>$this->input->post('title'),
					'desc_hcm'=>$this->input->post('desc_hcm'),
					'created'=>date("Y-m-d"),
					'attachment'=>$upload_data['file_name']
				);
			/**/

			$this->hcm_model->add($data);
			echo json_encode(array("success" => true));
		}

		public function edit(){
			$param=array(
					'hcm.id'=>$_GET['id']
				);
			$data['hcm']=$this->hcm_model->get_hcm_byid($param);
			$data['hcm_tab']=$this->hcm_model->get_hcm_tab();
			$this->load->view('hcm/edit',$data);
		}

		public function edit_process(){
			$config['upload_path'] = './asset/file';
			$config['allowed_types'] = '*';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
		    
			$this->upload->set_allowed_types('*');

			//$data['upload_data'] = '';

			if(!$this->upload->do_upload('attachment')){
				$error = array('error' => $this->upload->display_errors());
				$upload_data = 0;
				//$this->load->view('upload_form', $error);
				//print_r($error);
				//print_r($config);
				$filenya=$this->input->post('attachment2');
			}
			else{
				$upload_data = $this->upload->data();
				//$file_name = $upload_data['file_name'];
				$filenya=$upload_data['file_name'];
			}
			
			$data=array(
					'id'=>$this->input->post('id'),
					'hcm_tab_id'=>$this->input->post('hcm_tab_id'),
					'no_policy'=>$this->input->post('no_policy'),
					'title'=>$this->input->post('title'),
					'desc_hcm'=>$this->input->post('desc_hcm'),
					'created'=>date("Y-m-d"),
					'attachment'=>$filenya
				);

			$this->hcm_model->edit($data);
			echo json_encode(array("success" => true));
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->hcm_model->delete($data);
			echo json_encode(array('success'=>true));
		}
	}
?>