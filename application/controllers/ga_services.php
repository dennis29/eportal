<?php
	Class Ga_Services extends CI_Controller{
		function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model(array(
				'ga_service_model',
				'user_model',
				'priority_model',
				'doc_model',
				'divisi_model',
				'doc_sequence_model',
				'approval_model',
				'it_service_model',
				'inbox_app_model',
				'doc_history_model',
				'vehicle_model',
				'employee_model',
				'nopol_model',
				'driver_model'
				)
			);
		}

		public function index($id=0){

			$where = "";
			foreach($_GET as $k=>$v)
			{
				if($k!="per_page")
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->ga_service_model->where[$k] = $v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}
//			if($where!="") $where = "?".$where;
			
			$this->ga_service_model->where["employee_id"]=$_SESSION["employee_id"];
			$data['ga_services']=$this->ga_service_model->get_ga_services();

			//$data['total']=$data['it_services'];
			$config['total_rows']=count($data['ga_services']);
			$data['total_rows']=$config['total_rows'];
			$config['page_query_string'] = TRUE;
			$config['base_url'] = './ga_services/index?'.$where;
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->ga_service_model->limit = $id; //start
			$this->ga_service_model->offset = $config['per_page']; //per page
			
			$this->ga_service_model->where["employee_id"]=$_SESSION["employee_id"];
			$data['ga_services']=$this->ga_service_model->get_ga_services();
			//echo $this->db->last_query();
			//$data['users']=$this->user_model->get_users();
			//$data['doc']=$this->doc_model->get_docs();

			//print_r($data);

			
			$this->load->view('/ga_services/view',$data);
		}

		public function add(){
			$data['users']=$this->user_model->get_users();
			$data['priority']=$this->priority_model->get_priority();
			$data['divisi']=$this->divisi_model->get_divisi();
			$data['docs']=$this->doc_model->get_docs();
			$data['vehicle']=$this->vehicle_model->get_vehicle();

			$this->load->view('/ga_services/add',$data);
		}

		public function add_process(){

			$this->doc_sequence_model->where = array("doc_id" => $this->input->post('doc_id'));
			$doc_seq = $this->doc_sequence_model->get_doc_seq();
			
			$this->approval_model->offset=0;
			$this->approval_model->where = array("doc_id" => $this->input->post('doc_id'));
			$app = $this->approval_model->get_approval();

			//$data['add_itservice']=$this->it_service_model->add_itservice_byrole();
			$app2=$this->it_service_model->add_itservice_byrole();

			/*echo '<pre>';
			print_r($doc_seq);
			print_r($app2);
			echo $this->db->last_query();
			echo '<pre>';*/
			//print_r($app2);

			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('city','city','required');
			$this->form_validation->set_rules('destination','destination','required');
			$this->form_validation->set_rules('utility','utility','required');
			$this->form_validation->set_rules('date_req','date requset','required');
			$this->form_validation->set_rules('time_req','time requset','required');

			if($this->form_validation->run() != false){
				$param=$this->input->post('param');
				
				$config['upload_path'] = './asset/file';
				$config['allowed_types'] = '*';

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
			    
				$this->upload->set_allowed_types('*');

				//$data['upload_data'] = '';

				if(!$this->upload->do_upload('attachment')){
					$error = array('error' => $this->upload->display_errors());
					$upload_data = 0;
					/*$this->load->view('upload_form', $error);
					print_r($error);
					print_r($config);*/
					//echo 'gada';
				}
				else{
					$upload_data = $this->upload->data();
					//$file_name = $upload_data['file_name'];
					//echo 'ada';
				}

				if($param=='send'){

					//inbox app
					$data3=array(
							'doc_id'=>$this->input->post('doc_id'),
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->creator_id,
							'sub_approved_id'=>$app2[0]->subcreator_id,
							'created_by'=>$_SESSION["user_id"],
							'description'=>$this->input->post('description'),
							'employee_id'=>$this->input->post('employee_id'),
							'app_sequence'=>1,
							'priority_id'=>$this->input->post('priority_id'),
							'attachment'=>$upload_data['file_name'],
							'request_date'=>date("Y-m-d"),
							'duedate'=>$this->input->post('duedate'),
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'created_time' => date("Y-m-d H:i:s")
						);

					$this->inbox_app_model->add($data3);

					//input doc_history
					$data4 = array(
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->subcreator_id,
							'doc_id'=>$this->input->post('doc_id'),
							'doc_status'=>1,
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'approv_date'=>date("Y-m-d H:i:s"),
							'status'=>1,
							'created'=>date("Y-m-d H:i:s"),
							'updated'=>date("Y-m-d H:i:s")
						);
					$this->doc_history_model->add($data4);

					//GA Service
					$data=array(
						'doc_id'=>$this->input->post('doc_id'),
						'request_by'=>$this->input->post('request_by'),
						'employee_id'=>$this->input->post('employee_id'),
						'division_id'=>$this->input->post('division_id'),
						'vehicle_id'=>$this->input->post('vehicle_id'),
						'no_polisi'=>$this->input->post('no_polisi'),
						'driver_name'=>$this->input->post('driver_name'),
						'no_voucher'=>$this->input->post('no_voucher'),
						'date_req'=>$this->input->post('date_req'),
						'time_req'=>$this->input->post('time_req'),
						'priority_id'=>$this->input->post('priority_id'),
						'status_app_id'=>2,
						'destination'=>$this->input->post('destination'),
						'attachment'=>$upload_data['file_name'],
						'city'=>$this->input->post('city'),
						'utility'=>$this->input->post('utility'),
						'doc_seq_id'=>$app[0]->doc_name.'-'.$doc_seq[0]->next,
						'created_date'=>date("Y-m-d"),
						'created_time'=>date("H:i:s")
					);
					/*echo '<pre>';
					print_r($data);
					echo '</pre>';*/

					//sequence 
					$data2=array(
						'id'=>$doc_seq[0]->id,
						'doc_id'=>$doc_seq[0]->doc_id,
						'start'=>$doc_seq[0]->start,
						'next'=>$doc_seq[0]->next+1
					);

					/*echo '<pre>';
					print_r($data);
					echo '</pre>';*/

					$this->ga_service_model->add($data);
					$this->doc_sequence_model->edit($data2);

					//send email
					$acc=array();
					$arr=array();

					//$data['coba']=$this->user_model->send_mail();
					//print_r($data['coba']);
					//echo $this->db->last_query();

					

					$this->user_model->limit = 1;
					$this->user_model->where=array('employee_id'=>$this->input->post('employee_id'));
					$data['buat_cc']=$this->user_model->send_mail();

					//echo 'buat cc';
					//print_r($data['buat_cc']);

					//$this->user_model->where=array('employee_id'=>595);
					//$data['coba']=$this->user_model->send_mail();
					//print_r($data['coba']);

					//cc mail
					foreach ($data['buat_cc'] as $cc) {
						$acc[]=$cc->email;
					}

					//print_r($data['buat_cc']);
					//print_r($app2);
					//echo $app2[0]->creator_id;
					//echo $app2[0]->subcreator_id;

					if($app2[0]->creator_id==2){
						$this->user_model->limit = 0;
						$this->user_model->where=array('role_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();
						//echo $app2[0]->subcreator_id;
						//print_r($data['send_mail']);

					}elseif($app2[0]->creator_id==3){
						$this->user_model->limit = 0;
						$this->user_model->where=array('level_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==4){
						$this->user_model->limit = 0;
						$this->user_model->where=array('group_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==5){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==6){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==7){
						//$this->user_model->limit = 1;
						$this->user_model->where=array('users.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}

					//print_r($data['send_mail']);
					foreach ($data['send_mail'] as $key => $value) {
						//echo $key;

						//print_r($value);
						$arr[0]='admin.ga@gos.co.id';
						$arr[]=$value->email;
						
						//echo $ada2=implode(',', $ada);
					}
					//echo $arr;
					$tonya=implode(",",$arr);

					//echo $tonya;
					//echo 'acc: '.$acc[0];
					//echo $list = array($tonya);
					//$role="'".implode("','",$sesrole)."'";

					//email
					$config=$this->config->item('email');

					$this->load->library('email',$config);
					$this->email->initialize($config);

					$this->email->from('eportal@gos.co.id', 'ePortal GOS');
					$this->email->to($tonya);
					//$this->email->to('aries.it@gos.co.id');
					$this->email->cc($acc[0]);
					//$this->email->cc('another@another-example.com'); 
					//$this->email->bcc('them@their-example.com'); 

					$sub_email=$app2[0]->doc_name." Approval Request";

					//select atu atu

					$this->priority_model->where=array('id'=>$this->input->post('priority_id'));
					$data['priority']=$this->priority_model->get_priority();
					foreach ($data['priority'] as $priority) {
						$priority->priority_name;
					}

					/*print_r($data['client_sod']);
					print_r($data['sbu']);
					print_r($data['status_sod']);
					print_r($data['rekomendasi']);
					print_r($data['priority']);*/

					$request_by=$_SESSION['employee'];
					//$created_for=$this->input->post('created_for');
					$nopol=$this->input->post('no_polisi');
					$driver=$this->input->post('driver_name');
					$voucher=$this->input->post('no_voucher');
					$priority=$priority->priority_name;
					$date_req=$this->input->post('date_req');
					$time_req=$this->input->post('time_req');
					$destination=$this->input->post('destination');
					$city=$this->input->post('city');
					$utility=$this->input->post('utility');
					//$duedate=$this->input->post('duedate');
					//$priority=$pri_name;
					//$desc_mail=$this->input->post('description');

					$message = '<table border="0" cellpadding="0" cellspacing="0" width="100%">	
									<tr>
										<td style="padding: 10px 0 30px 0;">
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="20" style="border: 1px solid #cccccc; border-collapse: collapse;">
												<tr>
													<td align="center" bgcolor="#70bbd9" style="padding: 20px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">
													<b>'.$sub_email.'</b>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request By
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$request_by.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					if($this->input->post('vehicle_id')==1){
						$message.=										'<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							No Polisi
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$nopol.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Driver Name
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$driver.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					}elseif($this->input->post('vehicle_id')==2){
						$message.=										'<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							No Voucher
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$voucher.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					}else{
						$message.=										' ';
					}
																		
					$message.=											'<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Date Request
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$date_req.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Time Request
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$time_req.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Priority
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$priority.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Destination
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$destination.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							City
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$city.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							utility
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$utility.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ee4c50" style="padding: 15px">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
																	<a href="#" style="color: #ffffff;">ePortal.gos.co.id</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>';

					//echo $mess;

					$this->email->subject($sub_email);
					$this->email->message($message);	

					//$this->email->send();
					/*echo '<pre>';
					print_r($data3);
					echo '</pre>';*/

					echo json_encode(array("success" => true));

				}elseif($param=='draft'){
					$data=array(
						'doc_id'=>$this->input->post('doc_id'),
						'request_by'=>$this->input->post('request_by'),
						'employee_id'=>$this->input->post('employee_id'),
						'date_req'=>$this->input->post('date_req'),
						'time_req'=>$this->input->post('time_req'),
						'priority_id'=>$this->input->post('priority_id'),
						'status_app_id'=>1,
						'destination'=>$this->input->post('destination'),
						'attachment'=>$upload_data['file_name'],
						'city'=>$this->input->post('city'),
						'utility'=>$this->input->post('utility'),
						'doc_seq_id'=>$app[0]->doc_name.'-'.$doc_seq[0]->next,
						'created_date'=>date("Y-m-d"),
						'created_time'=>date("H:i:s")
					);

					$this->ga_service_model->add($data);

					echo json_encode(array("success" => true));
				}else{
					echo 'unknown';
				}

			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('city');
				$this->form_validation->set_message('destination');
				$this->form_validation->set_message('utility');
				$this->form_validation->set_message('date_req');
				$this->form_validation->set_message('time_req');
			}
		}
		
		public function edit(){
			$param=array(
					'id'=>$_GET['id']
				);
			$data['ga']=$this->ga_service_model->get_ga_service_byid($param);
			$data['users']=$this->user_model->get_users();
			$data['priority']=$this->priority_model->get_priority();
			$data['divisi']=$this->divisi_model->get_divisi();
			$data['docs']=$this->doc_model->get_docs();
			$data['vehicle']=$this->vehicle_model->get_vehicle();

			$this->load->view('/ga_services/edit',$data);
		}

		public function edit_process(){

			$this->doc_sequence_model->where = array("doc_id" => $this->input->post('doc_id'));
			$doc_seq = $this->doc_sequence_model->get_doc_seq();
			
			$this->approval_model->offset=0;
			$this->approval_model->where = array("doc_id" => $this->input->post('doc_id'));
			$app = $this->approval_model->get_approval();

			//$data['add_itservice']=$this->it_service_model->add_itservice_byrole();
			$app2=$this->it_service_model->add_itservice_byrole();

					/*echo '<pre>';
					print_r($app2);
					echo '</pre>';*/

			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('city','city','required');
			$this->form_validation->set_rules('destination','destination','required');
			$this->form_validation->set_rules('utility','utility','required');
			$this->form_validation->set_rules('date_req','date requset','required');
			$this->form_validation->set_rules('time_req','time requset','required');

			if($this->form_validation->run() != false){
				$param=$this->input->post('param');

				$config['upload_path'] = './asset/file';
				$config['allowed_types'] = '*';

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
			    
				$this->upload->set_allowed_types('*');

				//$data['upload_data'] = '';

				if(!$this->upload->do_upload('attachment')){
					$error = array('error' => $this->upload->display_errors());
					$upload_data = 0;
					/*$this->load->view('upload_form', $error);
					print_r($error);
					print_r($config);*/
					$filenya=$this->input->post('attachment2');
				}
				else{
					$upload_data = $this->upload->data();
					//$file_name = $upload_data['file_name'];
					$filenya=$upload_data['file_name'];
				}

				if($param=='send'){

					//inbox app
					$data3=array(
							'doc_id'=>$this->input->post('doc_id'),
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->creator_id,
							'sub_approved_id'=>$app2[0]->subcreator_id,
							'created_by'=>$_SESSION["user_id"],
							'description'=>$this->input->post('description'),
							'employee_id'=>$this->input->post('employee_id'),
							'app_sequence'=>1,
							'priority_id'=>$this->input->post('priority_id'),
							'attachment'=>$filenya,
							'request_date'=>date("Y-m-d"),
							'duedate'=>$this->input->post('duedate'),
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'created_time' => date("Y-m-d H:i:s")
						);

					$this->inbox_app_model->add($data3);

					//input doc_history
					$data4 = array(
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->subcreator_id,
							'doc_id'=>$this->input->post('doc_id'),
							'doc_status'=>1,
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'approv_date'=>date("Y-m-d H:i:s"),
							'status'=>1,
							'created'=>date("Y-m-d H:i:s"),
							'updated'=>date("Y-m-d H:i:s")
						);
					$this->doc_history_model->add($data4);

					//GA Service
					$data=array(
						'id'=>$this->input->post('id'),
						'doc_id'=>$this->input->post('doc_id'),
						'request_by'=>$this->input->post('request_by'),
						'employee_id'=>$this->input->post('employee_id'),
						'date_req'=>$this->input->post('date_req'),
						'time_req'=>$this->input->post('time_req'),
						'priority_id'=>$this->input->post('priority_id'),
						'status_app_id'=>2,
						'destination'=>$this->input->post('destination'),
						'attachment'=>$filenya,
						'city'=>$this->input->post('city'),
						'utility'=>$this->input->post('utility'),
						'doc_seq_id'=>$app[0]->doc_name.'-'.$doc_seq[0]->next,
						'created_date'=>date("Y-m-d"),
						'created_time'=>date("H:i:s")
					);

					/*echo '<pre>';
					print_r($data);
					echo '</pre>';*/

					//sequence 
					$data2=array(
						'id'=>$doc_seq[0]->id,
						'doc_id'=>$doc_seq[0]->doc_id,
						'start'=>$doc_seq[0]->start,
						'next'=>$doc_seq[0]->next+1
					);

					$this->ga_service_model->edit($data);
					$this->doc_sequence_model->edit($data2);

					//send email
					$acc=array();
					$arr=array();

					//$data['coba']=$this->user_model->send_mail();
					//print_r($data['coba']);
					//echo $this->db->last_query();

					

					$this->user_model->limit = 1;
					$this->user_model->where=array('employee_id'=>$this->input->post('employee_id'));
					$data['buat_cc']=$this->user_model->send_mail();

					//echo 'buat cc';
					//print_r($data['buat_cc']);

					//$this->user_model->where=array('employee_id'=>595);
					//$data['coba']=$this->user_model->send_mail();
					//print_r($data['coba']);

					//cc mail
					foreach ($data['buat_cc'] as $cc) {
						$acc[]=$cc->email;
					}

					//print_r($data['buat_cc']);
					//print_r($app2);
					//echo $app2[0]->creator_id;
					//echo $app2[0]->subcreator_id;

					if($app2[0]->creator_id==2){
						$this->user_model->limit = 0;
						$this->user_model->where=array('role_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();
						//echo $app2[0]->subcreator_id;
						//print_r($data['send_mail']);

					}elseif($app2[0]->creator_id==3){
						$this->user_model->limit = 0;
						$this->user_model->where=array('level_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==4){
						$this->user_model->limit = 0;
						$this->user_model->where=array('group_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==5){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==6){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==7){
						//$this->user_model->limit = 1;
						$this->user_model->where=array('users.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}

					//print_r($data['send_mail']);
					foreach ($data['send_mail'] as $key => $value) {
						//echo $key;

						//print_r($value);
						$arr[0]='admin.ga@gos.co.id';
						$arr[]=$value->email;
						
						//echo $ada2=implode(',', $ada);
					}
					//echo $arr;
					$tonya=implode(",",$arr);

					//echo $tonya;
					//echo 'acc: '.$acc[0];
					//echo $list = array($tonya);
					//$role="'".implode("','",$sesrole)."'";

					//email
					$config=$this->config->item('email');

					$this->load->library('email',$config);
					$this->email->initialize($config);

					$this->email->from('eportal@gos.co.id', 'ePortal GOS');
					$this->email->to($tonya);
					$this->email->cc($acc[0]);
					//$this->email->cc('another@another-example.com'); 
					//$this->email->bcc('them@their-example.com'); 

					$sub_email=$app2[0]->doc_name." Approval Request";

					//select atu atu
					$this->divisi_model->where=array('id'=>$this->input->post('division_id'));
					$data['divisi']=$this->divisi_model->get_divisi();
					
					foreach ($data['divisi'] as $divisi) {
						$divisi->divisi_name;
					}

					$this->vehicle_model->where=array('id'=>$this->input->post('vehicle_id'));
					$data['vehicle']=$this->vehicle_model->get_vehicle();
					foreach ($data['vehicle'] as $vehicle) {
						$vehicle->vehicle_name;
					}

					$this->priority_model->where=array('id'=>$this->input->post('priority_id'));
					$data['priority']=$this->priority_model->get_priority();
					foreach ($data['priority'] as $priority) {
						$priority->priority_name;
					}

					/*print_r($data['client_sod']);
					print_r($data['sbu']);
					print_r($data['status_sod']);
					print_r($data['rekomendasi']);
					print_r($data['priority']);*/

					$request_by=$_SESSION['employee'];
					//$created_for=$this->input->post('created_for');
					$nopol=$this->input->post('no_polisi');
					$driver=$this->input->post('driver_name');
					$voucher=$this->input->post('no_voucher');
					$priority=$priority->priority_name;
					$date_req=$this->input->post('date_req');
					$time_req=$this->input->post('time_req');
					$destination=$this->input->post('destination');
					$city=$this->input->post('city');
					$utility=$this->input->post('utility');
					//$duedate=$this->input->post('duedate');
					//$priority=$pri_name;
					//$desc_mail=$this->input->post('description');

					$message = '<table border="0" cellpadding="0" cellspacing="0" width="100%">	
									<tr>
										<td style="padding: 10px 0 30px 0;">
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="20" style="border: 1px solid #cccccc; border-collapse: collapse;">
												<tr>
													<td align="center" bgcolor="#70bbd9" style="padding: 20px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">
													<b>'.$sub_email.'</b>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request By
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$request_by.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					if($this->input->post('vehicle_id')==1){
						$message.=										'<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							No Polisi
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$nopol.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Driver Name
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$driver.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					}elseif($this->input->post('vehicle_id')==2){
						$message.=										'<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							No Voucher
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$voucher.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					}else{
						$message.=										' ';
					}
																		
					$message.=											'<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Date Request
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$date_req.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Time Request
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$time_req.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Priority
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$priority.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Destination
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$destination.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							City
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$city.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							utility
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$utility.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ee4c50" style="padding: 15px">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
																	<a href="#" style="color: #ffffff;">ePortal.gos.co.id</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>';

					//echo $mess;

					$this->email->subject($sub_email);
					$this->email->message($message);	

					//$this->email->send();

					echo json_encode(array("success" => true));

				}elseif($param=='draft'){
					$config['upload_path'] = './asset/file';
					$config['allowed_types'] = '*';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
				    
					$this->upload->set_allowed_types('*');

					//$data['upload_data'] = '';

					if(!$this->upload->do_upload('attachment')){
						$error = array('error' => $this->upload->display_errors());
						$upload_data = 0;
						/*$this->load->view('upload_form', $error);
						print_r($error);
						print_r($config);*/
						$filenya=$this->input->post('attachment2');
					}
					else{
						$upload_data = $this->upload->data();
						//$file_name = $upload_data['file_name'];
						$filenya=$upload_data['file_name'];
					}
					
					$data=array(
						'id'=>$this->input->post('id'),
						'doc_id'=>$this->input->post('doc_id'),
						'request_by'=>$this->input->post('request_by'),
						'employee_id'=>$this->input->post('employee_id'),
						'date_req'=>$this->input->post('date_req'),
						'time_req'=>$this->input->post('time_req'),
						'priority_id'=>$this->input->post('priority_id'),
						'status_app_id'=>1,
						'destination'=>$this->input->post('destination'),
						'attachment'=>$filenya,
						'city'=>$this->input->post('city'),
						'utility'=>$this->input->post('utility'),
						'doc_seq_id'=>$app[0]->doc_name.'-'.$doc_seq[0]->next,
						'created_date'=>date("Y-m-d"),
						'created_time'=>date("H:i:s")
					);
					/*echo '<pre>';
					print_r($data);
					echo '</pre>';*/

					$this->ga_service_model->edit($data);
					echo json_encode(array("success" => true));
				}else{
					echo 'unknown';
				}
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('city');
				$this->form_validation->set_message('destination');
				$this->form_validation->set_message('utility');
				$this->form_validation->set_message('date_req');
				$this->form_validation->set_message('time_req');
			}
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->ga_service_model->delete($data);
		}

		public function detail(){
			$param = array('ga_services.id'=>$_GET['id']);
			$data['detail']=$this->ga_service_model->get_ga_byid($param);

			$this->load->view('ga_services/detail',$data);

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}
		public function detail2(){
			$param = array('ga_services.doc_seq_id'=>$_GET['id']);
			$data['detail']=$this->ga_service_model->get_ga_byid($param);

			$this->load->view('ga_services/detail',$data);

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}

		public function admin_confirm(){
			
			$data=array(
						'id'=>$this->input->post('bukan_id'),
						'vehicle_id'=>$this->input->post('vehicle_id'),
						'no_polisi'=>$this->input->post('no_polisi'),
						'driver_name'=>$this->input->post('driver_name'),
						'no_voucher'=>$this->input->post('no_voucher'),
						'doc_seq_id'=>$this->input->post('doc_seq_id')
					);
			//print_r($data);
			$this->ga_service_model->edit($data);
		}		

		public function search(){
			//$param = array('ga_services.doc_seq_id'=>$_GET['id']);
			//$data['detail']=$this->ga_service_model->get_ga_byid($param);
			//$this->ga_service_model->where=array('request_by'=>523);
			$this->employee_model->offset=0;
			$data['employee']=$this->employee_model->get_employee();
			//$data['ga_services']=$this->ga_service_model->get_ga_services();
			$data['priority']=$this->priority_model->get_priority();
			$data['divisi']=$this->divisi_model->get_divisi();
			$data['vehicle']=$this->vehicle_model->get_vehicle();

			$this->load->model('status_app_model');
			$data['status_app']=$this->status_app_model->get_status_app();
			
			$this->load->view('ga_services/search',$data);

			/*echo '<pre>';
			print_r($data['employee']);
			echo '</pre>';*/
		}
	}
?>