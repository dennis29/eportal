<?php
	Class It_services extends CI_Controller{

		function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
		}

			$this->load->model(array(
					'it_service_model',
					'inbox_app_model',
					'doc_model',
					'problem_model',
					'priority_model',
					'employee_model',
					'doc_sequence_model',
					'doc_history_model',
					'approval_model',
					'user_model'
				)
			);
		}

		public function index($id=0){			

			$where = "";
			foreach($_GET as $k=>$v)
			{
				if($k!="per_page")
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->it_service_model->where[$k] = $v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}

			$config['page_query_string'] = TRUE;
			$config['base_url'] = './it_services/index?'.$where;

			if(!in_array('17', $_SESSION["role_id"]))
			$this->it_service_model->where = array('created_by'=>$_SESSION['user_id']);
			$data['it_services']=$this->it_service_model->count_all_it_services();

			//$data['total']=$data['it_services'];
			$config['total_rows']=$data['it_services'][0]->total;
			$data['total_rows']=$config['total_rows'];

			$config['base_url'] = './it_services/index?'.$where;
			
			$config['num_links'] = 1;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->it_service_model->limit = $id; //start
			$this->it_service_model->offset = $config['per_page']; //per page

			if(!in_array('17', $_SESSION["role_id"]))
			$this->it_service_model->where = array('created_by'=>$_SESSION['user_id']);
			$data['it_services']=$this->it_service_model->get_it_services();

			//$data['total_rows'] = count($data['it_services']);
			/*echo '<pre>';
			print_r($data['it_services']);
			echo '</pre>';*/

			$this->load->view('it_services/view',$data);		
		}

		public function report(){
			$this->load->view('it_services/report');
		}

		public function report_download(){
			$this->db->like('doc_name',$this->input->post('doc_name'));
			$this->db->where('request_date >=',$this->input->post('request_date1'));
			$this->db->where('request_date <=',$this->input->post('request_date2'));
			//if(!empty($this->where)) $this->db->where($this->where);
			
			$this->db->select('doc.doc_name,
							   it_services.doc_seq_id,
							   problem.problem_name,
							   it_services.created_for,
							   it_services.description,
							   employee.employee_name,
							   priority.priority_name,
							   it_services.request_date,
							   it_services.duedate,
							   status_app.status_app_name
							   ');

			$this->db->from('it_services');
			$this->db->join('employee','it_services.employee_id=employee.id','left');
			$this->db->join('doc','it_services.doc_id=doc.id','left');
			$this->db->join('problem','it_services.problem_id=problem.id','left');
			$this->db->join('priority','it_services.priority_id=priority.id','left');
			$this->db->join('status_app','it_services.status_app_id=status_app.id','left');

			/*$query=$this->db->query("SELECT `doc`.`doc_name`, 
											`it_services`.`doc_seq_id`, 
											`problem`.`problem_name`, 
											`it_services`.`created_for`, 
											`it_services`.`description`, 
											`employee`.`employee_name`, 
											`priority`.`priority_name`, 
											`it_services`.`request_date`, 
											`it_services`.`duedate`, 
											`status_app`.`status_app_name`
									FROM (`it_services`)
									LEFT JOIN `employee` ON `it_services`.`employee_id`=`employee`.`id`
									LEFT JOIN `doc` ON `it_services`.`doc_id`=`doc`.`id`
									LEFT JOIN `problem` ON `it_services`.`problem_id`=`problem`.`id`
									LEFT JOIN `priority` ON `it_services`.`priority_id`=`priority`.`id`
									LEFT JOIN `status_app` ON `it_services`.`status_app_id`=`status_app`.`id`
									WHERE `doc_name` =  '".$this->input->post('doc_name')."'
									AND (request_date >= '".$this->input->post('request_date1')."' 
										AND request_date <= '".$this->input->post('request_date2')."')
			");*/
			//$this->db->limit('10','0');
			$query=$this->db->get();
			//echo $this->db->last_query();

			$this->load->dbutil();
			//$query = $this->db->query("SELECT * FROM it_services");
			$data = $this->dbutil->csv_from_result($query, "\t", "\n");
			$this->load->helper('download');

			$docnya='IT_Service';

			$name_report = $docnya.'_'.$this->input->post('request_date1').'_'.$this->input->post('request_date2');

			$pref=array(
					'filename' 	=> $name_report.'.xls',
					'format'	=> 'xls'
				);
			$name = $name_report.'.xls';
			force_download($name,$data);
		}

		public function add(){
			
			$this->load->model('approval_model');
			$this->load->model('approval_detail_model');

			$this->it_service_model->offset=0;

			$this->approval_model->offset=0;
			$this->approval_detail_model->offset=0;
			$data['approval']=$this->approval_model->get_approval();
			$data['detail']=$this->approval_detail_model->get_detail();

			$data['it_services']=$this->it_service_model->get_it_services();
			$this->doc_model->where_in = array("doc_name", array("PLT","PLK"));
			$data['docs']=$this->doc_model->get_docs();
//			$data['problem']=$this->problem_model->get_problem();
			$data['priority']=$this->priority_model->get_priority();
			$data['users']=$this->user_model->get_users();

			//$data['employee']=$this->employee_model->get_employee();

			/*echo '<pre>';
			print_r($_SESSION);
			echo '</pre>';*/

			$this->load->view('it_services/add',$data);
			
		}

		public function add_process(){
			$this->doc_sequence_model->where = array("doc_id" => $this->input->post('doc_id'));
			$doc_seq = $this->doc_sequence_model->get_doc_seq();
			
			$this->approval_model->offset=0;
			$this->approval_model->where = array("doc_id" => $this->input->post('doc_id'));
			$app = $this->approval_model->get_approval();

			//$data['add_itservice']=$this->it_service_model->add_itservice_byrole();
			$app2 = $this->it_service_model->add_itservice_byrole();
			if(empty($app2))
			{
				echo json_encode(array("fail"=> true,"message"=>"Anda Tidak Memiliki Jalur Approval"));
				return;
			}

			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('doc_id','DOC Type','required');
			$this->form_validation->set_rules('problem_id','Problem','required');
			$this->form_validation->set_rules('created_for','Creator For','required');
			$this->form_validation->set_rules('priority_id','Priority','required');
			$this->form_validation->set_rules('duedate','Duedate','required');

			if($this->form_validation->run() != false){
				$param=$this->input->post('param');

				if ($param=="send"){

					//upload file
/*
					echo '<pre>';
					print_r($_FILES['attachment']);
					echo '</pre>';
*/
					$config['upload_path'] = './asset/file';
					$config['allowed_types'] = '*';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
		    
			    	$this->upload->set_allowed_types('*');

					//$data['upload_data'] = '';

					if(!$this->upload->do_upload('attachment')){
						$error = array('error' => $this->upload->display_errors());
						$upload_data = 0;
						/*$this->load->view('upload_form', $error);
						print_r($error);
						print_r($config);*/
					}
					else{
						$upload_data = $this->upload->data();
						$file_name = $upload_data['file_name'];
					}
					
					//inbox app
					$data3=array(
							'doc_id'=>$this->input->post('doc_id'),
							'approval_id'=>$app2[0]->id,
							'created_by'=>$_SESSION['user_id'],
							'approved_id'=>$app2[0]->creator_id,
							'sub_approved_id'=>$app2[0]->subcreator_id,
							'description'=>$this->input->post('description'),
							'employee_id'=>$this->input->post('employee_id'),
							'app_sequence'=>1,
							'priority_id'=>$this->input->post('priority_id'),
							'attachment'=>$upload_data['file_name'],
							'request_date'=>date("Y-m-d"),
							'duedate'=>$this->input->post('duedate'),
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'created_time' => date("Y-m-d H:i:s")
						);

					/*echo '<pre>';
					print_r($data3);
					echo '</pre>';*/

					$this->inbox_app_model->add($data3);

					//input doc_history
					$data4 = array(
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->subcreator_id,
							'doc_id'=>$this->input->post('doc_id'),
							'doc_status'=>2,
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'approv_date'=>date("Y-m-d H:i:s"),
							'status'=>2,
							'created'=>date("Y-m-d H:i:s"),
							'updated'=>date("Y-m-d H:i:s")
						);
					$this->doc_history_model->add($data4);
					
					//input It_service
					/*$config['file_name'] = $this->input->post('attachment');
					$config['upload_path'] = './uploads/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '100';
					$config['max_width']  = '1024';
					$config['max_height']  = '768';

					$this->load->library('upload', $config);*/
					
					$data=array(
						'doc_id'=>$this->input->post('doc_id'),
						'problem_id'=>$this->input->post('problem_id'),
						'created_for'=>$this->input->post('created_for'),
						'created_by'=>$_SESSION['user_id'],
						'description'=>$this->input->post('description'),
						'employee_id'=>$this->input->post('employee_id'),
						'priority_id'=>$this->input->post('priority_id'),
						'attachment'=>$upload_data['file_name'],
						'request_date'=>date("Y-m-d"),
						'status_app_id'=>2,
						'duedate'=>$this->input->post('duedate'),
						'doc_seq_id'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next
						//'upload_data' => $this->upload->data()
					);

					//print_r($data);

					//input doc_sequence
					$data2=array(
						'id'=>$doc_seq[0]->id,
						'doc_id'=>$doc_seq[0]->doc_id,
						'start'=>$doc_seq[0]->start,
						'next'=>$doc_seq[0]->next+1
					);

					$this->it_service_model->add($data);
					$this->doc_sequence_model->edit($data2);

					//send email function
					$acc=array();
					$arr=array();

					//$this->user_model->limit = 1;
					$this->user_model->where=array('employee_id'=>$this->input->post('employee_id'));
					$data['buat_cc']=$this->user_model->send_mail();

					//echo 'buat cc';
					//print_r($data['buat_cc']);

					//cc mail
					foreach ($data['buat_cc'] as $cc) {
						$acc[]=$cc->email;
					}

					//print_r($data['buat_cc']);
					//print_r($app2);

					if($app2[0]->creator_id==2){
						$this->user_model->limit = 0;
						$this->user_model->where=array('role_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();
						//echo $app2[0]->subcreator_id;
						//print_r($data['send_mail']);

					}elseif($app2[0]->creator_id==3){
						$this->user_model->limit = 0;
						$this->user_model->where=array('level_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==4){
						$this->user_model->limit = 0;
						$this->user_model->where=array('group_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==5){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==6){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==7){
						$this->user_model->limit = 1;
						$this->user_model->where=array('users.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}

					//print_r($data['send_mail']);
					foreach ($data['send_mail'] as $key => $value) {
						//echo $key;

						//print_r($value);
						//$arr[0]='helpdesk.it@gos.co.id';
						$arr[]=$value->email;
						
						//echo $ada2=implode(',', $ada);
					}
					//echo $arr;
					$tonya=implode(",",$arr);

					//echo $tonya;
					//echo 'acc: '.$acc[0];
					//echo $list = array($tonya);
					//$role="'".implode("','",$sesrole)."'";

					//email
					$config=$this->config->item('email');

					$this->load->library('email',$config);
					$this->email->initialize($config);

					$this->email->from('eportal@gos.co.id', 'ePortal GOS');
					$this->email->to($tonya);
					$this->email->cc($acc[0]);
					//$this->email->cc('another@another-example.com'); 
					//$this->email->bcc('them@their-example.com'); 

					$sub_email=$app2[0]->doc_name." Approval Request";

					if($this->input->post('priority_id')==1){
						$pri_name="Low";
					}elseif($this->input->post('priority_id')==2){
						$pri_name="Medium";
					}elseif($this->input->post('priority_id')==3){
						$pri_name="High";
					}else{
						$pri_name="Unknown";
					}

					$request_by=$_SESSION['employee'];
					$created_for=$this->input->post('created_for');
					$request_date=date("Y-m-d");
					$duedate=$this->input->post('duedate');
					$priority=$pri_name;
					$desc_mail=$this->input->post('description');

					$message = '<table border="0" cellpadding="0" cellspacing="0" width="100%">	
									<tr>
										<td style="padding: 10px 0 30px 0;">
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="20" style="border: 1px solid #cccccc; border-collapse: collapse;">
												<tr>
													<td align="center" bgcolor="#70bbd9" style="padding: 20px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">
													<b>'.$sub_email.'</b>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request By
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$request_by.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request For
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$created_for.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request Date
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$request_date.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Duedate
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$duedate.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Priority
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$priority.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Description
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$desc_mail.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ee4c50" style="padding: 15px">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
																	<a href="http://eportal.gos.co.id" style="color: #ffffff;">ePortal.gos.co.id</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>';

					//echo $mess;

					$this->email->subject($sub_email);
					$this->email->message($message);	
					
					//print_r($this->email);
					//$this->email->send();

					echo json_encode(array("success" => true));

					/*echo '<pre>';
					print_r($data3);
					print_r($data4);
					print_r($data);
					print_r($data2);
					print_r($app2);
					echo '</pre>';*/

				}elseif($param='draft'){

					$config['upload_path'] = './asset/file';
					$config['allowed_types'] = '*';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
			    
				    $this->upload->set_allowed_types('*');

					//$data['upload_data'] = '';
					/*echo '<pre>';
					print_r($_FILES['attachment']);
					echo '</pre>';*/

					if(!$this->upload->do_upload('attachment')){
						$error = array('error' => $this->upload->display_errors());
						$upload_data = 0;
						//$this->load->view('upload_form', $error);
						//print_r($error);
						//print_r($config);
					}else{
						$upload_data = $this->upload->data();
						//$file_name = $upload_data['file_name'];
					}
					
					//input It_service
					$data=array(
						'doc_id'=>$this->input->post('doc_id'),
						'problem_id'=>$this->input->post('problem_id'),
						'created_for'=>$this->input->post('created_for'),
						'created_by'=>$_SESSION['user_id'],
						'description'=>$this->input->post('description'),
						'employee_id'=>$this->input->post('employee_id'),
						'priority_id'=>$this->input->post('priority_id'),
						'attachment'=>$upload_data['file_name'],
						'request_date'=>date("Y-m-d"),
						'status_app_id'=>1,
						'duedate'=>$this->input->post('duedate'),
						'doc_seq_id'=>$app[0]->doc_name.'-'.$doc_seq[0]->next
					);

					//input doc_sequence
					$data2=array(
						'id'=>$doc_seq[0]->id,
						'doc_id'=>$doc_seq[0]->doc_id,
						'start'=>$doc_seq[0]->start,
						'next'=>$doc_seq[0]->next+1
					);

					//echo '<pre>';
					//print_r($data);
					//echo '</pre>';

					$this->it_service_model->add($data);
					$this->doc_sequence_model->edit($data2);
					echo json_encode(array("success" => true));

				}else{
					echo "unknown";
				}

			}else{

				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('doc_id');
				$this->form_validation->set_message('problem_id');
				$this->form_validation->set_message('created_for');
				$this->form_validation->set_message('priority_id');
				$this->form_validation->set_message('duedate');

			}

			
		}

		public function edit(){
			

			$param=array(
					'it_services.id'=>$_GET['id']
				);
			$data['it_service']=$this->it_service_model->get_it_service_byid($param);
			$this->doc_model->where_in = array("doc_name", array("PLT","PLK"));
			$data['docs']=$this->doc_model->get_docs();
			$data['problem']=$this->problem_model->get_problem();
			$data['priority']=$this->priority_model->get_priority();

			$this->employee_model->offset=0;
			$data['employee']=$this->employee_model->get_employee();

			$this->load->view('it_services/edit',$data);
		}

		public function edit_process(){

			$this->doc_sequence_model->where = array("doc_id" => $this->input->post('doc_id'));
			$doc_seq = $this->doc_sequence_model->get_doc_seq();
			
			$this->approval_model->offset=0;
			$this->approval_model->where = array("doc_id" => $this->input->post('doc_id'));
			$app = $this->approval_model->get_approval();
			$app2= $this->it_service_model->add_itservice_byrole();

			//print_r($app2);


			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('doc_id','DOC Type','required');
			$this->form_validation->set_rules('problem_id','Problem','required');
			$this->form_validation->set_rules('created_for','Creator For','required');
			$this->form_validation->set_rules('priority_id','Priority','required');
			$this->form_validation->set_rules('duedate','Duedate','required');

			if($this->form_validation->run() != false){
				$param=$this->input->post('param');
				$config['upload_path'] = './asset/file';
				$config['allowed_types'] = '*';

				$this->load->library('upload', $config);
				$this->upload->initialize($config);
		
				$this->upload->set_allowed_types('*');

				if(!$this->upload->do_upload('attachment')){
					$error = array('error' => $this->upload->display_errors());
					$upload_data = 0;
					/*$this->load->view('upload_form', $error);
					print_r($error);
					print_r($config);*/
					$filenya=$this->input->post('attachment2');
				}
				else{
					$upload_data = $this->upload->data();
					//$file_name = $upload_data['file_name'];
					$filenya=$upload_data['file_name'];
				}

				if($param=='send'){

					$config['upload_path'] = './asset/file';
					$config['allowed_types'] = '*';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
		    
			    	$this->upload->set_allowed_types('*');

					//inbox app
						$data3=array(
								'doc_id'=>$this->input->post('doc_id'),
								'approval_id'=>$app2[0]->id,
								'created_by'=>$_SESSION['user_id'],
								'approved_id'=>$app2[0]->creator_id,
								'sub_approved_id'=>$app2[0]->subcreator_id,
								'description'=>$this->input->post('description'),
								'employee_id'=>$this->input->post('employee_id'),
								'priority_id'=>$this->input->post('priority_id'),
								'app_sequence'=>1,
								'attachment'=>$filenya,
								'request_date'=>date("Y-m-d"),
								'duedate'=>$this->input->post('duedate'),
								'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
								'created_time' => date("Y-m-d H:i:s")
							);

						$this->inbox_app_model->add($data3);

						//input doc_history
	/*					foreach ($data['doc_sequence'] as $ds) {
							echo $ds->next;
	*/						$data4 = array(
								'approval_id'=>$app[0]->id,
								'approved_id'=>$app2[0]->subcreator_id,
								'doc_id'=>$this->input->post('doc_id'),
								'doc_status'=>2,
								'doc_number'=>$app[0]->doc_name.'-'.$doc_seq[0]->next,
								'approv_date'=>date("Y-m-d H:i:s"),
								'status'=>2,
								'created'=>date("Y-m-d H:i:s"),
								'updated'=>date("Y-m-d H:i:s")
							);
	//					}
							$this->doc_history_model->add($data4);


					//input It_service
		/*			foreach ($data['doc_sequence'] as $ds) {
						if($this->input->post('doc_id')==$ds->id){
	*/					$data=array(
							'id'=>$this->input->post('id'),
							'doc_id'=>$this->input->post('doc_id'),
							'problem_id'=>$this->input->post('problem_id'),
							'created_for'=>$this->input->post('created_for'),
							'created_by'=>$_SESSION['user_id'],
							'description'=>$this->input->post('description'),
							'employee_id'=>$this->input->post('employee_id'),
							'priority_id'=>$this->input->post('priority_id'),
							'attachment'=>$filenya,
							'request_date'=>date("Y-m-d"),
							'status_app_id'=>2,
							'duedate'=>$this->input->post('duedate'),
							'doc_seq_id'=>$app[0]->doc_name.'-'.$doc_seq[0]->next
						);

						//input doc_sequence
						$data2=array(
							'id'=>$doc_seq[0]->id,
							'doc_id'=>$doc_seq[0]->doc_id,
							'start'=>$doc_seq[0]->start,
							'next'=>$doc_seq[0]->next+1
						);

						$this->it_service_model->edit($data);
						$this->doc_sequence_model->edit($data2);

						$acc=array();
					$arr=array();

					//$this->user_model->limit = 1;
					$this->user_model->where=array('employee_id'=>$this->input->post('employee_id'));
					$data['buat_cc']=$this->user_model->send_mail();

					//echo 'buat cc';
					//print_r($data['buat_cc']);

					//cc mail
					foreach ($data['buat_cc'] as $cc) {
						$acc[]=$cc->email;
					}

					//print_r($data['buat_cc']);
					//print_r($app2);

					if($app2[0]->creator_id==2){
						$this->user_model->limit = 0;
						$this->user_model->where=array('role_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();
						//echo $app2[0]->subcreator_id;
						//print_r($data['send_mail']);

					}elseif($app2[0]->creator_id==3){
						$this->user_model->limit = 0;
						$this->user_model->where=array('level_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==4){
						$this->user_model->limit = 0;
						$this->user_model->where=array('group_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==5){
						$this->user_model->limit = 0;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==6){
						$this->user_model->limit = 0;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==7){
						$this->user_model->limit = 0;
						$this->user_model->where=array('users.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}

					//print_r($data['send_mail']);
					foreach ($data['send_mail'] as $key => $value) {
						//echo $key;

						//print_r($value);
						//$arr[0]='helpdesk.it@gos.co.id';
						$arr[]=$value->email;
						
						//echo $ada2=implode(',', $ada);
					}
					//echo $arr;
					$tonya=implode(",",$arr);

					//echo $tonya;
					//echo 'acc: '.$acc[0];
					//echo $list = array($tonya);
					//$role="'".implode("','",$sesrole)."'";

					//email
					$config=$this->config->item('email');

					$this->load->library('email',$config);
					$this->email->initialize($config);

					$this->email->from('eportal@gos.co.id', 'ePortal GOS');
					$this->email->to($tonya);
					$this->email->cc($acc[0]);
					//$this->email->cc('another@another-example.com'); 
					//$this->email->bcc('them@their-example.com'); 

					$sub_email=$app2[0]->doc_name." Approval Request";

					if($this->input->post('priority_id')==1){
						$pri_name="Low";
					}elseif($this->input->post('priority_id')==2){
						$pri_name="Medium";
					}elseif($this->input->post('priority_id')==3){
						$pri_name="High";
					}else{
						$pri_name="Unknown";
					}

					$request_by=$_SESSION['employee'];
					$created_for=$this->input->post('created_for');
					$request_date=date("Y-m-d");
					$duedate=$this->input->post('duedate');
					$priority=$pri_name;
					$desc_mail=$this->input->post('description');

					$message = '<table border="0" cellpadding="0" cellspacing="0" width="100%">	
									<tr>
										<td style="padding: 10px 0 30px 0;">
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="20" style="border: 1px solid #cccccc; border-collapse: collapse;">
												<tr>
													<td align="center" bgcolor="#70bbd9" style="padding: 20px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">
													<b>'.$sub_email.'</b>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request By
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$request_by.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request For
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$created_for.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request Date
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$request_date.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Duedate
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$duedate.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Priority
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$priority.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Description
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$desc_mail.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ee4c50" style="padding: 15px">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
																	<a href="http://eportal.gos.co.id" style="color: #ffffff;">ePortal.gos.co.id</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>';

					//echo $mess;

					$this->email->subject($sub_email);
					$this->email->message($message);	

					//$this->email->send();

					//redirect('main');
					echo json_encode(array("success" => true));

				}elseif($param='draft'){

					if(!$this->upload->do_upload('attachment')){
						$error = array('error' => $this->upload->display_errors());
						$upload_data = 0;
						/*$this->load->view('upload_form', $error);
						print_r($error);
						print_r($config);*/
						$filenya=$this->input->post('attachment2');
					}
					else{
						$upload_data = $this->upload->data();
						//$file_name = $upload_data['file_name'];
						$filenya=$upload_data['file_name'];
					}

					$data=array(
						'id'=>$this->input->post('id'),
						'doc_id'=>$this->input->post('doc_id'),
						'problem_id'=>$this->input->post('problem_id'),
						'created_for'=>$this->input->post('created_for'),
						'created_by'=>$_SESSION['user_id'],
						'description'=>$this->input->post('description'),
						'status_app_id'=>1,
						'employee_id'=>$this->input->post('employee_id'),
						'priority_id'=>$this->input->post('priority_id'),
						'attachment'=>$filenya,
						'duedate'=>$this->input->post('duedate')
					);
					$this->it_service_model->edit($data);

					//echo $this->db->last_query();
					
					echo json_encode(array("success" => true));
				}else{
					echo 'unknown';
				}

						
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('doc_id');
				$this->form_validation->set_message('problem_id');
				$this->form_validation->set_message('created_for');
				$this->form_validation->set_message('priority_id');
				$this->form_validation->set_message('duedate');
			}
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->it_service_model->delete($data);
			//redirect('dashboard');
		}

		public function detail(){
			$param = array('it_services.id'=>$_GET['id']);
			$data['detail']=$this->it_service_model->get_it_service_byid($param);

			$this->load->view('it_services/detail',$data);

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}

		public function detail2(){
			$param = array('it_services.doc_seq_id'=>$_GET['id']);
			$data['detail']=$this->it_service_model->get_it_service_byid($param);

			$this->load->view('it_services/detail',$data);

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}
		
		public function get_list($doc_id,$problem_id=0)
		{
			$this->load->model("problem_model","problem");
			$this->problem->where["doc_id"] = $doc_id;
			$pr = $this->problem->get_problem();
			$data = "<option value=''>== Pilih Problem Category ==</option>";
			//echo $this->db->last_query();
			$selected = "";
			foreach ($pr as $value) {
				if($problem_id!=0)
				{
					if($value->id==$problem_id)
						$selected = "selected='selected'";
					else
						$selected = "";
				}
				$data .= "<option value='".$value->id."' ".$selected.">".$value->problem_name."</option>";
			}
			echo $data;
		}

		public function search(){
			//$param = array('ga_services.doc_seq_id'=>$_GET['id']);
			//$data['detail']=$this->ga_service_model->get_ga_byid($param);
			//$this->ga_service_model->where=array('request_by'=>523);
			$this->employee_model->offset=0;
			$this->doc_model->where_in = array("doc_name", array("PLT","PLK"));
			$data['docs']=$this->doc_model->get_docs();
			$data['employee']=$this->employee_model->get_employee();
			$data['problem']=$this->problem_model->get_problem();
			$data['priority']=$this->priority_model->get_priority();

			$this->load->model('status_app_model');
			$data['status_app']=$this->status_app_model->get_status_app();
			//$data['status']=$this->status_app_modal->get_app_status();
			//$data['it_services']=$this->it_service_model->get_it_services();
			$this->load->view('it_services/search',$data);

			/*echo '<pre>';
			print_r($data['employee']);
			echo '</pre>';*/
		}
		
	}
?>