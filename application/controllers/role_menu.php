<?php
	Class role_menu extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model('role_menu_model');
			$this->load->model('user_model');
		}

		public function index(){
			$data['role_menu']=$this->role_menu_model->get_role_menu();
			$data['users']=$this->user_model->get_usr();
			//print_r($data);
			$this->load->view('role_menu/view',$data);
		}
	}
?>