<?php
	Class Sod extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model(array(
					'client_sod_model',
					'sod_model',
					'sbu_model',
					'status_sod_model',
					'rekomendasi_sod_model',
					'doc_model',
					'user_model',
					'priority_model',
					'doc_sequence_model',
					'approval_model',
					'it_service_model',
					'inbox_app_model',
					'doc_history_model',
					'employee_model'
				)
			);
		}

		public function index($id=0){

			$where = "";
			foreach($_GET as $k=>$v)
			{
				if($k!="per_page")
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->sod_model->where[$k] = $v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}

			$config['page_query_string'] = TRUE;
			$config['base_url'] = './sod/index?'.$where;
			
			if(!in_array('14', $_SESSION["role_id"]))
			$this->sod_model->where = array("sod.created_by" => $_SESSION['user_id']);
			$data['total']=$this->sod_model->get_sod();
			
			//$data['total'] = $this->db->get('sod');
			$config['total_rows'] = count($data['total']);
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->sod_model->limit = $id; //start
			$this->sod_model->offset = $config['per_page']; //per page

			//$this->sod_model->where=array('sod.created_by'=$_SESSION['user_id']);
			if(!in_array('14', $_SESSION["role_id"]))
			$this->sod_model->where = array("sod.created_by" => $_SESSION['user_id']);
			/*if(!empty($_GET)){
				$this->sod_model->where = array("status_app_id" => $_GET['status_app_id']);
				$this->sod_model->like = array("client_sod_id" => $_GET['client_sod_id']);
			}*/
			$data['sod']=$this->sod_model->get_sod();
			
			
//			print_r($data['sod']);
			$this->load->view('sod/view',$data);

/*			if($_SESSION['employee_id']=='535'){
				echo $this->db->last_query();	
			}*/
			//echo $this->db->last_query();
			//$_SESSION['user_id'];

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}

		public function sod_confirm($id=0){


/*			$where = "";
			foreach($_GET as $k=>$v)
			{
				if($k!="per_page")
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->sod_model->where[$k] = $v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}

			$config['page_query_string'] = TRUE;
*/
			$where = "";
			$q = "";
			foreach($_GET as $k=>$v)
			{
				if(!in_array($k, array("per_page","sort","dir","_")))
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->sod_model->where[$k] = $v;
						$q .= "&".$k."=".$v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}

			if(isset($_GET["per_page"]))
			{
				if($_GET["per_page"]=="")
				{
					$id = 0;
				}
				else
				{
					$id = $_GET["per_page"];
				}
			}
			else
			{
					$id = 0;				
			}
			if(isset($_GET["sort"]))
			{
				$asc = ($_GET["dir"]==1) ? "ASC" : "DESC";
				$this->sod_model->order_by = $_GET["sort"]." ".$asc;
				$q .= "&sort=".$_GET["sort"]."&dir=".$_GET["dir"];
			}
			$config['base_url'] = './sod/sod_confirm?_=0'.$q;
			
			if(!in_array('14', $_SESSION["role_id"]))
			$this->sod_model->where["sod.created_by"] = $_SESSION['user_id'];
			$this->sod_model->where["status_app_id"] = '3';
			$data['total']=$this->sod_model->get_sod();
			//$data['total'] = $this->db->get('sod');
			$config['total_rows'] = count($data['total']);
			$config['page_query_string'] = TRUE;
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->sod_model->limit = $id; //start
			$this->sod_model->offset = $config['per_page']; //per page

			//$this->sod_model->where=array('sod.created_by'=$_SESSION['user_id']);
			if(!in_array('14', $_SESSION["role_id"]))
			$this->sod_model->where["sod.created_by"] = $_SESSION['user_id'];
			$this->sod_model->where["status_app_id"] = '3';
			/*if(!empty($_GET)){
				$this->sod_model->where = array("status_app_id" => $_GET['status_app_id']);
				$this->sod_model->like = array("client_sod_id" => $_GET['client_sod_id']);
			}*/
			$data['sod']=$this->sod_model->get_sod();
			
			
//			print_r($data['sod']);
			$this->load->view('sod/sod_confirm',$data);
		}

		public function sod_document($id=0){

			$where = "";
			foreach($_GET as $k=>$v)
			{
				if($k!="per_page")
				{
					if($where!="") $where .= "&";
					$where .= $k."=".$v;
					if($v!=''){
						$this->sod_model->where[$k] = $v;
					}					
				}
				else
				{
					$id = $v;	
				}
			}

			$config['page_query_string'] = TRUE;
			$config['base_url'] = './sod/sod_document?'.$where;
			
			if(!in_array('14', $_SESSION["role_id"]))
			$this->sod_model->where["sod.created_by"] = $_SESSION['user_id'];
			$this->sod_model->where["sod.doc_print"] = '1';
			$data['total']=$this->sod_model->get_sod();
			
			//$data['total'] = $this->db->get('sod');
			$config['total_rows'] = count($data['total']);
			$data['total_rows']=$config['total_rows'];
			
			$config['num_links'] = 5;
			$config['per_page'] = 10;

			$this->pagination->initialize($config); 
			$data['page']=$this->pagination->create_links();
			//$data['semua']=$this->it_service_model->all();

			$this->sod_model->limit = $id; //start
			$this->sod_model->offset = $config['per_page']; //per page

			//$this->sod_model->where=array('sod.created_by'=$_SESSION['user_id']);
			if(!in_array('14', $_SESSION["role_id"]))
			$this->sod_model->where["sod.created_by"] = $_SESSION['user_id'];
			$this->sod_model->where["sod.doc_print"] = '1';
			/*if(!empty($_GET)){
				$this->sod_model->where = array("status_app_id" => $_GET['status_app_id']);
				$this->sod_model->like = array("client_sod_id" => $_GET['client_sod_id']);
			}*/
			$data['sod']=$this->sod_model->get_sod();
			
			
//			print_r($data['sod']);
			$this->load->view('sod/sod_document',$data);
			//echo $this->db->last_query();

			//echo $this->db->last_query();
			//$_SESSION['user_id'];

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}

		public function update_print_status(){
			$data=array(
					'id'=>$_GET['id'],
					'print_status'=>1
				);
			//print_r($data);
			
			$this->sod_model->update_print_status($data);
		}

		public function load_xml($nrk){
			$path = "/mnt/armes/pegawai.001.xml";
			error_reporting(E_ALL);
			ini_set('display_errors', 'On');

			$nrkvar = $nrk;
			$xml="";
			$f = fopen($path, "r");
			while($dt = fread($f, 4096))
			{
				$xml .= $dt;
			}
			fclose($f);

			preg_match_all( "/\<KARYAWAN\>(.*?)\<\/KARYAWAN\>/s", 
			$xml, $karyawanblocks );

			$json = "";

			foreach($karyawanblocks[1] as $block )
			{
				preg_match_all( "/\<nrk\>(.*?)\<\/nrk\>/", 
				$block, $nrk);
				preg_match_all( "/\<nama\>(.*?)\<\/nama\>/", 
				$block, $nama);
				preg_match_all( "/\<jabatan\>(.*?)\<\/jabatan\>/", 
				$block, $jabatan);
				preg_match_all( "/\<tmt\>(.*?)\<\/tmt\>/", 
				$block, $tmt);
				preg_match_all( "/\<tat\>(.*?)\<\/tat\>/", 
				$block, $tat);
				if(trim($nrkvar) == trim($nrk[1][0]))
					$json .= trim($nrk[1][0])."|".trim($nama[1][0])."|".trim($jabatan[1][0])."|".trim($tat[1][0])."|".trim($tmt[1][0]);
			}
			echo $json;
		}


		public function load_client($client)
		{
			$path = "/mnt/armes/vw_nmclient.001.xml";

			$q = $client;
			$xml="";
			$f = fopen($path, "r");
			while($dt = fread($f, 4096))
			{
				$xml .= $dt;
			}
			fclose($f);
			
			preg_match_all( "/\<CLIENT\>(.*?)\<\/CLIENT\>/s", $xml, $karyawanblocks );
			
			$arr = array();
			
			foreach($karyawanblocks[1] as $block )
			{
				preg_match_all( "/\<nama\>(.*?)\<\/nama\>/", 
				$block, $nama);
				preg_match_all( "/\<kode\>(.*?)\<\/kode\>/", 
				$block, $kode);
				$arr[trim($kode[1][0])] = trim($nama[1][0]);
			}
			
			$result = array();
			foreach ($arr as $key=>$value) {
				if (strpos(strtolower($value), $q) !== false) {
					array_push($result, strip_tags($key." - ".$value));
				}
				if (count($result) > 11)
					break;
			}
			echo json_encode(array("value"=>$result));
		}

		public function add(){

			$data['client']=$this->client_sod_model->get_client();
			$data['sbu']=$this->sbu_model->get_sbu();
			$data['status_sod']=$this->status_sod_model->get_status_sod();
			$data['rekomendasi']=$this->rekomendasi_sod_model->get_rekomendasi();
			$data['docs']=$this->doc_model->get_docs();
			$data['users']=$this->user_model->get_users();
			$data['priority']=$this->priority_model->get_priority();

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/

			$this->load->view('sod/add',$data);
		}

		public function add_process(){
			$this->doc_sequence_model->where = array("doc_id" => $this->input->post('doc_id'));
			$doc_seq = $this->doc_sequence_model->get_doc_seq();
			
			$this->approval_model->offset=0;
			$this->approval_model->where = array("doc_id" => $this->input->post('doc_id'));
			$app = $this->approval_model->get_approval();
			//$data['add_itservice']=$this->it_service_model->add_itservice_byrole();
			$app2=$this->it_service_model->add_itservice_byrole();
			//print_r($app2);
//			exit();
			//print_r($this->upload->do_upload('attachment_sod'));
			
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('doc_id','DOC Type','required');
			$this->form_validation->set_rules('client_sod_id','Client Name','required');
			//$this->form_validation->set_rules('effective_date','effective date','required');
			//$this->form_validation->set_rules('start_datefrom','start date from','required');
			//$this->form_validation->set_rules('start_dateto','start date to','required');

			if($this->form_validation->run() != false){

				$param=$this->input->post('param');

				if ($param=="send"){
					
					//upload file
					/*echo '<pre>';
					print_r($_FILES['attachment_sod']);
					echo '</pre>';*/

					$config['upload_path'] = './asset/file';
					$config['allowed_types'] = '*';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
		    
			    	$this->upload->set_allowed_types('*');

					//$data['upload_data'] = '';
					//print_r($this->upload->data('attachment_sod'));

					if(!$this->upload->do_upload('attachment_sod')){
						$error = array('error' => $this->upload->display_errors());
						$upload_data = 0;
						//$this->load->view('upload_form', $error);
						//print_r($error);
						//print_r($config);
					}
					else{
						$upload_data = $this->upload->data();
						//$file_name = $upload_data['file_name'];
					}
					
					//echo $this->db->last_query();
					//print_r($app2);
					//inbox app
					//echo $_SESSION["user_id"];
					$data3=array(
							'doc_id'=>$this->input->post('doc_id'),
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->creator_id,
							'sub_approved_id'=>$app2[0]->subcreator_id,
							'description'=>$this->input->post('description'),
							'employee_id'=>$this->input->post('employee_id'),
							'app_sequence'=>1,
							'priority_id'=>$this->input->post('priority_id'),
							'attachment'=>$upload_data['file_name'],
							'request_date'=>date("Y-m-d"),
							'created_by'=>$_SESSION["user_id"],
							'duedate'=>$this->input->post('duedate'),
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'created_time' => date("Y-m-d")
						);

					//print_r($data3);

					$this->inbox_app_model->add($data3);

					//input doc_history
					$data4 = array(
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->subcreator_id,
							'doc_id'=>$this->input->post('doc_id'),
							'doc_status'=>2,
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'status'=>2,
							'created'=>date("Y-m-d H:i:s"),
							'updated'=>date("Y-m-d H:i:s")
						);
					$this->doc_history_model->add($data4);

					//input sod
					$data=array(
							'doc_id'=>$this->input->post('doc_id'),
							'employee_id'=>$this->input->post('employee_id'),
							'client_sod_id'=>$this->input->post('client_sod_id'),
							'effective_date'=>$this->input->post('effective_date'),
							'sbu_id'=>$this->input->post('sbu_id'),
							'nrk'=>$this->input->post('nrk'),
							'name_sod'=>$this->input->post('name_sod'),
							'position_sod'=>$this->input->post('position_sod'),
							'status_sod_id'=>$this->input->post('status_sod_id'),
							'status_app_id'=>2,
							'rekomendasi_sod_id'=>$this->input->post('rekomendasi_sod_id'),
							'start_datefrom'=>$this->input->post('start_datefrom'),
							'start_dateto'=>$this->input->post('start_dateto'),
							'reason'=>$this->input->post('reason'),
							'pic_pam_id'=>$this->input->post('pic_pam_id'),
							'department'=>$this->input->post('department'),
							'priority_id'=>$this->input->post('priority_id'),
							'attachment_sod'=>$upload_data['file_name'],
							'request_date'=>date("Y-m-d H:i:s"),
							'created_time'=>date("Y-m-d H:i:s"),
							'created_by'=>$_SESSION['user_id'],
							'doc_seq_id'=>$app[0]->doc_name.'-'.$doc_seq[0]->next
						);

					//print_r($data);
					//input doc_sequence
					$data2=array(
						'id'=>$doc_seq[0]->id,
						'doc_id'=>$doc_seq[0]->doc_id,
						'start'=>$doc_seq[0]->start,
						'next'=>$doc_seq[0]->next+1
					);

					$this->sod_model->add($data);
					$this->doc_sequence_model->edit($data2);


					//send email
					$acc=array();
					$arr=array();

					//$data['coba']=$this->user_model->send_mail();
					
					//echo $this->db->last_query();

					

					//$this->user_model->limit = 1;
					$this->user_model->where=array('employee_id'=>$this->input->post('employee_id'));
					$data['buat_cc']=$this->user_model->send_mail();

					//echo 'buat cc';
					//print_r($data['buat_cc']);

					//$this->user_model->where=array('employee_id'=>595);
					//$data['coba']=$this->user_model->send_mail();
					//print_r($data['coba']);

					//cc mail
					foreach ($data['buat_cc'] as $cc) {
						$acc[]=$cc->email;
					}

					//print_r($data['buat_cc']);
					//print_r($app2);
					//echo $app2[0]->creator_id;
					//echo $app2[0]->subcreator_id;

					if($app2[0]->creator_id==2){
						$this->user_model->limit = 0;
						$this->user_model->where=array('role_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();
						//echo $app2[0]->subcreator_id;
						//print_r($data['send_mail']);

					}elseif($app2[0]->creator_id==3){
						$this->user_model->limit = 0;
						$this->user_model->where=array('level_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==4){
						$this->user_model->limit = 0;
						$this->user_model->where=array('group_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==5){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==6){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==7){
						$this->user_model->limit = 1;
						$this->user_model->where=array('users.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}

					//print_r($data['send_mail']);
					foreach ($data['send_mail'] as $key => $value) {
						//echo $key;

						//print_r($value);
						//$arr[0]='helpdesk.it@gos.co.id';
						$arr[]=$value->email;
						
						//echo $ada2=implode(',', $ada);
					}
					//echo $arr;
					$tonya=implode(",",$arr);

					//echo $tonya;
					//echo 'acc: '.$acc[0];
					//echo $list = array($tonya);
					//$role="'".implode("','",$sesrole)."'";

					//email
					$config=$this->config->item('email');

					$this->load->library('email',$config);
					$this->email->initialize($config);

					$this->email->from('eportal@gos.co.id', 'ePortal GOS');
					$this->email->to($tonya);
					//$this->email->to('aries.it@gos.co.id');
					$this->email->cc($acc[0]);
					//$this->email->cc('another@another-example.com'); 
					//$this->email->bcc('them@their-example.com'); 

					$sub_email=$app2[0]->doc_name." Approval Request";

					//select atu atu
					$this->client_sod_model->where=array('id'=>$this->input->post('client_sod_id'));
					$data['client_sod']=$this->client_sod_model->get_client();

					$this->sbu_model->where=array('id'=>$this->input->post('sbu_id'));
					$data['sbu']=$this->sbu_model->get_sbu();

					$this->status_sod_model->where=array('id'=>$this->input->post('status_sod_id'));
					$data['status_sod']=$this->status_sod_model->get_status_sod();

					$this->rekomendasi_sod_model->where=array('id'=>$this->input->post('rekomendasi_sod_id'));
					$data['rekomendasi']=$this->rekomendasi_sod_model->get_rekomendasi();

					$this->priority_model->where=array('id'=>$this->input->post('priority_id'));
					$data['priority']=$this->priority_model->get_priority();

					foreach ($data['client_sod'] as $client_sod) {
						$client_sod=$client_sod->client_name;
					}

					foreach ($data['sbu'] as $sbu) {
						$sbu=$sbu->sbu_name;
					}

					foreach ($data['status_sod'] as $status_sod) {
						$status_sod=$status_sod->status_sod_name;
					}

					foreach ($data['rekomendasi'] as $rekomendasi) {
						$rekomendasi=$rekomendasi->rekomendasi_sod_name;
					}

					foreach ($data['priority'] as $priority) {
						$priority=$priority->priority_name;
					}

					/*print_r($data['client_sod']);
					print_r($data['sbu']);
					print_r($data['status_sod']);
					print_r($data['rekomendasi']);
					print_r($data['priority']);*/

					$request_by=$_SESSION['employee'];
					//$created_for=$this->input->post('created_for');
					$request_date=date("Y-m-d");
					$effective_date=$this->input->post('effective_date');
					$client_sod_id=$this->input->post('client_sod_id');
					$nrk=$this->input->post('nrk');
					$name_sod=$this->input->post('name_sod');
					$position_sod=$this->input->post('position_sod');
					$start_datefrom=$this->input->post('start_datefrom');
					$start_dateto=$this->input->post('start_dateto');
					$reason=$this->input->post('reason');
					$department=$this->input->post('department');
					$request_date=$this->input->post('request_date');
					//$duedate=$this->input->post('duedate');
					//$priority=$pri_name;
					//$desc_mail=$this->input->post('description');

					$message = '<table border="0" cellpadding="0" cellspacing="0" width="100%">	
									<tr>
										<td style="padding: 10px 0 30px 0;">
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="20" style="border: 1px solid #cccccc; border-collapse: collapse;">
												<tr>
													<td align="center" bgcolor="#70bbd9" style="padding: 20px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">
													<b>'.$sub_email.'</b>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request By
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$request_by.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Client SOD
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$client_sod_id.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Effective Date
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$effective_date.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							SBU
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$sbu.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					if($this->input->post('massal')==1){
						$message.='										<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Massal
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							YES
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					}elseif($this->input->post('massal')==2){
						$message.='										<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Massal
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							NO
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							NRK
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$nrk.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Name
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$name_sod.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Position
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$position_sod.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					}else{
						$message.='';
					}
																		
					$message.=											'<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Status
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$status_sod.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Rekomendasi
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$rekomendasi.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Start Date From
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$start_datefrom.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Start Date To
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$start_dateto.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Reason
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$reason.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Department
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$department.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Priority
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$priority.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ee4c50" style="padding: 15px">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
																	<a href="#" style="color: #ffffff;">ePortal.gos.co.id</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>';

					//echo $mess;
					//echo $message;

					$this->email->subject($sub_email);
					$this->email->message($message);	

					//$this->email->send();

					echo json_encode(array("success" => true));

					/*echo '<pre>';
					print_r($data);
					//print_r($data3);
					//print_r($data4);
					//print_r($data2);
					//print_r($doc_seq);
					//print_r($app);
					//print_r($app2);
					echo $this->db->last_query();
					echo '</pre>';*/
				}elseif($param=="draft"){
					$this->doc_sequence_model->where = array("doc_id" => $this->input->post('doc_id'));
					$doc_seq = $this->doc_sequence_model->get_doc_seq();
					
					$this->approval_model->offset=0;
					$this->approval_model->where = array("doc_id" => $this->input->post('doc_id'));
					$app = $this->approval_model->get_approval();

					//upload file
					/*echo '<pre>';
					print_r($_FILES['attachment_sod']);
					echo '</pre>';*/
					//print_r($this->input->post('attachment_sod'));

					$config['upload_path'] = './asset/file';
					$config['allowed_types'] = '*';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
		    
			    	$this->upload->set_allowed_types('*');

					//$data['upload_data'] = '';

					if(!$this->upload->do_upload('attachment_sod')){
						$error = array('error' => $this->upload->display_errors());
						$upload_data = 0;
						//$this->load->view('upload_form', $error);
						//print_r($error);
						//print_r($config);
					}
					else{
						$upload_data = $this->upload->data();
						//$file_name = $upload_data['file_name'];
					}

					//input sod
					$data=array(
							'doc_id'=>$this->input->post('doc_id'),
							'employee_id'=>$this->input->post('employee_id'),
							'client_sod_id'=>$this->input->post('client_sod_id'),
							'effective_date'=>$this->input->post('effective_date'),
							'sbu_id'=>$this->input->post('sbu_id'),
							'nrk'=>$this->input->post('nrk'),
							'name_sod'=>$this->input->post('name_sod'),
							'position_sod'=>$this->input->post('position_sod'),
							'status_sod_id'=>$this->input->post('status_sod_id'),
							'status_app_id'=>1,
							'rekomendasi_sod_id'=>$this->input->post('rekomendasi_sod_id'),
							'start_datefrom'=>$this->input->post('start_datefrom'),
							'start_dateto'=>$this->input->post('start_dateto'),
							'reason'=>$this->input->post('reason'),
							'pic_pam_id'=>$this->input->post('pic_pam_id'),
							'department'=>$this->input->post('department'),
							'priority_id'=>$this->input->post('priority_id'),
							'attachment_sod'=>$upload_data['file_name'],
							'created_time'=>date("Y-m-d H:i:s"),
							'created_by'=>$_SESSION['user_id'],
							'doc_seq_id'=>'-'
						);

					//input doc_sequence
					/*$data2=array(
						'id'=>$doc_seq[0]->id,
						'doc_id'=>$doc_seq[0]->doc_id,
						'start'=>$doc_seq[0]->start,
						'next'=>$doc_seq[0]->next+1
					);*/

					$this->sod_model->add($data);
					//$this->doc_sequence_model->edit($data2);
					echo json_encode(array("success" => true));
				}else{
					echo 'unknown';
				}

			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('doc_id');
				$this->form_validation->set_message('client_sod_id');
				//$this->form_validation->set_message('effective_date');
				//$this->form_validation->set_message('start_datefrom');
				//$this->form_validation->set_message('start_dateto');
			}			
		}

		public function detail(){
			$param = array('sod.id'=>$_GET['id']);
			$data['detail']=$this->sod_model->get_sod_byid($param);

			$this->load->view('sod/detail',$data);

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}

		public function detail2(){
			$param = array('sod.doc_seq_id'=>$_GET['id']);
			$data['detail']=$this->sod_model->get_sod_byid($param);

			$this->load->view('sod/detail',$data);

			/*echo '<pre>';
			print_r($data);
			echo '</pre>';*/
		}

		public function edit(){

			$param = array('sod.id'=>$_GET['id']);
			$data['sod']=$this->sod_model->get_sod_byid($param);
			$data['client']=$this->client_sod_model->get_client();
			$data['sbu']=$this->sbu_model->get_sbu();
			$data['status_sod']=$this->status_sod_model->get_status_sod();
			$data['rekomendasi']=$this->rekomendasi_sod_model->get_rekomendasi();
			$data['docs']=$this->doc_model->get_docs();
			$data['users']=$this->user_model->get_users();
			$data['priority']=$this->priority_model->get_priority();

			$this->load->view('sod/edit',$data);
			/*echo '<pre>';
			print_r($data['sod']);
			echo '</pre>';*/
		}

		public function edit_process(){

			$this->doc_sequence_model->where = array("doc_id" => $this->input->post('doc_id'));
			$doc_seq = $this->doc_sequence_model->get_doc_seq();
			
			$this->approval_model->offset=0;
			$this->approval_model->where = array("doc_id" => $this->input->post('doc_id'));
			$app = $this->approval_model->get_approval();
			//$data['add_itservice']=$this->it_service_model->add_itservice_byrole();
			$app2=$this->it_service_model->add_itservice_byrole();
			//echo $this->db->last_query();
			$this->load->library('form_validation');
			$this->load->library('upload');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_message('integer', '%s harus diisi angka');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('doc_id','DOC Type','required');
			////$this->form_validation->set_rules('effective_date','effective date','required');
			////$this->form_validation->set_rules('start_datefrom','start date from','required');
			////$this->form_validation->set_rules('start_dateto','start date to','required');

			if($this->form_validation->run() != false){

				if(!$this->upload->do_upload('attachment')){
					$error = array('error' => $this->upload->display_errors());
					$upload_data = 0;
					/*$this->load->view('upload_form', $error);
					print_r($error);
					print_r($config);*/
					$filenya=$this->input->post('attachment2');
				}
				else{
					$upload_data = $this->upload->data();
					//$file_name = $upload_data['file_name'];
					$filenya=$upload_data['file_name'];
				}
				
				$param=$this->input->post('param');

				if ($param=="send"){
					

					//upload file
					/*echo '<pre>';
					print_r($_FILES['attachment_sod']);
					echo '</pre>';*/

					$config['upload_path'] = './asset/file';
					$config['allowed_types'] = '*';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
		    
			    	$this->upload->set_allowed_types('*');

					//$data['upload_data'] = '';

					if(!$this->upload->do_upload('attachment_sod')){
						$error = array('error' => $this->upload->display_errors());
						$upload_data = 0;
						//$this->load->view('upload_form', $error);
						//print_r($error);
						//print_r($config);
						$filenya=$this->input->post('attachment2');
					}
					else{
						$upload_data = $this->upload->data();
						//$file_name = $upload_data['file_name'];
						$filenya=$upload_data['file_name'];
					}
					
					//echo $this->db->last_query();
					//print_r($app2);
					//inbox app
					$data3=array(
							'doc_id'=>$this->input->post('doc_id'),
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->creator_id,
							'sub_approved_id'=>$app2[0]->subcreator_id,
							'description'=>$this->input->post('description'),
							'employee_id'=>$this->input->post('employee_id'),
							'created_by'=>$_SESSION["user_id"],
							'app_sequence'=>1,
							'priority_id'=>$this->input->post('priority_id'),
							'attachment'=>$filenya,
							'request_date'=>date("Y-m-d"),
							'duedate'=>$this->input->post('duedate'),
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'created_time' => date("Y-m-d")
						);

					//print_r($data3);

					$this->inbox_app_model->add($data3);

					//input doc_history
					$data4 = array(
							'approval_id'=>$app2[0]->id,
							'approved_id'=>$app2[0]->subcreator_id,
							'doc_id'=>$this->input->post('doc_id'),
							'doc_status'=>2,
							'doc_number'=>$app2[0]->doc_name.'-'.$doc_seq[0]->next,
							'status'=>2,
							'created'=>date("Y-m-d H:i:s"),
							'updated'=>date("Y-m-d H:i:s")
						);
					$this->doc_history_model->add($data4);

					//input sod
					$data=array(
							'id'=>$this->input->post('id'),
							'employee_id'=>$this->input->post('employee_id'),
							'doc_id'=>$this->input->post('doc_id'),
							'client_sod_id'=>$this->input->post('client_sod_id'),
							'effective_date'=>$this->input->post('effective_date'),
							'sbu_id'=>$this->input->post('sbu_id'),
							'nrk'=>$this->input->post('nrk'),
							'name_sod'=>$this->input->post('name_sod'),
							'position_sod'=>$this->input->post('position_sod'),
							'status_sod_id'=>$this->input->post('status_sod_id'),
							'status_app_id'=>2,
							'rekomendasi_sod_id'=>$this->input->post('rekomendasi_sod_id'),
							'start_datefrom'=>$this->input->post('start_datefrom'),
							'start_dateto'=>$this->input->post('start_dateto'),
							'reason'=>$this->input->post('reason'),
							'pic_pam_id'=>$this->input->post('pic_pam_id'),
							'department'=>$this->input->post('department'),
							'priority_id'=>$this->input->post('priority_id'),
							'attachment_sod'=>$filenya,
							'request_date'=>date("Y-m-d H:i:s"),
							'updated_date'=>date("Y-m-d H:i:s"),
							'created_by'=>$_SESSION['user_id'],
							'doc_seq_id'=>$app[0]->doc_name.'-'.$doc_seq[0]->next
						);
					//input doc_sequence
					$data2=array(
						'id'=>$doc_seq[0]->id,
						'doc_id'=>$doc_seq[0]->doc_id,
						'start'=>$doc_seq[0]->start,
						'next'=>$doc_seq[0]->next+1
					);

					$this->sod_model->edit($data);
					$this->doc_sequence_model->edit($data2);

					//send email
					$acc=array();
					$arr=array();

					//$data['coba']=$this->user_model->send_mail();
					
					//echo $this->db->last_query();

					

					//$this->user_model->limit = 1;
					$this->user_model->where=array('employee_id'=>$this->input->post('employee_id'));
					$data['buat_cc']=$this->user_model->send_mail();

					//echo 'buat cc';
					//print_r($data['buat_cc']);

					//$this->user_model->where=array('employee_id'=>595);
					//$data['coba']=$this->user_model->send_mail();
					//print_r($data['coba']);

					//cc mail
					foreach ($data['buat_cc'] as $cc) {
						$acc[]=$cc->email;
					}

					//print_r($data['buat_cc']);
					//print_r($app2);
					//echo $app2[0]->creator_id;
					//echo $app2[0]->subcreator_id;

					if($app2[0]->creator_id==2){
						$this->user_model->limit = 0;
						$this->user_model->where=array('role_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();
						//echo $app2[0]->subcreator_id;
						//print_r($data['send_mail']);

					}elseif($app2[0]->creator_id==3){
						$this->user_model->limit = 0;
						$this->user_model->where=array('level_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==4){
						$this->user_model->limit = 0;
						$this->user_model->where=array('group_id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==5){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==6){
						$this->user_model->limit = 1;
						$this->user_model->where=array('employee.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}elseif($app2[0]->creator_id==7){
						$this->user_model->limit = 1;
						$this->user_model->where=array('users.id'=>$app2[0]->subcreator_id);
						$data['send_mail']=$this->user_model->send_mail();

					}

					//print_r($data['send_mail']);
					foreach ($data['send_mail'] as $key => $value) {
						//echo $key;

						//print_r($value);
						//$arr[0]='helpdesk.it@gos.co.id';
						$arr[]=$value->email;
						
						//echo $ada2=implode(',', $ada);
					}
					//echo $arr;
					$tonya=implode(",",$arr);

					//echo $tonya;
					//echo 'acc: '.$acc[0];
					//echo $list = array($tonya);
					//$role="'".implode("','",$sesrole)."'";

					//email
					$config=$this->config->item('email');

					$this->load->library('email',$config);
					$this->email->initialize($config);

					$this->email->from('eportal@gos.co.id', 'ePortal GOS');
					$this->email->to($tonya);
					$this->email->cc($acc[0]);
					//$this->email->cc('another@another-example.com'); 
					//$this->email->bcc('them@their-example.com'); 

					$sub_email=$app2[0]->doc_name." Approval Request";

					//select atu atu
					$this->client_sod_model->where=array('id'=>$this->input->post('client_sod_id'));
					$data['client_sod']=$this->client_sod_model->get_client();

					$this->sbu_model->where=array('id'=>$this->input->post('sbu_id'));
					$data['sbu']=$this->sbu_model->get_sbu();

					$this->status_sod_model->where=array('id'=>$this->input->post('status_sod_id'));
					$data['status_sod']=$this->status_sod_model->get_status_sod();

					$this->rekomendasi_sod_model->where=array('id'=>$this->input->post('rekomendasi_sod_id'));
					$data['rekomendasi']=$this->rekomendasi_sod_model->get_rekomendasi();

					$this->priority_model->where=array('id'=>$this->input->post('priority_id'));
					$data['priority']=$this->priority_model->get_priority();

					foreach ($data['client_sod'] as $client_sod) {
						$client_sod=$client_sod->client_name;
					}

					foreach ($data['sbu'] as $sbu) {
						$sbu=$sbu->sbu_name;
					}

					foreach ($data['status_sod'] as $status_sod) {
						$status_sod=$status_sod->status_sod_name;
					}

					foreach ($data['rekomendasi'] as $rekomendasi) {
						$rekomendasi=$rekomendasi->rekomendasi_sod_name;
					}

					foreach ($data['priority'] as $priority) {
						$priority=$priority->priority_name;
					}

					/*print_r($data['client_sod']);
					print_r($data['sbu']);
					print_r($data['status_sod']);
					print_r($data['rekomendasi']);
					print_r($data['priority']);*/

					$request_by=$_SESSION['employee'];
					//$created_for=$this->input->post('created_for');
					$client_sod=$this->input->post('client_sod');
					$request_date=date("Y-m-d");
					$effective_date=$this->input->post('effective_date');
					$nrk=$this->input->post('nrk');
					$name_sod=$this->input->post('name_sod');
					$position_sod=$this->input->post('position_sod');
					$start_datefrom=$this->input->post('start_datefrom');
					$start_dateto=$this->input->post('start_dateto');
					$reason=$this->input->post('reason');
					$department=$this->input->post('department');
					$request_date=$this->input->post('request_date');
					//$duedate=$this->input->post('duedate');
					//$priority=$pri_name;
					//$desc_mail=$this->input->post('description');

					$message = '<table border="0" cellpadding="0" cellspacing="0" width="100%">	
									<tr>
										<td style="padding: 10px 0 30px 0;">
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="20" style="border: 1px solid #cccccc; border-collapse: collapse;">
												<tr>
													<td align="center" bgcolor="#70bbd9" style="padding: 20px 0 20px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Tahoma, Geneva, sans-serif;">
													<b>'.$sub_email.'</b>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ffffff" style="padding: 10px 30px 10px 30px;">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td>
																	<table border="0" cellpadding="0" cellspacing="0" width="100%">
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Request By
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$request_by.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Client SOD
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$client_sod.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Effective Date
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$effective_date.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							SBU
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$sbu.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					if($this->input->post('massal')==1){
						$message.='										<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Massal
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							YES
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					}elseif($this->input->post('massal')==2){
						$message.='										<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Massal
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							NO
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							NRK
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$nrk.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Name
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$name_sod.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Position
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$position_sod.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>';
					}else{
						$message.='';
					}
																		
					$message.=											'<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Status
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$status_sod.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Rekomendasi
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$rekomendasi.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Start Date From
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$start_datefrom.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Start Date To
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$start_dateto.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Reason
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$reason.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Department
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$department.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							Priority
																						</td>
																					</tr>
																				</table>
																			</td>
																			<td style="font-size: 0; line-height: 0;" width="20">
																				&nbsp;
																			</td>
																			<td width="260" valign="top">
																				<table border="0" cellpadding="0" cellspacing="0" width="100%">
																					
																					<tr>
																						<td style="padding: 5px 0 5px 0; color: #153643; font-family: Tahoma, Geneva, sans-serif; font-size: 14px; line-height: 20px;">
																							'.$priority.'
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td bgcolor="#ee4c50" style="padding: 15px">
														<table border="0" cellpadding="0" cellspacing="0" width="100%">
															<tr>
																<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
																	<a href="#" style="color: #ffffff;">ePortal.gos.co.id</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>';

					//echo $mess;

					$this->email->subject($sub_email);
					$this->email->message($message);	

					//$this->email->send();

					echo json_encode(array("success" => true));

					/*echo '<pre>';
					print_r($data);
					print_r($data3);
					print_r($data4);
					print_r($data2);
					print_r($doc_seq);
					print_r($app);
					print_r($app2);
					echo $this->db->last_query();
					echo '</pre>';*/
				}elseif($param=="draft"){
					$this->doc_sequence_model->where = array("doc_id" => $this->input->post('doc_id'));
					$doc_seq = $this->doc_sequence_model->get_doc_seq();
					
					$this->approval_model->offset=0;
					$this->approval_model->where = array("doc_id" => $this->input->post('doc_id'));
					$app = $this->approval_model->get_approval();

					//upload file
					/*echo '<pre>';
					print_r($_FILES['attachment_sod']);
					echo '</pre>';*/

					$config['upload_path'] = './asset/file';
					$config['allowed_types'] = '*';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
		    
			    	$this->upload->set_allowed_types('*');

					//$data['upload_data'] = '';
					//print_r($this->upload->do_upload('attachment_sod'));

					if(!$this->upload->do_upload('attachment_sod')){
						$error = array('error' => $this->upload->display_errors());
						$upload_data = 0;
						//$this->load->view('upload_form', $error);
						//print_r($error);
						//print_r($config);
						//echo 'gada';
						$filenya=$this->input->post('attachment2');
					}
					else{
						$upload_data = $this->upload->data();
						//$file_name = $upload_data['file_name'];
						//echo 'ada';
						$filenya=$upload_data['file_name'];
					}

					//input sod
					$data=array(
							'id'=>$this->input->post('id'),
							'employee_id'=>$this->input->post('employee_id'),
							'doc_id'=>$this->input->post('doc_id'),
							'client_sod_id'=>$this->input->post('client_sod_id'),
							'effective_date'=>$this->input->post('effective_date'),
							'sbu_id'=>$this->input->post('sbu_id'),
							'nrk'=>$this->input->post('nrk'),
							'name_sod'=>$this->input->post('name_sod'),
							'position_sod'=>$this->input->post('position_sod'),
							'status_sod_id'=>$this->input->post('status_sod_id'),
							'status_app_id'=>1,
							'rekomendasi_sod_id'=>$this->input->post('rekomendasi_sod_id'),
							'start_datefrom'=>$this->input->post('start_datefrom'),
							'start_dateto'=>$this->input->post('start_dateto'),
							'reason'=>$this->input->post('reason'),
							'pic_pam_id'=>$this->input->post('pic_pam_id'),
							'department'=>$this->input->post('department'),
							'priority_id'=>$this->input->post('priority_id'),
							'attachment_sod'=>$filenya,
							'created_by'=>$_SESSION['user_id'],
							'doc_seq_id'=>'-'
						);

					$this->sod_model->edit($data);
					echo json_encode(array("success" => true));
				}else{
					echo 'unknown';
				}
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('doc_id');
				//$this->form_validation->set_message('effective_date');
				//$this->form_validation->set_message('start_datefrom');
				//$this->form_validation->set_message('start_dateto');
			}
		}

		public function search(){
			$this->load->model('status_app_model');
			$data['status_app']=$this->status_app_model->get_status_app();
			$data['rekomendasi']=$this->rekomendasi_sod_model->get_rekomendasi();
			$data['employee']=$this->employee_model->get_employee();
			$this->load->view('sod/search',$data);
		}

		public function search_confirm(){
			$this->load->model('status_app_model');
			$data['status_app']=$this->status_app_model->get_status_app();
			$data['employee']=$this->employee_model->get_employee();
			$data['rekomendasi']=$this->rekomendasi_sod_model->get_rekomendasi();
			$this->load->view('sod/search_confirm',$data);
		}

		public function search_print(){
			//echo $this->db->last_query();
			$this->load->view('sod/search_print');
		}

		public function delete(){
			$data=array(
					'id'=>$_GET['id']
				);
			$this->sod_model->delete($data);
			//redirect('dashboard');
		}
	}
?>