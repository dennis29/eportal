<?php
	class Roles extends CI_Controller{

		public function __construct(){
			session_start();
			parent::__construct();

			if(!isset($_SESSION['username'])){
				redirect('admin');
			}

			$this->load->model('role_model');
		}

		public function index($id=0){
			$config['base_url']='./roles/index/';

			$data['total']=$this->db->get('roles');
			$config['total_rows']=$data['total']->num_rows();
			$data['total_rows']=$config['total_rows'];

			$config['per_page']=20;

			$this->pagination->initialize($config);
			$data['page']=$this->pagination->create_links();

			$this->role_model->limit=$id;
			$this->role_model->offset=$config['per_page'];

			$data['roles']=$this->role_model->get_roles();
			//print_r($data['total']);
			$this->load->view('roles/view',$data);
		}

		public function add(){
			$this->load->view('roles/add');
		}

		public function add_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('role_name','Role Name','required');

			if($this->form_validation->run() != false){
				$data=array(
					'role_name'=>$this->input->post('role_name'),
					'role_desc'=>$this->input->post('role_desc')
				);
				$this->role_model->add($data);
				echo json_encode(array('success'=>true));
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('role_name');
			}
			
			//redirect('roles');
			
		}

		public function edit(){
			$param=array(
					'id'=>$_GET['id']
				);
			$data['role']=$this->role_model->get_role_byid($param);

			//print_r($data);
			$this->load->view('roles/edit',$data);
		}

		public function edit_proccess(){
			$this->load->library('form_validation');
			$this->form_validation->set_message('required', '%s tidak boleh kosong');
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->set_rules('role_name','Role Name','required');

			if($this->form_validation->run() != false){
				$data=array(
					'id'=>$this->input->post('id'),
					'role_name'=>$this->input->post('role_name'),
					'role_desc'=>$this->input->post('role_desc')
				);
				$this->role_model->edit($data);
				//echo $this->db->last_query();
				echo json_encode(array('success'=>true));
				//redirect('roles');
			}else{
				echo json_encode(array("fail"=> true,"message"=>validation_errors()));
				$this->form_validation->set_message('role_name');
			}
			
		}

		public function edit_access(){
			$this->load->model('menu_model');
			$param=array(
					'id'=>$_GET['id']
				);
			$param2=array(
					'role_id'=>$_GET['id']
				);
			$data['menus']=$this->menu_model->get_menus();
			
			$data['role_only']=$this->role_model->get_role_only($param);
			
			$data['role_menu']=$this->role_model->get_role_menu_byid($param2);

			$arr=array();
			foreach ($data['role_menu'] as $key => $value) {
				$arr[$value['menu_id']] = array(
					'create'=>$value['create'],
					'read'=>$value['read'],
					'update'=>$value['update'],
					'delete'=>$value['delete']
				);
			}

			$data['arr']=$arr;
			//print_r($arr);
			
			$this->load->view('roles/edit_access',$data);
		}

		public function edit_access_proccess(){
			//	print_r($this->input->post());

			$arr = array();
			$i=0;
			foreach($this->input->post() as $k=>$v){
				//echo '<br/>'.$k.'='.$v.'<br/>';

				//list($cc, $mn_id, $cb) = explode("_", $k);

				if(!in_array($k, array("role_id","submit")))
				{

					list($cc, $mn_id, $cb) = explode("_",$k);

					/*echo $mn_id1.'<br/>';
					echo $cb1.'<br/>';
					echo $mn_id2.'<br/>';
					echo $cb2.'<br/>';
					echo $mn_id3.'<br/>';
					echo $cb3.'<br/>';
					echo $mn_id4.'<br/>';
					echo $cb4.'<br/>';*/

					$arr[$mn_id]['role_id'] = $this->input->post('role_id');
					$arr[$mn_id]['menu_id'] = $mn_id;
					$arr[$mn_id][$cb] = $v;

					$i++;
/*					$data=array(
						'role_id'=>$this->input->post('role_id'),
						'menu_id'=>$mn_id1,
						'create'=>$this->input->post($cb1),
						'read'=>$this->input->post($cb2),
						'update'=>$this->input->post($cb3),
						'delete'=>$this->input->post($cb4)
					);
					$this->role_model->edit_access_post($data);
*/
				}				
			}

//			echo "<pre>";
//			print_r($arr);
//			echo "</pre>";

			$this->load->model('menu_model');
			$rsmenu = $this->menu_model->get_menus();


			foreach($rsmenu as $k3=>$v3)
			{
				$this->load->model('role_menu_model');
				$where = array();
				$rsrolemenu = $this->role_menu_model->get_role_menu_by(array("menu_id" => $v3->id,'role_id' => $this->input->post('role_id')));
				if(!empty($rsrolemenu))
				{
					if(isset($arr[$v3->id]))
					{
						$data["read"] = 0;
						$data["update"] = 0;
						$data["create"] = 0;
						$data["delete"] = 0;
						foreach($arr[$v3->id] as $k2=>$v2)
						{
								if($k2=="read")
									$data["read"] = 1;
								else
								if($k2=="update")
									$data["update"] = 1;
								else
								if($k2=="create")
									$data["create"] = 1;
								else
								if($k2=="delete")
									$data["delete"] = 1;
								
								//print_r($where);
								//echo $k2."<br />";
								//print_r($data);
								//print_r($where);
						}
						$where = array("menu_id" => $v3->id,"role_id" => $this->input->post('role_id'));
						$this->role_menu_model->edit_access_post($where,$data);
						//echo $this->db->last_query()."<br />";
					}
					else
					{
						$data["read"] = 0;
						$data["update"] = 0;
						$data["create"] = 0;
						$data["delete"] = 0;
						$where = array("menu_id" => $v3->id,"role_id" => $this->input->post('role_id'));
						$this->role_menu_model->edit_access_post($where,$data);						
					}
				}
				else
				{
					if(isset($arr[$v3->id]))
					{
						$data["read"] = 0;
						$data["update"] = 0;
						$data["create"] = 0;
						$data["delete"] = 0;

						$data["role_id"] = $this->input->post("role_id");
						$data["menu_id"] = $v3->id;
						foreach($arr[$v3->id] as $k2=>$v2)
						{
								if($k2=="read")
									$data["read"] = 1;
								else
								if($k2=="update")
									$data["update"] = 1;
								else
								if($k2=="create")
									$data["create"] = 1;
								else
								if($k2=="delete")
									$data["delete"] = 1;
								
								//print_r($where);
								//echo $k2."<br />";
								//print_r($data);
								//print_r($where);
						}
						//$where = array("menu_id" => $v3->id,"role_id" => $this->input->post('role_id'));
						$this->role_menu_model->create_access_post($where,$data);

						unset($data["role_id"]);
						unset($data["menu_id"]);
						//echo $this->db->last_query()."<br />";
					}
				}
			}
			//redirect('main');

			/*$data=array(
					'id'=>$this->input->post('id'),
					'role_id'=>$this->input->post('role_id'),
					'menu_id'=>$this->input->post('menu_id'),
					'create'=>$this->input->post('create'),
					'read'=>$this->input->post('read'),
					'update'=>$this->input->post('update'),
					'delete'=>$this->input->post('delete')
				);
			$this->role_model->edit_access_post($data);*/
			//redirect('roles');
			/*print_r($this->input->post());

			foreach($this->input->post() as $k=>$v){
				echo '<br/>'.$k.'='.$v.'<br/>';

				list($cc, $mn_id, $cb) = explode("_", $k);

				echo $cc.'<br/>';
				echo $mn_id.'<br/>';
				echo $cb.'<br/>';
			
				$data=array(
					'id'=>$this->input->post('id'),
					'menu_id'=>'21',
					'create'=>$this->input->post('create'),
					'read'=>$this->input->post('read'),
					'update'=>$this->input->post('update'),
					'delete'=>$this->input->post('delete')
					);
				//echo $menu_id;
				
			}
			//redirect('roles');
			$this->role_model->edit_access_post($data);*/
			echo json_encode(array('success'=>true));
		}
	}
?>